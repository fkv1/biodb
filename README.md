# biodb

## Introduction

This is a Web application and database for caves and biological finds, including mechanisms to have taxa identified by other users. I (F. Volkmann) started this project after a [meeting](https://www.cave.at/de/termine?id=988) where biologists indicated the need to have something like this. My first intention was to implement a tiny demonstration of how it can be done, without actually implementing the functions. But then I realized I had to implement quite a lot to make even a tiny demonstration possible. In the end, it turned into a full-grown application.

The biologists finally showed no real interest in the application, because they prefer to do things the way they are used to (like keeping the data in their paper notebooks), and they only want to receive information from others, not the other way round. Nevertheless I kept my motivation to get the project done, as we need a cave database anyway. Most Austrian cave clubs use a Web application called Spelix, but it is totally broken and the developers have refused to fix even the most horrible or easy-to-fix bugs for at least 12 years now. I created a [bugtracker](https://gitlab.com/fkv1/spelix) with more than 60 bugs explained in detail, and they know about it, but refuse to fix them without even giving a reason. Such a poorly managed application is almost impossible to use and by no means an option for the future. To make it even worse, the database design is terrible, in many cases with different kinds of information stuffed into a single field, and other information spread redundantly over multiple fields.

As the application is mainly designed for central European, especially German speaking users and based on the Austrian cave cadastral system, it's in German and most of the source comments and database table/field names are in German too. I'm switching to that language for the rest of this file.

## Referenzinstallation

Die Datenbank ist in Betrieb auf [http://bio.volki.at](http://bio.volki.at), und alle [Issues](https://gitlab.com/fkv1/biodb/-/issues) mit dem Label "Daten" beziehen sich auf den dortigen Datenbestand.

## Zielsetzung

Diese Datenbank soll einen möglichst freien Zugang zu Höhlendaten und biologischen Funddaten für alle ermöglichen. Die Administration dem Höhlenverein unter dem derzeitgen Vorstand oder dem Naturhistorischen Museum zu überlassen, ist somit keine Option, weil solche Organisationen unverzüglich den Zugang für Außenstehende einschränken und die Funktionalität allein den eigenen Bedürfnissen anpassen würden. Sogar dass sie die Datenbank ganz abdrehen, ist zu befürchten. Die Administration sollte daher in den Händen vertrauenswürdiger Außenstehender bleiben, die keine Eigentinteressen an der Verwertung der Daten haben. Eine Mitgliedschaft beim Höhlenverein ist dennoch nötig, weil Administratoren alle Höhlendaten einsehen können, was nur für Mitglieder zulässig ist.

Auch das Erfassen von Daten soll jedem registrierten User möglich sein. Die Datenbank ist so designt, dass jeder einen eigenen Kommentar zu einer Höhle sowie eigene Ortsangaben erfassen kann, ohne die Eingaben anderer User zu überschreiben.

## Lizenz/Copyright

Den Programmcode stelle ich unter [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). Die Katastergruppengrenzen sind z.T. ©VÖH.

## Systemvoraussetzungen

- Apache unter einem linuxartigen Betriebssystem
- Shell-Login in den Webspace
- Python3 und etliche Python3-Module
- GraphicsMagick
- bei Ergänzungen der Katastergruppen: [Implementierung des 4-Farben-Theorems](https://gitlab.com/fkv1/4-color-theorem)

## Installation

Den Verzeichnisbaum im gewünschten Verzeichnis aus git auschecken und das Unterverzeichnis "media" anlegen. Dann mit create_tables.sql die Tabellen in der Datenbank anlegen und manuell eine Gruppe mit id=1 für Administratoren anlegen und eine mit id=2 für Schreibberechtigte auf den gesamten Höhlenkataster.
