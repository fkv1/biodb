#!/usr/bin/python3

import sys, os, mysql.connector, urllib.parse, multipart, re, json, random, magic, smtplib, email.utils
from mysql.connector import errorcode
from mako.template import Template
from mako.lookup import TemplateLookup
from pyproj import Transformer
#from pyproj import Transformer, set_use_global_context
from chardet.universaldetector import UniversalDetector
from email.message import EmailMessage

if not os.path.isfile('fund.conf'):
    print("fund.conf fehlt => Abbruch", file=sys.stderr)
    sys.exit(1)
exec(open('fund.conf').read())

#appdir = os.environ.get('DOCUMENT_ROOT', CONF_DEFAULT_DOCUMENT_ROOT) + '/fund/'
appdir = os.path.dirname(os.environ.get('SCRIPT_FILENAME', __file__)) + '/'
mediadir = appdir + 'media/'

debug_to_file = 1
debugstr = ''
errstr = ''

if debug_to_file:
    debugfile = open(appdir + 'debug.out', 'w') # alten Inhalt löschen
    debugfile.close()
def debug(p_str):
    global debugstr, debug_to_file
    if debugstr:
        debugstr += ' / '
    debugstr += p_str
    if debug_to_file:
        debugfile = open(appdir + 'debug.out', 'a')
        debugfile.write(p_str + '\n')
        debugfile.close()
from time import perf_counter # für Performance-Debugging
def timedebug(p_label):
    debug('time_' + p_label + ': ' + str(perf_counter()))

def err(p_err):
    global errstr
    if errstr:
        errstr += '\n'
    errstr += p_err

def http_error(p_code, p_text):
    print ('Status: ' + str(p_code) + ' ' + p_text + '\r\nContent-Type: text/plain; charset="UTF-8"\r\n\r\n' + p_text)
    exit()

cgiparams = {}
cgifiles = {}
cgiquery = os.environ.get('QUERY_STRING','')
#debug('Q: '+cgiquery)
# wenn kein "=" vorkommt, liefert urllib.parse.parse_qs() einen ValueError: bad query field
if cgiquery.find('=') > 0:
    # Leerzeichen am Anfang oder Ende sind in der Praxis nie nötig und fast immer ein Versehen
    cgiparams = {key:[val.strip() for val in valarr] for key,valarr in urllib.parse.parse_qs(cgiquery, strict_parsing=True, encoding='utf-8', errors='replace').items()}
#for p in cgiparams:
#    debug('P:' + p + ' V:' + str(cgiparams[p]))

def cb_field(p_field):
    inputname = p_field.field_name.decode()
    if p_field.value is None: # kommt vor in POST wenn Wert=''
        value = ''
    else:
        value = p_field.value.decode()
        #debug('Field '+inputname+' val:'+value)
        if os.environ.get('CONTENT_TYPE') == 'application/x-www-form-urlencoded':
            value = urllib.parse.unquote_plus(value)
    cgiparams.setdefault(inputname,[]).append(value.strip())

def cb_file(p_file):
    inputname = p_file.field_name.decode()
    filename = p_file.file_name.decode()
    # p_file ist multipart.multipart.File; p_file.file_object ist allerdings entweder _io.BytesIO (wenn der Multipart-Stream maximal 1M groß ist) oder tempfile._TemporaryFileWrapper (wenn größer). Ersteres hat die Methode .getvalue(), letzteres nicht.
    if hasattr(p_file.file_object, 'getvalue'):
        file_content = p_file.file_object.getvalue()
    else:
        # multipart.multipart.parse_form hat die Position für read() ans Ende gespult - man muss sie mit seek() zurückspulen um den Inhalt lesen zu können - sonst leer
        p_file.file_object.seek(0)
        file_content = p_file.file_object.read()
    if file_content:
        cgifiles.setdefault(inputname,[]).append({'filename': filename, 'content': file_content})

if os.environ.get('REQUEST_METHOD') == 'POST' and int(os.environ.get('CONTENT_LENGTH','0')):
    if int(os.environ['CONTENT_LENGTH']) > 100*1024*1024:
        http_error(413, 'zu gross')
    #debug('CT:'+os.environ.get('CONTENT_TYPE'))
    multipart.multipart.parse_form({'Content-Type': os.environ.get('CONTENT_TYPE')}, sys.stdin.buffer, cb_field, cb_file)
#debug('cgiparams: '+str(cgiparams))

def isfloat(p_str):
    return bool(re.match(r'(\d+(\.\d*)?|\d*\.\d+)$', p_str))
def cgiparam_num(p_name):
    if p_name in cgiparams:
        str = cgiparams[p_name][0]
        if str == '':
            return 0
        if re.match(r'[+-]?\d+$', str):
            return int(str)
        if isfloat(str):
            return float(str)
        return 0
    return 0
def cgiparam(p_name):
    if p_name in cgiparams:
        return cgiparams[p_name][0]
    return ''

# zu DB verbinden
try:
    biodb = mysql.connector.connect(database=CONF_DB_NAME, user=CONF_DB_USER, password=CONF_DB_PASS)
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        http_error(500, 'access to DB denied')
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        http_error(500, 'DB not available')
    else:
        http_error(500, 'DB error ' + err)
cursor = biodb.cursor()

#idempotency token
def idem_fresh():
    cgiidem = cgiparam_num('IDEM')
    if not cgiidem:
        err('kein idempotency token mitgeschickt')
        return False;
    cursor.execute("select count(0) from idem_used where token=%s", (cgiidem,))
    if cursor.fetchone()[0]:
        return False;
    return True;
def create_idem():
    cursor.execute("select idem_nextval()")
    newidem = cursor.fetchone()[0]
    biodb.commit() # in Mysql functions ist kein Commit zugelassen, daher hier
    return newidem
insdel_moeglich = False;
insdel_passiert = False;

realname_hash = {}
def get_realname(p_userid):
    if not p_userid:
        return ''
    if p_userid not in realname_hash:
        cursor.execute("select concat_ws(' ', name1, name2) from user where id=%s", (p_userid,))
        realname_hash[p_userid] = cursor.fetchone()[0]
    return realname_hash[p_userid]

def get_taxon_children(p_id):
    cursor.execute("select id,taxon,rank,syn,lit,(select count(0) from col b where parent=a.id and syn=0),(select count(0) from col b where parent=a.id and syn=1) from col a where parent=%s order by syn desc, taxon,id",(p_id,))
    result = []
    for row in cursor.fetchall():
        tp_id,tp_taxon,tp_rank,tp_syn,tp_lit,tp_anzc,tp_anzsyn = row
        result.append({'id':tp_id, 'taxon':tp_taxon, 'rank':tp_rank, 'syn':bool(tp_syn), 'lit':tp_lit, 'anzc':tp_anzc, 'anzsyn':tp_anzsyn})
    return result

if cgiparam_num('TAXID'):
    tp_parent = cgiparam_num('TAXID')
    result = get_taxon_children(tp_parent)
    cursor.close()
    biodb.close()
    # hier darf gecacht werden
    print('Content-type: application/json; charset=utf-8\r\n'
        + '\r\n' + json.dumps(result, ensure_ascii=False)
    )
    exit()
elif cgiparam('TAXSEARCH'):
    result = []
    tp_taxon = cgiparam('TAXSEARCH')
    tp_inkl_syn = cgiparam('TAXSEARCH_SYN')
    tp_rang = cgiparam('TAXSEARCH_RANG')
    def get_taxon_parent(p_id):
        if not p_id:
            return None
        cursor.execute("select parent from col where id=%s",(p_id,))
        tp_parent = cursor.fetchone()[0]
        return {'id': p_id, 'parent': get_taxon_parent(tp_parent), 'children': get_taxon_children(p_id)}
    sqlstr = "select id, parent from col where "
    tp_bindpars = []
    sternind = tp_taxon.find('*')
    if sternind < 5: # Stern erst ab 6. Stelle erlaubt für Performance
        sqlstr += "taxon=%s"
        tp_bindpars.append(tp_taxon)
    else:
        sqlstr += "taxon like %s and taxon rlike %s"
        tp_taxon_like = tp_taxon[:sternind] + "%" # damit übern Index geht
        tp_taxon_rlike = "^" + tp_taxon.replace('*','[[:alnum:]†_-]*') + "$";
        tp_bindpars.append(tp_taxon_like)
        tp_bindpars.append(tp_taxon_rlike)
    if not tp_inkl_syn:
        sqlstr += " and syn=0"
    if tp_rang:
        sqlstr += " and rank in(select col from rang where ebene=%s)"
        tp_bindpars.append(tp_rang)
    sqlstr += " order by id"
    cursor.execute(sqlstr, tp_bindpars)
    for row in cursor.fetchall():
        tp_id,tp_parent = row
        result.append({'id':tp_id, 'parent': get_taxon_parent(tp_parent)})
    cursor.close()
    biodb.close()
    # hier darf gecacht werden
    print('Content-type: application/json; charset=utf-8\r\n'
        + '\r\n' + json.dumps(result, ensure_ascii=False)
    )
    exit()
elif cgiparam('TAXCHECK'):
    tp_taxon = cgiparam('TAXCHECK')
    tp_rang = cgiparam_num('TAXSEARCH_RANG')
    def get_tp_pfad(p_id):
        if not p_id:
            return ''
        cursor.execute("select rank,parent,taxon from col where id=%s", (p_id,))
        rank,parent,taxon = cursor.fetchone()
        pfad = get_tp_pfad(parent)
        cursor.execute("select ebene from rang where col=%s and auswaehlbar=1", (rank,))
        row = cursor.fetchone()
        if row:
            ebene = row[0]
            if pfad:
                pfad += '|'
            pfad += str(ebene) + ':' + taxon
        return pfad
    result = {'syn': False, 'taxa': []}
    for _ in (1,2): # bei Sammelart evtl. 2 Durchgänge
        cursor.execute("select id,parent,syn from col where rank in (select col from rang where ebene=%s and auswaehlbar=1) and taxon=%s order by syn, id", (tp_rang, tp_taxon))
        for row in cursor.fetchall():
            tp_id, tp_parent, tp_syn = row
            if tp_syn:
                if len(result['taxa']):
                    break
                result['syn'] = True
            tp_pfad = get_tp_pfad(tp_parent)
            if tp_pfad:
                tp_pfad += '|'
            tp_pfad += str(tp_rang) + ':' + tp_taxon
            result['taxa'].append(tp_pfad)
        if len(result['taxa']):
            break
        # wenn für Sammelart nichts gefunden, dann die gleichnamige Art suchen
        tp_rang -= 1 # Art ist eine Stufe unter der Sammelart
        cursor.execute("select ebene from rang where name_la='species'")
        row = cursor.fetchone()
        if not (row and row[0] == tp_rang):
            break
    cursor.close()
    biodb.close()
    # hier darf gecacht werden
    print('Content-type: application/json; charset=utf-8\r\n'
        + '\r\n' + json.dumps(result, ensure_ascii=False)
    )
    exit()
elif cgiparam('BEZ_LON') and cgiparam('BEZ_LAT'):
    lon = cgiparam_num('BEZ_LON')
    lat = cgiparam_num('BEZ_LAT')
    epsg = cgiparam_num('EPSG') or 4326;
    if epsg != 4326:
        lat,lon = Transformer.from_crs(epsg,4326).transform(lat,lon);
    cursor.execute("select id, name, bezirk_id from gemeinde where st_within(st_geomfromtext('POINT(%s %s)',4326),grenze)", (lat, lon))
    if row := cursor.fetchone():
        gid, gname, bid = row
        cursor.execute("select name, bl from bezirk where id=%s", (bid,))
        bname, bl = cursor.fetchone()
        reply = {'lon': lon, 'lat': lat, 'in_AT': True, 'gemeinde': gname, 'gemeinde_id': gid, 'bezirk': bname, 'bezirk_id': bid, 'bl': bl}
    else:
        reply = {'lon': lon, 'lat': lat, 'in_AT': False}
    cursor.close()
    biodb.close()
    # hier darf gecacht werden
    print('Content-type: application/json; charset=utf-8\r\n'
        + '\r\n' + json.dumps(reply, ensure_ascii=False)
    )
    exit()

def esc_js_str(p_str):
    return p_str.replace('\\','\\\\').replace("'","\\'").replace('"','\\"').replace('\n','\\n').replace('\r','\\r').replace('\t','\\t').replace('\b','\\b').replace('\f','\\f')

s = { 'esc_js_str': esc_js_str }
s['scriptname'] = os.path.basename(__file__)
s['geonames_username'] = CONF_GEONAMES_USERNAME

# User und Session
userid = admin = kataster = 0
s['user'] = s['realname'] = ''
gruppenstr = ''
ingruppen = []
s['mailanz'] = False
sessionid=cgiparam('SESS')
if cgiparam('USER') and cgiparam('PASS'): # d.h. Login ist unmöglich bei Accounts, wo Username oder Passwort leer oder null ist
    user_lower = cgiparam('USER').lower() # es gibt keine Großbuchstaben in Loginnamen weil zu mühsam zum Eingeben und es könnte dann 2 User geben, deren Loginnamen sich nur durch die Groß-/Kleinschreibung unterscheiden
    cursor.execute("select id, login_d, logout_d, sessionid, name1, name2, gruppen from user where username=%s and passwd=%s", (user_lower, cgiparam('PASS')))
    row = cursor.fetchone()
    if row is None:
        err('Username oder Passwort falsch')
    else:
        userid, login_d, logout_d, sessionid, name1, name2, gruppenstr = row;
        if sessionid and not logout_d:
            # Session noch offen => übernehmen
            cursor.execute("update user set active_d=now() where id=%s", (userid,))
        else:
            sessionid = ''
            for _ in range(10):
                sessionid += random.choice('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
            cursor.execute("update user set sessionid=%s, login_d=now(), active_d=now(), logout_d=null where id=%s", (sessionid,userid))
        biodb.commit()
        s['user'] = user_lower
        s['realname'] = (name1 + ' ' + name2).strip()
elif sessionid and cgiparam('LOGOUT'):
    cursor.execute("update user set logout_d=now(), active_d=now() where sessionid=%s", (sessionid,))
    biodb.commit()
    sessionid=''
elif sessionid:
    cursor.execute("select logout_d, id, username, name1, name2, gruppen from user where sessionid=%s", (sessionid,))
    row = cursor.fetchone()
    if row is None:
        err('Session ungültig, mglw. schon länger abgelaufen')
    elif row[0]:
        err('Session abgelaufen/ausgeloggt')
    else:
        userid, s['user'], name1, name2, gruppenstr = row[1:]
        s['realname'] = (name1 + ' ' + name2).strip()
        # um dem User Wartezeit zu ersparen, erst nach dem Senden der Antwort
        # cursor.execute("update user set active_d=now() where id=%s", (userid,))
        # biodb.commit()
if userid:
    # Select nicht mehr nötig, Gruppen-String wird jetzt von Trigger in USER-Datensatz geschrieben
    #cursor.execute("with recursive cte as (select gruppe grp_id from user_gruppe where user=%s union all select gg.parent grp_id from gruppe_gruppe gg join cte on cte.grp_id=gg.child) select grp_id from cte", (userid,))
    # if ist nötig, denn Python hat einen Bug, durch den ''.split(...) [''] statt [] ergibt
    if gruppenstr:
        ingruppen = [int(x) for x in gruppenstr.split(',')]
    #ingruppen = [row[0] for row in cursor.fetchall()]
    # Admin ist Gruppe mit id=1
    if 1 in ingruppen:
        admin = True
    # Kataster ist Gruppe mit id=2. Ob auch Admin die Katasterberechtigung hat, kann über die Gruppenverwaltung gesteuert werden, daher hier nicht explizit:
    # if admin or 2 in ingruppen:
    if 2 in ingruppen:
        kataster = True
del gruppenstr
s['sessionid'] = sessionid
s['admin'] = admin

def update_user_active():
    if userid:
        # genauer, aber umständlicher wäre es, die Zeit zu nehmen, wann das Script aufgerufen wurde; dann dürfte man den User-Datensatz nur dann updaten, wenn dort nicht schon eine spätere Zeit steht
        cursor.execute("update user set active_d=now() where id=%s", (userid,))
        biodb.commit()
def closedb_and_exit():
    update_user_active()
    cursor.close()
    biodb.close()
    exit()

def recht_auf_datei(p_id,p_owner):
    if p_owner == userid or admin:
        return 2
    datei_recht = 0
    if ingruppen:
        cursor.execute("select count(0) from datei_recht where gruppe_id in (" + ','.join([str(x) for x in ingruppen]) + ") and datei_id=%s and recht='W'",(p_id,))
        sum_berechtigt = cursor.fetchone()
        if sum_berechtigt:
            datei_recht = 2
    if not datei_recht:
        #debug('recht-sel:'+"select sum(ss.a),sum(ss.b) from (select 1 a," + ("dr.gruppe.id in (" + ','.join([str(x) for x in ingruppen]) + ")" if ingruppen else "0") + " b from datei_recht dr where dr.datei_id=%s and dr.recht='R') ss")
        cursor.execute("select sum(ss.a),sum(ss.b) from (select 1 a," + ("dr.gruppe.id in (" + ','.join([str(x) for x in ingruppen]) + ")" if ingruppen else "0") + " b from datei_recht dr where dr.datei_id=%s and dr.recht='R') ss",(p_id,))
        (sum_alle,sum_berechtigt) = cursor.fetchone()
        if not sum_alle or sum_berechtigt:
            datei_recht = 1
    return datei_recht

if cgiparam('GALERIE'):
    g = []
    if cgiparam_num('BESUCHID'):
        cursor.execute("select d.id, dv.v, d.code, dv.filename, d.owner, d.beschreibung from datei d, datei_version dv where d.id=dv.datei_id and dv.mime like 'image/%' and d.id in(select datei_id from fund_datei where besuch_id=%s and fund_i=%s) and not exists(select 0 from datei_version dv1 where dv1.datei_id=dv.datei_id and dv1.v>dv.v) order by id", (cgiparam_num('BESUCHID'), cgiparam_num('FUND_I')))
    else:
        cursor.execute("select d.id, dv.v, d.code, dv.filename, d.owner, d.beschreibung from datei d, datei_version dv where d.id=dv.datei_id and dv.mime like 'image/%' and d.id in (select hd.datei_id from hoehle_datei hd where hd.hoehle_id=%s and kategorie=%s) and not exists(select 0 from datei_version dv1 where dv1.datei_id=dv.datei_id and dv1.v>dv.v)", (cgiparam_num('HOEHLEID'),cgiparam('KATEGORIE')))
    for row in cursor.fetchall():
        if recht_auf_datei(row[0],row[4]):
            g.append({'url': 'media/' + str(row[0]) + '_' + row[2] + '_' + str(row[1]), 'filename': row[3], 'beschreibung':row[5]})
    cursor.close()
    biodb.close()
    print('Content-type: text/html; charset=utf-8' + '\r\n\r\n' + '<!DOCTYPE html>')
    galerietemplate = Template(filename=appdir + 'templates/galerie.tpl')
    print(galerietemplate.render(g = g))
    print("</html>")
    exit()

if cgiparam_num('OPENFILE'):
    file_id = cgiparam_num('OPENFILE')
    file_v = cgiparam_num('V')
    cursor.execute("select filename,mime,size from datei_version where datei_id=%s and v=%s", (file_id,file_v))
    row = cursor.fetchone()
    if row is None:
        cursor.close()
        biodb.close()
        http_error(404, 'Datei '+str(file_id)+' existiert nicht');
    else:
        filename, mime, size = row
        cursor.execute("select owner,code from datei where id=%s", (file_id,))
        file_owner,file_code = cursor.fetchone()
        if not recht_auf_datei(file_id, file_owner):
            cursor.close()
            biodb.close()
            http_error(403, 'kein Leserecht')
        filepath = mediadir + str(file_id) + '_' + file_code + '_' + str(file_v)
        if re.match(r'[a-zA-Z0-9 ._!#$%&*/+-]*$', filename):
            fileattr = 'filename="' + filename + '"'
        else:
            fileattr = "filename*=utf-8''" + urllib.parse.quote(filename, safe='', encoding='utf-8');
        header = 'Content-Disposition: inline; ' + fileattr + '\r\n'
        #header += 'Content-Length: ' + str(os.path.getsize(filepath)) + '\r\n'
        header += 'Content-Length: ' + str(size) + '\r\n'
        header += 'Content-type: ' + mime
        #if re.match(r'text/', mime):
        if mime.startswith('text/'):
            detector = UniversalDetector()
            f = open(filepath, "rb")
            while buf := f.read():
                detector.feed(buf)
                if detector.done: break
            f.close()
            detector.close()
            header += '; charset=' + detector.result['encoding']
        header += '\r\n\r\n'
        sys.stdout.buffer.write(bytes(header, 'ascii'))
        f = open(filepath, "rb")
        while buf := f.read():
            sys.stdout.buffer.write(buf)
    closedb_and_exit()

if cgiparam('ZIP'):
    files = []
    filenames = set()
    dups = False
    for id_und_v in cgiparam('ZIP').split(','):
        underscore_pos = id_und_v.find('_')
        if underscore_pos < 1:
            continue
        file_id = id_und_v[:underscore_pos]
        file_v = id_und_v[underscore_pos+1:]
        if not file_id.isnumeric() or not file_v.isnumeric():
            continue
        cursor.execute("select filename from datei_version where datei_id=%s and v=%s", (file_id,file_v))
        row = cursor.fetchone()
        if row is None:
            continue
        filename = row[0]
        cursor.execute("select owner,code from datei where id=%s", (file_id,))
        file_owner,file_code = cursor.fetchone()
        if not recht_auf_datei(file_id, file_owner):
            continue
        if filename in filenames:
            dups = True
        else:
            filenames.add(filename)
        files.append({'id':file_id, 'v':file_v, 'code':file_code, 'filename':filename})
    if not len(files):
        cursor.close()
        biodb.close()
        http_error(204, 'Dateien nicht gefunden oder kein Leserecht')
    import tempfile, zipfile
    tmpdir = tempfile.mkdtemp()
    zip_path = tmpdir + '/' + 'temp.zip'
    zip = zipfile.ZipFile(zip_path, 'x', compression=zipfile.ZIP_DEFLATED, compresslevel=5)
    for f in files:
        filepath = mediadir + str(f['id']) + '_' + f['code'] + '_' + str(f['v'])
        filename_out = str(f['id']) + '_v' + f['v'] + '_' if dups else ''
        filename_out += f['filename']
        zip.write(filepath, filename_out)
    zip.close()
    header = 'Content-Disposition: inline; filename="biodb_download.zip"\r\n'
    header += 'Content-Length: ' + str(os.path.getsize(zip_path)) + '\r\n'
    header += 'Content-type: application/zip\r\n'
    header += '\r\n'
    sys.stdout.buffer.write(bytes(header, 'ascii'))
    f = open(zip_path, "rb")
    while buf := f.read():
        sys.stdout.buffer.write(buf)
    os.unlink(zip_path)
    os.rmdir(tmpdir)
    closedb_and_exit()

if cgiparam('HD_ZUO_H'):
    msg_parts = []
    h_id = cgiparam_num('HD_ZUO_H')
    k = cgiparam('HD_ZUO_K')
    cursor.execute("select user from hoehle where id=%s", (h_id,))
    row = cursor.fetchone()
    zugeordnet = 0
    if not row:
        msg_parts.append('Höhle '+str(h_id)+' existiert nicht')
    elif 'HD_ZUO_D' in cgiparams:
        schreibrecht_auf_hoehle = admin or row[0] == userid
        for d_id in cgiparams['HD_ZUO_D']:
            if not d_id.isnumeric():
                continue
            d_id = int(d_id)
            cursor.execute("select owner from datei where id=%s", (d_id,))
            row = cursor.fetchone()
            if not row:
                msg_parts.append('Datei ' + str(d_id) + ' existiert nicht')
                continue
            dateirecht = recht_auf_datei(d_id, row[0])
            if not dateirecht:
                msg_parts.append('kein Leserecht auf Datei ' + str(d_id))
                continue
            if not (schreibrecht_auf_hoehle or dateirecht==2):
                msg_parts.append('Schreibrecht weder auf Datei ' + str(d_id) + ' noch auf Höhle '+str(h_id))
                continue
            cursor.execute("select count(*) from hoehle_datei where hoehle_id=%s and datei_id=%s", (h_id, d_id))
            if cursor.fetchone()[0]:
                msg_parts.append('Datei ' + str(d_id) + ' und Höhle ' + str(h_id) + ' sind einander bereits zugeordnet')
                continue
            cursor.execute("insert into hoehle_datei (hoehle_id,datei_id,kategorie,user) values(%s,%s,%s,%s)", (h_id, d_id, k, userid))
            zugeordnet += 1
    if zugeordnet:
        biodb.commit()
    msg_parts.insert(0, str(zugeordnet) + ' neu zugeordnet')
    status = '. '.join(msg_parts) + '.'
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + status
    )
    closedb_and_exit()

if cgiparam('FD_ZUO_B'):
    besuch_id = cgiparam_num('FD_ZUO_B')
    fund_i = cgiparam_num('FD_ZUO_I')
    cursor.execute("select user,hoehle_id from besuch where id=%s", (besuch_id,))
    row = cursor.fetchone()
    if not row:
        cursor.close()
        biodb.close()
        http_error('404', 'Fund '+str(besuch_id)+'/'+str(fund_i)+' existiert nicht')
    besuchuser,hoehleid = row
    if besuchuser != userid:
        cursor.close()
        biodb.close()
        http_error('403', 'kein Schreibrecht auf Fund')
    if not cgiparam_num('FD_ZUO_H'):
        hoehleid = None
    if hoehleid:
        cursor.execute("select user from hoehle where id=%s", (hoehleid,))
        row = cursor.fetchone()
        schreibrecht_auf_hoehle = admin or row[0] == userid
    zugeordnet = zugeordnet_nur_hoehle = 0
    msg_parts = []
    if 'FD_ZUO_D' in cgiparams:
        for d_id in cgiparams['FD_ZUO_D']:
            if not d_id.isnumeric():
                continue
            d_id = int(d_id)
            cursor.execute("select owner from datei where id=%s", (d_id,))
            row = cursor.fetchone()
            if not row:
                msg_parts.append('Datei ' + str(d_id) + ' existiert nicht')
                continue
            dateirecht = recht_auf_datei(d_id, row[0])
            if not dateirecht:
                msg_parts.append('kein Leserecht auf Datei ' + str(d_id))
                continue
            inserted = inserted_hoehle = False
            cursor.execute("select count(*) from fund_datei where besuch_id=%s and fund_i=%s and datei_id=%s", (besuch_id, fund_i, d_id))
            if not cursor.fetchone()[0]:
                cursor.execute("insert into fund_datei(besuch_id,fund_i,datei_id) values(%s,%s,%s)", (besuch_id, fund_i, d_id))
                zugeordnet += 1
                inserted = True
            if hoehleid:
                cursor.execute("select count(*) from hoehle_datei where hoehle_id=%s and datei_id=%s", (hoehleid, d_id))
                if not cursor.fetchone()[0]:
                    if schreibrecht_auf_hoehle or dateirecht==2:
                        cursor.execute("insert into hoehle_datei (hoehle_id,datei_id,kategorie,user) values(%s,%s,'M',%s)", (hoehleid, d_id, userid))
                        if not inserted:
                            zugeordnet_nur_hoehle += 1
                    else:
                        msg_parts.append('Schreibrecht weder auf Datei ' + str(d_id) + ' noch auf Höhle '+str(hoehleid))
    if zugeordnet:
        biodb.commit()
    msg_parts.insert(0, str(zugeordnet) + ' neu zugeordnet')
    if zugeordnet_nur_hoehle:
        msg_parts.insert(0, str(zugeordnet_nur_hoehle) + ' nur der Höhle neu zugeordnet (war' + ('en' if zugeordnet_nur_hoehle>1 else '') + ' bereits dem Fund zugeordnet)')
    status = '. '.join(msg_parts) + '.'
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + status
    )
    closedb_and_exit()

if cgiparam('PD_ZUO_P'):
    pub_id = cgiparam_num('PD_ZUO_P')
    abschnitt = cgiparam('PD_ZUO_ABSCHNITT') or None
    cursor.execute("select user from publikation where id=%s", (pub_id,))
    row = cursor.fetchone()
    if not row:
        cursor.close()
        biodb.close()
        http_error('404', 'Publikation '+str(pub_id)+' existiert nicht')
    if row[0] != userid:
        cursor.close()
        biodb.close()
        http_error('403', 'kein Schreibrecht auf Publikation')
    zugeordnet = gehoben = 0
    msg_parts = []
    if 'PD_ZUO_D' in cgiparams:
        for d in cgiparams['PD_ZUO_D']:
            datei_id,datei_v = d.split('_')
            if not datei_id.isnumeric() or not datei_v.isnumeric():
                continue
            datei_id = int(datei_id)
            datei_v = int(datei_v)
            cursor.execute("select owner from datei where id=%s", (datei_id,))
            row = cursor.fetchone()
            if not row:
                msg_parts.append('Datei ' + str(datei_id) + ' existiert nicht')
                continue
            dateirecht = recht_auf_datei(datei_id, row[0])
            if not dateirecht:
                msg_parts.append('kein Leserecht auf Datei ' + str(datei_id))
                continue
            inserted = False
            cursor.execute("select datei_v from pub_datei where pub_id=%s and datei_id=%s", (pub_id, datei_id))
            row = cursor.fetchone()
            if row:
                if row[0] < datei_v:
                    cursor.execute("update pub_datei set datei_v=%s where pub_id=%s and datei_id=%s", (datei_v, pub_id, datei_id))
                    gehoben += 1
            else:
                cursor.execute("insert into pub_datei(pub_id, datei_id, datei_v, i, abschnitt) select %s,%s,%s,ifnull(max(i),0)+1,%s from pub_datei where pub_id=%s", (pub_id, datei_id, datei_v, abschnitt, pub_id))
                zugeordnet += 1
    if zugeordnet or gehoben:
        biodb.commit()
    msg_parts.insert(0, str(zugeordnet) + ' neu zugeordnet')
    if gehoben:
        msg_parts.append(str(gehoben) + ' auf neuere Dateiversion geändert')
    status = '. '.join(msg_parts) + '.'
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + status
    )
    closedb_and_exit()

if cgiparam('DEL_HDZUO_D'):
    hd_d_id = cgiparam('DEL_HDZUO_D')
    hd_h_id = cgiparam('DEL_HDZUO_H')
    berechtigt = False
    if admin or kataster:
        berechtigt = True
    else:
        cursor.execute("select user from hoehle where id=%s", (hd_h_id,))
        row = cursor.fetchone()
        if row and (row[0] == userid):
            berechtigt = True
    if not berechtigt:
        cursor.execute("select owner from datei where id=%s", (hd_d_id,))
        if row and recht_auf_datei(hd_d_id,row[0]) == 2:
            berechtigt = True
    if berechtigt:
        cursor.execute("delete from hoehle_datei where hoehle_id=%s and datei_id=%s", (hd_h_id, hd_d_id))
        biodb.commit()
        status = 'ok'
    else:
        status = 'nicht löschberechtigt'
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + status
    )
    closedb_and_exit()

if cgiparam('RECHTEAKT') and admin:
    cursor.execute("call rebuild_ortscore()");
    biodb.commit()
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + 'ok'
    )
    closedb_and_exit()

if cgiparam('VERKL_DFLT'):
    opts = {
        'VerklRedu': ['N','A','R','B'].index(cgiparam('VERKL_REDU')),
        'VerklAbs': cgiparam_num('VERKL_ABS'),
        'VerklAbsEinh': ['K','M'].index(cgiparam('VERKL_ABS_EINH')),
        'VerklRel': cgiparam_num('VERKL_REL'),
        'VerklMin': cgiparam_num('VERKL_MIN'),
        'VerklMinEinh': ['K','M'].index(cgiparam('VERKL_MIN_EINH')),
        'VerklQual': cgiparam_num('VERKL_QUAL'),
        'VerklSkal': cgiparam_num('VERKL_SKAL'),
        'VerklSkalPx': cgiparam_num('VERKL_SKAL_PX'),
        'VerklSkalMinPx': cgiparam_num('VERKL_SKAL_MINPX')
    }
    for o in opts:
        cursor.execute("select wert from user_opt where user=%s and optname=%s", (userid, o))
        if row := cursor.fetchone():
            if row[0] != opts[o]:
                cursor.execute("update user_opt set wert=%s where user=%s and optname=%s", (opts[o], userid, o))
        else:
            cursor.execute("insert into user_opt(user,optname,wert) values(%s,%s,%s)", (userid, o, opts[o]))
    biodb.commit()
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + 'ok'
    )
    closedb_and_exit()

# Suchprofile laden/ändern
if cgiparam('SUCHOPT_LADEN'):
    suchopt_name = cgiparam('SUCHOPT_LADEN')
    suchopt_form = cgiparam('FORM')
    result = {}
    cursor.execute("select opts from suchopt where user=%s and formular=%s and name=%s", (userid, suchopt_form, suchopt_name))
    if row := cursor.fetchone():
        result['formdata'] = json.loads(row[0])
    cursor.execute("select name from suchopt where user=%s and formular=%s order by case name when 'Default' then '0' else '1' end, name", (userid, suchopt_form))
    result['names'] = [row[0] for row in cursor.fetchall()]
    print('Content-type: application/json; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + json.dumps(result, ensure_ascii=False)
    )
    closedb_and_exit()
elif cgiparam('SUCHOPT_SPEICHERN') or cgiparam('SUCHOPT_LOESCHEN'):
    suchopt_name = cgiparam('SUCHOPT_SPEICHERN') or cgiparam('SUCHOPT_LOESCHEN');
    suchopt_form = cgiparam('FORM')
    cursor.execute("delete from suchopt where user=%s and formular=%s and name=%s", (userid, suchopt_form, suchopt_name))
    if cgiparam('SUCHOPT_SPEICHERN'):
        cursor.execute("insert into suchopt (user, formular, name, opts) values(%s,%s,%s,%s)", (userid, suchopt_form, suchopt_name, cgiparam('JSON')))
    biodb.commit()
    cursor.execute("select name from suchopt where user=%s and formular=%s order by case name when 'Default' then '0' else '1' end, name", (userid, suchopt_form))
    result = [row[0] for row in cursor.fetchall()]
    print('Content-type: application/json; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + json.dumps(result, ensure_ascii=False)
    )
    closedb_and_exit()

def send_mail(p_empf, p_typ, p_content):  # Returnwert: Fehlermeldung oder None
    cursor.execute("select mail, concat_ws(' ', name1, name2) from user where id=%s", (p_empf,))
    empf_mail, empf_realname = cursor.fetchone()
    if not empf_mail:
        return None
    if empf_realname and empf_mail.find('<') == -1:
        empf_mail = empf_realname + ' <' + empf_mail + '>'
    msg = EmailMessage()
    msg['From'] = "Funddatenbank <" + CONF_MAIL + ">"
    msg['To'] = empf_mail
    msg['Subject'] = "Funddatenbank: automatische Benachrichtigung"
    msg['Date'] = email.utils.formatdate(localtime=True)
    msg['Message-ID'] = email.utils.make_msgid()
    msg.set_content(p_content)

    smtp = smtplib.SMTP(host='s161.goserver.host', port=587, timeout=5)
    try:
        smtp.ehlo()
    except SMTPHeloError:
        smtp.quit()
        return "Fehler beim Senden der E-Mail-Benachrichtigung: EHLO abgelehnt"
    try:
        smtp.starttls()
    except SMTPNotSupportedError:
        smtp.quit()
        return "Fehler beim Senden der E-Mail-Benachrichtigung: StartTLS abgelehnt"
    try:
        smtp.login('web285p1', 'ot55xiis')
    except (SMTPAuthenticationError, SMTPNotSupportedError):
        smtp.quit()
        return "Fehler beim Senden der E-Mail-Benachrichtigung: Authentifizierung fehlgeschlagen"
    try:
        smtp.sendmail(CONF_MAIL, empf_mail, msg.as_string())
    except SMTPRecipientsRefused:
        smtp.quit()
        return "Fehler beim Senden der E-Mail-Benachrichtigung: Empfängeradresse funktioniert nicht"
    except SMTPSenderRefused:
        smtp.quit()
        return "Fehler beim Senden der E-Mail-Benachrichtigung: Mailserver mag die Absenderadresse nicht"
    except:
        smtp.quit()
        return "Fehler beim Senden der E-Mail-Benachrichtigung: unbekannter Fehler beim Senden des Mailinhalts"
    smtp.quit()
    cursor.execute("insert into maillog (sender, empfaenger, typ) values(%s,%s,%s)", (userid, p_empf, p_typ))
    biodb.commit()
    return None

def mailbenachrichtigung (p_empf, p_typ, p_text):  # Returnwert: Fehlermeldung oder None
    # schauen, ob Empfänger die Benachrichtigung eingeschaltet hat und ob die Bedingungen zutreffen
    cursor.execute("select mail, active_d from user where id=%s", (p_empf,))
    row = cursor.fetchone()
    empfmail, zuletzt_aktiv = row
    if empfmail:
        cursor.execute("select wert from user_opt where user=%s and optname=%s", (p_empf,p_typ))
        if row := cursor.fetchone():
            wert = row[0]
            if wert == 1:
                if zuletzt_aktiv is None:
                    wert = 2
                else:
                    cursor.execute("select max(gesendet_d) from maillog where empfaenger=%s and typ=%s", (p_empf,p_typ))
                    row = cursor.fetchone()
                    if row is None:
                        wert = 2
                    else:
                        zuletzt_gesendet = row[0]
                        if zuletzt_gesendet is None or zuletzt_gesendet < zuletzt_aktiv:
                            wert = 2
                p_text += "\n\nAuf Grund Ihrer Einstellungen erhalten sie keine weiteren solchen Benachrichtigungen, bis sie die Funddatenbank wieder besuchen (und dort wieder eingeloggt sind)."
            if wert == 2:
                return send_mail(p_empf, p_typ, p_text)
    return None

# Ajax: Bestimmungsanfrage senden
if cgiparam_num('ANFRAGE_AN') and userid:
    result = 'err'
    besuchid = cgiparam_num('BESUCH_ID')
    fund_i = cgiparam_num('FUND_I')
    empf = cgiparam_num('ANFRAGE_AN')
    anm = cgiparam('ANFRAGE_ANM') or None
    cursor.execute("select user from besuch where id=%s", (besuchid,))
    row = cursor.fetchone()
    if row and row[0] == userid: # Sender muss der User sein, der den Fund angelegt hat
        cursor.execute("insert into anfrage(besuch_id, fund_i, an_user, anmerkung) values(%s,%s,%s,%s)", (besuchid, fund_i, empf, anm))
        biodb.commit() # Zwischencommit, weil der Mailversand länger dauern kann
        # Mailbenachrichigung an Bestimmer senden, wenn der das so konfiguriert hat
        result = mailbenachrichtigung(empf, 'MailBestAnfr', s['realname'] + " bittet Sie um eine Bestimmung. Bitte loggen Sie sich in der Funddatenbank (" + CONF_HP + ") ein und wählen Sie das Menü Funde → Bstm.anfragen um diese Anfrage (und ggf. weitere) zu bearbeiten.")
        if result:
            result = 'mailerr: ' + result
        else:
            result = 'ok'
    print('Content-type: text/plain; charset=utf-8\r\n'
        + 'Pragma: no-cache\r\n'
        + 'Cache-Control: no-store\r\n'
        + '\r\n' + result
    )
    closedb_and_exit()

def save_user_opt(p_user, p_optname, p_optwert):
    if type(p_optwert) is int:
        debug('h1')
        feldname = 'wert'
    else:
        feldname = 'wert_s'
        if p_optwert == '':
            p_optwert = None
    cursor.execute("select "+feldname+" from user_opt where user=%s and optname=%s", (p_user,p_optname))
    if row := cursor.fetchone():
        if row[0] != p_optwert:
            cursor.execute("update user_opt set "+feldname+"=%s where user=%s and optname=%s", (p_optwert, p_user, p_optname))
    elif p_optwert:
        cursor.execute("insert into user_opt(user,optname,"+feldname+") values(%s,%s,%s)", (p_user, p_optname, p_optwert))

def upd_mailanz(p_user, p_wert):
    save_user_opt(p_user, 'MailAnz', p_wert)
seluser_id = cgiparam_num('SELUSER')

# Benutzer anlegen/ändern/löschen
if cgiparam('USERMOD_NAME_STRUKTUR') and admin:
    usermod_id = cgiparam_num('USERMOD_ID')
    usermod_username = cgiparam('USERMOD_USERNAME') or None
    usermod_passwd = cgiparam('USERMOD_PASSWD') or None
    usermod_name_struktur = cgiparam('USERMOD_NAME_STRUKTUR')
    usermod_name1 = cgiparam('USERMOD_NAME1')
    usermod_name2 = cgiparam('USERMOD_NAME2') or None
    usermod_mail = cgiparam('USERMOD_MAIL') or None
    usermod_mailanz = cgiparam_num('USERMOD_MAILANZ')
    usermod_beschreibung = cgiparam('USERMOD_BESCHREIBUNG') or None
    usermod_grp = cgiparams['USERMOD_GRP'] if 'USERMOD_GRP' in cgiparams else []
    if usermod_id:
        if cgiparam_num('USERMOD_DEL'):
            try:
                cursor.execute("delete from user where id=%s", (usermod_id,))
                biodb.commit()
                s['statusmsg'] = "User " + str(usermod_id) + " gelöscht."
                s['closewindow'] = True
                # seluser_id = 0 # überflüssig, ist sowieso 0
            except mysql.connector.Error as err:
                if err.errno in (errorcode.ER_ROW_IS_REFERENCED, errorcode.ER_ROW_IS_REFERENCED_2):
                    s['statusmsg'] = "Delete schiefgegangen, weil der User schon Daten angelegt hat."
                else:
                    s['statusmsg'] = "Delete schiefgegangen: " + esc_js_str(str(err))
                seluser_id = usermod_id
        else:
            try:
                cursor.execute("update user set username=%s, passwd=%s, name_struktur=%s, name1=%s, name2=%s, mail=%s, beschreibung=%s where id=%s", (usermod_username, usermod_passwd, usermod_name_struktur, usermod_name1, usermod_name2, usermod_mail, usermod_beschreibung, usermod_id))
                biodb.commit()
            except mysql.connector.Error as err:
                if err.errno in (errorcode.ER_DUP_UNIQUE, errorcode.ER_DUP_ENTRY):
                    s['statusmsg'] = "Update schiefgegangen, Loginname doppelt vergeben?"
                else:
                    s['statusmsg'] = "Update schiefgegangen: " + esc_js_str(str(err))
            seluser_id = usermod_id
    else:
        try:
            cursor.execute("insert into user (username, passwd, name_struktur, name1, name2, mail, beschreibung) values (%s,%s,%s,%s,%s,%s,%s) returning id", (usermod_username, usermod_passwd, usermod_name_struktur, usermod_name1, usermod_name2, usermod_mail, usermod_beschreibung))
            seluser_id = cursor.fetchone()[0]
            biodb.commit()
        except mysql.connector.Error as err:
            if err.errno in (errorcode.ER_DUP_UNIQUE, errorcode.ER_DUP_ENTRY):
                s['statusmsg'] = "Insert schiefgegangen, Loginname doppelt vergeben?"
            else:
                s['statusmsg'] = "Insert schiefgegangen: " + esc_js_str(str(err))
    if seluser_id:
        cursor.execute("delete from user_gruppe where user=%s", (seluser_id,))
        for g in usermod_grp:
            cursor.execute("insert into user_gruppe (user, gruppe) values (%s,%s)", (seluser_id, g))
        upd_mailanz(seluser_id, usermod_mailanz)
        biodb.commit()

# Benutzer(formular) anzeigen
    # Erklärung zur If-Bedingung:
    # SELUSER für Neuanlageformular nötig
    # seluser_id nötig für Neuanlage durchgeführt
    # USERMOD_DEL nötig um nochmal auf die Seite zurückzukehren und das Fenster zu schließen
    # userid nötig weil nur eingeloggte User die Liste sehen dürfen
if (seluser_id or cgiparam('SELUSER') or cgiparam_num('USERMOD_DEL')) and userid:
    seluser_username = seluser_passwd = seluser_name_struktur = seluser_name1 = seluser_name2 = seluser_mail = seluser_beschreibung = ''
    seluser_mailanz = False
    if seluser_id:
        cursor.execute("select username, passwd, name_struktur, name1, name2, mail, beschreibung from user where id=%s", (seluser_id,))
        seluser_username, seluser_passwd, seluser_name_struktur, seluser_name1, seluser_name2, seluser_mail, seluser_beschreibung = cursor.fetchone()
        cursor.execute("select wert from user_opt where user=%s and optname='MailAnz'", (seluser_id,))
        if row := cursor.fetchone():
            seluser_mailanz = bool(row[0])
    s['seluser_id'] = seluser_id or ''
    s['seluser_username'] = seluser_username or ''
    s['seluser_passwd'] = seluser_passwd or ''
    s['seluser_name_struktur'] = seluser_name_struktur
    s['seluser_name1'] = seluser_name1 or ''
    s['seluser_name2'] = seluser_name2 or ''
    s['seluser_mail'] = seluser_mail if seluser_mail and (seluser_mailanz or admin) else ''
    s['seluser_mailanz'] = seluser_mailanz
    s['seluser_beschreibung'] = seluser_beschreibung or ''
    if seluser_mail and (seluser_mailanz or admin):
        s['seluser_maillink'] = urllib.parse.quote(seluser_mail, safe='', encoding='utf-8');
        if seluser_mail.find(' ') == -1:
            s['seluser_maillink'] = urllib.parse.quote(seluser_name1 + ' ' + seluser_name2, safe='', encoding='utf-8') + ' <' + s['seluser_maillink'] + '>'; # Name <foo@bar> stat nur foo@bar
    else:
        s['seluser_maillink'] = None
    if seluser_id:
        cursor.execute("select gruppe from user_gruppe where user=%s", (seluser_id,))
        s['seluser_grp'] = [row[0] for row in cursor.fetchall()]
    else:
        s['seluser_grp'] = []
    cursor.execute("select id, name from gruppe order by name")
    s['gruppen'] = [{'id': row[0], 'name': row[1]} for row in cursor.fetchall()]
    print('Content-type: text/html; charset=utf-8' + '\r\n\r\n' + '<!DOCTYPE html>')
    userdet_template = Template(filename=appdir + 'templates/userdet.tpl')
    print(userdet_template.render(u = s))
    print("</html>")
    closedb_and_exit()

# Gruppe anlegen/ändern/löschen (genauso wie User anlegen/ändern/löschen)
selgrp_id = cgiparam_num('SELGRP')
if cgiparam('GRPMOD_NAME') and admin:
    grpmod_id = cgiparam_num('GRPMOD_ID')
    grpmod_name = cgiparam('GRPMOD_NAME')
    grpmod_beschreibung = cgiparam('GRPMOD_BESCHREIBUNG') or None
    grpmod_grp = cgiparams['GRPMOD_GRP'] if 'GRPMOD_GRP' in cgiparams else []
    if grpmod_id:
        if cgiparam_num('GRPMOD_DEL'):
            try:
                cursor.execute("delete from gruppe where id=%s", (grpmod_id,))
                biodb.commit()
                s['statusmsg'] = "Gruppe " + str(grpmod_id) + " gelöscht."
                s['closewindow'] = True
                # selgrp_id = 0 # überflüssig, ist sowieso 0
            except mysql.connector.Error as err:
                s['statusmsg'] = "Delete schiefgegangen: " + esc_js_str(str(err))
                selgrp_id = grpmod_id
        else:
            try:
                cursor.execute("update gruppe set name=%s, beschreibung=%s where id=%s", (grpmod_name, grpmod_beschreibung, grpmod_id))
                biodb.commit()
            except mysql.connector.Error as err:
                s['statusmsg'] = "Update schiefgegangen: " + esc_js_str(str(err))
            selgrp_id = grpmod_id
    else:
        try:
            cursor.execute("insert into gruppe (name, beschreibung) values (%s,%s) returning id", (grpmod_name, grpmod_beschreibung))
            selgrp_id = cursor.fetchone()[0]
            biodb.commit()
        except mysql.connector.Error as err:
            s['statusmsg'] = "Insert schiefgegangen: " + esc_js_str(str(err))
    if selgrp_id:
        cursor.execute("delete from gruppe_gruppe where child=%s", (selgrp_id,))
        for g in grpmod_grp:
            cursor.execute("insert into gruppe_gruppe (child, parent) values (%s,%s)", (selgrp_id, g))
        biodb.commit()

# Benutzergruppe anzeigen (genauso wie User anzeigen)
if (selgrp_id or cgiparam('SELGRP') or cgiparam_num('GRPMOD_DEL')) and userid:
    selgrp_name = selgrp_beschreibung = ''
    if selgrp_id:
        cursor.execute("select name, beschreibung from gruppe where id=%s", (selgrp_id,))
        selgrp_name, selgrp_beschreibung = cursor.fetchone()
    s['selgrp_id'] = selgrp_id or ''
    s['selgrp_name'] = selgrp_name
    s['selgrp_beschreibung'] = selgrp_beschreibung or ''
    if selgrp_id:
        cursor.execute("select parent from gruppe_gruppe where child=%s", (selgrp_id,))
        s['selgrp_grp'] = [row[0] for row in cursor.fetchall()]
    else:
        s['selgrp_grp'] = []
    cursor.execute("select id, name from gruppe order by name")
    s['gruppen'] = [{'id': row[0], 'name': row[1]} for row in cursor.fetchall()]
    print('Content-type: text/html; charset=utf-8' + '\r\n\r\n' + '<!DOCTYPE html>')
    grpdet_template = Template(filename=appdir + 'templates/grpdet.tpl')
    print(grpdet_template.render(u = s))
    print("</html>")
    closedb_and_exit()

# Verein anlegen/ändern/löschen
selverein_id = cgiparam_num('SELVEREIN')
if cgiparam('VEREINMOD_NAME') and admin:
    vereinmod_id = cgiparam_num('VEREINMOD_ID')
    vereinmod_name = cgiparam('VEREINMOD_NAME')
    vereinmod_rechtsform = cgiparam('VEREINMOD_RECHTSFORM') or None
    vereinmod_beschreibung = cgiparam('VEREINMOD_BESCHREIBUNG') or None
    vereinmod_homepage = cgiparam('VEREINMOD_HP') or None
    vereinmod_vrn = cgiparams['VEREINMOD_VRN'] if 'VEREINMOD_VRN' in cgiparams else []
    if vereinmod_id:
        if cgiparam_num('VEREINMOD_DEL'):
            try:
                cursor.execute("delete from verein where id=%s", (vereinmod_id,))
                biodb.commit()
                s['statusmsg'] = "Verein " + str(vereinmod_id) + " gelöscht."
                s['closewindow'] = True
                # selverein_id = 0 # überflüssig, ist sowieso 0
            except mysql.connector.Error as err:
                s['statusmsg'] = "Delete schiefgegangen: " + esc_js_str(str(err))
                selverein_id = vereinmod_id
        else:
            try:
                cursor.execute("update verein set name=%s, rechtsform=%s, beschreibung=%s, homepage=%s where id=%s", (vereinmod_name, vereinmod_rechtsform, vereinmod_beschreibung, vereinmod_homepage, vereinmod_id))
                biodb.commit()
            except mysql.connector.Error as err:
                s['statusmsg'] = "Update schiefgegangen: " + esc_js_str(str(err))
            selverein_id = vereinmod_id
    else:
        try:
            cursor.execute("insert into verein (name, rechtsform, beschreibung, homepage) values (%s,%s,%s,%s) returning id", (vereinmod_name, vereinmod_rechtsform, vereinmod_beschreibung, vereinmod_homepage))
            selverein_id = cursor.fetchone()[0]
            biodb.commit()
        except mysql.connector.Error as err:
            s['statusmsg'] = "Insert schiefgegangen: " + esc_js_str(str(err))
    if selverein_id:
        cursor.execute("delete from verein_verein where child=%s", (selverein_id,))
        for v in vereinmod_vrn:
            cursor.execute("insert into verein_verein (child, parent) values (%s,%s)", (selverein_id, v))
        biodb.commit()

# Verein anzeigen
if (selverein_id or cgiparam('SELVEREIN') or cgiparam_num('VEREINMOD_DEL')):
    selverein_name = selverein_beschreibung = selverein_homepage = ''
    selverein_rechtsform = None
    if selverein_id:
        cursor.execute("select name, rechtsform, beschreibung, homepage from verein where id=%s", (selverein_id,))
        selverein_name, selverein_rechtsform, selverein_beschreibung, selverein_homepage = cursor.fetchone()
    s['selverein_id'] = selverein_id or ''
    s['selverein_name'] = selverein_name
    s['selverein_rechtsform'] = selverein_rechtsform
    s['selverein_beschreibung'] = selverein_beschreibung or ''
    s['selverein_homepage'] = selverein_homepage or ''
    if selverein_id:
        cursor.execute("select parent from verein_verein where child=%s", (selverein_id,))
        s['selverein_vrn'] = [row[0] for row in cursor.fetchall()]
    else:
        s['selverein_vrn'] = []
    cursor.execute("select id, name from verein order by name")
    s['vereine'] = [{'id': row[0], 'name': row[1]} for row in cursor.fetchall()]
    print('Content-type: text/html; charset=utf-8' + '\r\n\r\n' + '<!DOCTYPE html>')
    verein_template = Template(filename=appdir + 'templates/verein.tpl')
    print(verein_template.render(u = s))
    print("</html>")
    closedb_and_exit()

# Einstellungen des Benutzers
useropts = {}
if userid:
    cursor.execute("select optname, wert, wert_s from user_opt where user=%s", (userid,))
    for row in cursor.fetchall():
        useropts[row[0]] = row[2] if row[1] is None else row[1]

# Einstellungen
if cgiparam('CFG'):
    # Passwort ändern
    if ('NPASS1' in cgiparams or 'NPASS2' in cgiparams) and userid:
        if cgiparam('NPASS1') != cgiparam('NPASS1'):
            err('unterschiedliche Passwörter eingegeben')
        elif cgiparam('NPASS1') == '':
            err('Passwort darf nicht leer sein')
        else:
            cursor.execute("update user set passwd=%s where id=%s", (cgiparam('NPASS1'), userid))
            biodb.commit()
            s['cfgmsg'] = 'Passwort geändert.'
    if 'USERBESCHREIBUNG' in cgiparams:
        s['cfg_userbeschreibung'] = cgiparam('USERBESCHREIBUNG')
        s['cfg_name_struktur'] = cgiparam('NAME_STRUKTUR')
        s['cfg_name1'] = cgiparam('NAME1')
        s['cfg_name2'] = cgiparam('NAME2')
        s['cfg_mail'] = cgiparam('MAIL')
        cursor.execute("update user set name_struktur=%s, name1=%s, name2=%s, mail=%s, beschreibung=%s where id=%s", (s['cfg_name_struktur'], s['cfg_name1'], s['cfg_name2'], s['cfg_mail'] or None, s['cfg_userbeschreibung'] or None, userid))
        s['realname'] = (s['cfg_name1'] + ' ' + s['cfg_name2']).strip()
        useropts['MailAnz'] = cgiparam_num('MAILANZ')
        upd_mailanz(userid, useropts['MailAnz'])
        for optname in ['MailBestAnfr','MailBestErl']:
            # weil es in Pythons mysql.connector kein affected_rows() gibt und cursor.rowcount unzuverlässig ist:
            cursor.execute("select wert from user_opt where user=%s and optname=%s", (userid, optname))
            if row := cursor.fetchone():
                if row[0] != cgiparam_num(optname.upper()):
                    cursor.execute("update user_opt set wert=%s where user=%s and optname=%s", (cgiparam_num(optname.upper()), userid, optname))
            else:
                cursor.execute("insert into user_opt (user, optname, wert) values (%s,%s,%s)", (userid, optname, cgiparam_num(optname.upper())))
            useropts[optname] = cgiparam_num(optname.upper())
        useropts['MapDefault'] = cgiparam_num('MAPDEFAULT')
        save_user_opt(userid, 'MapDefault', useropts['MapDefault'])
        useropts['MapCache'] = cgiparam_num('MAPCACHE')
        save_user_opt(userid, 'MapCache', useropts['MapCache'])
        useropts['FileDfltAutor'] = cgiparam('FILEDFLTAUTOR')
        save_user_opt(userid, 'FileDfltAutor', useropts['FileDfltAutor'])
        useropts['FileDfltLizenz'] = cgiparam('FILEDFLTLIZENZ')
        save_user_opt(userid, 'FileDfltLizenz', useropts['FileDfltLizenz'])
        useropts['OrtDfltAutor'] = cgiparam('ORTDFLTAUTOR')
        save_user_opt(userid, 'OrtDfltAutor', useropts['OrtDfltAutor'])
        useropts['OrtDfltLizenz'] = cgiparam('ORTDFLTLIZENZ')
        save_user_opt(userid, 'OrtDfltLizenz', useropts['OrtDfltLizenz'])
        for i in ('LH','KH','LF','KF','HF'):
            useropts['NewWin'+i] = cgiparam_num('NEWWIN_'+i)
            save_user_opt(userid, 'NewWin'+i, useropts['NewWin'+i])
        biodb.commit()
        s['cfgmsg'] = 'Einstellungen geändert.'
    else:
        cursor.execute("select name_struktur, name1, name2, mail, beschreibung from user where id=%s", (userid,))
        s['cfg_name_struktur'], s['cfg_name1'], s['cfg_name2'], s['cfg_mail'], s['cfg_userbeschreibung'] = cursor.fetchone()
    s['cfg'] = 1
s['useropts'] = useropts

# Benutzerliste
if cgiparam('USERLIST') and userid: # nur für Eingeloggte sichtbar, damit Namen und v.a. Mailadressen nicht an Spammer fallen
    s['userliste'] = []
    # Suchformular: auswerten und wiederbefüllen
    s['usersearch_name'] = cgiparam('USERSEARCH_NAME');
    s['usersearch_sort'] = cgiparam('USERSEARCH_SORT');
    bindparams = []
    wherestr = ''
    if s['usersearch_name']:
        wherestr = " where concat_ws(' ',name1,name2) like concat('%',%s,'%')"
        bindparams.append(s['usersearch_name']);
    sortstr = "id"
    if s['usersearch_sort'] == 'N':
        sortstr = "case when name_struktur in ('FG','GP','R') then concat_ws(' ',name1,name2) else concat_ws(' ',name2,name1) end, id"
    cursor.execute("select id, username, name_struktur, name1, name2, mail, login_d, logout_d, active_d, (select group_concat(g.name order by g.name separator ', ') from gruppe g where g.id in(select gruppe from user_gruppe where user=user.id)) from user" + wherestr + " order by " + sortstr, bindparams)
    for row in cursor.fetchall():
        ulist_id, ulist_username, ulist_name_struktur, ulist_name1, ulist_name2, ulist_mail, ulist_login_d, ulist_logout_d, ulist_active_d, ulist_gruppen = row;
        if ulist_mail and (ulist_id != userid and not admin):
            ulist_mailanz = False
            cursor.execute("select wert from user_opt where user=%s and optname='MailAnz'", (ulist_id,))
            if row2 := cursor.fetchone():
                ulist_mailanz = bool(row2[0])
            if not ulist_mailanz:
                ulist_mail = ''
        s['userliste'].append({'id': ulist_id, 'username': ulist_username or '', 'name_struktur': ulist_name_struktur, 'name1': ulist_name1 or '', 'name2': ulist_name2 or '', 'mail': ulist_mail or '', 'login_d': ulist_login_d or '', 'logout_d': ulist_logout_d or '', 'active_d': ulist_active_d or '', 'gruppen': ulist_gruppen or ''})

# Gruppenliste
if cgiparam('GRPLIST') and userid: # nur für Eingeloggte sichtbar, weil auch Benutzerliste nur für Eingeloggte sichtbar ist
    s['grpliste'] = []
    # Suchformular: auswerten und wiederbefüllen
    s['grpsearch_name'] = cgiparam('GRPSEARCH_NAME');
    s['grpsearch_sort'] = cgiparam('GRPSEARCH_SORT');
    bindparams = []
    wherestr = ''
    if s['grpsearch_name']:
        wherestr = " where name like concat('%',%s,'%')"
        bindparams.append(s['grpsearch_name']);
    sortstr = "id"
    if s['grpsearch_sort'] == 'N':
        sortstr = "name, id"
    cursor.execute("select id, name, beschreibung, (select group_concat(g.name order by g.name separator ', ') from gruppe g where g.id in(select parent from gruppe_gruppe where child=gruppe.id)), (select group_concat(g.name order by g.name separator ', ') from gruppe g where g.id in(select child from gruppe_gruppe where parent=gruppe.id)) from gruppe" + wherestr + " order by " + sortstr, bindparams)
    for row in cursor.fetchall():
        glist_id, glist_name, glist_beschreibung, glist_pgruppen, glist_cgruppen = row;
        s['grpliste'].append({'id': glist_id, 'name': glist_name, 'beschreibung': glist_beschreibung or '', 'pgruppen': glist_pgruppen or '', 'cgruppen': glist_cgruppen or ''})

# Katastergruppenliste
if cgiparam('KATGRLIST'):
    s['katgrliste'] = []
    # Suchformular: auswerten und wiederbefüllen
    s['katgrsearch_nr'] = cgiparam('KATGRSEARCH_NR');
    s['katgrsearch_name'] = cgiparam('KATGRSEARCH_NAME');
    s['katgrsearch_sort'] = cgiparam('KATGRSEARCH_SORT');
    bindparams = []
    wherestr = ''
    whereparts = []
    if s['katgrsearch_nr']:
        whereparts.append("k.nr=%s")
        bindparams.append(s['katgrsearch_nr']);
    if s['katgrsearch_name']:
        whereparts.append("k.name like concat('%',%s,'%')")
        bindparams.append(s['katgrsearch_name']);
    if whereparts:
        wherestr = " where " + ' and '.join(whereparts)
    sortstr = "nr"
    if s['katgrsearch_sort'] == 'N':
        sortstr = "k.name, k.nr"
    cursor.execute("select k.nr, k.name, group_concat(v.name order by v.name separator ', ') from katgr k left join katgr_verein kv on k.nr=kv.katgr left join verein v on kv.verein=v.id" + wherestr + " group by k.nr order by " + sortstr, bindparams)
    for row in cursor.fetchall():
        glist_nr, glist_name, glist_vereine = row;
        s['katgrliste'].append({'nr': glist_nr, 'name': glist_name, 'vereine': glist_vereine})

# Vereinliste
if cgiparam('VRNLIST'):
    s['vrnliste'] = []
    # Suchformular: auswerten und wiederbefüllen
    s['vrnsearch_name'] = cgiparam('VRNSEARCH_NAME')
    s['vrnsearch_sort'] = cgiparam('VRNSEARCH_SORT')
    bindparams = []
    wherestr = ''
    if s['vrnsearch_name']:
        wherestr = " where name like concat('%',%s,'%')"
        bindparams.append(s['vrnsearch_name']);
    sortstr = "id"
    if s['vrnsearch_sort'] == 'N':
        sortstr = "name, id"
    cursor.execute("select id, name, rechtsform, beschreibung, homepage, (select group_concat(v.name order by v.name separator ', ') from verein v where v.id in(select parent from verein_verein where child=verein.id)) from verein" + wherestr + " order by " + sortstr, bindparams)
    for row in cursor.fetchall():
        vlist_id, vlist_name, vlist_rechtsform, vlist_beschreibung, vlist_homepage, vlist_pvereine = row;
        if vlist_rechtsform == 'V': vlist_rechtsform = 'Verein'
        elif vlist_rechtsform == 'S': vlist_rechtsform = 'Sektion'
        elif vlist_rechtsform == 'B': vlist_rechtsform = 'Behörde'
        else: vlist_rechtsform = ''
        s['vrnliste'].append({'id': vlist_id, 'name': vlist_name, 'rechtsform': vlist_rechtsform, 'beschreibung': vlist_beschreibung or '', 'homepage': vlist_homepage or '', 'pvereine': vlist_pvereine or ''})

# Taxonomie-Baum
s['taxpick'] = cgiparam('TAXBAUM') == '2'
if cgiparam('TAXBAUM'):
    s['taxbaum'] = []
    cursor.execute("select id,taxon,rank,lit,syn from col where parent is null")
    for row in cursor.fetchall():
        tb_id, tb_taxon, tb_rank, tb_lit, tb_syn = row
        tb_lit = tb_lit or ''
        s['taxbaum'].append({'id': tb_id, 'taxon': tb_taxon, 'rank': tb_rank, 'lit': tb_lit, 'syn': tb_syn});
    s['inittaxsearch'] = cgiparam('INITTAXSEARCH')
    s['inittaxsearch_rang'] = cgiparam_num('INITTAXSEARCH_RANG')

# Ränge
if cgiparam('RANGLIST'):
    s['ranglist'] = 1
s['raenge'] = []
cursor.execute("select ebene, name_la, name_la_abk1, name_la_abk2, col, name_de, name_en, suffix_tiere, suffix_pflanzen, suffix_pilze, suffix_algen, suffix_bakterien, suffix_viren, synonym, auswaehlbar from rang order by ebene desc, synonym")
for row in cursor.fetchall():
    s['raenge'].append({
        'ebene': row[0],
        'name_la': row[1] or '',
        'name_la_abk1': row[2] or '',
        'name_la_abk2': row[3] or '',
        'col': row[4] or '',
        'name_de': row[5] or '',
        'name_en': row[6] or '',
        'suffix_tiere': row[7] or '',
        'suffix_pflanzen': row[8] or '',
        'suffix_pilze': row[9] or '',
        'suffix_algen': row[10] or '',
        'suffix_bakterien': row[11] or '',
        'suffix_viren': row[12] or '',
        'synonym': bool(row[13]),
        'auswaehlbar': bool(row[14])
    })
    if row[1] == 'species':
        s['artebene'] = artebene = row[0]
    elif row[1] == 'genus':
        s['gattungsebene'] = gattungsebene = row[0]
def get_rang(p_ebene):
    if p_ebene is None:
        return ''
    for r in s['raenge']:
        if r['ebene'] == p_ebene:
            return r['name_de'] or r['name_la'] or r['name_en'] or r['col']
    return ''

# Liste der erhaltenen und gesendeten Bestimmungsanfragen
if cgiparam('ALISTE'):
    alist = {'ein':[], 'aus':[]}
    # eingegangene
    cursor.execute("select concat_ws(' ',u.name1,u.name2), b.id, b.besuch_d, f.i, a.gesendet_d, a.anmerkung from user u, besuch b, fund f, anfrage a where u.id=b.user and b.id=f.besuch_id and f.besuch_id=a.besuch_id and f.i=a.fund_i and a.an_user=%s and a.status='N' order by a.gesendet_d", (userid,))
    for row in cursor.fetchall():
        alist_realname, alist_besuch_id, alist_besuch_d, alist_fund_i, alist_gesendet_d, alist_anmerkung = row;
        alist['ein'].append({
            'besuch_id': alist_besuch_id,
            'fund_i': alist_fund_i,
            'realname': alist_realname,
            'besuch_d': alist_besuch_d,
            'gesendet_d': alist_gesendet_d,
            'anmerkung': alist_anmerkung or ''
        })
    #cursor.execute("select concat_ws(' ',u.name1,u.name2), b.id, b.besuch_d, f.i, a.gesendet_d, a.anmerkung, a.erledigt_d, a.status, a.ablehngrund, case when isnull(best.taxon) then null when best.rang>" + str(gattungsebene) + " then best.taxon else concat_ws(' ', best.taxon, case when best.rang=" + str(gattungsebene) + " then 'sp.' else concat_ws(' f. ', concat_ws(' cv. ', concat_ws(' var. ', concat_ws(' ssp. ', best.epitheton, best.ssp), best.var), best.cv), best.form) end) end from user u, besuch b, fund f, anfrage a left join bestimmung best on best.besuch_id=a.besuch_id and best.fund_i=a.fund_i and best.user=a.an_user where u.id=a.an_user and b.id=f.besuch_id and f.besuch_id=a.besuch_id and f.i=a.fund_i and b.user=%s order by a.erledigt_d desc, a.gesendet_d desc, a.status", (userid,))
    #cursor.execute("select concat_ws(' ',u.name1,u.name2), b.id, b.besuch_d, f.i, a.gesendet_d, a.anmerkung, a.erledigt_d, a.status, a.ablehngrund, case when best.rang>=" + str(gattungsebene) + " then concat(best.taxon,' sp.') when best.rang>" + str(gattungsebene) + " then best.taxon else concat_ws(' ', best.taxon, concat_ws(' f. ', concat_ws(' cv. ', concat_ws(' var. ', concat_ws(' ssp. ', best.epitheton, best.ssp), best.var), best.cv), best.form)) end from user u, besuch b, fund f, anfrage a left join bestimmung best on best.besuch_id=a.besuch_id and best.fund_i=a.fund_i and best.user=a.an_user where u.id=a.an_user and b.id=f.besuch_id and f.besuch_id=a.besuch_id and f.i=a.fund_i and b.user=%s order by a.erledigt_d desc, a.gesendet_d desc, a.status", (userid,))
    # vereinfachte Version wegen Bug http://bugs.mysql.com/110422
    cursor.execute("select concat_ws(' ',u.name1,u.name2), b.id, b.besuch_d, f.i, a.gesendet_d, a.anmerkung, a.erledigt_d, a.status, a.ablehngrund, case when best.rang>=" + str(gattungsebene) + " then concat(best.taxon,' sp.') when best.rang>" + str(gattungsebene) + " then best.taxon else concat_ws(' ', best.taxon, concat_ws(' var. ', concat_ws(' ssp. ', best.epitheton, best.ssp), best.var)) end from user u, besuch b, fund f, anfrage a left join bestimmung best on best.besuch_id=a.besuch_id and best.fund_i=a.fund_i and best.user=a.an_user where u.id=a.an_user and b.id=f.besuch_id and f.besuch_id=a.besuch_id and f.i=a.fund_i and b.user=%s order by a.erledigt_d desc, a.gesendet_d desc, a.status", (userid,))
    for row in cursor.fetchall():
        alist_realname, alist_besuch_id, alist_besuch_d, alist_fund_i, alist_gesendet_d, alist_anmerkung, alist_erledigt_d, alist_status, alist_ablehngrund, alist_bestimmt_als = row;
        alist['aus'].append({
            'besuch_id': alist_besuch_id,
            'fund_i': alist_fund_i,
            'realname': alist_realname,
            'besuch_d': alist_besuch_d,
            'gesendet_d': alist_gesendet_d,
            'anmerkung': alist_anmerkung or '',
            'erledigt_d': alist_erledigt_d or '',
            'status': alist_status,
            'ablehngrund': alist_ablehngrund or '',
            'bestimmt_als': alist_bestimmt_als or ''
        })
    s['alist'] = alist

def float2str(p_float):
    if p_float is None:
        return ''
    s = str(p_float)
    if '.' in s:
        while s[-1] in '0.':
            if s[-1] == '.':
                return s[:-1]
            s = s[:-1]
    return s

# Routine für Koordinatentransformation
#set_use_global_context(True)
epsg = {'M28':31257, 'M31':31258, 'M34':31259}
transf = {e: Transformer.from_crs(epsg[e],4326) for e in epsg} # 4326 ist wgs84
def to_wgs84(p_koosystem, p_rw, p_hw):
    if p_rw is None or p_hw is None:
        return None, None
    if p_koosystem == 'G':
        return p_rw, p_hw
    hw, rw = transf[p_koosystem].transform(p_hw, p_rw)
    return rw, hw

d_liste = bool(cgiparam('DLIST'))
d_det_id = cgiparam_num('D_DET_ID')
d_det_v = cgiparam_num('D_DET_V')
if cgiparam_num('D_DET_DEL') and idem_fresh() and d_det_id:
    cursor.execute("select owner,code from datei where id=%s", (d_det_id,))
    d_det_owner, d_det_code = cursor.fetchone()
    alleloeschen = cgiparam_num('D_DET_DEL') == 2
    if alleloeschen:
        if d_det_owner != userid:
            cursor.close()
            biodb.close()
            http_error(403, "kein Löschrecht")
        cursor.execute("select v from datei_version where datei_id=%s", (d_det_id,))
        for row in cursor.fetchall():
            d_det_v = row[0]
            os.unlink(mediadir + str(d_det_id) + '_' + d_det_code + '_' + str(d_det_v))
        cursor.execute("delete from datei_version where datei_id=%s", (d_det_id,))
        cursor.execute("delete from datei where id=%s", (d_det_id,))
        biodb.commit()
        insdel_passiert = True
        d_det_v = d_det_id = 0
        d_liste = True
    elif d_det_v:
        cursor.execute("select count(*) from datei_version where datei_id=%s and v>%s", (d_det_id,d_det_v))
        if cursor.fetchone()[0] > 0:
            cursor.close()
            biodb.close()
            http_error(403, "nicht die neueste Version")
        cursor.execute("select uploader from datei_version where datei_id=%s and v=%s", (d_det_id,d_det_v))
        if cursor.fetchone()[0] != userid:
            cursor.close()
            biodb.close()
            http_error(403, "kein Löschrecht auf diese Version")
        os.unlink(mediadir + str(d_det_id) + '_' + d_det_code + '_' + str(d_det_v))
        cursor.execute("delete from datei_version where datei_id=%s and v=%s", (d_det_id,d_det_v))
        biodb.commit()
        insdel_passiert = True
        d_det_v -= 1

# Höhlen
hoehlen = {}
s['hoehlen'] = []
def get_hoehlen(p_mit_koo):
    prev_hoehle_id = None
    bind_pars = []
    select_str = "select h.id, ifnull(concat(h.katgr,'/',ifnull(h.nr,'-')),''), h.name"
    if p_mit_koo:
        select_str += ", o.koosystem, o.rw, o.hw, o.kooabw, o.sh, genese_anzeige"
    select_str += " from hoehle h"
    if p_mit_koo:
        select_str += ", eingang_ort eo, ort o, mv_ortscore s where h.id=eo.hoehle_id and eo.eingang='a' and eo.ort_id=o.id and o.id=s.ort_id and s.user_id=%s and s.score>0"
        if cgiparam('KARTE'):
            select_str += " and h.id not in(select id_alt from hoehle_altneu)"
        bind_pars.append(userid)
    select_str += " order by isnull(h.katgr), h.katgr, isnull(h.nr), h.nr, h.id"
    if p_mit_koo:
        select_str += ", s.score desc"
    cursor.execute(select_str, bind_pars)
    for row in cursor.fetchall():
        hoehle_id, hoehle_katnr, hoehle_name, hoehle_koosystem, hoehle_rw, hoehle_hw, hoehle_kooabw, hoehle_sh, hoehle_genese = (row if p_mit_koo else row+(None,)*6)
        if p_mit_koo and (hoehle_koosystem is None or hoehle_rw is None or hoehle_hw is None):
            continue
        if hoehle_id == prev_hoehle_id:
            continue
        prev_hoehle_id = hoehle_id
        hoehlen[hoehle_id] = {'katnr': hoehle_katnr}
        hoe_dict = {'id': hoehle_id, 'katnr': hoehle_katnr, 'name': hoehle_name}
        if cgiparam('KARTE'):
            hoehle_rw, hoehle_hw = to_wgs84(hoehle_koosystem, hoehle_rw, hoehle_hw)
            if hoehle_kooabw is None:
                hoehle_kooabw = '?'
            hoe_dict |= {'kooabw': hoehle_kooabw, 'sh': hoehle_sh}
            kookey = str(hoehle_rw) + '|' + str(hoehle_hw)
            s['karte']['hoehlenorte'].setdefault(kookey, {'rw_g': hoehle_rw, 'hw_g': hoehle_hw, 'genese': hoehle_genese, 'hoehlen': []})['hoehlen'].append(hoe_dict)
            if cgiparam_num('OPEN_HOE') == hoehle_id:
                s['show_marker'] = 'H' + kookey;
        else:
            s['hoehlen'].append(hoe_dict)

def size2str(p_size):
    if p_size is None:
        return ''
    if not p_size:
        return '0'
    i = 0
    while p_size > 1024**(i+1):
        i += 1
    return str(round (p_size / 1024**i)) + ' ' + ['B','K','M','T'][i]

# Dateisuche/-liste
if d_liste:
    # falls Löschen-Button betätigt wurde, die ausgewählten Dateien löschen
    if cgiparam('DEL'):
        file_ids = set()
        for id_und_v in cgiparam('DEL').split(','):
            underscore_pos = id_und_v.find('_')
            if underscore_pos < 1:
                continue
            file_id = id_und_v[:underscore_pos]
            if not file_id.isnumeric():
                continue
            if file_id in file_ids:
                continue
            cursor.execute("select owner from datei where id=%s", (file_id,))
            file_owner = cursor.fetchone()[0]
            if recht_auf_datei(file_id, file_owner) < 2:
                continue
            file_ids.add(file_id)
        for file_id in file_ids:
            cursor.execute("select code from datei where id=%s", (file_id,))
            file_code = cursor.fetchone()[0]
            cursor.execute("select v from datei_version where datei_id=%s", (file_id,))
            for row in cursor.fetchall():
                file_v = row[0]
                os.unlink(mediadir + str(file_id) + '_' + file_code + '_' + str(file_v))
            cursor.execute("delete from datei_version where datei_id=%s", (file_id,))
            cursor.execute("delete from datei where id=%s", (file_id,))
            biodb.commit()
    # Suchformular wiederbefüllen
    s['dsearch_id'] = cgiparam_num('DSEARCH_ID') or ''
    s['dsearch_owner_eigene'] = cgiparam('DSEARCH_OWNER_EIGENE')
    s['dsearch_owner'] = cgiparam('DSEARCH_OWNER')
    s['dsearch_uploader_eigene'] = cgiparam('DSEARCH_UPLOADER_EIGENE')
    s['dsearch_uploader'] = cgiparam('DSEARCH_UPLOADER')
    s['dsearch_filename'] = cgiparam('DSEARCH_FILENAME')
    s['dsearch_re'] = cgiparam('DSEARCH_RE')
    s['dsearch_anl_von'] = cgiparam('DSEARCH_ANL_VON')
    s['dsearch_anl_bis'] = cgiparam('DSEARCH_ANL_BIS')
    s['dsearch_upl_von'] = cgiparam('DSEARCH_UPL_VON')
    s['dsearch_upl_bis'] = cgiparam('DSEARCH_UPL_BIS')
    s['dsearch_mime'] = cgiparam('DSEARCH_MIME')
    # s['dsearch_minsize'] = cgiparam_num('DSEARCH_MINSIZE') or ''
    # s['dsearch_maxsize'] = cgiparam_num('DSEARCH_MAXSIZE') or ''
    s['dsearch_hoehlen'] = cgiparam('DSEARCH_HOEHLEN') or ''
    s['dsearch_hoehle_id'] = cgiparam_num('DSEARCH_HOEHLE_ID') or ''
    s['dsearch_kategorie'] = cgiparam('DSEARCH_KATEGORIE') or ''
    s['dsearch_besuch_id'] = cgiparam_num('DSEARCH_BESUCH_ID') or ''
    s['dsearch_fund_i'] = cgiparam_num('DSEARCH_FUND_I') or ''
    s['dsearch_allevers'] = cgiparam('DSEARCH_ALLEVERS')
    s['sort1'] = cgiparam('SORT1')
    s['sort1_dir'] = cgiparam('SORT1_DIR')
    s['sort2'] = cgiparam('SORT2')
    s['sort2_dir'] = cgiparam('SORT2_DIR')
    s['hdzuo_h'] = cgiparam_num('HDZUO_H')
    s['fdzuo_b'] = cgiparam_num('FDZUO_B')
    s['fdzuo_i'] = cgiparam_num('FDZUO_I')
    s['pdzuo_p'] = cgiparam_num('PDZUO_P')
    if s['fdzuo_b'] and cgiparam_num('DLIST') < 2:
        # normalerweise lädt derjenige, der den Fund anlegt, auch die dabei gemachten Fotos hoch
        s['dsearch_owner_eigene'] = True
        # Wenn Fund einer Höhle zugeordnet ist, dann per Default die dieser Höhle zugeordneten Dateien listen
        cursor.execute("select hoehle_id from besuch where id=%s", (s['fdzuo_b'],))
        row = cursor.fetchone()
        if row and row[0]:
            s['dsearch_hoehle_id'] = row[0]
            s['dsearch_hoehlen'] = 'H'
    # wenn Suchformular schon abgeschickt wurde, dann Ergebnisliste anzeigen
    if cgiparam_num('DLIST') > 1:
        s['dliste'] = []
        bindparams = []
        whereparts = ['d.id=dv.datei_id']
        sort_parts = []
        sort_part = ''
        if s['sort1'] == 'I':
            sort_part = 'd.id'
        elif s['sort1'] == 'N':
            sort_part = 'dv.filename'
        elif s['sort1'] == 'G':
            sort_part = 'ifnull(dv.size,-1)'
        elif s['sort1'] == 'A':
            sort_part = 'd.angelegt_d'
        elif s['sort1'] == 'U':
            sort_part = 'dv1.upload_d'
        if sort_part:
            if s['sort1_dir'] == 'DESC':
                sort_part += ' desc'
            sort_parts.append(sort_part)
        sort_part = ''
        if s['sort2'] == 'I':
            sort_part = 'd.id'
        elif s['sort2'] == 'N':
            sort_part = 'dv.filename'
        elif s['sort2'] == 'G':
            sort_part = 'ifnull(dv.size,-1)'
        elif s['sort2'] == 'A':
            sort_part = 'angelegt_d'
        elif s['sort2'] == 'U':
            sort_part = 'dv1.upload_d'
        elif s['sort2'] == 'V':
            sort_part = 'dv.v'
        if sort_part:
            if s['sort2_dir'] == 'DESC':
                sort_part += ' desc'
            sort_parts.append(sort_part)
        if 'd.id' not in sort_parts and 'd.id desc' not in sort_parts:
            sort_parts.append("d.id")
        sortstr = ','.join(sort_parts)
        if s['dsearch_id']:
            whereparts.append("d.id=%s")
            bindparams.append(s['dsearch_id']);
        if s['dsearch_owner']:
            whereparts.append("d.user in(select id from user where concat_ws(' ',name1,name2) like concat('%',%s,'%'))")
            bindparams.append(s['dsearch_owner']);
        if s['dsearch_uploader']:
            whereparts.append("dv.uploader in(select id from user where concat_ws(' ',name1,name2) like concat('%',%s,'%'))")
            bindparams.append(s['dsearch_uploader']);
        if s['dsearch_filename']:
            if s['dsearch_re'] == 'S':
                whereparts.append("dv.filename like concat('%',%s,'%')")
                bindparams.append(s['dsearch_filename']);
            elif s['dsearch_re'] == 'A':
                whereparts.append("dv.filename like concat(%s,'%')")
                bindparams.append(s['dsearch_filename']);
            elif s['dsearch_re'] == 'E':
                whereparts.append("dv.filename like concat('%',%s)")
                bindparams.append(s['dsearch_filename']);
            elif s['dsearch_re'] == 'R':
                whereparts.append("dv.filename rlike %s")
                bindparams.append(s['dsearch_filename']);
            elif s['dsearch_re'] == 'G':
                whereparts.append("dv.filename like %s")
                bindparams.append(s['dsearch_filename'].replace('*','%').replace('?','_'))
            else:
                whereparts.append("h.name=%s")
                bindparams.append(s['dsearch_filename']);
        if s['dsearch_upl_von']:
            whereparts.append("date(dv.upload_d)>=%s")
            bindparams.append(s['dsearch_upl_von']);
        if s['dsearch_upl_bis']:
            whereparts.append("date(dv.upload_d)<=%s")
            bindparams.append(s['dsearch_upl_bis']);
        if s['dsearch_anl_von']:
            whereparts.append("date(d.angelegt_d)>=%s")
            bindparams.append(s['dsearch_anl_von']);
        if s['dsearch_anl_bis']:
            whereparts.append("date(d.angelegt_d)<=%s")
            bindparams.append(s['dsearch_anl_bis']);
        if s['dsearch_mime']:
            whereparts.append("dv.mime like concat(%s,'%')")
            bindparams.append(s['dsearch_mime']);
        if not s['dsearch_allevers']:
            whereparts.append("not exists(select 0 from datei_version dv1 where dv1.datei_id=d.id and dv1.v>dv.v)")
            sortstr += ",dv.v"
        if s['dsearch_hoehlen']:
            if s['dsearch_hoehlen'] == 'H':
                dh_cond = ''
                if s['dsearch_hoehle_id']:
                    dh_cond += " and hd.hoehle_id=%s"
                    bindparams.append(s['dsearch_hoehle_id'])
                if s['dsearch_kategorie']:
                    dh_cond += " and hd.kategorie=%s"
                    bindparams.append(s['dsearch_kategorie'])
                whereparts.append("exists(select 0 from hoehle_datei hd where hd.datei_id=d.id" + dh_cond + ")")
            elif s['dsearch_hoehlen'] == 'K':
                whereparts.append("not exists(select 0 from hoehle_datei hd where hd.datei_id=d.id)")
        #debug(' and '.join(whereparts))
        cursor.execute("select d.id, d.owner, d.code, dv.v, dv.filename, dv.mime, dv.uploader, dv.upload_d, dv.size from datei d, datei_version dv where " + ' and '.join(whereparts) + " order by " + sortstr, bindparams)
        for row in cursor.fetchall():
            dlist_id, dlist_owner, dlist_code, dlist_v, dlist_filename, dlist_mime, dlist_uploader, dlist_upload_d, dlist_size = row
            dlist_recht = recht_auf_datei(dlist_id, dlist_owner)
            s['dliste'].append({'id': dlist_id, 'v': dlist_v, 'filename': dlist_filename, 'code': dlist_code, 'mime': dlist_mime, 'upload_d': dlist_upload_d, 'size': size2str(dlist_size), 'owner': get_realname(dlist_owner), 'uploader': get_realname(dlist_uploader), 'recht': dlist_recht})

    # Selectbox für MIME-Types mit den in der DB vorkommenden befüllen, mit und ohne Subtypes
    cursor.execute("select distinct mime from datei_version where mime is not null")
    dsearch_mimetypes = []
    mime_maintype = None
    for row in cursor.fetchall():
        mime = row[0]
        slash_idx = mime.find('/')
        if slash_idx == -1:
            if mime != mime_maintype:
                mime_maintype = mime
                dsearch_mimetypes.append(mime_maintype)
        else:
            cur_maintype = mime[:slash_idx]
            if cur_maintype != mime_maintype:
                mime_maintype = cur_maintype
                dsearch_mimetypes.append(mime_maintype)
            dsearch_mimetypes.append(mime)
    s['dsearch_mimetypes'] = dsearch_mimetypes
    # Selectbox für Höhlen
    get_hoehlen(False)
    if s['hdzuo_h']:
        s['hdzuo_h_text'] = ''
        for h in s['hoehlen']:
            if h['id'] == s['hdzuo_h']:
                s['hdzuo_h_text'] = 'ID=' + str(h['id'])
                if h['name']:
                    s['hdzuo_h_text'] += ', ' + h['name']
                if h['katnr']:
                    s['hdzuo_h_text'] = h['katnr'] + ' (' + s['hdzuo_h_text'] + ')'
                break
    elif s['pdzuo_p']:
        cursor.execute("select titel from publikation where id=%s", (s['pdzuo_p'],))
        row = cursor.fetchone()
        s['pdzuo_text'] = str(s['pdzuo_p']) + ' (' + row[0] + ')' if row else '?'

def verkl_fillmaskdata():
    if cgiparam('VERKL_REDU'):
        s['verkl_redu'] = cgiparam('VERKL_REDU')
        s['verkl_abs'] = cgiparam_num('VERKL_ABS')
        s['verkl_abs_einh'] = cgiparam('VERKL_ABS_EINH')
        s['verkl_rel'] = cgiparam_num('VERKL_REL')
        s['verkl_min'] = cgiparam_num('VERKL_MIN')
        s['verkl_min_einh'] = cgiparam('VERKL_MIN_EINH')
        s['verkl_qual'] = cgiparam_num('VERKL_QUAL')
        s['verkl_skal'] = cgiparam_num('VERKL_SKAL')
        s['verkl_skal_px'] = cgiparam_num('VERKL_SKAL_PX')
        s['verkl_skal_minpx'] = cgiparam_num('VERKL_SKAL_MINPX')
    elif 'VerklRedu' in useropts:
        s['verkl_redu'] = ['N','A','R','B'][useropts.get('VerklRedu')]
        s['verkl_abs'] = useropts.get('VerklAbs')
        s['verkl_abs_einh'] = ['K','M'][useropts.get('VerklAbsEinh')]
        s['verkl_rel'] = useropts.get('VerklRel')
        s['verkl_min'] = useropts.get('VerklMin')
        s['verkl_min_einh'] = ['K','M'][useropts.get('VerklMinEinh')]
        s['verkl_qual'] = useropts.get('VerklQual')
        s['verkl_skal'] = useropts.get('VerklSkal')
        s['verkl_skal_px'] = useropts.get('VerklSkalPx')
        s['verkl_skal_minpx'] = useropts.get('VerklSkalMinPx')
    else:
        # Systemdefaults
        s['verkl_redu'] = 'B'
        s['verkl_abs'] = 2
        s['verkl_abs_einh'] = 'M'
        s['verkl_rel'] = 50
        s['verkl_min'] = 1
        s['verkl_min_einh'] = 'M'
        s['verkl_qual'] = 71
        s['verkl_skal'] = 1
        s['verkl_skal_px'] = 2400
        s['verkl_skal_minpx'] = 3200

def upl_createtmp(p_dateiinhalt, p_mime, p_tmppath):
    datei = open(p_tmppath, 'wb')
    datei.write(p_dateiinhalt)
    datei.close()
    filesize = len(p_dateiinhalt)
    # Pillow ist im Moment unbrauchbar: nicht nur werden die Dateien viel größer als mit graphicsmagick (vielleicht wegen density=72 vs. 1?), sondern Pillow erzeugt auch fehlerhaftes Exif
    # tmppath = None
    # if mime == 'image/jpeg':
    #    # automatisch verkleinern
    #    maxres = 2400
    #    quality = 75
    #    min_ersparnis = 2 * 1024 * 1024
    #    from PIL import Image, ImageOps
    #    from io import BytesIO
    #    im = Image.open(BytesIO(dateiinhalt))
    #    ImageOps.exif_transpose(im, in_place=True)
    #    if im.width > maxres or im.height > maxres:
    #        if im.height > im.width:
    #            newwidth = int(round(im.width * maxres / im.height))
    #            newheight = maxres
    #        else:
    #            newheight = int(round(im.height * maxres / i.width))
    #            newwidth = maxres
    #        im.resize((newwidth,newheight), Image.Resampling.LANCZOS, reducing_gap=3)
    #    save_opts = {'progressive': True, 'quality': quality}
    #    if 'exif' in im.info:
    #    # Folgendes geht im Moment nicht wegen des auf https://github.com/hMatoba/Piexif/issues/95 beschriebenen Bugs. Der dort angeführte Workaround hilft nicht.
    #    #    import piexif
    #    #    exif_dict = piexif.load(im.info['exif'])
    #    #    if 'thumbnail' in exif_dict:
    #    #        # Workaround, der nicht funktioniert
    #    #        if 41729 in exif_dict['Exif'] and isinstance(exif_dict['Exif'][41729], int):
    #    #            exif_dict['Exif'][41729] = str(exif_dict['Exif'][41729]).encode('utf-8')
    #    #        del exif_dict['thumbnail']
    #    #        save_opts['exif'] = piexif.dump(exif_dict)
    #        save_opts['exif'] = im.info['exif']
    #    tmppath = mediadir + 'tmp_' + dcode
    #    im.save(tmppath, 'jpeg', **save_opts)
    #    im.close()
    #    newsize = os.path.getsize(tmppath)
    #    if newsize <= filesize + min_ersparnis:
    #        filesize = newsize
    #    else:
    #        os.unlink(tmppath)
    #        tmppath = None
    if p_mime == 'image/jpeg' and cgiparam('VERKL_REDU') in('A','R','B'):
        # automatisch verkleinern
        verkl_min = cgiparam_num('VERKL_MIN') * 1024
        if cgiparam('VERKL_MIN_EINH') == 'M':
            verkl_min *= 1024
        if filesize >= verkl_min:
            verkl_abs = cgiparam_num('VERKL_ABS') * 1024
            if cgiparam('VERKL_ABS_EINH') == 'M':
                verkl_abs *= 1024
            verkl_rel = cgiparam_num('VERKL_REL')
            verkl_qual = cgiparam_num('VERKL_QUAL')
            verkl_skal_px = cgiparam_num('VERKL_SKAL_PX')
            verkl_skal_minpx = cgiparam_num('VERKL_SKAL_MINPX')
            from PIL import Image
            from io import BytesIO
            im = Image.open(BytesIO(p_dateiinhalt))
            im.close()
            resizepar = ''
            if verkl_skal_px and max(im.width, im.height) > max(verkl_skal_px,verkl_skal_minpx):
                resizepar = ' -resize '+str(verkl_skal_px)+'x'+str(verkl_skal_px)
            if ret := os.system('gm convert -auto-orient -interlace line -quality '+str(verkl_qual)+resizepar+' '+p_tmppath+' '+p_tmppath+'_mod'):
                err('Bildconvertierung retourniert '+str(ret))
            else:
                newsize = os.path.getsize(p_tmppath+'_mod')
                #debug('abs:'+str(verkl_abs)+' rel:'+str(verkl_rel)+' filesize:'+str(filesize)+' newsize:'+str(newsize))
                if verkl_abs and newsize + verkl_abs <= filesize or verkl_rel and newsize <= filesize * (100-verkl_rel) / 100:
                    os.unlink(p_tmppath)
                    p_tmppath += '_mod'
                    filesize = newsize
                else:
                    os.unlink(tmppath+'_mod')
    return p_tmppath, filesize

# Dateiupload
if cgiparam('D_UPL') and userid:
    dfirstid = dlastid = 0
    dbesuchid = cgiparam_num('BESUCHID') or cgiparam_num('RET_BESUCHID')
    if dbesuchid:
        cursor.execute("select user from besuch where id=%s", (dbesuchid,))
        row = cursor.fetchone()
        if not row:
            err('Besuch '+str(dbesuchid)+' existiert nicht')
            dbesuchid = 0
        elif row[0] != userid:
            err('kein Schreibrecht auf Besuch '+str(dbesuchid))
            dbesuchid = 0
        dfundi = cgiparam_num('FUND_I') or cgiparam_num('RET_FUNDI')
        cursor.execute("select hoehle_id from besuch where id=%s", (dbesuchid,))
        row = cursor.fetchone()
        dhoehleid = row[0] if row else None
    dpub_id = cgiparam_num('PUB_ID') or cgiparam_num('RET_PUBID')
    if dpub_id:
        cursor.execute("select user,titel from publikation where id=%s", (dpub_id,))
        row = cursor.fetchone()
        if not row:
            err('Publikation '+str(dpub_id)+' existiert nicht')
            dpub_id = 0
        elif row[0] != userid:
            err('kein Schreibrecht auf Publikation '+str(dpub_id))
            dpub_id = 0
        dpub_displtext = str(dpub_id) + ' (' + row[1] + ')'
        cursor.execute("select ifnull(max(i),0) from pub_datei where pub_id=%s", (dpub_id,))
        dpubd_i = cursor.fetchone()[0]
        dabschnitt = cgiparam('DABSCHNITT')
    if 'DATEI' in cgifiles and idem_fresh():
        dfilename = cgiparam('DFILENAME') or None
        dnum = cgiparam('DNUM') == '1'
        dbeschr = cgiparam('DBESCHR') or None
        dmime = cgiparam('DMIME') or 'A'
        dmime_mnl = cgiparam('DMIME_MNL') or None
        if not dbesuchid:
            dhoehlenids = [int(h) for h in cgiparams.setdefault('DHOEHLE_ID',[]) if h.isnumeric() and int(h)>0]
            dkategorie = cgiparam('DKATEGORIE')
        dquelle = cgiparam('DQUELLE') or None
        dlizenz = cgiparam('DLIZENZ') or None
        dgeheim = cgiparam('DGEHEIM') or None
        gruppen_r = [int(g) for g in cgiparams.setdefault('DGRUPPEN_R',[]) if g.isnumeric() and int(g)>0]
        gruppen_w = [int(g) for g in cgiparams.setdefault('DGRUPPEN_W',[]) if g.isnumeric() and int(g)>0]
        if dfilename and dnum:
            dextension = ''
            dotpos = dfilename.rfind('.')
            if 0 < dotpos < len(dfilename)-1:
                dextension = dfilename[dotpos:]
                dfilename = dfilename[:dotpos]
            filenum = 0
            # bei Nummerierung bestehende Dateien berücksichtigen
            # unklar inwieweit es Sinn macht, auch Dateien anderer User zu berücksichtigen - das kann zu komischen Ergebnissen führen, wenn deren Fotos neuer sind oder wenn die Rücksichtnahme nicht gegenseitig ist
            cursor.execute("select substr(filename, "+str(len(dfilename)+1)+") from datei_version where substr(filename,1,"+str(len(dfilename))+")=%s and datei_id in(select id from datei where owner=%s)", (dfilename,userid))
            for row in cursor.fetchall():
                numsuffix = row[0]
                if dextension:
                    if not numsuffix.endswith(dextension):
                        continue
                    numsuffix = numsuffix[:numsuffix.rindex(dextension)] # Dateiendung weg
                if numsuffix.isnumeric():
                    numsuffix = int(numsuffix)
                    if numsuffix > filenum:
                        filenum = numsuffix
            lastfilenum = filenum + len(cgifiles['DATEI'])
            anz_digits = len(str(lastfilenum))
        for datei in cgifiles['DATEI']:
            if dfilename:
                dateiname = dfilename
                if dnum:
                    filenum += 1
                    dateiname += str(filenum).zfill(anz_digits) + dextension
            else:
                dateiname = datei['filename']
                if not dateiname:
                    continue
            dateiinhalt = datei['content']
            if dmime == 'A':
                mime = magic.from_buffer(dateiinhalt, mime=True)
            elif dmime == 'M' and dmime_mnl:
                mime = dmime_mnl
            else:
                mime = None
            dcode = ''
            for _ in range(6):
                dcode += random.choice('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
            tmppath, filesize = upl_createtmp(dateiinhalt, mime, mediadir + 'tmp_' + dcode)
            cursor.execute("insert into datei (owner, lastuser, code, beschreibung, quelle, geheim, lizenz) values(%s,%s,%s,%s,%s,%s,%s) returning id", (userid, userid, dcode, dbeschr, dquelle, dgeheim, dlizenz))
            datei_id = cursor.fetchone()[0]
            if not dfirstid:
                dfirstid = datei_id
            dlastid = datei_id
            cursor.execute("insert into datei_version (datei_id, v, uploader, lastuser, filename, mime, upload_d, size) values(%s,1,%s,%s,%s,%s,now(),%s)", (datei_id, userid, userid, dateiname, mime, filesize))
            if dbesuchid:
                cursor.execute("insert into fund_datei (besuch_id,fund_i,datei_id) values (%s,%s,%s)", (dbesuchid, dfundi, datei_id))
                if dhoehleid and cgiparam_num('DFUNDH'):
                    cursor.execute("insert into hoehle_datei (hoehle_id, datei_id, kategorie, user) values(%s,%s,'M',%s)", (dhoehleid, datei_id, userid))
            elif dpub_id:
                dpubd_i += 1
                cursor.execute("insert into pub_datei(pub_id, datei_id, datei_v, i, abschnitt) values(%s,%s,1,%s,%s)", (dpub_id, datei_id, dpubd_i, dabschnitt))
            else:
                cursor.execute("select id from gruppe")
                for row in cursor.fetchall():
                    grp_id = row[0]
                    if grp_id in gruppen_r:
                        cursor.execute("insert into datei_recht(datei_id, gruppe_id, recht) values(%s,%s,'R')", (datei_id, grp_id))
                    if grp_id in gruppen_w:
                        cursor.execute("insert into datei_recht(datei_id, gruppe_id, recht) values(%s,%s,'W')", (datei_id, grp_id))
                for h in dhoehlenids:
                    cursor.execute("insert into hoehle_datei (hoehle_id,datei_id,kategorie,user) values (%s,%s,%s,%s)", (h, datei_id, dkategorie,userid))
            biodb.commit()
            filepath = mediadir + str(datei_id) + '_' + dcode + '_1'
            os.rename(tmppath, filepath)
            insdel_passiert = True
    if not cgiparam_num('HOEHLEID') and not cgiparam_num('BESUCHID') and not cgiparam_num('PUB_ID'):
        s['d_upl'] = 1
        insdel_moeglich = True
        # Daten für Befüllung der Maskenfelder
        s['firstid'] = dfirstid
        s['lastid'] = dlastid
        verkl_fillmaskdata()
        s['ret_hoehleid'] = cgiparam_num('RET_HOEHLEID')
        s['ret_besuchid'] = cgiparam_num('RET_BESUCHID')
        s['ret_fundi'] = cgiparam_num('RET_FUNDI')
        s['ret_pubid'] = cgiparam_num('RET_PUBID')
        if dbesuchid:
            s['fund_h'] = dhoehleid
            if dhoehleid:
                cursor.execute("select name, ifnull(concat(katgr,'/',ifnull(nr,'-')),'') from hoehle where id=%s", (dhoehleid,))
                dhoehlename,dhoehlekatnr = cursor.fetchone()
                s['fund_h_text'] = 'ID=' + str(dhoehleid)
                if dhoehlename:
                    s['fund_h_text'] += ', ' + dhoehlename
                if dhoehlekatnr:
                    s['fund_h_text'] = dhoehlekatnr + ' (' + s['fund_h_text'] + ')'
        elif dpub_id:
            s['pub_displtext'] = dpub_displtext
        else:
            get_hoehlen(False)
            cursor.execute("select g.id, g.name from gruppe g order by g.name, g.id")
            s['gruppen'] = [{'id': row[0], 'name': row[1]} for row in cursor.fetchall()]

# Dateidetails
if d_det_id and d_det_v:
    cursor.execute("select filename,mime,size,upload_d,uploader,lastuser,aend_komm from datei_version where datei_id=%s and v=%s", (d_det_id, d_det_v))
    row = cursor.fetchone()
    if not row:
        d_det_id = 0
if d_det_id and d_det_v:
    d_det_filename, d_det_mime, d_det_size, d_det_upload_d, d_det_uploader, d_det_lastuser, d_det_aendkomm = row
    cursor.execute("select code,beschreibung,owner,angelegt_d,lastuser,geaendert_d,quelle,geheim,lizenz from datei where id=%s", (d_det_id,))
    d_det_code, d_det_beschreibung, d_det_owner, d_det_angelegt_d, d_det_geaendert_von, d_det_geaendert_d, d_det_quelle, d_det_geheim, d_det_lizenz = cursor.fetchone()
    d_det_recht = recht_auf_datei(d_det_id, d_det_owner)
    d_det_ersetzrecht = d_det_recht==2 and d_det_uploader==userid or admin
    # die Performanceeinbuße durchs doppelte Lesen der Dateirechte ist vernachlässigbar
    d_det_gruppen_r = set()
    d_det_gruppen_w = set()
    cursor.execute("select gruppe_id,recht from datei_recht where datei_id=%s", (d_det_id,))
    for gruppe_id,recht in cursor.fetchall():
        if recht == 'W':
            d_det_gruppen_w.add(gruppe_id)
        else:
            d_det_gruppen_r.add(gruppe_id)
    get_hoehlen(False)
    d_det_hoehlen = {}
    # vielleicht besser nach Katnr sortieren?
    cursor.execute("select hoehle_id,kategorie from hoehle_datei where datei_id=%s order by hoehle_id", (d_det_id,))
    for row in cursor.fetchall():
        d_det_hoehlen[row[0]] = row[1]
    if cgiparam_num('IDEM') and idem_fresh() and d_det_recht == 2 and not insdel_passiert:
        d_newowner = cgiparam_num('D_DET_OWNER') if admin or userid==d_det_owner else d_det_owner
        d_newbeschreibung = cgiparam('D_DET_BESCHR') or None
        d_newquelle = cgiparam('D_DET_QUELLE') or None
        d_newgeheim = cgiparam('D_DET_GEHEIM') or None
        d_newlizenz = cgiparam('D_DET_LIZENZ') or None
        if d_newowner != d_det_owner or d_newbeschreibung != d_det_beschreibung or d_newquelle != d_det_quelle or d_newgeheim != d_det_geheim or d_newlizenz != d_det_lizenz:
            cursor.execute("update datei set owner=%s, lastuser=%s, geaendert_d=now(), beschreibung=%s, quelle=%s, geheim=%s, lizenz=%s where id=%s", (d_newowner, userid, d_newbeschreibung, d_newquelle, d_newgeheim, d_newlizenz, d_det_id))
            cursor.execute("select geaendert_d from datei where id=%s", (d_det_id,))
            d_det_geaendert_d = cursor.fetchone()[0]
            d_det_owner, d_det_geaendert_von, d_det_beschreibung, d_det_quelle, d_det_geheim, d_det_lizenz = d_newowner, userid, d_newbeschreibung, d_newquelle, d_newgeheim, d_newlizenz
        update = False
        d_newfilename = cgiparam('D_DET_FILENAME') or None
        d_newmime = cgiparam('D_DET_MIME') or None
        d_newaendkomm = cgiparam('D_DET_AENDKOMM') or None
        if 'D_DET_NEUDATEI' in cgifiles:
            d_newcontent = cgifiles['D_DET_NEUDATEI'][0]['content']
            if cgiparam_num('D_DET_FILENAME_AUTO'):
                d_newfilename = cgifiles['D_DET_NEUDATEI'][0]['filename']
            if cgiparam_num('D_DET_MIME_AUTO'):
                d_newmime = magic.from_buffer(d_newcontent, mime=True)
            tmppath, d_det_size = upl_createtmp(d_newcontent, d_newmime, mediadir + str(d_det_id) + '_' + d_det_code + '.tmp')
            if cgiparam('D_DET_VERSIONIEREN') or not d_det_ersetzrecht:
                d_det_v += 1
                cursor.execute("insert into datei_version (datei_id, v, uploader, lastuser, filename, mime, upload_d, aend_komm, size) values(%s,%s,%s,%s,%s,%s,now(),%s,%s) returning upload_d", (d_det_id, d_det_v, userid, userid, d_newfilename, d_newmime, d_newaendkomm, d_det_size))
                d_det_upload_d = cursor.fetchone()[0]
                d_det_filename = d_newfilename
                d_det_mime = d_newmime
                d_det_aendkomm = d_newaendkomm
                d_det_uploader = d_det_lastuser = userid
            else:
                update = True
            # wenn nicht versioniert wird, passiert mit folgendem os.rename() ein automatischer unlink auf die bestehende Datei mit selbem Versionssuffix
            os.rename(tmppath, mediadir + str(d_det_id) + '_' + d_det_code + '_' + str(d_det_v))
            insdel_passiert = True
        elif d_newfilename != d_det_filename or d_newmime != d_det_mime or d_newaendkomm != d_det_aendkomm:
            update = True
        if update:
            cursor.execute("update datei_version set lastuser=%s, filename=%s, mime=%s, size=%s, upload_d=now(), aend_komm=%s where datei_id=%s and v=%s", (userid, d_newfilename, d_newmime, d_det_size, d_newaendkomm, d_det_id, d_det_v))
            cursor.execute("select upload_d from datei_version where datei_id=%s and v=%s", (d_det_id,d_det_v))
            d_det_upload_d = cursor.fetchone()[0]
            d_det_lastuser = userid
            d_det_filename = d_newfilename
            d_det_mime = d_newmime
            d_det_aendkomm = d_newaendkomm
        if d_det_owner == userid or admin:
            d_newgruppen_r = {int(g) for g in cgiparams.setdefault('D_DET_GRUPPEN_R',[]) if g.isnumeric() and int(g)>0}
            d_newgruppen_w = {int(g) for g in cgiparams.setdefault('D_DET_GRUPPEN_W',[]) if g.isnumeric() and int(g)>0}
            for g in d_newgruppen_r:
                if g not in d_det_gruppen_r:
                    #debug('ins '+str(d_det_id) + '/' + str(g) + '/R')
                    cursor.execute("insert into datei_recht (datei_id, gruppe_id, recht) values(%s,%s,'R')", (d_det_id, g))
                    insdel_passiert = True
                    d_det_gruppen_r.add(g)
            for g in d_newgruppen_w:
                if g not in d_det_gruppen_w:
                    #debug('ins '+str(d_det_id) + '/' + str(g) + '/W')
                    cursor.execute("insert into datei_recht (datei_id, gruppe_id, recht) values(%s,%s,'W')", (d_det_id, g))
                    insdel_passiert = True
                    d_det_gruppen_w.add(g)
            for g in d_det_gruppen_r.copy():
                if g not in d_newgruppen_r:
                    #debug('del '+str(d_det_id) + '/' + str(g) + '/R')
                    cursor.execute("delete from datei_recht where datei_id=%s and gruppe_id=%s and recht='R'", (d_det_id, g))
                    insdel_passiert = True
                    d_det_gruppen_r.remove(g)
            for g in d_det_gruppen_w.copy():
                if g not in d_newgruppen_w:
                    #debug('del '+str(d_det_id) + '/' + str(g) + '/W')
                    cursor.execute("delete from datei_recht where datei_id=%s and gruppe_id=%s and recht='W'", (d_det_id, g))
                    insdel_passiert = True
                    d_det_gruppen_w.remove(g)
        d_newh = {}
        for p in cgiparams:
            if p.startswith('D_DET_HOEHLE_ID'):
                d_newh_id_str = p.removeprefix('D_DET_HOEHLE_ID')
                if d_newh_id_str.isnumeric():
                    if d_newkat := cgiparam('D_DET_KATEGORIE'+d_newh_id_str):
                        d_newh[int(d_newh_id_str)] = d_newkat
            elif p.startswith('D_DET_NEWH'):
                if d_newh_id := cgiparam_num(p):
                    if d_newkat := cgiparam('D_DET_NEWK'+p.removeprefix('D_DET_NEWH')):
                        d_newh[d_newh_id] = d_newkat
        for h in d_det_hoehlen.copy():
            if h not in d_newh:
                cursor.execute("delete from hoehle_datei where hoehle_id=%s and datei_id=%s", (h, d_det_id))
                insdel_passiert = True
                del d_det_hoehlen[h]
            elif d_det_hoehlen[h] != d_newh[h]:
                cursor.execute("update hoehle_datei set kategorie=%s, user=%s where hoehle_id=%s and datei_id=%s", (d_newh[h], userid, h, d_det_id))
                d_det_hoehlen[h] = d_newh[h]
                del d_newh[h]
            else:
                del d_newh[h]
        for h in d_newh:
            cursor.execute("insert into hoehle_datei (hoehle_id,datei_id,kategorie,user) values(%s,%s,%s,%s)", (h, d_det_id, d_newh[h], userid))
            insdel_passiert = True
            d_det_hoehlen[h] = d_newh[h]
            # die neu zugeordneten Höhlen werden bei der nächsten Anzeige hinten angereiht. Vielleicht wär es besser, sie gleich hinein zu sortieren, aber Geschmackssache
        biodb.commit()
    cursor.execute("select max(v) from datei_version where datei_id=%s", (d_det_id,))
    d_det_maxv = cursor.fetchone()[0]
    d_det_speichern = cgiparam('D_DET_SAVE')
    s['d_det_id'] = d_det_id
    s['d_det_v'] = d_det_v
    s['d_det_maxv'] = d_det_maxv
    s['d_det_code'] = d_det_code
    s['d_det_beschreibung'] = d_det_beschreibung or ''
    #s['d_det_owner'] = get_realname(d_det_owner)
    s['d_det_owner_id'] = d_det_owner
    s['d_det_angelegt_d'] = d_det_angelegt_d
    s['d_det_geaendert_von'] = get_realname(d_det_geaendert_von)
    s['d_det_geaendert_d'] = d_det_geaendert_d or ''
    s['d_det_quelle'] = d_det_quelle or ''
    s['d_det_geheim'] = d_det_geheim or ''
    s['d_det_lizenz'] = d_det_lizenz or ''
    s['d_det_filename'] = d_det_filename or ''
    s['d_det_mime'] = d_det_mime or ''
    s['d_det_size_str'] = size2str(d_det_size)
    s['d_det_size'] = d_det_size or ''
    s['d_det_upload_d'] = d_det_upload_d or ''
    s['d_det_uploader'] = get_realname(d_det_uploader)
    s['d_det_lastuser'] = get_realname(d_det_lastuser)
    s['d_det_aendkomm'] = d_det_aendkomm or ''
    s['d_det_hoehlen'] = d_det_hoehlen
    s['d_det_recht'] = d_det_recht
    s['d_det_ersetzrecht'] = d_det_ersetzrecht
    s['user_is_owner'] = d_det_owner == userid
    s['d_det_users'] = []
    cursor.execute("select id, concat_ws(' ', name1, name2) from user order by case when name_struktur in ('FG','GP','R') then concat_ws(' ',name1,name2) else concat_ws(' ',name2,name1) end, id")
    for row in cursor.fetchall():
        s['d_det_users'].append({'id': row[0], 'name': row[1] or ''})
    cursor.execute("select g.id, g.name from gruppe g order by g.name, g.id")
    s['gruppen'] = [{'id': row[0], 'name': row[1], 'r': row[0] in d_det_gruppen_r, 'w': row[0] in d_det_gruppen_w} for row in cursor.fetchall()]
    insdel_moeglich = d_det_recht==2
    if d_det_recht == 2:
        verkl_fillmaskdata()

# Publikationen
if cgiparam_num('DELPUB') and idem_fresh():
    pub_del_id = cgiparam_num('DELPUB')
    cursor.execute("select user from publikation where id=%s", (pub_del_id,))
    row = cursor.fetchone()
    if row:
        if row[0] != userid:
            cursor.close()
            biodb.close()
            http_error(403, "kein Löschrecht")
        cursor.execute("delete from publikation where id=%s", (pub_del_id,))
        biodb.commit()
        insdel_passiert = True

# Publikationsliste
if cgiparam('PLIST'):
    s['publiste'] = []
    # Suchformular: auswerten und wiederbefüllen
    s['psearch_id'] = cgiparam_num('PSEARCH_ID') or ''
    s['psearch_user'] = cgiparam('PSEARCH_USER')
    s['psearch_status'] = cgiparam('PSEARCH_STATUS')
    s['psearch_autor'] = cgiparam('PSEARCH_AUTOR')
    s['psearch_titel'] = cgiparam('PSEARCH_TITEL')
    s['psearch_zeitschrift'] = cgiparam('PSEARCH_ZEITSCHRIFT')
    s['psearch_herausgeber'] = cgiparam('PSEARCH_HERAUSGEBER')
    s['psearch_jahrvon'] = cgiparam_num('PSEARCH_JAHRVON') or ''
    s['psearch_jahrbis'] = cgiparam_num('PSEARCH_JAHRBIS') or ''
    bindparams = []
    where_parts = []
    if s['psearch_id']:
        where_parts.append("id=%s")
        bindparams.append(s['psearch_id'])
    if s['psearch_user']:
        where_parts.append("user in(select id from user where concat_ws(' ',name1,name2) like concat('%',%s,'%'))")
        bindparams.append(s['psearch_user'])
    if s['psearch_status']:
        where_parts.append("status=%s")
        bindparams.append(s['psearch_status'])
    if s['psearch_autor']:
        where_parts.append("autor like concat('%',%s,'%')")
        bindparams.append(s['psearch_autor'])
    if s['psearch_titel']:
        where_parts.append("titel like concat('%',%s,'%')")
        bindparams.append(s['psearch_titel'])
    if s['psearch_zeitschrift']:
        where_parts.append("zeitschrift like concat('%',%s,'%')")
        bindparams.append(s['psearch_zeitschrift'])
    if s['psearch_herausgeber']:
        where_parts.append("herausgeber like concat('%',%s,'%')")
        bindparams.append(s['psearch_herausgeber'])
    if s['psearch_jahrvon']:
        where_parts.append("jahr>=%s")
        bindparams.append(s['psearch_jahrvon'])
    if s['psearch_jahrbis']:
        where_parts.append("jahr<=%s")
        bindparams.append(s['psearch_jahrbis'])
    wherestr = " where " + ' and '.join(where_parts) if len(where_parts) else ""
    cursor.execute("select p.id,p.status,p.autor,p.titel,p.untertitel,p.zeitschrift,p.ausgabe,p.seiten,p.jahr,p.auflage,p.herausgeber,p.publik_d,p.link,(select count(*) from pub_datei pd where pd.pub_id=p.id) from publikation p" + wherestr + " order by p.id", bindparams)
    for row in cursor.fetchall():
        plist_id, plist_status, plist_autor, plist_titel, plist_untertitel, plist_zeitschrift,plist_ausgabe,plist_seiten,plist_jahr,plist_auflage,plist_herausgeber,plist_publik_d,plist_link,plist_anzdateien = row;
        if plist_status == 'P': plist_status = 'publiziert'
        elif plist_status == 'E': plist_status = 'Entwurf'
        elif plist_status == '1': plist_status = 'Erstpublikation'
        elif plist_status == 'F': plist_status = 'Fremdpublikation'
        else: plist_status = '?'
        s['publiste'].append({'id': plist_id, 'status': plist_status, 'autor': plist_autor or '', 'titel': plist_titel or '', 'untertitel': plist_untertitel or '', 'zeitschrift': plist_zeitschrift or '', 'ausgabe': plist_ausgabe or '', 'seiten': plist_seiten or '', 'jahr': plist_jahr or '', 'auflage': plist_auflage or '', 'herausgeber': plist_herausgeber or '', 'publik_d': plist_publik_d or '', 'link': plist_link or '', 'anzdateien': plist_anzdateien})

# Publikation-Details
elif cgiparam('PUB_ID'): # '0' für neu, sonst bestehende ID
    pub_id = cgiparam_num('PUB_ID')
    if cgiparam('PD_LIST_AEND') and cgiparam_num('IDEM') and idem_fresh(): # Zuordnungen ändern
        cursor.execute("select user from publikation where id=%s", (pub_id,))
        row = cursor.fetchone()
        if not row or row[0] != userid:
            cursor.close()
            biodb.close()
            http_error(403, "existiert nicht oder kein Schreibrecht")
        if 'PD_LIST_DEL' in cgiparams:
            for p in cgiparams['PD_LIST_DEL']:
                if p.isnumeric():
                    cursor.execute("delete from pub_datei where pub_id=%s and datei_id=%s", (pub_id, int(p)))
                    insdel_passiert = True
        newvals = {}
        for p in cgiparams:
            if p.startswith('PD_LIST_I_'):
                pd_d_id = p[10:]
                if pd_d_id.isnumeric():
                    newvals[pd_d_id] = {
                        'i': None if cgiparam(p)=='' else cgiparam_num(p),
                        'abschnitt': cgiparam('PD_LIST_ABSCHNITT_'+pd_d_id) or None,
                        'name': cgiparam('PD_LIST_NAME_'+pd_d_id) or None
                    }
        for pd_d_id in newvals.keys():
            cursor.execute("update pub_datei set i=%s, abschnitt=%s, name=%s where pub_id=%s and datei_id=%s", (newvals[pd_d_id]['i'], newvals[pd_d_id]['abschnitt'], newvals[pd_d_id]['name'], pub_id, pd_d_id))
        biodb.commit()
    if userid and 'PUB_STATUS' in cgiparams and not cgiparam('PD_LIST_AEND') and cgiparam_num('IDEM') and idem_fresh(): # Publikation speichern/ändern
        pub_status = cgiparam('PUB_STATUS') or None
        pub_publik_d = cgiparam('PUB_PUBLIK_D') or None
        pub_autor = cgiparam('PUB_AUTOR') or None
        pub_titel = cgiparam('PUB_TITEL') or None
        pub_untertitel = cgiparam('PUB_UNTERTITEL') or None
        pub_zeitschrift = cgiparam('PUB_ZEITSCHRIFT') or None
        pub_ausgabe = cgiparam('PUB_AUSGABE') or None
        pub_seiten = cgiparam('PUB_SEITEN') or None
        pub_jahr = cgiparam_num('PUB_JAHR') or None
        pub_auflage = cgiparam_num('PUB_AUFLAGE') or None
        pub_herausgeber = cgiparam('PUB_HERAUSGEBER') or None
        pub_kommverlag = cgiparam('PUB_KOMMVERLAG') or None
        pub_link = cgiparam('PUB_LINK') or None
        pub_anmerkung = cgiparam('PUB_ANMERKUNG') or None
        if pub_id:
            cursor.execute("select user, angelegt_d from publikation where id=%s", (pub_id,))
            row = cursor.fetchone()
            if not row or row[0] != userid:
                cursor.close()
                biodb.close()
                http_error(403, "existiert nicht oder kein Schreibrecht")
            pub_user, pub_angelegt_d = row
            cursor.execute("update publikation set status=%s, publik_d=%s, autor=%s, titel=%s, untertitel=%s, zeitschrift=%s, ausgabe=%s, seiten=%s, jahr=%s, auflage=%s, herausgeber=%s, kommverlag=%s, link=%s, anmerkung=%s where id=%s", (pub_status, pub_publik_d, pub_autor, pub_titel, pub_untertitel, pub_zeitschrift, pub_ausgabe, pub_seiten, pub_jahr, pub_auflage, pub_herausgeber, pub_kommverlag, pub_link, pub_anmerkung, pub_id))
            cursor.execute("delete from pub_hoehle where pub_id=%s", (pub_id,))
        else:
            pub_user = userid
            cursor.execute("insert into publikation(user, status, publik_d, autor, titel, untertitel, zeitschrift, ausgabe, seiten, jahr, auflage, herausgeber, kommverlag, link, anmerkung) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning id, angelegt_d", (pub_user, pub_status, pub_publik_d, pub_autor, pub_titel, pub_untertitel, pub_zeitschrift, pub_ausgabe, pub_seiten, pub_jahr, pub_auflage, pub_herausgeber, pub_kommverlag, pub_link, pub_anmerkung))
            pub_id, pub_angelegt_d = cursor.fetchone()
            insdel_passiert = True
        if 'PHOEHLE_ID' in cgiparams:
            for h_id in cgiparams['PHOEHLE_ID']:
                if h_id.isnumeric():
                    cursor.execute("insert into pub_hoehle(pub_id,hoehle_id) values(%s,%s)", (pub_id, h_id))
        biodb.commit()
    else:
        if pub_id:
            cursor.execute("select user, angelegt_d, status, publik_d, autor, titel, untertitel, zeitschrift, ausgabe, seiten, jahr, auflage, herausgeber, kommverlag, link, anmerkung from publikation where id=%s", (pub_id,))
            row = cursor.fetchone()
            if not row:
                cursor.close()
                biodb.close()
                http_error(410, "existiert nicht oder nicht mehr")
            pub_user, pub_angelegt_d, pub_status, pub_publik_d, pub_autor, pub_titel, pub_untertitel, pub_zeitschrift, pub_ausgabe, pub_seiten, pub_jahr, pub_auflage, pub_herausgeber, pub_kommverlag, pub_link, pub_anmerkung = row
        else:
            pub_angelegt_d = pub_status = pub_publik_d = pub_autor = pub_titel = pub_untertitel = pub_zeitschrift = pub_ausgabe = pub_seiten = pub_jahr = pub_auflage = pub_herausgeber = pub_kommverlag = pub_link = pub_anmerkung = None
            pub_user = userid
    s['pub_id'] = pub_id or 0
    s['pub_angelegt_d'] = pub_angelegt_d or ''
    s['pub_status'] = pub_status or ''
    s['pub_publik_d'] = pub_publik_d or ''
    s['pub_autor'] = pub_autor or ''
    s['pub_titel'] = pub_titel or ''
    s['pub_untertitel'] = pub_untertitel or ''
    s['pub_zeitschrift'] = pub_zeitschrift or ''
    s['pub_ausgabe'] = pub_ausgabe or ''
    s['pub_seiten'] = pub_seiten or ''
    s['pub_jahr'] = pub_jahr or ''
    s['pub_auflage'] = pub_auflage or ''
    s['pub_herausgeber'] = pub_herausgeber or ''
    s['pub_kommverlag'] = pub_kommverlag or ''
    s['pub_link'] = pub_link or ''
    s['pub_anmerkung'] = pub_anmerkung or ''
    s['pub_userid'] = pub_user
    s['pub_user_realname'] = get_realname(s['pub_userid'])
    s['pub_schreibrecht'] = bool(pub_user and (pub_user==userid or not pub_id))
    insdel_moeglich = s['pub_schreibrecht']
    s['pub_dateien'] = []
    s['pub_hoehlen'] = []
    s['pub_exphoehlen'] = set()
    if pub_id:
        cursor.execute("select pd.datei_id, pd.datei_v, pd.i, pd.abschnitt, pd.name, dv.filename, dv.mime, dv.size, (select max(dv1.v) from datei_version dv1 where dv.datei_id=dv1.datei_id), d.owner, d.code from pub_datei pd, datei d, datei_version dv where pd.pub_id=%s and pd.datei_id=d.id and pd.datei_id=dv.datei_id and pd.datei_v=dv.v order by pd.i, pd.name, dv.filename, pd.datei_id", (pub_id,))
        for row in cursor.fetchall():
            s['pub_dateien'].append({'datei_id': row[0], 'datei_v': row[1], 'i': row[2] or '', 'abschnitt': row[3] or '', 'name': row[4] or '', 'filename': row[5] or '', 'mime': row[6] or '', 'size': size2str(row[7]), 'maxv': row[8], 'recht': recht_auf_datei(row[0], row[9]), 'code': row[10]})
        cursor.execute("select id, katgr, nr, name from hoehle where id in(select hoehle_id from hoehle_datei where datei_id in(select datei_id from pub_datei where pub_id=%s) union all select hoehle_id from pub_hoehle where pub_id=%s)", (pub_id,pub_id))
        for row in cursor.fetchall():
            s['pub_hoehlen'].append({'id': row[0], 'katgr': row[1] or '', 'nr': row[2] or '', 'name': row[3] or ''})
        cursor.execute("select hoehle_id from pub_hoehle where pub_id=%s", (pub_id,))
        for row in cursor.fetchall():
            s['pub_exphoehlen'].add(row[0])
    get_hoehlen(False)

# Landkarte
s['koopick'] = cgiparam('KARTE') == 'P'
if cgiparam('KARTE'):
    s['wasanz'] = cgiparam('KARTE')
    s['karte'] = {'hoehlenorte': {}, 'besuchorte': {}, 'showlocate': os.environ.get('HTTPS')=='on'}
    if s['wasanz'][0] == 'K':
        if cgiparam('ID'):
            cursor.execute("select nr, farbe, ST_AsGeoJSON(grenze), ST_AsGeoJSON(centroid(grenze)) from katgr where nr=%s", (cgiparam('ID'),))
        else:
            cursor.execute("select nr, farbe, ST_AsGeoJSON(grenze), null from katgr order by nr")
        s['katgr'] = [[],[]]
        for row in cursor.fetchall():
            katgr_nr, katgr_farbe, katgr_flaeche, json = row
            s['katgr'][katgr_nr[0]=='K'].append({'nr': katgr_nr, 'farbe': katgr_farbe, 'flaeche': katgr_flaeche})
        if cgiparam('ID'): # wenn nur eine Gruppe, denn darauf zentrieren
            s['katgr_zentrum'] = json[json.find('['):json.find(']')+1] # nur die Koo mit Klammern
    if not cgiparam('HLISTE'): # sonst weiter unten befüllt
        get_hoehlen(True)
    if not cgiparam('BLISTE'): # sonst weiter unten befüllt
        cursor.execute("select id, user, date_format(besuch_d,'%Y-%m-%d'), ort_id, hoehle_id from besuch order by besuch_d")
        for k_besuchid, k_user, k_besuch_d, k_ort_id, k_hoehle_id in cursor.fetchall():
            if k_hoehle_id:
                cursor.execute("select koosystem, rw, hw, kooabw from ort where id in(select ort_id from eingang_ort where hoehle_id=%s and eingang='a') order by case user when %s then -2 when %s then -1 else kooabw end, id", (k_hoehle_id, userid, k_user))
            else:
                cursor.execute("select koosystem, rw, hw, kooabw from ort where id=%s", (k_ort_id,))
            if row := cursor.fetchone():
                k_koosystem, k_rw, k_hw, k_kooabw = row
                cursor.fetchall()
                k_user = get_realname(k_user)
                k_rw, k_hw = to_wgs84(k_koosystem, k_rw, k_hw)
                kookey = str(k_rw) + '|' + str(k_hw)
                s['karte']['besuchorte'].setdefault(kookey, {'rw_g': k_rw, 'hw_g': k_hw, 'kooabw': k_kooabw, 'besuche': []})['besuche'].append({'id': k_besuchid, 'user': k_user, 'besuch_d': k_besuch_d})
                if cgiparam_num('OPEN_ORT') == k_besuchid:
                    s['show_marker'] = 'B' + kookey;

# Länder, Bezirke, Gemeinden (für: Besuchliste und Besuch anzeigen)
s['laender'] = []
cursor.execute("select alpha2, name_de from land order by case name_de when 'Österreich' then 0 else 1 end, name_de, alpha2")
for row in cursor.fetchall():
    s['laender'].append({'code': row[0], 'name': row[1]})
s['bezirke'] = []
cursor.execute("select id, name, bl from bezirk order by name, id")
for row in cursor.fetchall():
    s['bezirke'].append({'id': row[0], 'name': row[1], 'bl': row[2]})
s['gemeinden'] = []
cursor.execute("select id, name, bezirk_id from gemeinde order by name, id")
for row in cursor.fetchall():
    s['gemeinden'].append({'id': row[0], 'name': row[1], 'bezirk_id': row[2]})

# zulässige Werte für Bereiche
bereiche = {'P': 'photisch',
            'M': 'mesophotisch',
            'A': 'aphotisch',
            'L': 'Lampenflora',
            'S': 'Eingangsschachtgrund'
           }
s['bereiche'] = bereiche
def get_bereich(p_key):
    if p_key is None or p_key not in bereiche:
        return ''
    return bereiche[p_key]

# Höhle löschen (vgl. Besuch löschen)
if cgiparam('DELHOEHLE'):
    if idem_fresh():
        hoehleid = cgiparam_num('HOEHLEID')
        cursor.execute("select user from hoehle where id=%s", (hoehleid,))
        row = cursor.fetchone()
        if row and (row[0] == userid or kataster):
            # fixme: sonstige Kinder-Datensätze
            # keinen verwaisten Ort-Datensatz zurücklassen
            cursor.execute("delete o from ort o where o.id in(select ort_id from eingang_ort eo where eo.hoehle_id=%s) and not exists(select 0 from eingang_ort eo where eo.ort_id=o.id and eo.hoehle_id!=%s) and not exists(select 0 from besuch b where b.ort_id=o.id)", (hoehleid, hoehleid))
            cursor.execute("delete from hoehle where id=%s", (hoehleid,))
            biodb.commit()
            insdel_passiert = True
    hoehleid = 0

# Besuch löschen:
# muss vor dem Laden der Liste passieren, weil der User danach zweckmäßigerweise zur Liste der verbliebenen Besuche kommen soll
if cgiparam('DELBESUCH'):
    if idem_fresh():
        besuchid = cgiparam_num('BESUCHID')
        cursor.execute("select user,ort_id from besuch where id=%s", (besuchid,))
        row = cursor.fetchone()
        if row and row[0] == userid:
            delbesuch_ortid = row[1]
            # fund-Datensätze löschen (enthalten für sich nicht viel Information), dranhängende Datensätze nicht
            cursor.execute("update anfrage set status='O' where status='N' and besuch_id=%s", (besuchid,))
            cursor.execute("delete from fund where besuch_id=%s", (besuchid,))
            cursor.execute("delete from besuch where id=%s", (besuchid,))
            if delbesuch_ortid:
                cursor.execute("delete from ort where id=%s and id not in (select ort_id from besuch) and id not in (select ort_id from eingang_ort)", (delbesuch_ortid,))
            biodb.commit()
            insdel_passiert = True
    besuchid = 0

# Suchprofile
def get_suchprofile(p_form):
    if userid:
        loaddefault = False
        opts_str = ""
        if cgiparam('LOADDEFAULT'):
            loaddefault = True
            opts_str = ",opts"
        cursor.execute("select name" + opts_str + " from suchopt where user=%s and formular=%s order by case name when 'Default' then '0' else '1' end, name", (userid, p_form))
        s['suchopts'] = []
        for row in cursor.fetchall():
            s['suchopts'].append(row[0])
            if row[0] == 'Default' and loaddefault:
                el_dict = json.loads(row[1])
                for el_name in el_dict:
                    cgiparams.setdefault(el_name,[]).append(el_dict[el_name])

def get_bl_abk(p_land, p_bl):
    if p_bl is None:
        return ''
    if p_land == 'AT':
        if   p_bl == 'Burgenland'       : return 'B'
        elif p_bl == 'Kärnten'          : return 'K'
        elif p_bl == 'Niederösterreich' : return 'NÖ'
        elif p_bl == 'Oberösterreich'   : return 'OÖ'
        elif p_bl == 'Salzburg'         : return 'S'
        elif p_bl == 'Steiermark'       : return 'St'
        elif p_bl == 'Tirol'            : return 'T'
        elif p_bl == 'Vorarlberg'       : return 'V'
        elif p_bl == 'Wien'             : return 'W'
    return p_bl
def get_bezirk_name(p_id):
    if p_id is None:
        return ''
    for bezirk in s['bezirke']:
        if bezirk['id'] == p_id:
            return bezirk['name']
def get_gemeinde_name(p_id):
    if p_id is None:
        return ''
    for gemeinde in s['gemeinden']:
        if gemeinde['id'] == p_id:
            return gemeinde['name']

erkundungsstaende = {
    'E': 'vollständig erkundet',
    'e': 'teilweise erkundet',
    'L': 'lokalisiert',
    'O': 'Ortsbesuch nötig+ausständig',
    'B': 'Lage nicht verif. mangels Zugangsberecht.',
    'V': 'verschollen',
    'Z': 'zerstört und nicht mehr lokalisierbar',
    'N': 'Existenz nicht verifizierbar',
    'F': 'Fantasiehöhle'
}
vermessungsstaende = {
    'N': 'kein Plan',
    'O': 'Plan(skizze) ohne Messdaten',
    'T': 'bekannte Teile tw. vermessen',
    'B': 'bekannte Teile vermessen',
    'V': 'vollständig vermessen'
}
wasseroptions = {
    'T': 'trocken',
    'G': 'Gerinne',
    'H': 'Halbsiphon',
    'E': 'Siphon(e) ephemerisch',
    'S': 'Siphone perennierend'
}
zustaende = {
    'I': 'intakt',
    'K': 'tw. zu Keller erweitert',
    'V': 'innen tw. eingestürzt/verfüllt',
    'e': 'K-Objekt tw. wegerodiert',
    'b': 'durch Bauarbeiten tw. zerstört',
    'E': 'durch Erosion verschwunden',
    'B': 'durch Bauarbeiten zerstört',
    'U': 'unbekannt oder nicht existent'
}
genesen = {
    'H'     : 'Höhle',
    'HN'    :   'Naturhöhle ≥ 5m',
    'HNP'   :     'primäre Höhle',
    'HNPT'  :       'Kalktuffhöhle',
    'HNPL'  :       'Lavatunnel',
    'HNS'   :     'sekundäre Höhle',
    'HNST'  :       'tektonisch/Korrosion',
#   'HNSL'  :       'Lösungshöhle',
#   'HNSLE' :         'epigen',
#   'HNSLEV':           'vados',
#   'HNSLEP':           'phreatisch',
#   'HNSLH' :         'hypogen',
#   'HNSLHG':           'geothermal',
#   'HNSLHS':           'Schwefels.',
    'HNSF'  :       'Frostverwitterung',
    'HNSW'  :       'Wollsackverw. (Granit)',
    'HNSÜ'  :       'Überdeck. (exkl. Wolls.)',
    'HNSU'  :       'Uferhöhle (Kehle+Folge)',
    'HNSB'  :       'biogen',
    'HNSBT' :         'Tunnelerosion',
    'HNSBD' :         'Dachsbau',
    'HK'    :   'künstliche Höhle',
    'HKW'   :     'Wohnhöhle',
    'HKL'   :     'Lagerhöhle',
    'HKH'   :     'Hauerlucke',
    'HKE'   :     'Erdstall',
    'HKEA'  :       'Typ A (Gänge)',
    'HKEB'  :       'Typ B (Schächte)',
    'HKEC'  :       'Typ C (Rundgänge)',
    'HKED'  :       'Typ D (horiz.+Kammern)',
   #'HKEF'  :       'frühgeschichtlich',
    'HKG'   :     'Getreidegrube',
    'HKA'   :     'Andachtshöhle',
    'S'     : 'Stollen',
    'SB'    :   'Bergwerk',
    'SBE'   :     'Erz',
    'SBK'   :     'Kohle',
    'SBG'   :     'Gips',
    'SBk'   :     'Kalk',
    'SBS'   :     'Sand/Schotter',
    'SBZ'   :     'Salz',
    'SBA'   :     'Asbest',
    'SBs'   :     'Serpentin',
    'SBH'   :     'Hornstein',
    'SBP'   :     'Sondierstollen',
    'SBg'   :     'Erde/Guano',
    'SBF'   :     'paläont. Fundstätte',
    'SL'    :   'Luftschutzstollen',
    'SF'    :   'Fabriksstollen',
    'Sl'    :   'Lagerstollen',
    'SlW'   :     'Weinkeller',
    'SlK'   :     'Kartoffel-/Rübenkeller',
    'SlH'   :     'Herrschaftskeller',
    'SlS'   :     'Sprengmittellager',
    'SlM'   :     'Sondermülllager',
    'SV'    :   'Verkehrstunnel',
    'SVE'   :     'Eisenbahntunnel',
    'SVS'   :     'Straßentunnel',
    'SVF'   :     'Fußgängertunnel',
    'ST'    :   'Materialtransportstollen',
    'Sw'    :   'Wasserstollen',
    'SwL'   :     'Wasserleitung',
    'SwQ'   :     'Quellstollen',
    'SwD'   :     'Druckstollen',
    'SwS'   :     'Schwemmstollen',
    'SwK'   :     'Kanal',
    'SwE'   :     'Entwässerungsstollen',
    'SM'    :   'Minierstollen',
    'Ü'     : 'künstl. überdeckte Anlage',
    'ÜG'    :   'Grotte',
    'ÜE'    :   'Eiskeller',
    'ÜK'    :   'Hauskeller/Krypta/Gruft',
    'ÜZ'    :   'Zisterne',
    'ÜW'    :   'Wasserleitung',
    'ÜP'    :   'Poterne',
    'B'     : 'Brunnen',
    'BZ'    :   'Zieh-/Pumpbrunnen',
    'BA'    :   'artesischer Brunnen',
    'K'     : 'kein höhlenartiges Objekt',
    'F'     : 'Fantasiehöhle'
}
def umschluess(p_code, p_dict):
    if p_code is None:
        return ''
    if p_code in p_dict:
        return p_dict[p_code]
    else:
        # err('ungültiger Code '+p_code)
        return None
def colinc():
    global xlsx_col
    xlsx_col += 1
    return xlsx_col - 1
def write_num(p_num):
    col = colinc()
    if p_num is not None:
        worksheet.write_number(xlsx_row, col, p_num)

# Höhlenliste
if cgiparam('HLISTE') or cgiparam('HGPX') or cgiparam('HXLSX') or cgiparam('DELHOEHLE'):
    get_suchprofile('HLIST_FRM')
    if not cgiparam('HLISTE') == '2':
        def bool2str(p_bool):
            if p_bool is None:
                return ''
            return 'ja' if p_bool else 'nein'
        zeile = cgiparam('HLIST_ZEILE') or 'H'
        if cgiparam('HGPX'):
            koohash = {}
            from xml.dom import minidom
            gpxdoc = minidom.Document()
            gpx = gpxdoc.createElement('gpx')
            gpx.setAttribute('xmlns','http://www.topografix.com/GPX/1/1')
            gpx.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance')
            gpx.setAttribute('xsi:schemaLocation','http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd')
            gpx.setAttribute('creator',CONF_GPX_CREATOR)
        elif cgiparam('HXLSX'):
            import xlsxwriter, io
            xlsx_output = io.BytesIO()
            workbook = xlsxwriter.Workbook(xlsx_output, {
                'in_memory': True,
                'default_date_format': 'yyyy-mm-dd hh:mm:ss'
            })
            workbook.set_properties({
                'title': 'Höhlenliste',
                'author': 'Funddatenbank',
                'category': 'Speläologie'
            })
            workbook_format_date = workbook.add_format({'num_format': 'yyyy-mm-dd', 'align': 'left'})
            workbook_format_datetime = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss', 'align': 'left'})
            worksheet = workbook.add_worksheet('Höhlen')
            workbook_format_ueberschr = workbook.add_format({
                'bold': True,
                'bg_color': '#CCEEFF',
                'align': 'center'
            })
            worksheet.set_row(0, None, workbook_format_ueberschr)
            ueberschriften = [
                'Höhle-ID',
                'Kat.gr.',
                'Nr.',
                'Name',
                'andere_namen',
                'Länge',
                'HU -',
                'HU +',
                'He',
                'Ve',
                'bes. geschützt',
                'Naturdenkmal',
                'Kulturdenkmal',
                'Schauhöhle',
                'Erkundung',
                'Vermessung',
                'Wasser',
                'Eis',
                'Zustand']
            if zeile != 'H':
                ueberschriften += [
                    'Eingang',
                    'Eingangsname'
                ]
            ueberschriften += [
                'Koo-System',
                'RW',
                'HW',
                'Toleranz',
                'Seehöhe',
                'Land',
                'Bundesland',
                'Bezirk',
                'Gemeinde'
            ]
            xlsx_col = 0
            for ueberschrift in ueberschriften:
                worksheet.write_string(0, xlsx_col, ueberschrift)
                xlsx_col += 1
            xlsx_row = 1
        sqlstr = "select h.id, h.katgr, h.nr, h.name, h.andere_namen, h.l, h.hu_minus, h.hu_plus, h.he, h.ve, h.besonders_geschuetzt, h.naturdenkmal, h.kulturdenkmal, h.schauhoehle, h.erkundungsstand, h.vermessungsstand, h.wasser, h.eis, h.zustand, not isnull(h.ersetzgrund), h.genese_anzeige, e.ref, e.name, o.id, o.koosystem, o.rw, o.hw, o.kooabw, o.sh, o.land, o.bl, o.bezirk_id, o.gemeinde_id from hoehle h left join eingang e on h.id=e.hoehle_id left join eingang_ort eo on e.hoehle_id=eo.hoehle_id and e.ref=eo.eingang left join (ort o join mv_ortscore s on o.id=s.ort_id and s.user_id=%s and s.score>0) on eo.ort_id=o.id"
        bindparams = [userid]
        where_parts = []
        if cgiparam_num('HLIST_ID'):
            where_parts.append('h.id=%s')
            bindparams.append(cgiparam_num('HLIST_ID'))
        if cgiparam('HLIST_KATGR'):
            katgr = cgiparam('HLIST_KATGR')
            bindparams.append(katgr)
            if cgiparam('HLIST_MITOHNEK'):
                where_parts.append("h.katgr in(%s, %s)")
                if katgr[0] == 'K':
                    bindparams.append(katgr[1:])
                else:
                    bindparams.append('K' + katgr)
            else:
                where_parts.append("h.katgr=%s")
        if cgiparam('HLIST_MITNR') == 'O':
            where_parts.append('h.nr is null')
        elif cgiparam('HLIST_MITNR') == 'M':
            if cgiparam('HLIST_NR'):
                where_parts.append('h.nr=%s')
                bindparams.append(cgiparam_num('HLIST_NR'))
            else:
                where_parts.append('h.nr is not null')
        if cgiparam('HLIST_NAME'):
            hname = cgiparam('HLIST_NAME')
            hname_retype = cgiparam('HLIST_RE')
            if hname_retype == 'S':
                where_parts.append("(h.name like concat('%',%s,'%') or h.andere_namen like concat('%',%s,'%'))")
                bindparams.append(hname)
            elif hname_retype == 'A':
                where_parts.append("(h.name like concat(%s,'%') or h.andere_namen like concat(%s,'%'))")
                bindparams.append(hname)
            elif hname_retype == 'E':
                where_parts.append("(h.name like concat('%',%s) or h.andere_namen like concat('%',%s))")
                bindparams.append(hname)
            elif hname_retype == 'R':
                # in andere_namen nach regex suchen ist schwierig (^ und $ sollten auf die einzelnen durch Beistriche getrennten Teile, jeweils ohne leading/trailing Leerzeichen, angewandt werden)
                where_parts.append("(h.name rlike %s or h.andere_namen rlike %s)")
                bindparams.append(hname)
            elif hname_retype == 'G':
                where_parts.append("(h.name like %s or h.andere_namen like %s)")
                hname = hname.replace('*','%').replace('?','_')
                bindparams.append(hname)
            else:
                where_parts.append("(h.name=%s or h.andere_namen rlike concat('(^|,)\\\\s*',%s,'\\\\s*(,|$)'))")
                bindparams.append(hname)
                for i in ('\\','(',')','[','$','^','{','}','?'):
                    hname = hname.replace(i, '\\'+i)
            bindparams.append(hname)
        if cgiparam('HLIST_EIGENE'):
            where_parts.append("h.user=%s")
            bindparams.append(userid)
        elif cgiparam('HLIST_USER'):
            where_parts.append("h.user in(select id from user where concat_ws(' ',name1,name2) like concat('%',%s,'%'))")
            bindparams.append(cgiparam('HLIST_USER'))
        if cgiparam('HLIST_BESG'):
            where_parts.append("h.besonders_geschuetzt=" + str(cgiparam_num('HLIST_BESG')))
        if cgiparam('HLIST_NATD'):
            if cgiparam('HLIST_NATD') == '3':
                where_parts.append("h.naturdenkmal>0")
            else:
                where_parts.append("h.naturdenkmal=" + str(cgiparam_num('HLIST_NATD')))
        if cgiparam('HLIST_KULD'):
            where_parts.append("h.kulturdenkmal=" + str(cgiparam_num('HLIST_KULD')))
        if cgiparam('HLIST_SCHAU'):
            where_parts.append("h.schauhoehle=" + str(cgiparam_num('HLIST_SCHAU')))
        if cgiparam('HLIST_GENESE'):
            where_parts.append("h.id in(select hg.hoehle_id from hoehle_genese hg where hg.genese like binary concat(%s,'%'))")
            bindparams.append(cgiparam('HLIST_GENESE'))
        if cgiparam('XLIST_LAND'):
            if cgiparam('XLIST_LAND') == 'L': # Suche nach leerem Land
                where_parts.append("o.land is null")
            elif cgiparam('XLIST_LAND') == 'AT' and cgiparam('XLIST_BL') == 'L':
                where_parts.append("o.land='AT' and o.bl is null")
            elif cgiparam('XLIST_BL'):
                bindparams.append(cgiparam('XLIST_LAND'))
                bindparams.append(cgiparam('XLIST_BL'))
                if cgiparam('XLIST_BEZIRK'):
                    if cgiparam('XLIST_BEZIRK') == 'L':
                        where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id is null")
                    else:
                        bindparams.append(cgiparam('XLIST_BEZIRK'))
                        if cgiparam('XLIST_GEMEINDE'):
                            if cgiparam('XLIST_GEMEINDE') == 'L':
                                where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id=%s and o.gemeinde_id is null")
                            else:
                                where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id=%s and o.gemeinde_id=%s")
                                bindparams.append(cgiparam('XLIST_GEMEINDE'))
                        else:
                            where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id=%s")
                else:
                    where_parts.append("o.land=%s and o.bl=%s")
            else:
                where_parts.append("o.land=%s")
                bindparams.append(cgiparam('XLIST_LAND'))
        if cgiparam('XLIST_SH') and cgiparam('XLIST_SHREL'):
            xlist_sh = cgiparam_num('XLIST_SH')
            xlist_shrel = cgiparam('XLIST_SHREL')
            where_parts.append("o.sh" + ('>=' if xlist_shrel == 'M' else '=' if xlist_shrel == 'G' else '<=') + "%s")
            bindparams.append(xlist_sh)
        if cgiparam('KARTE') or cgiparam('HGPX'):
            # in Karte sind ersetzte Höhlen nur hinderlich und in GPX braucht sie erst recht keiner
            where_parts.append("h.id not in(select id_alt from hoehle_altneu)")
        if where_parts:
            sqlstr += ' where ' + ' and '.join(where_parts)
        sort_parts = []
        if cgiparam('KARTE') or cgiparam('HGPX'):
            sort_parts.append("isnull(h.katgr),h.katgr,isnull(h.nr),h.nr,isnull(h.name),h.name")
        else:
            if cgiparam('SORT1'):
                for sortinp in ('SORT1', 'SORT2'):
                    sortinpval = cgiparam(sortinp)
                    if sortinpval == 'K':
                        if cgiparam(sortinp + '_DIR') == 'DESC':
                            sortstr = 'isnull(h.katgr) desc,h.katgr desc,isnull(h.nr) desc,h.nr desc'
                        else:
                            sortstr = "isnull(h.katgr),h.katgr,isnull(h.nr),h.nr"
                    elif sortinpval == 'N':
                        if cgiparam(sortinp + '_DIR') == 'DESC':
                            sortstr = 'isnull(h.name) desc,h.name desc'
                        else:
                            sortstr = 'isnull(h.name),h.name'
                    else:
                        if sortinpval == 'L': sortstr = 'h.l'
                        elif sortinpval == 'H': sortstr = '(h.hu_plus+hu_minus)'
                        elif sortinpval == 'S': sortstr = 'o.sh'
                        else: sortstr = 'h.id'
                        if cgiparam(sortinp + '_DIR') == 'DESC':
                            sortstr += ' desc'
                    if sortstr not in sort_parts:
                        sort_parts.append(sortstr)
            else:
                # Default
                sort_parts.append("isnull(h.katgr),h.katgr,isnull(h.nr),h.nr,h.name")
            if 'h.id' not in sort_parts:
                sort_parts.append('h.id')
            sort_parts.append('e.ref')
            sort_parts.append('o.id')
            sort_parts.append('s.score desc')
        sqlstr += ' order by ' + ','.join(sort_parts)
        #debug('zzh sqlstr='+sqlstr)
        s['hliste'] = []
        hlist_minrw = hlist_maxrw = hlist_minhw = hlist_maxhw = None
        ausgabe_anzahl = 0
        prev_id = prev_eingang = None
        #debug('sql:'+sqlstr)
        #debug('bindparams:'+str(bindparams))
        cursor.execute(sqlstr, bindparams)
        for row in cursor.fetchall():
            hlist_id, hlist_katgr, hlist_nr, hlist_name, hlist_andere_namen, hlist_l, hlist_hu_minus, hlist_hu_plus, hlist_he, hlist_ve, hlist_besonders_geschuetzt, hlist_naturdenkmal, hlist_kulturdenkmal, hlist_schauhoehle, hlist_erkundungsstand, hlist_vermessungsstand, hlist_wasser, hlist_eis, hlist_zustand, hlist_ersetzt, hlist_genese, hlist_eingang, hlist_eingang_name, hlist_oid, hlist_koosystem, hlist_rw, hlist_hw, hlist_kooabw, hlist_sh, hlist_land, hlist_bl, hlist_bezirk, hlist_gemeinde = row
            if hlist_ersetzt and (cgiparam('KARTE') or cgiparam('HGPX')):
                continue # ersetzte Höhlen nicht anzeigen, sondern nur den Ersatz
            hlist_w = cgiparam_num('XLIST_W')
            hlist_o = cgiparam_num('XLIST_O')
            hlist_s = cgiparam_num('XLIST_S')
            hlist_n = cgiparam_num('XLIST_N')
            if (hlist_w and hlist_o and hlist_s and hlist_s and hlist_n):
                if hlist_koosystem in epsg:
                    hlist_rw,hlist_hw = to_wgs84(hlist_koosystem,hlist_rw,hlist_hw)
                elif hlist_koosystem != 'G':
                    continue
                if not (hlist_w <= hlist_rw <= hlist_o and hlist_s <= hlist_hw <= hlist_n):
                    continue
            if hlist_id == prev_id and (zeile == 'H' or zeile == 'E' and hlist_eingang == prev_eingang):
                continue
            prev_id = hlist_id
            prev_eingang = hlist_eingang
            ausgabe_anzahl += 1
            if cgiparam('KARTE') or cgiparam('HGPX'):
                if hlist_koosystem is None or hlist_rw is None or hlist_hw is None:
                    continue
                hlist_rw, hlist_hw = to_wgs84(hlist_koosystem, hlist_rw, hlist_hw)
                if hlist_katgr or cgiparam('HGPX'):
                    hlist_katnr = (hlist_katgr or '-') + '/'
                    if hlist_nr:
                        hlist_katnr += str(hlist_nr)
                        if zeile != 'H' and hlist_eingang:
                            hlist_katnr += hlist_eingang
                    else:
                        hlist_katnr += '-'
                else:
                    hlist_katgr = ''
                kookey = str(hlist_rw) + '|' + str(hlist_hw)
                if cgiparam('KARTE'):
                    if hlist_eingang_name and hlist_eingang_name != hlist_name:
                        hlist_name = hlist_eingang_name + ' (' + hlist_name + ')'
                    hoe_dict = {'id': hlist_id, 'katnr': hlist_katnr, 'name': hlist_name, 'kooabw': '?' if hlist_kooabw is None else hlist_kooabw, 'sh': hlist_sh}
                    s['karte']['hoehlenorte'].setdefault(kookey, {'rw_g': hlist_rw, 'hw_g': hlist_hw, 'genese': hlist_genese, 'hoehlen': []})['hoehlen'].append(hoe_dict)
                    if cgiparam('OPEN_HOE') == str(hlist_id) + hlist_eingang:
                        s['show_marker'] = 'H' + kookey;
                    else:
                        if hlist_minrw is None or hlist_rw < hlist_minrw:
                            hlist_minrw = hlist_rw
                        if hlist_maxrw is None or hlist_rw > hlist_maxrw:
                            hlist_maxrw = hlist_rw
                        if hlist_minhw is None or hlist_hw < hlist_minhw:
                            hlist_minhw = hlist_hw
                        if hlist_maxhw is None or hlist_hw > hlist_maxhw:
                            hlist_maxhw = hlist_hw
                elif kookey in koohash:
                    pass # auch in GPX bringen deckungsgleiche Einträge nichts
                else:
                    koohash[kookey] = True
                    wpt = gpxdoc.createElement('wpt')
                    wpt.setAttribute('lon',str(hlist_rw))
                    wpt.setAttribute('lat',str(hlist_hw))
                    wptname = gpxdoc.createElement('name')
                    wptname.appendChild(gpxdoc.createTextNode(hlist_katnr + ('' if hlist_kooabw is None else ' F=' + str(hlist_kooabw))))
                    wpt.appendChild(wptname)
                    wptdesc = gpxdoc.createElement('cmt') # Garmin, sonst desc statt cmt
                    name_str = hlist_name if hlist_eingang_name is None else hlist_name + ' (' + hlist_eingang_name + ')'
                    hu_str = ' H='
                    if hlist_hu_minus is None and hlist_hu_plus is None:
                        hu_str += '?'
                    elif hlist_hu_minus is not None:
                        hu_str += '-' + float2str(hlist_hu_minus)
                        if hlist_hu_plus is not None:
                            hu_str += '/+' + float2str(hlist_hu_plus)
                    elif hlist_hu_plus is not None:
                            hu_str += '+' + float2str(hlist_hu_plus)
                    wptdesc.appendChild(gpxdoc.createTextNode(name_str + ' L=' + ('?' if hlist_l is None else float2str(hlist_l)) + hu_str + ' HE=' + ('?' if hlist_he is None else float2str(hlist_he))))
                    wpt.appendChild(wptdesc)
                    if hlist_sh is not None:
                        wptele = gpxdoc.createElement('ele')
                        wptele.appendChild(gpxdoc.createTextNode(str(hlist_sh)))
                        wpt.appendChild(wptele)
                    wptsym = gpxdoc.createElement('sym')
                    wptsym.appendChild(gpxdoc.createTextNode('Geocache'))
                    wpt.appendChild(wptsym)
                    gpx.appendChild(wpt)
            else:
                if hlist_name is None:
                    hlist_name = ''
                if hlist_andere_namen is None:
                    hlist_andere_namen = ''
                if hlist_katgr is None:
                    hlist_katgr = ''
                hlist_besonders_geschuetzt = bool2str(hlist_besonders_geschuetzt)
                if hlist_naturdenkmal is None:
                    hlist_naturdenkmal = ''
                elif hlist_naturdenkmal == 1:
                    hlist_naturdenkmal = 'ja'
                elif hlist_naturdenkmal == 2:
                    hlist_naturdenkmal = 'Gebiet'
                else:
                    hlist_naturdenkmal = 'nein'
                hlist_kulturdenkmal = bool2str(hlist_kulturdenkmal)
                hlist_schauhoehle = bool2str(hlist_schauhoehle)
                hlist_erkundungsstand = umschluess(hlist_erkundungsstand, erkundungsstaende)
                hlist_vermessungsstand = umschluess(hlist_vermessungsstand, vermessungsstaende)
                hlist_wasser = umschluess(hlist_wasser, wasseroptions)
                hlist_eis = bool2str(hlist_eis)
                hlist_zustand = umschluess(hlist_zustand, zustaende)
                if hlist_eingang is None:
                    hlist_eingang = ''
                if hlist_eingang_name is None:
                    hlist_eingang_name = ''
                if hlist_koosystem is None:
                    hlist_koosystem = ''
                if hlist_land is None:
                    hlist_land = ''
                hlist_bl = get_bl_abk(hlist_land, hlist_bl)
                hlist_bezirk = get_bezirk_name(hlist_bezirk)
                hlist_gemeinde = get_gemeinde_name(hlist_gemeinde)
                if cgiparam('HXLSX'):
                    def str2date(p_str):
                        import datetime
                        if not p_str:
                            return None
                        yyyy = int(p_str[0:4])
                        mm = int(p_str[5:7])
                        dd = int(p_str[8:10])
                        t = p_str[11:]
                        if t:
                            h = int(t[0:2])
                            m = int(t[3:5])
                            s = int(t[6:8])
                            return datetime.datetime(yyyy,mm,dd,h,m,s)
                        return datetime.date(yyyy,mm,dd)
                    def dateformat(p_str):
                        if p_str:
                            if p_str[11:]:
                                return workbook_format_datetime
                            return workbook_format_date
                        return None
                    xlsx_col = 0
                    write_num(hlist_id)
                    worksheet.write_string(xlsx_row, colinc(), hlist_katgr)
                    write_num(hlist_nr)
                    worksheet.write_string(xlsx_row, colinc(), hlist_name)
                    worksheet.write_string(xlsx_row, colinc(), hlist_andere_namen)
                    write_num(hlist_l)
                    write_num(hlist_hu_minus)
                    write_num(hlist_hu_plus)
                    write_num(hlist_he)
                    write_num(hlist_ve)
                    worksheet.write_string(xlsx_row, colinc(), hlist_besonders_geschuetzt)
                    worksheet.write_string(xlsx_row, colinc(), hlist_naturdenkmal)
                    worksheet.write_string(xlsx_row, colinc(), hlist_kulturdenkmal)
                    worksheet.write_string(xlsx_row, colinc(), hlist_schauhoehle)
                    worksheet.write_string(xlsx_row, colinc(), hlist_erkundungsstand)
                    worksheet.write_string(xlsx_row, colinc(), hlist_vermessungsstand)
                    worksheet.write_string(xlsx_row, colinc(), hlist_wasser)
                    worksheet.write_string(xlsx_row, colinc(), hlist_eis)
                    worksheet.write_string(xlsx_row, colinc(), hlist_zustand)
                    if zeile != 'H':
                        worksheet.write_string(xlsx_row, colinc(), hlist_eingang)
                        worksheet.write_string(xlsx_row, colinc(), hlist_eingang_name)
                    worksheet.write_string(xlsx_row, colinc(), hlist_koosystem)
                    write_num(hlist_rw)
                    write_num(hlist_hw)
                    write_num(hlist_kooabw)
                    write_num(hlist_sh)
                    worksheet.write_string(xlsx_row, colinc(), hlist_land)
                    worksheet.write_string(xlsx_row, colinc(), hlist_bl)
                    worksheet.write_string(xlsx_row, colinc(), hlist_bezirk)
                    worksheet.write_string(xlsx_row, colinc(), hlist_gemeinde)
                    xlsx_row += 1
                else:
                    s['hliste'].append({'id': hlist_id, 'katgr': hlist_katgr, 'nr': hlist_nr or '', 'name': hlist_name, 'andere_namen': hlist_andere_namen, 'ersetzt': hlist_ersetzt, 'l': float2str(hlist_l), 'hu_minus': float2str(hlist_hu_minus), 'hu_plus': float2str(hlist_hu_plus), 'he': float2str(hlist_he), 've': float2str(hlist_ve), 'besonders_geschuetzt': hlist_besonders_geschuetzt, 'naturdenkmal': hlist_naturdenkmal, 'kulturdenkmal': hlist_kulturdenkmal, 'schauhoehle': hlist_schauhoehle, 'erkundungsstand': hlist_erkundungsstand, 'vermessungsstand': hlist_vermessungsstand, 'wasser': hlist_wasser, 'eis': hlist_eis, 'zustand': hlist_zustand, 'eingang': hlist_eingang, 'eingang_name': hlist_eingang_name, 'o_id': hlist_oid or '', 'koosystem': hlist_koosystem, 'rw': float2str(hlist_rw), 'hw': float2str(hlist_hw), 'kooabw': float2str(hlist_kooabw),'sh': float2str(hlist_sh), 'land': hlist_land, 'bl': hlist_bl, 'bezirk': hlist_bezirk, 'gemeinde': hlist_gemeinde})
            if 0 < cgiparam_num('HLIST_MAXZEILEN') <= ausgabe_anzahl:
                break
        if cgiparam('HGPX'):
            print('Content-type: application/gpx+xml; charset=utf-8\r\n'
                + 'Pragma: no-cache\r\n'
                + 'Cache-Control: no-store\r\n'
                + '\r\n' + '<?xml version="1.0" encoding="UTF-8"?>' + gpx.toxml()
            )
            closedb_and_exit()
        elif cgiparam('HXLSX'):
            worksheet.autofit()
            workbook.close()
            xlsx_output.seek(0)
            sys.stdout.buffer.write(bytes(
                  'Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\r\n'
                + 'Pragma: no-cache\r\n'
                + 'Cache-Control: no-store\r\n'
                + '\r\n',
                'ascii'
            ))
            while buf := xlsx_output.read():
                sys.stdout.buffer.write(buf)
            closedb_and_exit()
    if cgiparam('KARTE'):
        if hlist_minrw is not None and hlist_maxrw is not None and hlist_minhw is not None and hlist_maxhw is not None:
            #s['hoemarkers_zentrum_rw'] = (hlist_minrw + hlist_maxrw) / 2
            #s['hoemarkers_zentrum_hw'] = (hlist_minhw + hlist_maxhw) / 2
            s['mapbounds'] = '[[' + str(hlist_minhw) + ',' + str(hlist_minrw) + '],[' + str(hlist_maxhw) + ',' + str(hlist_maxrw) + ']]'
    else:
        # Suchformular mit zuletzt eingegebenen Daten vorbefüllen
        s['hlist_frm'] = True
        s['hlist_id'] = cgiparam_num('HLIST_ID') or ''
        s['hlist_katgr'] = cgiparam('HLIST_KATGR')
        s['hlist_mitohnek'] = cgiparam('HLIST_MITOHNEK')
        s['hlist_mitnr'] = cgiparam('HLIST_MITNR')
        s['hlist_nr'] = cgiparam_num('HLIST_NR') or ''
        s['hlist_name'] = cgiparam('HLIST_NAME')
        s['hlist_re'] = cgiparam('HLIST_RE')
        s['hlist_ort_id'] = cgiparam('HLIST_ORT_ID')
        s['hlist_eigene'] = cgiparam('HLIST_EIGENE')
        s['hlist_user'] = cgiparam('HLIST_USER')
        s['hlist_besg'] = cgiparam('HLIST_BESG')
        s['hlist_natd'] = cgiparam('HLIST_NATD')
        s['hlist_kuld'] = cgiparam('HLIST_KULD')
        s['hlist_schau'] = cgiparam('HLIST_SCHAU')
        s['hlist_genese'] = cgiparam('HLIST_GENESE')
        s['xlist_land'] = cgiparam('XLIST_LAND')
        s['xlist_bl'] = cgiparam('XLIST_BL')
        s['xlist_bezirk'] = cgiparam('XLIST_BEZIRK') # nicht cgiparam_num, weil auch 'L' sein kann
        s['xlist_gemeinde'] = cgiparam('XLIST_GEMEINDE') # detto
        s['xlist_w'] = cgiparam('XLIST_W')
        s['xlist_o'] = cgiparam('XLIST_O')
        s['xlist_s'] = cgiparam('XLIST_S')
        s['xlist_n'] = cgiparam('XLIST_N')
        s['xlist_shrel'] = cgiparam('XLIST_SHREL')
        s['xlist_sh'] = cgiparam('XLIST_SH')
        s['hlist_maxzeilen'] = cgiparam('HLIST_MAXZEILEN')
        s['zeile'] = cgiparam('HLIST_ZEILE') or 'H'
        s['sort1'] = cgiparam('SORT1')
        s['sort1_dir'] = cgiparam('SORT1_DIR')
        s['sort2'] = cgiparam('SORT2')
        s['sort2_dir'] = cgiparam('SORT2_DIR')
        cursor.execute("select g.nr from katgr g where exists(select 0 from hoehle h where h.katgr=g.nr) order by g.nr")
        s['hlist_katgruppen'] = [row[0] for row in cursor.fetchall()]
        s['genesen'] = genesen

# Höhle speichern
hoehleid = 0
if userid and 'HNAME' in cgiparams:
    hoehleid = cgiparam_num('HOEHLEID')
    sqlpars = []
    sqlpars.append(cgiparam('KATGR_K') + cgiparam('KATGR_NUM') if cgiparam('KATGR_NUM') else None)
    sqlpars.append(cgiparam('NR') or None)
    sqlpars.append(cgiparam('HNAME') or None)
    sqlpars.append(cgiparam('ANDERE_NAMEN') or None)
    sqlpars.append(cgiparam_num('L') if cgiparam('L') else None)
    sqlpars.append(cgiparam_num('HU_MINUS') if cgiparam('HU_MINUS') else None)
    sqlpars.append(cgiparam_num('HU_PLUS') if cgiparam('HU_PLUS') else None)
    sqlpars.append(cgiparam_num('HE') if cgiparam('HE') else None)
    sqlpars.append(cgiparam_num('VE') if cgiparam('VE') else None)
    sqlpars.append(cgiparam_num('NATURDENKMAL'))
    sqlpars.append(cgiparam_num('KULTURDENKMAL'))
    sqlpars.append(cgiparam_num('BESONDERS_GESCHUETZT'))
    sqlpars.append(cgiparam_num('SCHAUHOEHLE'))
    sqlpars.append(cgiparam('WEBSITE') or None)
    sqlpars.append(cgiparam('ERKUNDUNGSSTAND') or None)
    sqlpars.append(cgiparam('VERMESSUNGSSTAND') or None)
    sqlpars.append(cgiparam('WASSER') or None)
    sqlpars.append(cgiparam_num('EIS') if cgiparam('EIS') else None)
    sqlpars.append(cgiparam('ZUSTAND') or None)
    ersetzgrund = cgiparam('ERSETZGRUND') or None
    sqlpars.append(ersetzgrund)
    sqlpars.append(userid)
    if hoehleid:
        sqlstr = "update hoehle set katgr=%s, nr=%s, name=%s, andere_namen=%s, l=%s, hu_minus=%s, hu_plus=%s, he=%s, ve=%s, naturdenkmal=%s, kulturdenkmal=%s, besonders_geschuetzt=%s, schauhoehle=%s, website=%s, erkundungsstand=%s, vermessungsstand=%s, wasser=%s, eis=%s, zustand=%s, ersetzgrund=%s, lastuser=%s where id=%s"
        sqlpars.append(hoehleid)
        if not kataster:
            sqlstr += " and user=%s"
            sqlpars.append(userid)
        cursor.execute(sqlstr, sqlpars)
        cursor.execute("delete from hoehle_genese where hoehle_id=%s", (hoehleid,))
        cursor.execute("delete from hoehle_altneu where id_alt=%s", (hoehleid,))
    elif idem_fresh():
        sqlpars.append(userid)
        cursor.execute("insert into hoehle (katgr, nr, name, andere_namen, l, hu_minus, hu_plus, he, ve, naturdenkmal, kulturdenkmal, besonders_geschuetzt, schauhoehle, website, erkundungsstand, vermessungsstand, wasser, eis, zustand, ersetzgrund, lastuser, user) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning id", sqlpars)
        hoehleid = cursor.fetchone()[0]
        if anm := cgiparam('HANM'):
            cursor.execute("insert into hoehle_anm(hoehle_id, user, anmerkung) values(%s,%s,%s)", (hoehleid, userid, anm))
        insdel_passiert = True
    for p in cgiparams:
        if p.startswith('GEN_GENESE'):
            gen_genese = cgiparam(p)
            gen_idx = p.removeprefix('GEN_GENESE')
            if gen_genese and gen_idx.isnumeric():
                gen_prio = cgiparam_num('GEN_PRIO'+gen_idx) or None
                gen_phase = cgiparam_num('GEN_PHASE'+gen_idx) or None
                gen_l = cgiparam('GEN_L'+gen_idx)
                gen_l = float(gen_l) if isfloat(gen_l) else None
                cursor.execute("insert into hoehle_genese (hoehle_id, genese, prio, phase, l) values(%s,%s,%s,%s,%s)", (hoehleid, gen_genese, gen_prio, gen_phase, gen_l))
    if ersetzgrund:
        for p in cgiparams:
            if p.startswith('NEUHOE_NR'):
                ers_hoe_idx = p[9:]
                neu_hoehle_nr = cgiparam_num(p)
                neu_hoehle_katgr = cgiparam('NEUHOE_KATGR'+ers_hoe_idx)
                if neu_hoehle_nr and neu_hoehle_katgr:
                    cursor.execute("select id from hoehle where katgr=%s and nr=%s", (neu_hoehle_katgr, neu_hoehle_nr))
                    if row := cursor.fetchone():
                        neu_hoehle_id = row[0]
                        alt_eingang = cgiparam('ALTHOE_REF'+ers_hoe_idx) or None
                        neu_eingang = cgiparam('NEUHOE_REF'+ers_hoe_idx) or None
                        cursor.execute("insert into hoehle_altneu (id_alt, id_neu, eingang_alt, eingang_neu) values(%s, %s, %s, %s)", (hoehleid, neu_hoehle_id, alt_eingang, neu_eingang))
    biodb.commit()

# Höhle anzeigen
if hoehleid or ('HOEHLEID' in cgiparams and not cgiparam('DELHOEHLE')):
    h = {'userid': 0,
         'lastuserid': 0,
         'created': '',
         'changed': '',
         'schreibrecht': False,
         'katgr_num': '',
         'nr': '',
         'name': '',
         'andere_namen': '',
         'l': '',
         'hu_minus': '',
         'hu_plus': '',
         'he': '',
         've': '',
         'naturdenkmal': 0,
         'kulturdenkmal': False,
         'besonders_geschuetzt': False,
         'schauhoehle': False,
         'website': '',
         'erkundungsstand': None,
         'vermessungsstand': 'N',
         'wasser': 'T',
         'eis': '0',
         'zustand': '',
         'gen': [],
         'ersetzgrund': '',
         'ersetzungen': []
        }
    if not hoehleid:
        hoehleid = cgiparam_num('HOEHLEID')
    if hoehleid:
        cursor.execute("select user, lastuser, katgr, nr, name, andere_namen, l, hu_minus, hu_plus, he, ve, naturdenkmal, kulturdenkmal, besonders_geschuetzt, schauhoehle, website, erkundungsstand, vermessungsstand, wasser, eis, zustand, ersetzgrund from hoehle where id=%s", (hoehleid,))
        row = cursor.fetchone()
        if row is None:
            err('Höhle mit id '+str(hoehleid)+' nicht gefunden');
            hoehleid = 0
    if hoehleid:
        h['userid'], h['lastuserid'], h['katgr_num'], h['nr'], h['name'], h['andere_namen'], h['l'], h['hu_minus'], h['hu_plus'], h['he'], h['ve'], h['naturdenkmal'], h['kulturdenkmal'], h['besonders_geschuetzt'], h['schauhoehle'], h['website'], h['erkundungsstand'], h['vermessungsstand'], h['wasser'], h['eis'], h['zustand'], h['ersetzgrund'] = row
        if h['katgr_num'] is None:
            h['katgr_num'] = ''
        if h['nr'] is None:
            h['nr'] = ''
        if h['name'] is None:
            h['name'] = ''
        if h['andere_namen'] is None:
            h['andere_namen'] = ''
        h['l'] = float2str(h['l'])
        h['hu_minus'] = float2str(h['hu_minus'])
        h['hu_plus'] = float2str(h['hu_plus'])
        h['he'] = float2str(h['he'])
        h['ve'] = float2str(h['ve'])
        if h['website'] is None:
            h['website'] = ''
        if h['erkundungsstand'] is None:
            h['erkundungsstand'] = ''
        if h['vermessungsstand'] is None:
            h['vermessungsstand'] = ''
        if h['wasser'] is None:
            h['wasser'] = ''
        if h['eis'] is None:
            h['eis'] = ''
        else:
            h['eis'] = str(h['eis']) # '0' oder '1'
        if h['zustand'] is None:
            h['zustand'] = ''
        if h['ersetzgrund'] is None:
            h['ersetzgrund'] = ''
        else:
            cursor.execute("select id_neu, eingang_alt, eingang_neu from hoehle_altneu where id_alt=%s order by eingang_alt", (hoehleid,))
            for row in cursor.fetchall():
                ers_neu_id, ers_alt_eingang, ers_neu_eingang = row
                cursor.execute("select katgr,nr from hoehle where id=%s", (ers_neu_id,))
                ers_neu_katgr, ers_neu_nr = cursor.fetchone()
                h['ersetzungen'].append({'id_neu': ers_neu_id, 'katgr_neu': ers_neu_katgr, 'nr_neu': ers_neu_nr, 'eingang_alt': ers_alt_eingang or '', 'eingang_neu': ers_neu_eingang or ''})
        cursor.execute("select id, katgr, nr from hoehle where id in(select id_alt from hoehle_altneu where id_neu=%s) order by katgr, nr, id", (hoehleid,))
        for row in cursor.fetchall():
            h.setdefault('ersetzt', []).append({'id': row[0], 'katgr': row[1] or '-', 'nr': row[2] or '-'})
        cursor.execute("select genese, prio, phase, l from hoehle_genese where hoehle_id=%s order by isnull(prio), prio, isnull(phase), phase, isnull(l), l", (hoehleid,))
        for row in cursor.fetchall():
            h['gen'].append({'genese': row[0], 'prio': row[1] or '', 'phase': row[2] or '', 'l': row[3] or ''})
        h['previd'] = h['nextid'] = h['prevnr'] = h['nextnr'] = None
        if h['nr']:
            if h['nr'] > 1:
                cursor.execute("select max(nr) from hoehle where katgr=%s and nr<%s", (h['katgr_num'], h['nr']))
                h['prevnr'] = cursor.fetchone()[0]
                if h['prevnr']:
                    cursor.execute("select id from hoehle where katgr=%s and nr=%s limit 1", (h['katgr_num'], h['prevnr']))
                    h['previd'] = cursor.fetchone()[0]
            cursor.execute("select min(nr) from hoehle where katgr=%s and nr>%s", (h['katgr_num'], h['nr']))
            h['nextnr'] = cursor.fetchone()[0]
            if h['nextnr']:
                    cursor.execute("select id from hoehle where katgr=%s and nr=%s limit 1", (h['katgr_num'], h['nextnr']))
                    h['nextid'] = cursor.fetchone()[0]
        cursor.execute("select count(*) from fund where besuch_id in(select id from besuch where hoehle_id=%s)", (hoehleid,))
        h['anzfunde'] = cursor.fetchone()[0]
        cursor.execute("select min(zeit) from hoehle_hist where hoehle_id=%s and spalte='ID' and alt is null", (hoehleid,))
        h['created'] = cursor.fetchone()[0] or ''
        cursor.execute("select max(zeit) from hoehle_hist where hoehle_id=%s", (hoehleid,))
        h['changed'] = cursor.fetchone()[0] or ''
        if h['userid'] == userid:
            h['schreibrecht'] = True
            h['user_realname'] = s['realname']
        else:
            h['user_realname'] = get_realname(h['userid'])
            h['schreibrecht'] = kataster
        if h['lastuserid'] == userid:
            h['lastuser_realname'] = s['realname']
        else:
            h['lastuser_realname'] = get_realname(h['lastuserid'])
    elif userid:
        h['schreibrecht'] = True
    cursor.execute("select nr, name from katgr order by nr")
    h['katgruppen'] = {nr: name for (nr, name) in cursor.fetchall()}
    h['wasseroptions'] = wasseroptions
    h['erkundungsstaende'] = erkundungsstaende
    h['vermessungsstaende'] = vermessungsstaende
    h['zustaende'] = zustaende
    h['genesen'] = genesen
    if hoehleid or h['schreibrecht']:
        h['id'] = hoehleid
        s['hoehle'] = h
        if h['schreibrecht']:
            insdel_moeglich = True

if hoehleid:
    s['hoehle']['users'] = []
    cursor.execute("select id, concat_ws(' ', name1, name2) from user order by case when name_struktur in ('FG','GP','R') then concat_ws(' ',name1,name2) else concat_ws(' ',name2,name1) end, id")
    for row in cursor.fetchall():
        s['hoehle']['users'].append({'id': row[0], 'name': row[1] or ''})

    # Anmerkung speichern/löschen
    anm_olduserid = cgiparam_num('ANMOLDUSER')
    if anm_olduserid and (anm_olduserid == userid or kataster):
        if cgiparam_num('DELANM'):
            cursor.execute("delete from hoehle_anm where hoehle_id=%s and user=%s", (hoehleid, anm_olduserid))
            biodb.commit()
        else:
            # ANMUSER ist disabled außer wenn kataster=True
            anm_newuserid = cgiparam_num('ANMUSER') if kataster else anm_olduserid
            cursor.execute("select count(*) from hoehle_anm where hoehle_id=%s and user=%s", (hoehleid, anm_newuserid))
            mit_neuem_user_vorhanden = cursor.fetchone()[0] > 0
            if mit_neuem_user_vorhanden:
                if anm_olduserid != anm_newuserid:
                    err('Änderung der Anmerkung auf User ' + str(anm_newuserid) + ' fehlgeschlagen, weil es von diesem User schon eine andere Anmerkung gibt')
                else:
                    cursor.execute("update hoehle_anm set anmerkung=%s where hoehle_id=%s and user=%s", (cgiparam('ANMTEXT'), hoehleid, anm_olduserid))
                    biodb.commit()
            else:
                insert = True
                if anm_olduserid != anm_newuserid:
                    cursor.execute("select count(*) from hoehle_anm where hoehle_id=%s and user=%s", (hoehleid, anm_olduserid))
                    if cursor.fetchone()[0] > 0:
                        insert = False
                        cursor.execute("update hoehle_anm set user=%s, anmerkung=%s where hoehle_id=%s and user=%s", (anm_newuserid, cgiparam('ANMTEXT'), hoehleid, anm_olduserid))
                if insert:
                    cursor.execute("insert into hoehle_anm(hoehle_id, user, anmerkung) values(%s,%s,%s)", (hoehleid, anm_newuserid, cgiparam('ANMTEXT')))
                biodb.commit()

    # Liste der Anmerkungen, mit Änderungs-/Anlageformular
    s['userid'] = userid
    s['anmerkungen'] = []
    eigene_vorhanden = False
    cursor.execute("select user, geaendert_d, anmerkung from hoehle_anm where hoehle_id=%s order by isnull(geaendert_d), geaendert_d, user", (hoehleid,))
    for row in cursor.fetchall():
        anm_userid, anm_geaendert_d, anm_text = row
        if anm_userid == userid:
            eigene_vorhanden = True
        anm_schreibrecht = anm_userid == userid or kataster
        s['anmerkungen'].append({
            'schreibrecht': anm_schreibrecht,
            'userid': anm_userid,
            'user_aenderbar': kataster,
            'geaendert_d': anm_geaendert_d or '',
            'text': anm_text or ''
        })

    if userid and not eigene_vorhanden:
        s['anmerkungen'].append({
            'schreibrecht': True,
            'userid': userid,
            'user_aenderbar': kataster,
            'geaendert_d': '',
            'text': ''
        })

    ewasseropts = {
        'N': 'nein',
        'U': 'Uferhöhle bei Mittelwasser',
        'H': 'Uferhöhle bei Hochwasser',
        'Q': 'Quelle',
        'q': 'ephem. Quelle',
        'P': 'Ponor',
        'E': 'Estavelle'
    }
    emorphs = {
        'H': 'Halbhöhle',
        'G': 'Gang/Schluf',
        'S': 'Schacht',
        'D': 'Doline',
        'U': 'unbekannt',
        'N': 'nicht höhlenartig'
    }
    ezustaende = {
        'O': 'offen',
        'T': 'Tür/Gitter offen',
        'Z': 'zugesperrt',
        'U': 'unbefahrbar',
        'V': 'verschüttet/verstürzt',
        't': 'mit Kalktuff zu',
        'S': 'zugeschlichtet',
        'Ü': 'überwindbare Mauer',
        'A': 'abgezäunt/vergittert',
        'M': 'zugemauert',
        'z': 'zerstört',
        '?': 'unbekannt'
    }

    # Eingang löschen oder speichern
    eingformular = eingang_soeben_angelegt = False
    if 'REF' in cgiparams:
        eingformular = True
    ref = cgiparam('REF')
    orig_ref = cgiparam('ORIG_REF')
    def get_next_free_ref(p_ref):
        # Eingang nicht doppelt anlegen, sondern nächstes freies Kürzel suchen.
        # Reihenfolge (gem. Konvention): a ,, z, aa .. az, ba .. bz usw.
        schon_alle_durch = False
        l_ref = p_ref
        while True:
            cursor.execute("select count(0) from eingang where hoehle_id=%s and ref=%s", (hoehleid, l_ref))
            if cursor.fetchone()[0] == 0:
                return l_ref
            if l_ref[-1] < 'z':
                l_ref = l_ref[:-2] + chr(ord(l_ref[-1])+1)
            elif len(l_ref) == 1:
                l_ref = 'aa'
            elif l_ref == 'zz':
                if schon_alle_durch:
                    err('a-zz alle schon vergeben')
                    return ''
                schon_alle_durch = True
                l_ref = 'a'
            else:
                l_ref = chr(ord(ref[0])+1) + 'a'
            if l_ref == p_ref:
                err('a-zz alle schon vergeben')
                return ''
    if h['schreibrecht']:
        # Eingang löschen
        if cgiparam_num('DELEING'):
            if idem_fresh():
                cursor.execute("delete from eingang where hoehle_id=%s and ref=%s", (hoehleid, orig_ref))
                biodb.commit()
                insdel_passiert = True
            ref = orig_ref = ''
            eingformular = False
        # Eingang speichern
        elif 'ORIG_REF' in cgiparams:
            ref = ref or 'a'
            ename = cgiparam('NAME') or None
            ewasser = cgiparam('WASSER') or None
            emorph = cgiparam('MORPH') or None
            ezustand = cgiparam('ZUSTAND') or None
            if orig_ref:
                if orig_ref != ref:
                    # Eingangskürzel geändert. Es dürfen keine 2 Eingänge das selbe Kürzel haben. Wenn das Kürzel auf eines geändert wird, das es schon gibt, dann tauschen.
                    # Die Kind-Datensätze eingang_ort werden per "on update cascade" automatisch mitgeändert.
                    cursor.execute("update eingang set ref='^°', lastuser=%s where hoehle_id=%s and ref=%s", (userid, hoehleid, ref))
                cursor.execute("update eingang set ref=%s, name=%s, wasser=%s, morph=%s, zustand=%s, lastuser=%s where hoehle_id=%s and ref=%s", (ref, ename, ewasser, emorph, ezustand, userid, hoehleid, orig_ref))
                if orig_ref != ref:
                    cursor.execute("update eingang set ref=%s where hoehle_id=%s and ref='^°'", (orig_ref, hoehleid))
                biodb.commit()
            elif idem_fresh():
                ref = get_next_free_ref(ref)
                if ref:
                    cursor.execute("insert into eingang (hoehle_id, ref, name, wasser, morph, zustand, lastuser) values(%s,%s,%s,%s,%s,%s,%s)", (hoehleid, ref, ename, ewasser, emorph, ezustand, userid))
                    biodb.commit()
                    insdel_passiert = True
                    eingang_soeben_angelegt = True

    # Eingang anzeigen:
    if eingformular:
        ename = emorph = ''
        ewasser = 'N' # Default für Neuanlage: kein Wasser
        ezustand = 'O' # Default für Neuanlage: offen
        if ref:
            cursor.execute("select name, wasser, morph, zustand from eingang where hoehle_id=%s and ref=%s", (hoehleid,ref))
            ename, ewasser, emorph, ezustand = cursor.fetchone()
        s['eingang'] = {
            'ref': ref,
            'newref': ref or get_next_free_ref('a'),
            'name': ename or '',
            'wasser': ewasser or '',
            'morph': emorph or '',
            'zustand': ezustand or '',
            'ewasseropts': ewasseropts,
            'emorphs': emorphs,
            'ezustaende': ezustaende
        }
        if h['schreibrecht']:
            insdel_moeglich = True;

        # Ortsangaben
        if ref:
            ortformular = 'ORT_ID' in cgiparams or eingang_soeben_angelegt
            def get_ort_schreibrecht(p_ort_id):
                if kataster:
                    return True
                if userid and not p_ort_id: # Neuanlage
                    return True
                cursor.execute("select user from ort where id=%s", (p_ort_id,))
                if row := cursor.fetchone():
                    if row[0] == userid:
                        return True
                    cursor.execute("select gruppe_id from ort_recht where ort_id=%s and recht='W'", (p_ort_id,))
                    for row in cursor.fetchall():
                        if row[0] in ingruppen:
                            return True
                return False
            ort_id = cgiparam_num('ORT_ID')
            ort_schreibrecht = get_ort_schreibrecht(ort_id)

            # Ort löschen
            if ort_schreibrecht and cgiparam_num('DELORT'):
                if cgiparam_num('DELORT'):
                    if idem_fresh():
                        cursor.execute("delete from eingang_ort where ort_id=%s", (ort_id,))
                        cursor.execute("delete from ort_recht where ort_id=%s", (ort_id,))
                        cursor.execute("delete from ort where id=%s", (ort_id,))
                        biodb.commit()
                        insdel_passiert = True
                    ort_id = 0
                    ortformular = False
            # Ort speichern
            elif ort_schreibrecht and 'ORT_SH' in cgiparams:
                ortuser = cgiparam_num('ORT_USER') or None
                koosystem = cgiparam('ORT_KOOSYST') or None
                rw = cgiparam_num('ORT_RW') if cgiparam('ORT_RW') else None
                hw = cgiparam_num('ORT_HW') if cgiparam('ORT_HW') else None
                # fixme: kooabw, sh und sh_tol prüfen wie beim Speichern von BESUCH
                kooabw = cgiparam_num('ORT_KOOABW') if cgiparam('ORT_KOOABW') else None
                sh = cgiparam_num('ORT_SH') if cgiparam('ORT_SH') else None
                sh_tol = cgiparam_num('ORT_SHTOL') if cgiparam('ORT_SHTOL') else None
                land = cgiparam('ORT_LAND') or None
                bl = cgiparam('ORT_BL') or None
                bezirk_id = cgiparam_num('ORT_BEZIRK') or None
                gemeinde_id = cgiparam_num('ORT_GEMEINDE') or None
                beschreibung = cgiparam('ORT_BESCHREIBUNG') or None
                zugang = cgiparam('ORT_ZUGANG') or None
                quelle = cgiparam('ORT_QUELLE') or None
                geheim = cgiparam('ORT_GEHEIM') or None
                lizenz = cgiparam('ORT_LIZENZ') or None
                gruppen_r = [int(g) for g in cgiparams.setdefault('ORT_GRUPPEN_R',[]) if g.isnumeric() and int(g)>0]
                gruppen_w = [int(g) for g in cgiparams.setdefault('ORT_GRUPPEN_W',[]) if g.isnumeric() and int(g)>0]
                bindparams = [koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang, quelle, geheim, lizenz]
                if ort_id:
                    sqlstr = "update ort set geaendert_d=current_timestamp(), koosystem=%s, rw=%s, hw=%s, kooabw=%s, sh=%s, sh_tol=%s, land=%s, bl=%s, bezirk_id=%s, gemeinde_id=%s, beschreibung=%s, zugang=%s, quelle=%s, geheim=%s, lizenz=%s"
                    if kataster:
                        sqlstr += ", user=%s"
                        bindparams += [ortuser]
                    sqlstr += " where id=%s"
                    cursor.execute(sqlstr, bindparams + [ort_id])
                    cursor.execute("select id from gruppe")
                    for row in cursor.fetchall():
                        grp_id = row[0]
                        if grp_id in gruppen_r:
                            cursor.execute("select count(*) from ort_recht where ort_id=%s and gruppe_id=%s and recht='R'", (ort_id, grp_id))
                            anz = cursor.fetchone()[0]
                            if not anz:
                                cursor.execute("insert into ort_recht(ort_id, gruppe_id, recht) values(%s,%s,'R')", (ort_id, grp_id))
                        else:
                            cursor.execute("delete from ort_recht where ort_id=%s and gruppe_id=%s and recht='R'", (ort_id, grp_id))
                        if grp_id in gruppen_w:
                            cursor.execute("select count(*) from ort_recht where ort_id=%s and gruppe_id=%s and recht='W'", (ort_id, grp_id))
                            anz = cursor.fetchone()[0]
                            if not anz:
                                cursor.execute("insert into ort_recht(ort_id, gruppe_id, recht) values(%s,%s,'W')", (ort_id, grp_id))
                        else:
                            cursor.execute("delete from ort_recht where ort_id=%s and gruppe_id=%s and recht='W'", (ort_id, grp_id))
                    biodb.commit()
                elif idem_fresh():
                    if not kataster:
                        ortuser = userid # sonst könnte jeder User unbegrenzt viele Ortsangaben anlegen
                    cursor.execute("insert into ort(user, koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang, quelle, geheim, lizenz) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning id", [ortuser] + bindparams)
                    ort_id = cursor.fetchone()[0]
                    cursor.execute("insert into eingang_ort(hoehle_id, eingang, ort_id) values(%s,%s,%s)", (hoehleid, ref, ort_id))
                    cursor.execute("select id from gruppe")
                    for row in cursor.fetchall():
                        grp_id = row[0]
                        if grp_id in gruppen_r:
                            cursor.execute("insert into ort_recht(ort_id, gruppe_id, recht) values(%s,%s,'R')", (ort_id, grp_id))
                        if grp_id in gruppen_w:
                            cursor.execute("insert into ort_recht(ort_id, gruppe_id, recht) values(%s,%s,'W')", (ort_id, grp_id))
                    biodb.commit()
                    insdel_passiert = True

            # Liste der Eingang-Ortsangaben
            eorte = []
            eigene_ortsangabe_vorhanden = False
            sqlstr = "select o.id, o.user, o.angelegt_d, o.geaendert_d, o.koosystem, o.rw, o.hw, o.kooabw, o.sh, o.sh_tol, o.land, o.bl, o.bezirk_id, o.gemeinde_id from ort o, mv_ortscore s where o.id in (select eo.ort_id from eingang_ort eo where eo.hoehle_id=%s and eo.eingang=%s) and o.id=s.ort_id and s.user_id=%s and s.score>0 order by s.score, o.angelegt_d, o.geaendert_d"
            cursor.execute(sqlstr, (hoehleid, ref, userid))
            for row in cursor.fetchall():
                eorte_id, eorte_user, eorte_angelegt_d, eorte_geaendert_d, eorte_koosystem, eorte_rw, eorte_hw, eorte_kooabw, eorte_sh, eorte_sh_tol, eorte_land, eorte_bl, eorte_bezirk_id, eorte_gemeinde_id = row
                eorte.append({
                    'id': eorte_id,
                    'user': get_realname(eorte_user),
                    'angelegt_d': eorte_angelegt_d or '',
                    'geaendert_d': eorte_geaendert_d or '',
                    'koosystem': eorte_koosystem or '',
                    'rw': float2str(eorte_rw),
                    'hw': float2str(eorte_hw),
                    'kooabw': '' if eorte_kooabw is None else eorte_kooabw,
                    'sh': '' if eorte_sh is None else eorte_sh,
                    'sh_tol': '' if eorte_sh_tol is None else eorte_sh_tol,
                    'land': eorte_land or '',
                    'bl': eorte_bl or '',
                    'bezirk': get_bezirk_name(eorte_bezirk_id),
                    'gemeinde': get_gemeinde_name(eorte_gemeinde_id)
                })
                if eorte_user == userid:
                    eigene_ortsangabe_vorhanden = True
            s['eorte'] = eorte
            # Wenn es nur eine einzige Ortsangabe gibt, diese gleich anzeigen, damit man nicht extra draufklicken und nochmal auf die Serverantwort warten muss.
            # Allerdings wird die Seite dann durch die vielen Angaben unübersichtlich, und in der Ortsangaben-Liste stehen eh schon die wichtigsten Details.
            if len(eorte) == 1 and not ortformular:
                ortformular = True
                ort_id = eorte[0]['id']
                ort_schreibrecht = get_ort_schreibrecht(ort_id)

            # Ort anzeigen:
            if ortformular:
                # Defaults für Neuanlage:
                ortuser = userid
                koosystem = rw = hw = kooabw = sh = sh_tol = land = bl = bezirk_id = gemeinde_id = beschreibung = zugang = geheim = ''
                quelle = useropts.get('OrtDfltAutor') or ''
                lizenz = useropts.get('OrtDfltLizenz') or ''
                if ort_id:
                    cursor.execute("select user, koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang, quelle, geheim, lizenz from ort where id=%s", (ort_id,))
                    if row := cursor.fetchone():
                        ortuser, koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang, quelle, geheim, lizenz = row
                cursor.execute("select g.id, g.name, exists(select 0 from ort_recht rr where rr.ort_id=%s and g.id=rr.gruppe_id and rr.recht='R'), exists(select 0 from ort_recht rw where rw.ort_id=%s and g.id=rw.gruppe_id and rw.recht='W') from gruppe g order by g.name, g.id", (ort_id, ort_id))
                ortgruppen = [{'id': row[0], 'name': row[1], 'r': row[2], 'w': row[3]} for row in cursor.fetchall()]
                s['eort'] = {
                    'id': ort_id,
                    'schreibrecht': ort_schreibrecht,
                    'kataster': kataster,
                    'user': ortuser,
                    'gruppen': ortgruppen,
                    'eigene_ortsangabe_vorhanden': eigene_ortsangabe_vorhanden,
                    'koosystem': koosystem or '',
                    'rw': float2str(rw),
                    'hw': float2str(hw),
                    'kooabw': '' if kooabw is None else kooabw,
                    'sh': '' if sh is None else sh,
                    'sh_tol': '' if sh_tol is None else sh_tol,
                    'land': land or '',
                    'bl': bl or '',
                    'bezirk_id': bezirk_id,
                    'gemeinde_id': gemeinde_id,
                    'beschreibung': beschreibung or '',
                    'quelle': quelle or '',
                    'zugang': zugang or '',
                    'geheim': geheim or '',
                    'lizenz': lizenz or ''
                }
                if ort_schreibrecht:
                    insdel_moeglich = True;
    s['eingang_soeben_angelegt'] = eingang_soeben_angelegt

    # Liste der Eingänge
    eingangliste = []
    #sqlstr = "select e.ref, e.name, e.wasser, e.morph, e.zustand, o.koosystem, o.rw, o.hw, o.kooabw, o.sh, o.land, o.bl, o.bezirk_id, o.gemeinde_id from eingang e left join eingang_ort eo on e.hoehle_id=eo.hoehle_id and e.ref=eo.eingang left join ort o on eo.ort_id=o.id where e.hoehle_id=%s and o.user=%s or not exists(select 0 from ort_recht o_r where o_r.ort_id=o.id)"
    #if ingruppen:
    #    sqlstr += " or exists(select 0 from ort_recht o_r where o_r.ort_id=o.id and o_r.gruppe_id in (" + ','.join(str(g) for g in ingruppen) + "))"
    #sqlstr += "order by e.ref, o.user!=%s, o.user!=%s, isnull(o.kooabw), o.kooabw, o.id"
    #cursor.execute(sqlstr, (hoehleid, userid, userid, h['userid']))
    sqlstr = "select e.ref, e.name, e.wasser, e.morph, e.zustand, o.koosystem, o.rw, o.hw, o.kooabw, o.sh, o.land, o.bl, o.bezirk_id, o.gemeinde_id from eingang e left join eingang_ort eo on e.hoehle_id=eo.hoehle_id and e.ref=eo.eingang left join (ort o join mv_ortscore s on o.id=s.ort_id and s.user_id=%s and s.score>0) on eo.ort_id=o.id where e.hoehle_id=%s order by e.ref, s.score desc"
    cursor.execute(sqlstr, (userid, hoehleid))
    prev_ref = None
    for row in cursor.fetchall():
        eingliste_ref, eingliste_name, eingliste_wasser, eingliste_morph, eingliste_zustand, eingliste_koosystem, eingliste_rw, eingliste_hw, eingliste_kooabw, eingliste_sh, eingliste_land, eingliste_bl, eingliste_bezirk, eingliste_gemeinde = row
        if eingliste_ref != prev_ref:
            prev_ref = eingliste_ref
            eingangliste.append({
              'ref': eingliste_ref or '',
              'name': eingliste_name or '',
              'wasser': umschluess(eingliste_wasser, ewasseropts),
              'morph': umschluess(eingliste_morph, emorphs),
              'zustand': umschluess(eingliste_zustand, ezustaende),
              'koosystem': eingliste_koosystem or '',
              'rw': float2str(eingliste_rw),
              'hw': float2str(eingliste_hw),
              'kooabw': '' if eingliste_kooabw is None else eingliste_kooabw,
              'sh': '' if eingliste_sh is None else eingliste_sh,
              'land': eingliste_land or '',
              'bl': eingliste_bl or '',
              'bezirk': get_bezirk_name(eingliste_bezirk),
              'gemeinde': get_gemeinde_name(eingliste_gemeinde)
            })
    s['eingaenge'] = eingangliste
    # Dateien listen/anzeigen
    s['hdateien'] = {'M':[], 'P':[], 'B':[]}
    cursor.execute("select d.id,d.code,d.beschreibung,d.owner,dv.v,dv.filename,dv.mime,dv.size,hd.kategorie from datei d, datei_version dv, hoehle_datei hd where d.id=dv.datei_id and d.id=hd.datei_id and hd.hoehle_id=%s and not exists(select 0 from datei_version dv1 where dv1.datei_id=dv.datei_id and dv1.v>dv.v) order by dv.filename,d.id", (hoehleid,))
    for row in cursor.fetchall():
        hd_id, hd_code, hd_beschreibung, hd_owner, hd_v, hd_filename, hd_mime, hd_size, hd_kategorie = row
        s['hdateien'][hd_kategorie].append({'id':hd_id, 'v':hd_v, 'code':hd_code, 'beschreibung':hd_beschreibung or '', 'filename':hd_filename or '', 'mime':hd_mime or '', 'size':size2str(hd_size), 'recht': recht_auf_datei(hd_id,hd_owner)})
    s['hpub'] = []
    cursor.execute("select p.id,p.status,p.autor,p.titel,p.jahr from publikation p where id in(select pub_id from pub_datei where datei_id in(select datei_id from hoehle_datei where hoehle_id=%s) union all select pub_id from pub_hoehle where hoehle_id=%s)", (hoehleid,hoehleid))
    for row in cursor.fetchall():
        s['hpub'].append({'id': row[0], 'status': 'publiziert' if row[1]=='P' else 'Entwurf' if row[1]=='E' else 'Erstpublikatin' if row[1]=='1' else 'Fremdpublikation' if row[1]=='F' else '?', 'autor': row[2] or '', 'titel': row[3] or '', 'jahr': row[4] or ''})

# Besuchliste
if cgiparam('BLISTE') or cgiparam('GPX') or cgiparam('XLSX') or cgiparam('DELBESUCH'):
    get_hoehlen(True)
    get_suchprofile('BLIST_FRM')
    zeile = cgiparam('BLIST_ZEILE') or 'B'
    if cgiparam('KARTE') or cgiparam('GPX'):
        zeile = 'O' # weil deckungsgleiche Punkte nicht dargestellt werden können
    if cgiparam('GPX'):
        koohash = {}
        from xml.dom import minidom
        gpxdoc = minidom.Document()
        gpx = gpxdoc.createElement('gpx')
        gpx.setAttribute('xmlns','http://www.topografix.com/GPX/1/1')
        gpx.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance')
        gpx.setAttribute('xsi:schemaLocation','http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd')
        gpx.setAttribute('creator',CONF_GPX_CREATOR)
    elif cgiparam('XLSX'):
        import xlsxwriter, io
        xlsx_output = io.BytesIO()
        workbook = xlsxwriter.Workbook(xlsx_output, {
            'in_memory': True,
            'default_date_format': 'yyyy-mm-dd hh:mm:ss'
        })
        workbook.set_properties({
            'title': 'Fundliste',
            'author': 'Funddatenbank',
            'category': 'Biologie'
        })
        workbook_format_date = workbook.add_format({'num_format': 'yyyy-mm-dd', 'align': 'left'})
        workbook_format_datetime = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss', 'align': 'left'})
        worksheet = workbook.add_worksheet('Funde')
        workbook_format_ueberschr = workbook.add_format({
            'bold': True,
            'bg_color': '#CCEEFF',
            'align': 'center'
        })
        worksheet.set_row(0, None, workbook_format_ueberschr)
        ueberschriften = []
        if zeile == 'O':
            ueberschriften.append('Ort-ID')
        else:
            ueberschriften.extend(['Besuch-ID', 'angelegt von', 'Besuch-Datum'])
            if zeile != 'Bg':
                ueberschriften.append('angelegt am')
                ueberschriften.append('geändert am')
        ueberschriften.extend(['Höhle', 'Koo-System', 'RW', 'HW', 'Fehler', 'Seehöhe', 'Land', 'Bundesland', 'Bezirk', 'Gemeinde', 'Ortsbeschreibung'])
        if zeile == 'O':
            ueberschriften.append('Besuche')
        else:
            ueberschriften.append('Kommentar')
            if zeile == 'B':
                ueberschriften.append('Funde')
            elif zeile == 'F':
                ueberschriften.append('Beschreibung')
                ueberschriften.append('Bereich')
        if zeile in ('F','Bg'):
            ueberschriften.append('Anzahl Indiv.')
            ueberschriften.append('Art')
        else:
            ueberschriften.append('Arten')
        if zeile == 'Bg':
            ueberschriften.append('Bestimmer')
            ueberschriften.append('Best.komm.')
        else:
            ueberschriften.append('Art unbestimmt')
        xlsx_col = 0
        for ueberschrift in ueberschriften:
            worksheet.write_string(0, xlsx_col, ueberschrift)
            xlsx_col += 1
        xlsx_row = 1

    sqlstr = "select o.id, b.id, f.i, bg.i, b.user, b.angelegt_d, b.geaendert_d, concat_ws(' ', date_format(b.besuch_d,'%Y-%m-%d'), date_format(b.besuch_t,'%T')), b.hoehle_id, o.koosystem, o.rw, o.hw, o.kooabw, o.sh, o.land, o.bl, o.bezirk_id, o.gemeinde_id, o.beschreibung, b.kommentar, f.beschreibung, f.individuen, f.indiv_genau, f.bereich, bg.user, bg.bestimmer, bg.rang, bg.taxon, bg.epitheton, bg.kommentar"
    # Liste der Arten
    #sqlstr += "(select group_concat(distinct case when b.rang=" + str(gattungsebene) + " and b.taxon not like '%virus' then concat(b.taxon,' sp.') when b.rang>=" + str(gattungsebene) + " then b.taxon else concat_ws(' ',b.taxon,b.epitheton) end order by b.taxon, isnull(b.epitheton), b.epitheton separator ', ') from bestimmung b where b.besuch_id=bes.id and b.rang between " + str(artebene) + ' and ' + str(gattungsebene) + " and not exists(select 0 from bestimmung b2 where b2.besuch_id=b.besuch_id and b2.fund_i=b.fund_i and b2.rang<b.rang)), "
    # Anzahl Funde: man kann alle mitzählen oder nur die mit den gesuchten Taxa;
    # aber letzteres bringt wahrscheinlich wenig, weil die Zahl immer >=1 ist und selten mehr;
    # außerdem ist es verwirrend, wenn je nach Suchparameter was anderes in der Spalte drinsteht
    #sqlstr += "(select count(0) from fund f where f.besuch_id=bes.id), "
    # Anzahl unbestimmte Arten: detto; die Suche erlaubt sowieso die Einschränkung
    # auf unbestimmte Arten innerhalb eines bestimmten Taxons
    #sqlstr += "(select count(0) from fund f where f.besuch_id=bes.id and not exists(select 0 from bestimmung b where b.besuch_id=f.besuch_id and b.fund_i=f.i and b.rang=" + str(artebene) + "))"
    # Ort ist entweder über bes.ort_id oder über hoehle.ort_id verknüpft.
    # In letzterem Fall kann es mehrere verknüfte Orte geben. In letzterem Fall hat Priorität:
    # 1) der Ort, den der User selbst angelegt hat
    # 2) der Ort, den der Ersteller der Höhle angelegt hat
    # 3) der Ort mit der geringsten Koo-Abweichung
    # 4) der Ort mit der kleineren ID
    #where_parts = ["(o.id = bes.ort_id or o.id in(select ho_selected.ort_id from hoehle_ort ho_selected where ho_selected.hoehle_id=bes.hoehle_id and exists(select 0 from ort o_selected where o_selected.id=ho_selected.ort_id and (o_selected.user=%s or not exists(select 0 from ort o_compare where o_compare.id in (select ho_compare.ort_id from hoehle_ort ho_compare where ho_compare.hoehle_id=bes.hoehle_id and ho_compare.ort_id!=o_selected.id) and (o_compare.user=%s or o_compare.user=(select hoehle.user from hoehle where hoehle.id=bes.hoehle_id) or o_selected.user!=(select hoehle.user from hoehle where hoehle.id=bes.hoehle_id) and (o_compare.kooabw<o_selected.kooabw or o_compare.kooabw=o_selected.kooabw and o_compare.id<o_selected.id)))))))"]
    #bindparams = [userid, userid]
    bindparams = []
    # View-artiger Join wegen http://bugs.mysql.com/110422
    #sqlstr += " from (select b.*, ifnull(ho.ort_id,b.ort_id) ort_id_consol from besuch b left join hoehle_ort ho on ho.hoehle_id=b.hoehle_id) bes, ort o"
    sqlstr += " FROM besuch b left join eingang_ort eo on b.hoehle_id=eo.hoehle_id and eo.eingang='a' join ort o on o.id in(b.ort_id,eo.ort_id) "
    if zeile not in ('F','Bg'):
        sqlstr += "left "
    sqlstr += "join fund f on b.id=f.besuch_id "
    if zeile != 'Bg' and not cgiparam('BLIST_BESTIMMER'):
        sqlstr += "left "
    sqlstr += "join bestimmung bg on (f.besuch_id,f.i)=(bg.besuch_id,bg.fund_i)"
    where_parts = []
    if cgiparam_num('BLIST_ORT_ID'):
        where_parts.append('o.id=%s')
        bindparams.append(cgiparam_num('BLIST_ORT_ID'))
    if cgiparam_num('BLIST_ID'):
        where_parts.append('b.id=%s')
        bindparams.append(cgiparam_num('BLIST_ID'))
    if cgiparam('BLIST_BDAT_VON'):
        where_parts.append('b.besuch_d>=%s')
        bindparams.append(cgiparam('BLIST_BDAT_VON'))
    if cgiparam('BLIST_BDAT_BIS'):
        where_parts.append('b.besuch_d<=%s')
        bindparams.append(cgiparam('BLIST_BDAT_BIS'))
    if cgiparam('BLIST_EIGENE'):
        where_parts.append("b.user=%s")
        bindparams.append(userid)
    elif cgiparam('BLIST_USER'):
        # direkt "like '%%s%'") geht nicht, weil %s durch String in einfachen Anführungszeichen ersetzt wird
        where_parts.append("b.user in(select id from user where concat_ws(' ',name1,name2) like concat('%',%s,'%'))")
        bindparams.append(cgiparam('BLIST_USER'))
    if cgiparam_num('BLIST_PRB_NR') or cgiparam('BLIST_PRB_EIGENE') or cgiparam('BLIST_PRB_USER'):
        prb_where_parts = []
        if cgiparam('BLIST_PRB_NR'):
            prb_where_parts.append("(p.nr=%s or p.aktion='N' and p.par1=%s)")
            bindparams.append(cgiparam_num('BLIST_PRB_NR'))
            bindparams.append(cgiparam_num('BLIST_PRB_NR'))
        if cgiparam('BLIST_PRB_EIGENE'):
            prb_where_parts.append("p.user=%s")
            bindparams.append(userid)
        elif cgiparam('BLIST_PRB_USER'):
            prb_where_parts.append("p.user in(select id from user where concat_ws(' ',name1,name2) like concat('%',%s,'%'))")
            bindparams.append(cgiparam('BLIST_PRB_USER'))
        where_parts.append("(f.besuch_id,f.i) in(select p.besuch_id,p.fund_i from probe p where (" + " and ".join(prb_where_parts) + "))")
    if cgiparam('BLIST_HOEHLEN'):
        blist_hoehlen = cgiparam('BLIST_HOEHLEN')
        if blist_hoehlen == 'H':
            if cgiparam_num('BLIST_HOEHLE_ID'):
                where_parts.append('b.hoehle_id=%s')
                bindparams.append(cgiparam_num('BLIST_HOEHLE_ID'))
            else:
                where_parts.append('b.hoehle_id is not null')
        elif blist_hoehlen == 'K':
            where_parts.append('b.hoehle_id is null')
    if cgiparam('XLIST_LAND'):
        if cgiparam('XLIST_LAND') == 'L': # Suche nach leerem Land
            where_parts.append("o.land is null")
        elif cgiparam('XLIST_LAND') == 'AT' and cgiparam('XLIST_BL') == 'L':
            where_parts.append("o.land='AT' and o.bl is null")
        elif cgiparam('XLIST_BL'):
            bindparams.append(cgiparam('XLIST_LAND'))
            bindparams.append(cgiparam('XLIST_BL'))
            if cgiparam('XLIST_BEZIRK'):
                if cgiparam('XLIST_BEZIRK') == 'L':
                    where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id is null")
                else:
                    bindparams.append(cgiparam('XLIST_BEZIRK'))
                    if cgiparam('XLIST_GEMEINDE'):
                        if cgiparam('XLIST_GEMEINDE') == 'L':
                            where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id=%s and o.gemeinde_id is null")
                        else:
                            where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id=%s and o.gemeinde_id=%s")
                            bindparams.append(cgiparam('XLIST_GEMEINDE'))
                    else:
                        where_parts.append("o.land=%s and o.bl=%s and o.bezirk_id=%s")
            else:
                where_parts.append("o.land=%s and o.bl=%s")
        else:
            where_parts.append("o.land=%s")
            bindparams.append(cgiparam('XLIST_LAND'))
    if cgiparam('XLIST_SH') and cgiparam('XLIST_SHREL'):
        xlist_sh = cgiparam_num('XLIST_SH')
        xlist_shrel = cgiparam('XLIST_SHREL')
        where_parts.append("o.sh" + ('>=' if xlist_shrel == 'M' else '=' if xlist_shrel == 'G' else '<=') + "%s")
        bindparams.append(xlist_sh)
    bereiche_keinergecheckt = True
    bereiche_allegecheckt = True
    bereiche_whereparts = []
    bereiche_einschrliste = []
    if cgiparam('BLIST_BEREICH_N'):
        bereiche_whereparts.append("f.bereich is null")
        bereiche_keinergecheckt = False
    else:
        bereiche_allegecheckt = False
    for b in bereiche:
        if cgiparam('BLIST_BEREICH_' + b):
            bereiche_einschrliste.append(b)
            bereiche_keinergecheckt = False
        else:
            bereiche_allegecheckt = False
    if bereiche_keinergecheckt:
        bereiche_allegecheckt = True # sonst leere Treffermenge, macht keinen Sinn
    if not bereiche_allegecheckt:
        if bereiche_einschrliste:
            bereiche_whereparts.append("f.bereich in('" + "','".join(bereiche_einschrliste) + "')")
        where_parts.append("(" + " or ".join(bereiche_whereparts) + ")")
    blist_bestrang = cgiparam_num('BLIST_BESTRANG')
    blist_bestminmax = cgiparam('BLIST_BESTMINMAX')
    if cgiparam('BLIST_TAXON'):
        blist_rang = cgiparam_num('BLIST_RANG')
        blist_taxon = cgiparam('BLIST_TAXON').strip()
        if blist_rang == artebene:
            minrang = artebene
            maxrang = gattungsebene
            if blist_bestrang:
                if blist_bestminmax == 'MAX':
                    minrang = max(minrang, blist_bestrang);
                else:
                    maxrang = min(maxrang, blist_bestrang);
            if blist_taxon.find(' ') == -1: # dann nur Gattungsname
                taxonwhere = "bg.rang between " + str(minrang) + " and " + str(maxrang) + " and bg.taxon=%s"
            else: # dann Art/Sammelart; Artname aufgeteilt auf taxon+epitheton, außer bei Viren, dort alles in taxon
                maxrang = min(maxrang, artebene+1) # auf Arten oder Sammelarten einschränken
                taxonwhere = "bg.rang between " + str(minrang) + " and " + str(maxrang) + " and concat_ws(' ',bg.taxon,bg.epitheton)=%s"
            bindparams.append(blist_taxon)
            if blist_bestminmax == 'MAX':
                taxonwhere += " and not exists(select 0 from bestimmung bg2 where bg.besuch_id=bg2.besuch_id and bg.fund_i=bg2.fund_i and bg2.rang<" + str(blist_bestrang) + ")"
        else:
            # höherer Rang: hier muss epitheton nicht geprüft werden, aber ggf. best_col
            if blist_bestrang:
                if blist_bestminmax == 'MAX':
                    if blist_bestrang > blist_rang:
                        taxonwhere = "false"
                    elif blist_bestrang == blist_rang:
                        taxonwhere = "bg.rang=" + str(blist_rang) + " and bg.taxon=%s and not exists(select 0 from bestimmung bg2 where bg.besuch_id=bg2.besuch_id and bg.fund_i=bg2.fund_i and bg2.rang<" + str(blist_bestrang) + ")"
                        bindparams.append(blist_taxon)
                    else:
                        taxonwhere = "(bg.rang=" + str(blist_rang) + " and bg.taxon=%s or (bg.besuch_id,bg.fund_i,bg.i) in (select bc.besuch_id,bc.fund_i,bc.best_i from best_col bc where bc.ebene=" + str(blist_rang) + " and bc.taxon=%s)) and not exists (select 0 from bestimmung bg2 where bg.besuch_id=bg2.besuch_id and bg.fund_i=bg2.fund_i and bg2.rang<" + str(blist_bestrang) + ")"
                        bindparams.append(blist_taxon)
                        bindparams.append(blist_taxon)
                else:
                    if blist_bestrang == blist_rang:
                        taxonwhere = "bg.rang=" + str(blist_rang) + " and bg.taxon=%s"
                    elif blist_bestrang < blist_rang:
                        taxonwhere = "bg.rang<=" + str(blist_bestrang) + " and (bg.besuch_id,bg.fund_i,bg.i) in(select bc.besuch_id,bc.fund_i,bc.best_i from best_col bc where bc.ebene=" + str(blist_rang) + " and bc.taxon=%s)"
                    else:
                        taxonwhere = "(bg.rang<=" + str(blist_bestrang) + " and (bg.rang=" + str(blist_rang) + " and taxon=%s or (bg.besuch_id,bg.fund_i,bg.i) in (select bc.besuch_id,bc.fund_i,bc.best_i from best_col bc where bc.ebene=" + str(blist_rang) + " and bc.taxon=%s)))"
                        bindparams.append(blist_taxon)
                    bindparams.append(blist_taxon)
            else:
                taxonwhere = "(bg.rang=" + str(blist_rang) + " and bg.taxon=%s or (bg.besuch_id,bg.fund_i,bg.i) in (select bc.besuch_id,bc.fund_i,bc.best_i from best_col bc where bc.ebene=" + str(blist_rang) + " and bc.taxon=%s))"
                bindparams.append(blist_taxon)
                bindparams.append(blist_taxon)
        where_parts.append(taxonwhere)
    elif blist_bestrang:
        # "maximal bis auf ... bestimmt" heißt rang>=... weil höhere Ränge in DB die niedrigeren IDs haben
        if blist_bestminmax == 'MAX':
            if zeile == 'Bg':
                where_parts.append("bg.rang>=" + str(blist_bestrang))
            else:
                 where_parts.append("f.i is not null and not exists(select 0 from bestimmung bg2 where f.besuch_id=bg2.besuch_id and f.i=bg2.fund_i and bg2.rang<" + str(blist_bestrang) + ")")
        else:
            where_parts.append("bg.rang<=" + str(blist_bestrang))
    if where_parts:
        sqlstr += ' where ' + ' and '.join(where_parts)
    sort_parts = []
    if cgiparam('KARTE'):
        sort_parts.append("b.besuch_d") # bei deckungsgleichen Popup mit Besuchen, sortiert nach DDatum
    elif cgiparam('GPX'):
        sort_parts.append("b.besuch_d desc") # um von deckungsgleichen nur den neuesten auszugeben
    else:
        if zeile == 'O' and cgiparam('SORT1') != 'SH':
            sort_parts.append('o.id') # nach Ort gruppieren, bevor nach was anderem sortiert wird
        for sortinp in ('SORT1', 'SORT2'):
            sortinpval = cgiparam(sortinp)
            if sortinpval:
                sortstr = 'b.id'
                if sortinpval == 'U':
                    sortstr = "(select case when name_struktur in ('FG','GP','R') then concat_ws(' ',name1,name2) else concat_ws(' ',name2,name1) end from user where id=b.user)"
                elif sortinpval == 'BD': sortstr = 'b.besuch_d'
                elif sortinpval == 'AD': sortstr = 'b.angelegt_d'
                elif sortinpval == 'SH': sortstr = 'o.sh'
                if cgiparam(sortinp + '_DIR') == 'DESC':
                    sortstr += ' desc'
                if sortstr not in sort_parts:
                    sort_parts.append(sortstr)
                if zeile == 'O' and sortinp == 'SORT1' and 'o.id' not in sort_parts:
                    sort_parts.append('o.id')
        if 'o.id' not in sort_parts:
            sort_parts.append('o.id')
        if 'b.id' not in sort_parts:
            sort_parts.append('b.id')
        sort_parts.append('f.i')
        sort_parts.append('bg.i')
    sqlstr += ' order by ' + ','.join(sort_parts)
    s['bliste'] = []
    prev_besuch_id = prev_fund_i = 0
    blist_anz_besuche = blist_anz_funde = blist_anz_unbestimmt = 0
    blist_arten = []
    blist_artbestimmt = {}
    #debug('zzz sqlstr='+sqlstr)
    cursor.execute(sqlstr, bindparams)
    rows = cursor.fetchall()

    # Ergebnismenge weiter nach Suchkriterien einschränken
    i = len(rows)
    while i > 0:
        i -= 1
        blist_ort_id, blist_besuch_id, blist_fund_i, blist_best_i, blist_user, blist_angelegt_d, blist_geaendert_d, blist_besuch_d, blist_hoehle_id, blist_koosystem, blist_rw, blist_hw, blist_kooabw, blist_sh, blist_land, blist_bl, blist_bezirk, blist_gemeinde, blist_ortsbeschreibung, blist_kommentar, blist_fundbeschreibung, blist_individuen, blist_indiv_genau, blist_bereich, bg_user, bg_bestimmer, bg_rang, bg_taxon, bg_epitheton, bg_kommentar = rows[i]
        blist_w = cgiparam_num('XLIST_W')
        blist_o = cgiparam_num('XLIST_O')
        blist_s = cgiparam_num('XLIST_S')
        blist_n = cgiparam_num('XLIST_N')
        if (blist_w and blist_o and blist_s and blist_s and blist_n):
            if blist_koosystem in epsg:
                blist_rw,blist_hw = to_wgs84(blist_koosystem,blist_rw,blist_hw)
            elif blist_koosystem != 'G':
                del rows[i]
                continue
            if not (blist_w <= blist_rw <= blist_o and blist_s <= blist_hw <= blist_n):
                del rows[i]
                continue
        bestimmer_gesucht = cgiparam('BLIST_BESTIMMER')
        if bestimmer_gesucht:
            bestimmer = bg_bestimmer or get_realname(bg_user)
            if bestimmer.upper().find(bestimmer_gesucht.upper()) == -1:
                del rows[i]
                continue

    # jetzt stimmt die Ergebnismenge, aber es sind u.U. mehrere Zeilen der Ergebnismenge zu einer Ausgabezeile zusammenzufassen
    while rows:
        row = rows.pop(0)
        blist_ort_id, blist_besuch_id, blist_fund_i, blist_best_i, blist_user, blist_angelegt_d, blist_geaendert_d, blist_besuch_d, blist_hoehle_id, blist_koosystem, blist_rw, blist_hw, blist_kooabw, blist_sh, blist_land, blist_bl, blist_bezirk, blist_gemeinde, blist_ortsbeschreibung, blist_kommentar, blist_fundbeschreibung, blist_individuen, blist_indiv_genau, blist_bereich, bg_user, bg_bestimmer, bg_rang, bg_taxon, bg_epitheton, bg_kommentar = row
        if blist_besuch_id == prev_besuch_id:
            if blist_fund_i and blist_fund_i != prev_fund_i:
                blist_anz_funde += 1
        else:
            blist_anz_besuche += 1
            if blist_fund_i:
                blist_anz_funde += 1
            prev_besuch_id = blist_besuch_id
        if bg_epitheton is None:
            bg_epitheton = ''
        prev_fund_i = blist_fund_i
        if blist_fund_i:
            if blist_best_i:
                if bg_rang == gattungsebene and not bg_taxon.endswith('virus'):
                    gefunden = False
                    for art in blist_arten:
                        if art.startswith(bg_taxon) or art.startswith(bg_taxon + ' sp.'):
                            gefunden = True
                            break
                    if not gefunden:
                        bg_taxon += ' sp.'
                        blist_arten.append(bg_taxon)
                elif bg_rang < gattungsebene:
                    gefunden = False
                    for art in blist_arten:
                        if art == (bg_taxon + ' ' + bg_epitheton).strip():
                            gefunden = True
                            break
                        if art.rstrip(' sp.') == bg_taxon:
                            blist_arten[blist_arten.index(art)] = (bg_taxon + ' ' + bg_epitheton).strip()
                            gefunden = True
                            break
                    if not gefunden:
                        blist_arten.append((bg_taxon + ' ' + bg_epitheton).strip())
                if bg_rang <= artebene:
                    blist_artbestimmt.setdefault(blist_fund_i,True)
            if blist_fund_i not in blist_artbestimmt:
                blist_artbestimmt[blist_fund_i] = False
        # neue Ausgabezeile wenn:
        #  - letzte Ergebniszeile oder
        #  - nächste Ergebniszeile gehört in neue Ausgabezeile
        if not rows or rows[0][0] != blist_ort_id or zeile == 'B' and rows[0][1] != blist_besuch_id or zeile == 'F' and rows[0][2] != blist_fund_i or zeile == 'Bg':

            # Zeile ausgeben
            if blist_fund_i:
                for bestimmt in blist_artbestimmt.values():
                    if not bestimmt:
                        blist_anz_unbestimmt += 1
            if blist_user == userid:
                blist_user = s['realname']
            else:
                blist_user = get_realname(blist_user)
            blist_bestimmer = ''
            if zeile == 'Bg':
                blist_bestimmer = bg_bestimmer or get_realname(bg_user)
            if bg_kommentar is None:
                bg_kommentar = ''
            blist_arten.sort()
            blist_artenstr = ', '.join(blist_arten)
            if cgiparam('KARTE') or cgiparam('GPX'):
                # genauso wie weiter oben; dzt. zu wenig Gemeinsamkeit für eine Funktion
                blist_rw, blist_hw = to_wgs84(blist_koosystem, blist_rw, blist_hw)
                kookey = str(blist_rw) + '|' + str(blist_hw)
                if cgiparam('KARTE'):
                    s['karte']['besuchorte'].setdefault(kookey, {'rw_g': blist_rw, 'hw_g': blist_hw, 'kooabw': blist_kooabw, 'besuche': []})['besuche'].append({'id': blist_besuch_id, 'user': blist_user, 'besuch_d': blist_besuch_d})
                elif kookey in koohash:
                    pass # auch in GPX bringen deckungsgleiche Einträge nichts => nur neuesten
                else:
                    koohash[kookey] = True
                    wpt = gpxdoc.createElement('wpt')
                    wpt.setAttribute('lon',str(blist_rw))
                    wpt.setAttribute('lat',str(blist_hw))
                    wptname = gpxdoc.createElement('name')
                    wptname.appendChild(gpxdoc.createTextNode(blist_besuch_d + ' ' + blist_user + ('' if blist_kooabw is None else ' F=' + str(blist_kooabw))))
                    wpt.appendChild(wptname)
                    wptdesc = gpxdoc.createElement('cmt') # Garmin, sonst desc statt cmt
                    wptdesc.appendChild(gpxdoc.createTextNode((hoehlen[blist_hoehle_id]['katnr'] +', ' if blist_hoehle_id else '') + 'Fehler=' + ('?' if blist_kooabw is None else str(blist_kooabw)) + 'm, Sh=' + ('?' if blist_sh is None else str(blist_sh)) + 'm, Ortsbeschreibung: ' + (blist_ortsbeschreibung or '(keine)')))
                    wpt.appendChild(wptdesc)
                    if blist_sh is not None:
                        wptele = gpxdoc.createElement('ele')
                        wptele.appendChild(gpxdoc.createTextNode(str(blist_sh)))
                        wpt.appendChild(wptele)
                    wptsym = gpxdoc.createElement('sym')
                    wptsym.appendChild(gpxdoc.createTextNode('Geocache'))
                    wpt.appendChild(wptsym)
                    gpx.appendChild(wpt)
            else:
                if blist_hoehle_id in hoehlen:
                    blist_hoehle = hoehlen[blist_hoehle_id]['katnr']
                else:
                    blist_hoehle = ''
                if blist_kommentar is None:
                    blist_kommentar = ''
                if blist_kooabw is None:
                    blist_kooabw = ''
                if blist_sh is None:
                    blist_sh = ''
                if blist_land is None:
                    blist_land = ''
                blist_bl = get_bl_abk(blist_land, blist_bl)
                blist_bezirk = get_bezirk_name(blist_bezirk)
                blist_gemeinde = get_gemeinde_name(blist_gemeinde)
                if blist_ortsbeschreibung is None:
                    blist_ortsbeschreibung = ''
                if blist_fundbeschreibung is None:
                    blist_fundbeschreibung = ''
                if blist_individuen is None:
                    blist_individuen = ''
                elif blist_indiv_genau == 'U':
                    blist_individuen = '~' + str(blist_individuen)
                elif blist_indiv_genau == 'M':
                    blist_individuen = '≥' + str(blist_individuen)
                else:
                    blist_individuen = str(blist_individuen)
                blist_bereich = get_bereich(blist_bereich)
                if cgiparam('XLSX'):
                    def str2date(p_str):
                        import datetime
                        if not p_str:
                            return None
                        yyyy = int(p_str[0:4])
                        mm = int(p_str[5:7])
                        dd = int(p_str[8:10])
                        t = p_str[11:]
                        if t:
                            h = int(t[0:2])
                            m = int(t[3:5])
                            s = int(t[6:8])
                            return datetime.datetime(yyyy,mm,dd,h,m,s)
                        return datetime.date(yyyy,mm,dd)
                    def dateformat(p_str):
                        if p_str:
                            if p_str[11:]:
                                return workbook_format_datetime
                            return workbook_format_date
                        return None
                    xlsx_col = 0
                    if zeile == 'O':
                        write_num(blist_ort_id)
                    else:
                        write_num(blist_besuch_id)
                        worksheet.write_string(xlsx_row, colinc(), blist_user)
                        worksheet.write_datetime(xlsx_row, colinc(), str2date(blist_besuch_d), dateformat(blist_besuch_d))
                        if zeile != 'Bg':
                            worksheet.write_datetime(xlsx_row, colinc(), blist_angelegt_d)
                            worksheet.write_datetime(xlsx_row, colinc(), blist_geaendert_d)
                    worksheet.write_string(xlsx_row, colinc(), blist_hoehle)
                    worksheet.write_string(xlsx_row, colinc(), blist_koosystem)
                    write_num(blist_rw)
                    write_num(blist_hw)
                    write_num(blist_kooabw)
                    write_num(blist_sh)
                    worksheet.write_string(xlsx_row, colinc(), blist_land)
                    worksheet.write_string(xlsx_row, colinc(), blist_bl)
                    worksheet.write_string(xlsx_row, colinc(), blist_bezirk)
                    worksheet.write_string(xlsx_row, colinc(), blist_gemeinde)
                    worksheet.write_string(xlsx_row, colinc(), blist_ortsbeschreibung)
                    if zeile == 'O':
                        ueberschriften.append('Besuche')
                    else:
                        worksheet.write_string(xlsx_row, colinc(), blist_kommentar)
                        if zeile == 'B':
                            write_num(blist_anz_funde)
                        elif zeile == 'F':
                            worksheet.write_string(xlsx_row, colinc(), blist_fundbeschreibung)
                            worksheet.write_string(xlsx_row, colinc(), blist_bereich)
                    if zeile in ('F', 'Bg'):
                        worksheet.write_string(xlsx_row, colinc(), blist_individuen)
                    worksheet.write_string(xlsx_row, colinc(), blist_artenstr)
                    if zeile == 'Bg':
                        worksheet.write_string(xlsx_row, colinc(), blist_bestimmer)
                        worksheet.write_string(xlsx_row, colinc(), bg_kommentar)
                    else:
                        write_num(blist_anz_unbestimmt)
                    xlsx_row += 1
                else:
                    s['bliste'].append({'ort_id': blist_ort_id, 'besuch_id': blist_besuch_id, 'fund_i': blist_fund_i, 'best_i': blist_best_i, 'user': blist_user, 'angelegt_d': blist_angelegt_d, 'geaendert_d': blist_geaendert_d, 'besuch_d': blist_besuch_d, 'hoehle': blist_hoehle, 'koosystem': blist_koosystem, 'rw': float2str(blist_rw), 'hw': float2str(blist_hw), 'kooabw': blist_kooabw, 'sh': blist_sh, 'land': blist_land, 'bl': blist_bl, 'bezirk': blist_bezirk, 'gemeinde': blist_gemeinde, 'ortsbeschreibung': blist_ortsbeschreibung, 'kommentar': blist_kommentar, 'fundbeschreibung': blist_fundbeschreibung, 'individuen': blist_individuen, 'bereich': blist_bereich, 'anz_besuche': blist_anz_besuche, 'anz_funde': blist_anz_funde, 'anz_unbestimmt': blist_anz_unbestimmt, 'arten': blist_artenstr, 'bestimmer': blist_bestimmer, 'bestkomm': bg_kommentar})
            # Ende Zeile ausgeben

            blist_anz_besuche = blist_anz_funde = blist_anz_unbestimmt = 0
            blist_arten.clear()
            blist_artbestimmt.clear()
    if cgiparam('GPX'):
        print('Content-type: application/gpx+xml; charset=utf-8\r\n'
            + 'Pragma: no-cache\r\n'
            + 'Cache-Control: no-store\r\n'
            + '\r\n' + '<?xml version="1.0" encoding="UTF-8"?>' + gpx.toxml()
        )
        closedb_and_exit()
    elif cgiparam('XLSX'):
        worksheet.autofit()
        workbook.close()
        xlsx_output.seek(0)
        sys.stdout.buffer.write(bytes(
              'Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\r\n'
            + 'Pragma: no-cache\r\n'
            + 'Cache-Control: no-store\r\n'
            + '\r\n',
            'ascii'
        ))
        while buf := xlsx_output.read():
            sys.stdout.buffer.write(buf)
        closedb_and_exit()
    # Suchformular mit zuletzt eingegebenen Daten vorbefüllen
    s['blist_ort_id'] = cgiparam('BLIST_ORT_ID')
    s['blist_id'] = cgiparam('BLIST_ID')
    s['blist_bdat_von'] = cgiparam('BLIST_BDAT_VON')
    s['blist_bdat_bis'] = cgiparam('BLIST_BDAT_BIS')
    s['blist_eigene'] = cgiparam('BLIST_EIGENE')
    s['blist_user'] = cgiparam('BLIST_USER')
    s['blist_bestimmer'] = cgiparam('BLIST_BESTIMMER')
    s['blist_prb_nr'] = cgiparam('BLIST_PRB_NR')
    s['blist_prb_eigene'] = cgiparam('BLIST_PRB_EIGENE')
    s['blist_prb_user'] = cgiparam('BLIST_PRB_USER')
    s['xlist_land'] = cgiparam('XLIST_LAND')
    s['xlist_bl'] = cgiparam('XLIST_BL')
    s['xlist_bezirk'] = cgiparam('XLIST_BEZIRK') # nicht cgiparam_num, weil auch 'L' sein kann
    s['xlist_gemeinde'] = cgiparam('XLIST_GEMEINDE') # detto
    s['xlist_w'] = cgiparam('XLIST_W')
    s['xlist_o'] = cgiparam('XLIST_O')
    s['xlist_s'] = cgiparam('XLIST_S')
    s['xlist_n'] = cgiparam('XLIST_N')
    s['xlist_shrel'] = cgiparam('XLIST_SHREL')
    s['xlist_sh'] = cgiparam('XLIST_SH')
    s['blist_rang'] = cgiparam_num('BLIST_RANG')
    s['blist_taxon'] = cgiparam('BLIST_TAXON')
    s['blist_bestminmax'] = cgiparam('BLIST_BESTMINMAX')
    s['blist_bestrang'] = cgiparam_num('BLIST_BESTRANG')
    s['blist_hoehlen'] = cgiparam('BLIST_HOEHLEN')
    s['blist_hoehle_id'] = cgiparam_num('BLIST_HOEHLE_ID')
    s['blist_bereich_N'] = cgiparam('BLIST_BEREICH_N') or bereiche_allegecheckt
    for b in bereiche:
        s['blist_bereich_' + b] = cgiparam('BLIST_BEREICH_' + b) or bereiche_allegecheckt
    s['zeile'] = cgiparam('BLIST_ZEILE') or 'B'
    s['sort1'] = cgiparam('SORT1')
    s['sort1_dir'] = cgiparam('SORT1_DIR')
    s['sort2'] = cgiparam('SORT2')
    s['sort2_dir'] = cgiparam('SORT2_DIR')

# Besuch speichern
besuchid = 0
if userid and 'BESUCH_D' in cgiparams:
    besuchid = cgiparam_num('BESUCHID')
    besuch_d = cgiparam('BESUCH_D')
    besuch_t = cgiparam('BESUCH_T') or None
    hoehle_oder_koo = cgiparam('ORT_R')
    hoehle_id = cgiparam_num('ORT_HOEHLE_ID') if hoehle_oder_koo == 'H' else None
    kommentar = cgiparam('BESUCHKOMMENTAR') or None
    koosystem = cgiparam('ORT_KOOSYST')
    rw = cgiparam_num('ORT_RW') if cgiparam('ORT_RW') else None
    hw = cgiparam_num('ORT_HW') if cgiparam('ORT_HW') else None
    kooabw = cgiparam('ORT_KOOABW')
    if kooabw == '':
        kooabw = None
    elif kooabw.isnumeric():
        kooabw = int(kooabw)
    else:
        err('KOOABW nicht numerisch')
        kooabw = None
    land = cgiparam('ORT_LAND') or None
    bl = cgiparam('ORT_BL') or None
    bezirk_id = cgiparam_num('ORT_BEZIRK') or None
    gemeinde_id = cgiparam_num('ORT_GEMEINDE') or None
    sh = cgiparam('ORT_SH')
    if sh == '':
        sh = None
    elif isfloat(sh):
        sh = float(sh)
    else:
        err('SH nicht numerisch')
        sh = None
    if sh is None:
        sh_tol = None
    else:
        sh_tol = cgiparam('ORT_SHTOL')
        if sh_tol == '':
            sh_tol = None
        elif isfloat(sh_tol):
            sh_tol = float(sh_tol)
        else:
            err('SHTOL nicht numerisch')
            sh_tol = None
    beschreibung = cgiparam('ORT_BESCHREIBUNG') or None
    zugang = cgiparam('ORT_ZUGANG') or None
    ort_id = 0
    delort_id = None
    if hoehle_oder_koo == 'H':
        get_hoehlen(False)
        if hoehle_id in hoehlen:
            if besuchid:
                # durch Update verwaisten Ort können wir noch nicht hier löschen wegen FK constraint, daher merken und erst nach Update löschen
                cursor.execute("select ort_id from besuch b where id=%s and not exists(select 0 from besuch b1 where b.id!=b1.id and b.ort_id=b1.ort_id) and not exists(select 0 from eingang_ort eo where eo.ort_id=b.ort_id)", (besuchid,))
                if row := cursor.fetchone():
                    delort_id = row[0]
            #ort_id=hoehlen[hoehle_id]['ort_id']
            ort_id=None # keine Redundanz, denn Ortsdatensätze zu Höhle können geändert werden
        else:
            err('falsche Höhlen-ID ' + str(hoehle_id))
    else:
        if besuchid:
            cursor.execute("select ort_id from besuch where id=%s", (besuchid,))
            ort_id = cursor.fetchone()[0]
            if ort_id:
                cursor.execute("select user, koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung from ort where id=%s", (ort_id,))
                user_vgl, koosystem_vgl, rw_vgl, hw_vgl, kooabw_vgl, sh_vgl, shtol_vgl, land_vgl, bl_vgl, bezirk_id_vgl, gemeinde_id_vgl, beschreibung_vgl = cursor.fetchone()
                if koosystem_vgl!=koosystem or rw_vgl!=rw or hw_vgl!=hw or kooabw_vgl!=kooabw or sh_vgl!=sh or shtol_vgl!=sh_tol or land_vgl!=land or bl_vgl!=bl or bezirk_id_vgl!=bezirk_id or gemeinde_id_vgl!=gemeinde_id or beschreibung_vgl!=beschreibung:
                    if user_vgl == userid:
                        cursor.execute("select count(0) from eingang_ort where ort_id=%s",(ort_id,))
                        if cursor.fetchone()[0]: # kann vorkommen, wenn Besuch-Ort von Höhle auf manuell geändert wird
                            ort_id = 0 # Höhlenkoordinaten nicht ändern
                        else:
                            cursor.execute("update ort set geaendert_d=current_timestamp(), koosystem=%s, rw=%s, hw=%s, kooabw=%s, sh=%s, sh_tol=%s, land=%s, bl=%s, bezirk_id=%s, gemeinde_id=%s, beschreibung=%s, zugang=%s where id=%s", (koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang, ort_id))
                    else:
                        ort_id = 0 # von anderem User angelegten Ort nicht ändern, sondern eigenen anlegen
        if not ort_id:
            # schauen, ob der Ort schon genau gleich angelegt ist (in dem Fall wurde er offenbar vom User übernommen)
            where_parts = []
            bind_params = []
            where_parts.append("koosystem=%s")
            bind_params.append(koosystem)
            where_parts.append("rw=%s")
            bind_params.append(rw)
            where_parts.append("hw=%s")
            bind_params.append(hw)
            if kooabw is None:
                where_parts.append("kooabw is null")
            else:
                where_parts.append("kooabw=%s")
                bind_params.append(kooabw)
            if sh is None:
                where_parts.append("sh is null")
            else:
                where_parts.append("sh=%s")
                bind_params.append(sh)
            if sh_tol is None:
                where_parts.append("sh_tol is null")
            else:
                where_parts.append("sh_tol=%s")
                bind_params.append(sh_tol)
            if land is None:
                where_parts.append("land is null")
            else:
                where_parts.append("land=%s")
                bind_params.append(land)
            if bl is None:
                where_parts.append("bl is null")
            else:
                where_parts.append("bl=%s")
                bind_params.append(bl)
            if bezirk_id is None:
                where_parts.append("bezirk_id is null")
            else:
                where_parts.append("bezirk_id=%s")
                bind_params.append(bezirk_id)
            if gemeinde_id is None:
                where_parts.append("gemeinde_id is null")
            else:
                where_parts.append("gemeinde_id=%s")
                bind_params.append(gemeinde_id)
            if beschreibung is None:
                where_parts.append("beschreibung is null")
            else:
                where_parts.append("beschreibung=%s")
                bind_params.append(beschreibung)
            if zugang is None:
                where_parts.append("zugang is null")
            else:
                where_parts.append("zugang=%s")
                bind_params.append(zugang)
            sortstr = " order by case user when %s then 0 else id end"
            bind_params.append(userid)
            cursor.execute("select id from ort where " + " and ".join(where_parts) + sortstr, bind_params)
            if row := cursor.fetchone():
                # schon früher angelegten Ort übernehmen
                ort_id = row[0]
                cursor.fetchall()
            else:
                cursor.execute("insert into ort(user, koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning id", (userid, koosystem, rw, hw, kooabw, sh, sh_tol, land, bl, bezirk_id, gemeinde_id, beschreibung, zugang))
                ort_id = cursor.fetchone()[0]
    if besuchid:
        cursor.execute("update besuch set besuch_d=%s, besuch_t=%s, hoehle_id=%s, ort_id=%s, kommentar=%s where id=%s and user=%s", (besuch_d, besuch_t, hoehle_id, ort_id, kommentar, besuchid, userid))
        # Ort löschen wenn durch Besuch-Update verwaist
        if delort_id:
            cursor.execute("delete from ort where id=%s", (delort_id,))
        biodb.commit()
        # rowcount unzuverlässig wenn gepuffert
        #if not cursor.rowcount:
        #    err('Besuch-Datensatz mit id=' + str(besuchid) + ' nicht gefunden oder von anderem User')
        #    besuchid = 0
    elif idem_fresh():
        cursor.execute("insert into besuch (user, besuch_d, besuch_t, hoehle_id, ort_id, kommentar) values(%s,%s,%s,%s,%s,%s) returning id", (userid, besuch_d, besuch_t, hoehle_id, ort_id, kommentar))
        # if cursor.with_rows:
        besuchid = cursor.fetchone()[0]
        biodb.commit()
        insdel_passiert = True
        # else:
        #    err('Insert auf Besuch fehlgeschlagen')

# Besuch anzeigen
if besuchid or ('BESUCHID' in cgiparams and not cgiparam('DELBESUCH')):
    get_hoehlen(True)
    b = {'userid': 0,
         'schreibrecht': False,
         'angelegt_dt': '',
         'geaendert_dt': '',
         'besuch_d': '',
         'besuch_t': '',
         'hoehle_id': cgiparam_num('ORT_HOEHLE_ID'),
         'koosystem': '',
         'rw': '',
         'hw': '',
         'kooabw': '',
         'land': '',
         'bl': '',
         'bezirk_id': 0,
         'gemeinde_id': 0,
         'sh': '',
         'sh_tol': '',
         'ortsbeschreibung': '',
         'zugang': '',
         'kommentar': ''
        }
    if not besuchid:
        besuchid = cgiparam_num('BESUCHID')
    if besuchid:
        cursor.execute("select user, date_format(angelegt_d,'%Y-%m-%d %T'), date_format(geaendert_d,'%Y-%m-%d %T'), date_format(besuch_d,'%Y-%m-%d'), date_format(besuch_t,'%T'), hoehle_id, ort_id, kommentar from besuch where id=%s", (besuchid,))
        row = cursor.fetchone()
        if row is None:
            err('Besuch-Datensatz mit id '+str(besuchid)+' nicht gefunden');
            besuchid = 0
        else:
            b['userid'], b['angelegt_dt'], b['geaendert_dt'], b['besuch_d'], b['besuch_t'], b['hoehle_id'], ort_id, b['kommentar'] = row
            if b['hoehle_id']:
                cursor.execute("select koosystem, rw, hw, kooabw, land, bl, bezirk_id, gemeinde_id, sh, sh_tol, beschreibung, zugang from ort where id in(select ort_id from eingang_ort where hoehle_id=%s and eingang='a') order by case user when %s then -2 when %s then -1 else kooabw end, id", (b['hoehle_id'], userid, b['userid']))
            else:
                cursor.execute("select koosystem, rw, hw, kooabw, land, bl, bezirk_id, gemeinde_id, sh, sh_tol, beschreibung, zugang from ort where id=%s", (ort_id,))
            row = cursor.fetchone()
            b['koosystem'], b['rw'], b['hw'], b['kooabw'], b['land'], b['bl'], b['bezirk_id'], b['gemeinde_id'], b['sh'], b['sh_tol'], b['ortsbeschreibung'], b['zugang'] = row
            cursor.fetchall()
            if b['kooabw'] is None:
                b['kooabw'] = ''
            b['rw'] = float2str(b['rw'])
            b['hw'] = float2str(b['hw'])
            if b['land'] is None:
                b['land'] = ''
            if b['bl'] is None:
                b['bl'] = ''
            if b['sh'] is None:
                b['sh'] = ''
            if b['sh_tol'] is None:
                b['sh_tol'] = ''
            if b['ortsbeschreibung'] is None:
                b['ortsbeschreibung'] = ''
            if b['zugang'] is None:
                b['zugang'] = ''
            if cgiparam_num('DUP'):
                besuchid = 0
                b['angelegt_dt'] = b['geaendert_dt'] = b['besuch_d'] = b['besuch_t'] = b['kommentar'] = ''
            else:
                if b['kommentar'] is None:
                    b['kommentar'] = ''
                if b['userid'] == userid:
                    b['schreibrecht'] = True
                    b['user_realname'] = s['realname']
                else:
                    b['user_realname'] = get_realname(b['userid'])
    if userid and not besuchid:
        b['schreibrecht'] = True
        # if not cgiparam_num('DUP'):
        #     b['land'] = 'AT' # Default für Neuanlage, seit automatischer Versorgung nicht mehr nötig
    if besuchid or b['schreibrecht']: # ohne Schreibrecht keine Neuanlage möglich, Parameter BESUCHID=0 kann dann nur manipuliert sein
        b['id'] = besuchid
        s['besuch'] = b
        if b['schreibrecht']:
            insdel_moeglich = True

if besuchid:
    # Fund löschen oder speichern
    fundformular = False
    if 'FUND_I' in cgiparams:
        fundformular = True
    fund_i = cgiparam_num('FUND_I')
    fund_schreibrecht = s['besuch']['schreibrecht']
    if fund_schreibrecht:
        # Fund löschen
        if cgiparam_num('DELFUND'):
            if idem_fresh():
                cursor.execute("update anfrage set status='O' where status='N' and besuch_id=%s and fund_i=%s", (besuchid, fund_i))
                cursor.execute("delete from fund where besuch_id=%s and i=%s", (besuchid, fund_i))
                biodb.commit()
                insdel_passiert = True
            fund_i = 0
            fundformular = False
        # Fund speichern
        elif 'FUND_BESCHREIBUNG' in cgiparams:
            fundbeschreibung = cgiparam('FUND_BESCHREIBUNG') or None
            individuen = cgiparam_num('FUND_INDIVIDUEN') if cgiparam('FUND_INDIVIDUEN') else None
            indiv_genau = cgiparam('FUND_INDIV_GENAU')
            bereich = cgiparam('FUND_BEREICH') or None
            if fund_i:
                cursor.execute("update fund set beschreibung=%s, individuen=%s, indiv_genau=%s, bereich=%s where besuch_id=%s and i=%s", (fundbeschreibung, individuen, indiv_genau, bereich, besuchid, fund_i))
                biodb.commit()
            elif idem_fresh():
                cursor.execute("insert into fund (besuch_id, beschreibung, individuen, indiv_genau, bereich) values(%s,%s,%s,%s,%s) returning i", (besuchid, fundbeschreibung, individuen, indiv_genau, bereich))
                # if cursor.with_rows:
                fund_i = cursor.fetchone()[0]
                biodb.commit()
                insdel_passiert = True
                # else:
                #    err('Insert auf Fund fehlgeschlagen')
        # Datei-Upload
        #if 'FUND_DATEI' in cgifiles and idem_fresh():
        #    for datei in cgifiles['FUND_DATEI']:
        #        dateiname = datei['filename']
        #        if not dateiname:
        #            continue
        #        dateiinhalt = datei['content']
        #        # fixme: datei_version, hoehle_datei
        #        mime = magic.from_buffer(dateiinhalt, mime=True)
        #        cursor.execute("insert into datei (filename, mime, user) values(%s,%s,%s) returning id", (dateiname, mime, userid))
        #        datei_id = cursor.fetchone()[0]
        #        cursor.execute("insert into fund_datei (besuch_id,fund_i,datei_id) values (%s,%s,%s)", (besuchid, fund_i, datei_id))
        #        biodb.commit()
        #        datei = open(mediadir + str(datei_id), 'wb')
        #        datei.write(dateiinhalt)
        #        datei.close()
        #        insdel_passiert = True
    # Datei löschen
    if cgiparam_num('DELFILE') and idem_fresh():
        delfileid = cgiparam_num('DELFILE')
        cursor.execute("select owner,code from datei where id=%s", (delfileid,))
        row = cursor.fetchone()
        if row is None:
            err('Datei mit ID=' + delfileid + ' nicht gefunden')
        else:
            delfileowner, delfilecode = row
            if delfileowner != userid:
                err('kein Löschrecht auf Datei')
            else:
                cursor.execute("select v from datei_version where datei_id=%s", (delfileid,))
                for row in cursor.fetchall():
                    delfilev = row[0]
                    os.unlink(mediadir + str(delfileid) + '_' + delfilecode + '_' + str(delfilev))
                # folgendes passiert eh über delete cascade
                # cursor.execute("delete from fund_datei where datei_id=%s", (delfileid,))
                cursor.execute("delete from datei where id=%s", (delfileid,))
                biodb.commit()
                insdel_passiert = True
    elif cgiparam_num('DELFILEZUO'):
        delfileid = cgiparam_num('DELFILEZUO')
        cursor.execute("select count(*) from datei where id=%s", (delfileid,))
        anz = cursor.fetchone()[0]
        if not anz:
            err('Datei mit ID=' + delfileid + ' nicht gefunden')
        elif not fund_schreibrecht:
            err('kein Löschrecht auf Dateizuordnung')
        else:
            cursor.execute("delete from fund_datei where datei_id=%s", (delfileid,))
            biodb.commit()

    # Fund anzeigen:
    if fundformular:
        fundbeschreibung = individuen = indiv_genau = ''
        bereich = None
        if fund_i:
            cursor.execute("select beschreibung, individuen, indiv_genau, bereich from fund where besuch_id=%s and i=%s", (besuchid,fund_i))
            fundbeschreibung, individuen, indiv_genau, bereich = cursor.fetchone()
            if fundbeschreibung is None:
                fundbeschreibung = ''
            individuen = '' if individuen is None else str(individuen)
            if indiv_genau is None:
                indiv_genau = ''
        s['fund'] = {
            'besuchid': besuchid,
            'i': fund_i,
            'beschreibung': fundbeschreibung,
            'individuen': individuen,
            'indiv_genau': indiv_genau,
            'bereich': bereich,
            'schreibrecht': fund_schreibrecht
        }
        if fund_schreibrecht:
            insdel_moeglich = True;
            # Liste möglicher Bestimmungsanfrage-Empfänger
            fundusers = []
            cursor.execute("select id, concat_ws(' ', name1, name2) from user where id!=%s and name_struktur!='R' and id not in (select an_user from anfrage where besuch_id=%s and fund_i=%s) order by case when name_struktur in ('FG','GP','R') then concat_ws(' ',name1,name2) else concat_ws(' ',name2,name1) end, id", (userid, besuchid, fund_i))
            for row in cursor.fetchall():
                fundusers.append({'id': row[0], 'name': row[1] or ''})
            s['fund']['users'] = fundusers

    # Dateiliste
    if fund_i:
        dateien = []
        cursor.execute("select d.id, d.owner, dv.v, dv.filename, dv.mime, dv.size, date_format(dv.upload_d,'%Y-%m-%d') from datei d, datei_version dv where d.id=dv.datei_id and d.id in(select fd.datei_id from fund_datei fd where fd.besuch_id=%s and fd.fund_i=%s) and not exists(select 0 from datei_version dv1 where dv1.datei_id=d.id and dv1.v>dv.v) order by d.id", (besuchid, fund_i))
        for row in cursor.fetchall():
            dateien.append({
                'id': row[0],
                'v': row[2],
                'filename': row[3],
                'mime': row[4],
                'filesize': row[5],
                'upload_d': row[6],
                'loeschrecht': row[1] == userid
            })
        s['dateien'] = dateien

    # Proben
        prb_action = cgiparam('PRB_ACTION')
        if prb_action:
            prb_id = cgiparam_num('PRB_ID')
            if prb_action == 'DEL':
                cursor.execute("delete from probe where id=%s and user=%s", (prb_id, userid))
            else:
                prb_nr = cgiparam('PRB_NR') or None
                prb_datum = cgiparam('PRB_DATUM')
                prb_aktion = cgiparam('PRB_AKTION')
                prb_par1 = cgiparam('PRB_PAR1') or None
                prb_par2 = cgiparam('PRB_PAR2') or None
                prb_anmerkung = cgiparam('PRB_ANMERKUNG') or None
                if prb_id:
                    cursor.execute("update probe set nr=%s, datum=%s, aktion=%s, par1=%s, par2=%s, anmerkung=%s where id=%s and user=%s", (prb_nr, prb_datum, prb_aktion, prb_par1, prb_par2, prb_anmerkung, prb_id, userid))
                else:
                    cursor.execute("insert into probe(besuch_id,fund_i,user,nr,datum,aktion,par1,par2,anmerkung) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",(besuchid, fund_i, userid, prb_nr, prb_datum, prb_aktion, prb_par1, prb_par2, prb_anmerkung))
            biodb.commit()
        s['prb_aktionen'] = {
            'E': 'erhalten',
            'G': 'gelagert',
            'N': 'neue Nummer',
            'Ü': 'übergeben',
            'A': 'gesendet',
            'R': 'Rückmeldung',
            'W': 'entsorgt',
            'V': 'verschwunden',
            'S': 'Sonstiges'
        }
        s['proben'] = []
        cursor.execute("select id,user,nr,datum,aktion,par1,par2,anmerkung from probe where besuch_id=%s and fund_i=%s order by datum,id", (besuchid, fund_i))
        for prb_id,prb_user,prb_nr,prb_datum,prb_aktion,prb_par1,prb_par2,prb_anmerkung in cursor.fetchall():
            prb_schreibrecht = prb_user == userid
            prb_user_realname = s['realname'] if prb_user == userid else get_realname(prb_user)
            s['proben'].append({'id': prb_id, 'schreibrecht': prb_schreibrecht, 'nr': prb_nr or '', 'datum': prb_datum, 'aktion': prb_aktion, 'par1': prb_par1 or '', 'par2': prb_par2 or '', 'anmerkung': prb_anmerkung or '', 'user': prb_user, 'user_realname': prb_user_realname})
        if userid:
            # Zeile für Neuanlage
            s['proben'].append({'id': 0, 'schreibrecht': True, 'nr': '', 'datum': '', 'aktion': 'G', 'par1': '', 'par2': '', 'anmerkung': ''})
            # Wegen der Zeile für Neuanlage müsste bei geöffnetem Fund immer ein idempotency Token erzeugt (bzw. beim Speichern einer Probenzeile geprüft) werden, aber der Schaden, der beim doppelten Speichern angerichtet wird, ist gering und somit zahlt es sich kaum aus, für das Token die Performance zu verschlechtern.

    # Bestimmungen
        # Bestimmung speichern
        best_i = cgiparam_num('BEST_I')
        if cgiparam('DELBEST') and best_i:
            if idem_fresh():
                cursor.execute("delete from bestimmung where besuch_id=%s and fund_i=%s and i=%s and user=%s", (besuchid, fund_i, best_i, userid))
                biodb.commit()
                insdel_passiert = True
            best_i = 0
        # Bestimmung ablehnen
        elif cgiparam('ABLEHNUNG'):
            ablehngrund = cgiparam('ABLEHNGRUND') or None
            cursor.execute("update anfrage set status='A', erledigt_d=current_timestamp(), ablehngrund=%s where besuch_id=%s and fund_i=%s and an_user=%s", (ablehngrund, besuchid, fund_i, userid))
            biodb.commit()
            if result := mailbenachrichtigung(s['besuch']['userid'], 'MailBestErl', s['realname'] + " hat die Bestimmung von Besuch-ID " + str(besuchid) + " / Fund-Index:" + str(fund_i) + " abgelehnt" + (' mit der Begründung: ' + ablehngrund + '.\n' if ablehngrund else '. ') + "Sie können dieses Ergebis auch in der Funddatenbank (" + CONF_HP + ") übers Menü Funde → Bstm.anfragen einsehen."):
                err(result)
        elif cgiparam('BEST_RANG'):
            bestimmer = cgiparam('BEST_BESTIMMER') or None
            methode = cgiparam('BEST_METHODE') or None
            detail = cgiparam('BEST_DETAIL') or None
            rang = cgiparam_num('BEST_RANG')
            taxon = cgiparam('BEST_TAXON')
            epitheton = cgiparam('BEST_EPITHETON') or None
            ssp = cgiparam('BEST_SSP') or None
            var = cgiparam('BEST_VAR') or None
            cv = cgiparam('BEST_CV') or None
            form = cgiparam('BEST_FORM') or None
            geschlecht = cgiparam('BEST_GESCHLECHT') or None
            bestkomm = cgiparam('BEST_KOMMENTAR') or None
            col_updaten = False
            benachrichtigen = False
            if best_i:
                cursor.execute("select user from bestimmung where besuch_id=%s and fund_i=%s and i=%s", (besuchid, fund_i, best_i))
                row = cursor.fetchone()
                if row and row[0] == userid:
                    cursor.execute("update bestimmung set bestimmer=%s, methode=%s, detail=%s, rang=%s, taxon=%s, epitheton=%s, ssp=%s, var=%s, cv=%s, form=%s, geschlecht=%s, kommentar=%s where besuch_id=%s and fund_i=%s and i=%s", (bestimmer, methode, detail, rang, taxon, epitheton, ssp, var, cv, form, geschlecht, bestkomm, besuchid, fund_i, best_i))
                    cursor.execute("delete from best_col where besuch_id=%s and fund_i=%s and best_i=%s", (besuchid, fund_i, best_i))
                    col_updaten = True
            elif idem_fresh():
                cursor.execute("select count(0) from bestimmung where besuch_id=%s and fund_i=%s and user=%s", (besuchid, fund_i, userid))
                if cursor.fetchone()[0]:
                    err('Versuch erkannt, eine Bestimmung zum zweiten Mal anzulegen. Bitte stattdessen die existierende anpassen.');
                else:
                    cursor.execute("insert into bestimmung (besuch_id, fund_i, user, bestimmer, methode, detail, rang, taxon, epitheton, ssp, var, cv, form, geschlecht, kommentar) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning i", (besuchid, fund_i, userid, bestimmer, methode, detail, rang, taxon, epitheton, ssp, var, cv, form, geschlecht, bestkomm))
                    best_i = cursor.fetchone()[0]
                    if s['besuch']['userid'] != userid:
                        cursor.execute("select count(0) from anfrage where besuch_id=%s and fund_i=%s and an_user=%s and status='N'", (besuchid, fund_i, userid))
                        if cursor.fetchone()[0]:
                            cursor.execute("update anfrage set status='E', erledigt_d=current_timestamp() where besuch_id=%s and fund_i=%s and an_user=%s and status='N'", (besuchid, fund_i, userid))
                            benachrichtigen = True;
                        insdel_passiert = True
                        col_updaten = True
            if col_updaten:
                bestpfad = cgiparam('BEST_PFAD')
                if bestpfad:
                    for node in bestpfad.split('|'):
                        (colebene,coltaxon) = node.split(':',1)
                        colebene = int(colebene)
                        cursor.execute("insert into best_col(besuch_id, fund_i, best_i, ebene, taxon) values(%s,%s,%s,%s,%s)", (besuchid, fund_i, best_i, colebene, coltaxon))
                biodb.commit()
            if benachrichtigen:
                besterg_str = taxon
                if epitheton:
                    besterg_str += ' ' + epitheton
                    if ssp:
                        besterg_str += ' ssp. ' + ssp
                    if var:
                        besterg_str += ' var. ' + var
                    if cv:
                        besterg_str += ' cv. ' + cv
                    if form:
                        besterg_str += ' f. ' + form
                if result := mailbenachrichtigung(s['besuch']['userid'], 'MailBestErl', s['realname'] + " hat Besuch-ID " + str(besuchid) + " / Fund-Index " + str(fund_i) + " bestimmt auf: " + besterg_str + ". Weitere Details finden Sie in der Funddatenbank (" + CONF_HP + ") übers Menü Funde → Bstm.anfragen bzw. über Funde → Liste."):
                    err(result)

        # zulässige Werte für Methoden
        methoden = [ {'key': 'V', 'text': 'vor Ort'},
                     {'key': 'F', 'text': 'Foto'},
                     {'key': 'P', 'text': 'Präparat'}
                   ]
        s['methoden'] = methoden
        def get_methode(p_key):
            if p_key is None:
                return ''
            for m in methoden:
                if m['key'] == p_key:
                    return m['text']
            return ''

        # zulässige Werte für Details
        details = [ {'key': 'F', 'text': 'freies Auge'},
                    {'key': 'L', 'text': 'Makro/Lupe'},
                    {'key': 'M', 'text': 'Mikroskop'},
                    {'key': 'D', 'text': 'DNA'}
                  ]
        s['details'] = details
        def get_detail(p_key):
            if p_key is None:
                return ''
            for d in details:
                if d['key'] == p_key:
                    return d['text']
            return ''

        # Bestimmungenliste
        bestliste = []
        cursor.execute("select i, user, bestimmer, angelegt_d, geaendert_d, methode, detail, rang, taxon, epitheton, ssp, var, cv, form, geschlecht, kommentar from bestimmung where besuch_id=%s and fund_i=%s order by i", (besuchid,fund_i))
        for row in cursor.fetchall():
            bestl_i, bestl_userid, bestl_bestimmer, bestl_angelegt_d, bestl_geaendert_d, bestl_methode, bestl_detail, bestl_rang, bestl_taxon, bestl_epitheton, bestl_ssp, bestl_var, bestl_cv, bestl_form, bestl_geschlecht, bestl_kommentar = row
            bestl_user_realname = s['realname']
            if bestl_userid != userid:
                bestl_user_realname = get_realname(bestl_userid)
            if not bestl_bestimmer:
                bestl_bestimmer = bestl_user_realname
            bestliste.append({
                'i': bestl_i,
                'user_realname': bestl_user_realname,
                'bestimmer': bestl_bestimmer,
                'angelegt_d': bestl_angelegt_d,
                'geaendert_d': bestl_geaendert_d,
                'methode': get_methode(bestl_methode),
                'detail': get_detail(bestl_detail),
                'rang': get_rang(bestl_rang),
                'taxon': bestl_taxon,
                'epitheton': bestl_epitheton or '',
                'ssp': bestl_ssp or '',
                'var': bestl_var or '',
                'cv': bestl_cv or '',
                'form': bestl_form or '',
                'geschlecht': bestl_geschlecht or '',
                'kommentar': bestl_kommentar or ''
            })
            s['besuch']['mit_dateien_oder_bestimmungen'] = True
        s['bestimmungen'] = bestliste

        # Bestimmung-Formular
        b={}
        best_user_realname = best_bestimmer = best_angelegt_d = best_geaendert_d = best_methode = best_detail = best_taxon = best_epitheton = best_ssp = best_var = best_cv = best_form = best_geschlecht = best_kommentar = best_pfad = ''
        best_userid = best_rang = 0
        best_schreibrecht = False
        best_eigene_bestimmung_vorhanden = False;
        if not best_i:
            cursor.execute("select i from bestimmung where besuch_id=%s and fund_i=%s and user=%s", (besuchid,fund_i,userid))
            if row := cursor.fetchone():
                best_i = row[0]
                best_schreibrecht = True
        if best_i:
            cursor.execute("select user, bestimmer, angelegt_d, geaendert_d, methode, detail, rang, taxon, epitheton, ssp, var, cv, form, geschlecht, kommentar from bestimmung where besuch_id=%s and fund_i=%s and i=%s", (besuchid,fund_i,best_i))
            row = cursor.fetchone()
            if row is None:
                err('Bestimmung nicht gefunden')
                best_i = 0
            else:
                best_userid, best_bestimmer, best_angelegt_d, best_geaendert_d, best_methode, best_detail, best_rang, best_taxon, best_epitheton, best_ssp, best_var, best_cv, best_form, best_geschlecht, best_kommentar = row
                best_user_realname = s['realname']
                if best_userid == userid:
                    best_schreibrecht = True
                    best_eigene_bestimmung_vorhanden = True;
                else:
                    best_user_realname = get_realname(best_userid)
                    cursor.execute("select count(0) from bestimmung where besuch_id=%s and fund_i=%s and user=%s", (besuchid,fund_i,userid))
                    best_eigene_bestimmung_vorhanden = bool(cursor.fetchone()[0])
                cursor.execute("select ebene,taxon from best_col where besuch_id=%s and fund_i=%s and best_i=%s order by ebene desc", (besuchid,fund_i,best_i))
                for row in cursor.fetchall():
                    if best_pfad:
                        best_pfad += '|'
                    best_pfad += str(row[0]) + ':' + row[1]
        b['userid'] = best_userid
        b['user_realname'] = best_user_realname
        b['bestimmer'] = best_bestimmer or ''
        b['angelegt_dt'] = best_angelegt_d
        b['geaendert_dt'] = best_geaendert_d
        b['methode'] = best_methode or ''
        b['detail'] = best_detail or ''
        b['rang'] = best_rang
        b['taxon'] = best_taxon
        b['epitheton'] = best_epitheton or ''
        b['ssp'] = best_ssp or ''
        b['var'] = best_var or ''
        b['cv'] = best_cv or ''
        b['form'] = best_form or ''
        b['geschlecht'] = best_geschlecht or ''
        b['kommentar'] = best_kommentar or ''
        b['pfad'] = best_pfad
        b['i'] = best_i
        b['schreibrecht'] = best_schreibrecht or not best_i
        #debug('best_t:'+str(best_i)+' bestschr:'+str(best_schreibrecht)+' bschrr:'+str(b['schreibrecht']))
        b['eigene_bestimmung_vorhanden'] = best_eigene_bestimmung_vorhanden
        b['form_oeffnen'] = best_i or cgiparam('BEST_I') == '0'
        s['best'] = b
        insdel_moeglich |= b['schreibrecht']

        # eingelangte Bestimmungsanfrage
        if userid != s['besuch']['userid'] and not best_eigene_bestimmung_vorhanden:
            cursor.execute("select gesendet_d, anmerkung from anfrage where besuch_id=%s and fund_i=%s and an_user=%s and status='N'", (besuchid,fund_i,userid))
            if row := cursor.fetchone():
                s['fund']['offene_anfrage'] = {'anfrage_d': row[0], 'kommentar': row[1] or ''}

    cursor.execute("select count(0) from fund_datei where besuch_id=%s", (besuchid,))
    if cursor.fetchone()[0]:
        s['besuch']['abhaengige_datensaetze_vorhanden'] = True
    else:
        cursor.execute("select count(0) from bestimmung where besuch_id=%s", (besuchid,))
        if cursor.fetchone()[0]:
            s['besuch']['abhaengige_datensaetze_vorhanden'] = True
        else:
            cursor.execute("select count(0) from probe where besuch_id=%s", (besuchid,))
            if cursor.fetchone()[0]:
                s['besuch']['abhaengige_datensaetze_vorhanden'] = True

    # Fundliste
    fundliste = []
    cursor.execute("select i, beschreibung, individuen, indiv_genau, bereich, (select count(0) from fund_datei fd where fd.besuch_id=f.besuch_id and fd.fund_i = f.i), (select count(0) from bestimmung b where b.besuch_id=f.besuch_id and b.fund_i=f.i), (select min(if(rang=" + str(artebene) + " and ssp is not null, 0, rang)) from bestimmung b where b.besuch_id=f.besuch_id and b.fund_i=f.i), (select group_concat(distinct concat_ws(' ssp. ',concat_ws(' ',taxon,epitheton),ssp) order by 1 separator ', ') from bestimmung b where b.besuch_id=f.besuch_id and b.fund_i=f.i and if(rang=" + str(artebene) + " and ssp is not null, 0, rang) = (select min(if(rang=" + str(artebene) + " and ssp is not null, 0, rang)) from bestimmung b where b.besuch_id=f.besuch_id and b.fund_i=f.i)) from fund f where besuch_id=%s order by i", (besuchid,))
    for row in cursor.fetchall():
        anzahlstr = ''
        if row[2] is not None:
            if row[3] == 'U':
                anzahlstr = '~'
            elif row[3] == 'M':
                anzahlstr = '≥'
            anzahlstr += str(row[2])
        fundliste.append({
            'i': row[0],
            'beschreibung': row[1] or '', # None -> leer
            'individuen': anzahlstr,
            'bereich': get_bereich(row[4]),
            'anzdateien': row[5],
            'anzbest': row[6],
            'rang': get_rang(row[7]) if row[7] else '' if row[7] is None else 'Unterart',
            'taxon': row[8] or ''
        })
    s['funde'] = fundliste

# Benachrichtigung über erhaltene Anfragen anzeigen
s['anz_anfragen'] = 0
if userid:
    cursor.execute("select count(0) from anfrage where an_user=%s and status='N'", (userid,))
    s['anz_anfragen'] = cursor.fetchone()[0]

# Menüs
submenu1 = []
submenu1.append(["Login/-out", "openlogindlg()"])
if userid:
    submenu1.append(["Einstellungen", "opencfg()"])
    submenu1.append(["Benutzerliste", "userlist()"])
    if admin:
        submenu1.append(["User anlegen", "openuser(0)"])
    submenu1.append(["Ben.gruppen", "grplist()"])
    if admin:
        submenu1.append(["Gruppe anlegen", "opengrp(0)"])
        submenu1.append(["Rechte akt.", "rechteakt()"])
submenu1.append(["Taxonomie", "taxbaum()"])
submenu1.append(["Ränge", "ranglist()"])
submenu1.append(["neues Fenster", "window.open('"+s['scriptname']+("?SESS="+sessionid if sessionid else '')+"','_blank')"])
submenu2 = []
submenu2.append(["Suche", "hliste(false)"])
submenu2.append(["Liste", "hliste(true)"])
submenu2.append(["Karte", "openkarte('H')"])
if userid:
    submenu2.append(["neu", "openhoehle()"])
submenu2.append(["Kat.gruppen", "katgrlist()"])
submenu2.append(["Vereine", "vrnlist()"])
submenu3 = []
submenu3.append(["Liste", "bliste()"])
if userid:
    submenu3.append(["neu", "openbesuch()"])
    submenu3.append(["Bstm.anfragen", "aliste()"])
submenu3.append(["Karte", "openkarte('F')"])
submenu4 = []
submenu4.append(["Suche", "dliste()"])
if userid:
    submenu4.append(["hochladen", "d_upl()"])
submenu4.append(["Publikationen", "publist()"])
if userid:
    submenu4.append(["neue Publ.", "openpub()"])
submenu5 = []
submenu5.append(["Siteinfo", "siteinfo()"])
submenu5.append(["Anleitung", "anleitung()"])
submenu5.append(["Lizenzen", "lizenzen()"])
submenu5.append(["Bugs", "openlink('https://gitlab.com/fkv1/biodb/-/issues',true)"])
if not userid:
    submenu5.append(["Registrierung", "reginfo()"])
s['menu'] = [
    { 'breite': 120, 'name': 'Allgemeines', 'submenu': submenu1 },
    { 'breite': 120, 'name': 'Höhlen', 'submenu': submenu2 },
    { 'breite': 120, 'name': 'Funde', 'submenu': submenu3 },
    { 'breite': 120, 'name': 'Dateien', 'submenu': submenu4 },
    { 'breite': 100, 'name': 'Hilfe', 'submenu': submenu5 },
]

# idempotency token, siehe https://www.youtube.com/watch?v=IP-rGJKSZ3s&t=377s
if insdel_passiert:
    usedidem = cgiparam_num('IDEM')
    if usedidem:
        cursor.execute("insert into idem_used values(%s)", (usedidem,))
        biodb.commit()
#debug('cgi-IDEM:'+cgiparam('IDEM'))
s['idem']=0
#debug('insd_m:'+str(insdel_moeglich))
if insdel_moeglich:
    s['idem'] = create_idem()
    #debug('newidem:'+str(s['idem']))

update_user_active()
cursor.close()
biodb.close()

if errstr:
    s['error'] = errstr
if debugstr:
    s['debug'] = debugstr

template_name = 'fund.tpl'
if s['scriptname'] == 'fund_test.py':
    template_name = 'fund_test.tpl'
print('Content-type: text/html; charset=utf-8' + '\r\n\r\n' + '<!DOCTYPE html>')
template_lookup = TemplateLookup(directories=[appdir + '/templates'])
fundtemplate = Template(filename=appdir + 'templates/' + template_name, lookup=template_lookup)
print(fundtemplate.render(s = s))
print("</html>")
