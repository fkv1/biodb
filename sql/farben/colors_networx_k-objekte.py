#!/usr/bin/python3

import sys, os, mysql.connector
from mysql.connector import errorcode

if not os.path.isfile('../../fund.conf'):
    print("fund.conf fehlt => Abbruch", file=sys.stderr)
    sys.exit(1)
exec(open('../../fund.conf').read())

appdir = os.path.dirname(os.environ.get('SCRIPT_FILENAME', __file__)) + '/'

debug_to_file = 1
debugstr = ''
errstr = ''

# zu DB verbinden
biodb = mysql.connector.connect(database=CONF_DB_NAME, user=CONF_DB_USER, password=CONF_DB_PASS)
cursor = biodb.cursor()

import networkx as nx, brute_force_coloring
G=nx.Graph()
cursor.execute("select nr, ST_AsGeoJSON(centroid(grenze)) from katgr where nr like 'K%' order by nr")
for row in cursor.fetchall():
    nr, json = row
    json = json[json.find('[')+1:json.find(']')]
    hw, rw = json.split(', ')
    hw = float(hw)
    rw = float(rw)
    G.add_node(nr)

cursor.execute("select nr from katgr where nr like 'K%' order by nr")
for row in cursor.fetchall():
    nr = row[0]
    #print('suche für: '+str(nr))
    cursor.execute("select nr from katgr a where a.nr like 'K%' and a.nr>%s and st_intersects(a.grenze, (select b.grenze from katgr b where b.nr=%s)) order by a.nr", (nr,nr))
    for row2 in cursor.fetchall():
        nr2 = row2[0]
        #print(nr + '-' + nr2)
        G.add_edge(nr, nr2)
dict = brute_force_coloring.brute_force_color(G, 4, 'True')
#for k in dict:
#    print (k + ': '+str(dict[k]))

# cursor.execute("update katgr set farbe=null")
for nr in dict:
    cursor.execute("update katgr set farbe=%s where nr=%s", (dict[nr], nr))

biodb.commit()

cursor.close()
biodb.close()
