#!/usr/bin/python3.9

import json
f = open ('gemeinden.geojson', "r")
j_root = json.loads(f.read())
f.close()

for j_gemeinde in j_root['features']:
    #if j_gemeinde['type'] != 'Feature':
    #    print ("type=" + j_gemeinde['type'])
    j_gid = j_gemeinde['properties']['g_id']
    j_name = j_gemeinde['properties']['g_name']
    sql = "insert into gemeinde (id, name, grenze) values (" + str(j_gid) + ", '" + j_name + "', MPolyFromText('MULTIPOLYGON(("
    j_geom = j_gemeinde['geometry']
    mp = j_geom['type'] == 'MultiPolygon'
    cnt_polygon = 0
    def handle_polygon(p_pointlist):
        global cnt_polygon, sql
        if cnt_polygon:
            sql += ","
        cnt_polygon += 1
        sql += "("
        cnt_point = 0
        for point in p_pointlist:
            if cnt_point:
                sql += ","
            cnt_point += 1
            sql += str(point[1]) + " " + str(point[0])
        sql += ")"
    j_mp_list = j_geom['coordinates']
    # coordinates haben in geojson die Form (mit Punkt = [HW RW] ):
    # [ [ [ Punkt... ], [ Punkt... ] ] ] für Ringe mit Löchern
    # [ [ [ Punkt... ] ], [ [ Punkt... ] ] ] für disjunkte Ringe
    # [ [ [ Punkt... ] ] ] wenn nur 1 Ring ohne Loch
    # disjunkte Ringe mit Löchern kommen in AT nicht vor
    # wir müssen also eine Klammernebene wegnehmen, aber die Ebene ist nicht immer gleich
    if mp:
        if len(j_mp_list) == 1:
            for p in j_mp_list[0]:
                handle_polygon(p)
        else:
            for p in j_mp_list:
                handle_polygon(p[0])
    else:
        handle_polygon(j_mp_list[0])
    sql += "))', 4326));"
    print(sql)
