#!/usr/local/bin/python3.9

import re

id=0
parents=[]
check_for_lit = True
#f = open("dataset-9880.txt", "r")
f = open("col.txt", "r")
print("truncate table col;")
print("set autocommit=off;")
while line:=f.readline():
	if res := re.match(r'( *)(\*?)([^ ][^[]*) \[(\w+)\]', line):
		#print("Syn: "+ res.group(2) + ' text: ' + res.group(3) + ' kat: ' + res.group(4))
		indent = len(res.group(1)) >> 1
		syn = res.group(2) == '*'
		text = res.group(3)
		rank = res.group(4)

		# Fehler in Importdatei
		if text == "'Gammarus'":
			text = "Gammarus"
		elif text == "Simulium)":
			text = "Simulium"
		elif text == "?genus (Macroscaphosoma) Pic, 1928":
			text = "Macroscaphosoma Pic, 1928"
		#elif text == "triplacidiids":
		#	text = "Triplacidiids"
		#elif text == "pedinopsids":
		#	text = "Pedinopsids"
		else:
			text = text.replace("'Cichlasoma'", "Cichlasoma")
		text = text.replace("\u00A0", " ").replace("  "," ").replace("Noctiluca(e)","Noctilucæ")

		#if indent == 0:
		#	if text == 'Viruses':
		#		break # Virologen interessiert unsere Funddatenbank nicht. und alle anderen machen nichts mit Viren
		#	continue # Biota
		if indent == 0 and text == 'Viruses':
			check_for_lit = False
		taxon = text
		if indent == 0:
			subgenus_in_klammern = False
		elif rank == 'kingdom':
			subgenus_in_klammern = text not in ('Fungi','Plantae')
		lit = ''
		if check_for_lit:
			if rank == 'subgenus' and subgenus_in_klammern:
				if res := re.match(r'((?:\w|†|-)+) \(([A-Za-z]+)\) (.*)', text):
					# bei Tieren steht der Subgenus in der Klammer, das davor ist der Genus
					taxon = res.group(2)
					lit = res.group(3)
				elif res := re.match(r'((?:\w|†|-)+) (.*)', text):
					taxon = res.group(1)
					lit = res.group(2)
				else:
					taxon = text
			else:
				if rank == 'species' and subgenus_in_klammern:
					if res := re.match(r'((?:\w|†|-)+) \([A-Z][a-z-]+\) ([a-z-].*)', text):
						# bei Tieren steht der Subgenus in der Klammer => bei der Art rausnehmen
						text = res.group(1) + ' ' + res.group(2)
				if res := re.match(r'((?:× )?(?:\w|†|-)+) (.*)', text):
					#print("1: text="+text+" group1="+res.group(1)+" group2="+res.group(2))
					taxon = res.group(1)
					while re.match (r'(?:× [a-zA-Z][a-z]*(?:-[a-z]+)*|[a-z]+(?:-[a-z]+)*|(?:subsp|nothosubsp|var|f|ab)\.)(?: |$)', res.group(2)) and not re.match(r'd[aeio] ', res.group(2)):
						#print("2: text="+text+" group1="+res.group(1)+" group2="+res.group(2))
						if re.match(r'\(', res.group(2)):
							#print("3")
							break
						text = res.group(2)
						if res := re.match(r'((?:× )?(?:\w|-)+\.?) (.*)', text):
							#print("4")
							taxon += ' ' + res.group(1)
						else:
							#print("5")
							taxon = text
							break
					if res:
						lit = res.group(2)
		lit = lit.replace("'", r"\'")
		#print("taxon: "+taxon+" lit.: "+lit)
		id += 1
		parent = str(parents[indent - 1]) if indent else "null"
		print("insert into col(id,taxon,rank,parent,syn,lit) values(" + str(id) + ",'" + taxon + "','" + rank + "'," + parent  + "," + ("1" if syn else "0") + "," + ("'" + lit + "'" if lit else "null") + ");")
		if id % 1000 == 0:
			print("commit;")
		if len(parents) <= indent:
			parents.append(id)
		else:
			parents[indent] = id
print("commit;")
f.close()
