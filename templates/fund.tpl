<%
  def esc_js_str(p_s):
    return s['esc_js_str'](p_s)
  def If(p_cond, p_val1, p_val2 = ''):
    return p_val1 if p_cond else p_val2
  def Unless(p_cond, p_val1, p_val2 = ''):
    return p_val2 if p_cond else p_val1
%>\
<head>
  <title>Höhlen- und biologische Funddatenbank</title>
  <meta name="viewport" content="width=device-width,initial-scale=1" id="VIEWPORT"/>
  % if 'karte' in s:
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin=""/>
  % if s['karte']['showlocate']:
  <link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.79.0/dist/L.Control.Locate.min.css">
  % endif
  <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
  % if s['karte']['showlocate']:
  <script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.79.0/dist/L.Control.Locate.min.js"></script>
  % endif
  <% cache = s['useropts'].get('MapCache') %>\
  % if cache:
  <script src="https://unpkg.com/pouchdb@^5.2.0/dist/pouchdb.js"></script>
  <script src="https://unpkg.com/leaflet.tilelayer.pouchdbcached@latest/L.TileLayer.PouchDBCached.js"></script>
  % endif
  % if s['koopick']:
  <script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.9.0/proj4.js"></script>
  % endif
  % endif
  <script>
    function openlink(p_url, p_newwindow=false) {
      if (p_newwindow)
        window.open(p_url, '_blank');
      else
        location.href = p_url;
    }
    ##function scroll_to_bottom() {
    ##  window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: "smooth" });
    ##}
    function scroll_to_id(p_id, p_smooth=true) {
      var element = document.getElementById(p_id);
      if (element)
        element.scrollIntoView(p_smooth ? {behavior: "smooth"} : {});
    }
    function siteinfo() {
      alert(
        'Höhlen- und biologische Funddatenbank\n' +
        'designt und programmiert ab Feb 2023 von Friedrich Volkmann (www.volki.at)\n' +
        'Scriptsprachen: Python3, Javascript\n' +
        'Datenbank: MariaDB\n\n' +
        'Der Sourcecode steht unter GPLv3 und ist auf https://gitlab.com/fkv1/biodb verfügbar.\n' +
        'Die Höhlendaten sind zu einem guten Teil Eigentum des Landesvereins für Höhlenkunde in Wien und NÖ, aber mit Ausnahme der Lageangaben zur Veröffentlichung freigegeben.\n' +
        'Die Katastergruppengrenzen sind z.T. ©VÖH, vgl. https://hoehle.org/hoehlenverzeichnis, und ebenfalls zur Veröffentlichung freigegeben.'
      );
    }
    % if not s['user']:
    function reginfo() {
      alert('Registrierung bis auf weiteres durch Mail an web' + String.fromCharCode(1<<6) + 'volki.at oder indem ihr mich (Friedrich Volkmann) persönlich kontaktiert (Kontaktdaten siehe www.volki.at). Schreibt mir bitte einen Wunsch-Loginnamen (z.B. Initialen) und wenn ihr möchtet ein Anfangspasswort (könnt ihr später ändern). Wenn es sein kann, dass ich euch (oder zumindest euren Namen) noch nicht kenne, dann schreibt mir bitte auch, wer ihr seid und warum ihr euch registriert. Der Aufwand dient dazu, dass sich keine Spammer oder Verrückten registrieren.');
    }
    % endif
    function closemenu(num) {
      document.getElementById("MENU"+num).style.display="none";
    }
    function closemenus() {
      window.removeEventListener('keydown', win_keydown);
      window.removeEventListener('click', win_click);
      for (i=1; i<=${len(s['menu'])}; i++)
        closemenu(i);
    }
    function win_keydown(e) {
      if (e.code == 'Escape')
        closemenus();
    }
    function win_click(e) {
      closemenus();
    }
    function openmenu(num) {
      closemenus();
      document.getElementById("MENU"+num).style.display="block";
      window.addEventListener('keydown', win_keydown);
      window.addEventListener('click', win_click);
    }
    function flipmenu(num) {
      if (document.getElementById('MENU' + num).style.display=='none')
        openmenu(num);
      else {
        closemenu(num);
        window.removeEventListener('keydown', win_keydown);
        window.removeEventListener('click', win_click);
      }
    }
    function openlogindlg() {
      document.getElementById('LOGDIV').style.display='inline-block';
      document.getElementById('LOGDIV').focus();
    }
    function opencfg() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&CFG=1';
    }
    % if s['admin']:
    function rechteakt() {
      if (!confirm("Rechtematrix aktualisieren: Diese Funktion sollte nach Änderungen an Gruppen oder Gruppenzugehörigkeiten aufgerufen werden, damit die Änderungen an den Leserechten auf Ortsdaten wirksam werden. Das kann aber ein paar Minuten dauern, und währenddessen funktioniert die Berechtigungsabfrage für die User nicht, die gerade online sind. Keinesfalls starten, wenn der Prozess bereits läuft. Jetzt starten? (Dann bitte warten, bis die Fertig-Meldung kommt.)"))
        return;
      const oldcursor = document.body.style.cursor;
      document.body.style.cursor = 'progress';
      var url = '${s['scriptname']}?SESS=${s['sessionid']}&RECHTEAKT=1';
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var resptext = this.responseText.trim();
        document.body.style.cursor = oldcursor;
        if (resptext == 'ok') {
          alert('Fertig.');
          return true;
        }
        alert('Resp:'+resptext);
        alert('Schiefgegangen, Erklärung vielleicht in Error-Log".');
        return false;
      }
      xhr.onerror = function() {
        alert('Schiefgegangen, Server nicht erreichbar?');
      }
      xhr.open('GET', url, false); // synchron
      xhr.send();
    }
    % endif
    function openuser(p_id) {
      % if s['user']:
      window.open('${s['scriptname']}?SESS=${s['sessionid']}&SELUSER=' + p_id, 'USER'+p_id, 'height=600,width=800,menubar=no,toolbar=no,location=no,personalbar=no,status=no,resizable=yes,dependent=yes,minimizable=yes,titlebar=no');
      % else:
      alert('Um persönliche Daten einzusehen, bitte einloggen. Mit dieser Restriktion sollen sie vor Bots (z.B. zum Sammeln von Mailadressen für Spamming) verborgen werden.');
      % endif
    }
    function opengrp(p_id) {
      % if s['user']:
      window.open('${s['scriptname']}?SESS=${s['sessionid']}&SELGRP=' + p_id, 'GRP'+p_id, 'height=400,width=800,menubar=no,toolbar=no,location=no,personalbar=no,status=no,resizable=yes,dependent=yes,minimizable=yes,titlebar=no');
      % else:
      alert('Um Benutzergruppen einzusehen, bitte einloggen.');
      % endif
    }
    function openverein(p_id) {
      window.open('${s['scriptname']}?SESS=${s['sessionid']}&SELVEREIN=' + p_id, 'VRN'+p_id, 'height=600,width=800,menubar=no,toolbar=no,location=no,personalbar=no,status=no,resizable=yes,dependent=yes,minimizable=yes,titlebar=no');
    }
    function userlist() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&USERLIST=1';
    }
    function grplist() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&GRPLIST=1';
    }
    function dliste() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&DLIST=1';
    }
    function ddetails(p_id,p_v) {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_DET_ID='+p_id+'&D_DET_V='+p_v;
    }
    function publist() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&PLIST=1';
    }
    function openpub(p_id=0) {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&PUB_ID='+p_id;
    }
    function hliste(p_auto) {
      // p_auto: ob Liste gleich ausgeben soll oder erst mal nur Suchformular
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&HLISTE='+(p_auto?'1':'2')+'&LOADDEFAULT=1';
    }
    function katgrlist() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&KATGRLIST=1';
    }
    function vrnlist() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&VRNLIST=1';
    }
    function taxbaum() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&TAXBAUM=1';
    }
    function ranglist() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&RANGLIST=1';
    }
    function bliste() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&BLISTE=1&LOADDEFAULT=1';
    }
    function aliste() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&ALISTE=1';
    }
    function d_upl() {
      location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_UPL=1';
    }
    % if 'verkl_redu' in s:
    function verkl_save_default(p_form) {
      let p='';
      for (const i of p_form.elements) {
        if (i.name=='VERKL_SKAL')
          p += '&VERKL_SKAL=' + (i.checked ? '1' : '0');
        else if (i.name.startsWith('VERKL_'))
          p += '&' + i.name + '=' + i.value;
      }
      let url = '${s['scriptname']}?SESS=${s['sessionid']}&VERKL_DFLT=1'+p;
      let xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText == 'OK')
          alert('Default gespeichert.');
        else
          alert('Fehler beim Speichern als Default: ' + this.status + '/' + this.statusText);
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Speichern als Default: ' + e.message);
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Speichern als Default');
      }
      xhr.open('GET', url, true); // asynchron
      xhr.send();
    }
    % endif
    % if 'dliste' in s:
    function do_hd_zuo(p_form) {
      if (!p_form.HD_ZUO_H.value) {
        alert('keine Höhle ausgewählt');
        return false
      }
      let idset = new Set();
      for (const c of document.getElementsByTagName('input'))
        if (c.name.substring(0,4)=='DCHK' && c.checked)
          idset.add(c.name.substring(4,c.name.indexOf('_')));
      if (!idset.size) {
        alert('keine Datei ausgewählt');
        return false
      }
      let p=''
      for (const id of idset)
          p += '&HD_ZUO_D=' + id;
      let url = '${s['scriptname']}?SESS=${s['sessionid']}&HD_ZUO_H=' + p_form.HD_ZUO_H.value + '&HD_ZUO_K=' + p_form.HD_ZUO_K.value + p;
      let xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText == 'OK')
          alert(this.responseText);
        else
          alert('Fehler beim Zuordnen: HTTP-Status ' + this.status + '/' + this.statusText);
        document.getElementById('HD_ZUO_BTN').disabled = false;
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Zuordnen: ' + e.message);
        document.getElementById('HD_ZUO_BTN').disabled = false;
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Zuordnen');
        document.getElementById('HD_ZUO_BTN').disabled = false;
      }
      document.getElementById('HD_ZUO_BTN').disabled = true;
      xhr.open('GET', url, true); // asynchron
      xhr.send();
    }
    function do_fd_zuo(p_form, p_besuchid, p_fund_i) {
      let idset = new Set();
      for (const c of document.getElementsByTagName('input'))
        if (c.name.substring(0,4)=='DCHK' && c.checked)
          idset.add(c.name.substring(4,c.name.indexOf('_')));
      if (!idset.size) {
        alert('keine Datei ausgewählt');
        return false
      }
      let p=''
      for (const id of idset)
          p += '&FD_ZUO_D=' + id;
      let url = '${s['scriptname']}?SESS=${s['sessionid']}&FD_ZUO_B=' + p_besuchid + '&FD_ZUO_I=' + p_fund_i + (p_form.FD_ZUO_H.checked ? '&FD_ZUO_H=1' : '') + p;
      let xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText == 'OK')
          alert(this.responseText);
        else
          alert('Fehler beim Zuordnen: HTTP-Status ' + this.status + '/' + this.statusText);
        document.getElementById('FD_ZUO_BTN').disabled = false;
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Zuordnen: ' + e.message);
        document.getElementById('FD_ZUO_BTN').disabled = false;
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Zuordnen');
        document.getElementById('FD_ZUO_BTN').disabled = false;
      }
      document.getElementById('FD_ZUO_BTN').disabled = true;
      xhr.open('GET', url, true); // asynchron
      xhr.send();
    }
    function do_pd_zuo(p_form, p_pub_id) {
      let idset = new Set();
      let maxv = []
      for (const c of document.getElementsByTagName('input'))
        if (c.name.substring(0,4)=='DCHK' && c.checked) {
          const id = Number(c.name.substring(4,c.name.indexOf('_')));
          const v = Number(c.name.substring(c.name.indexOf('_')+1));
          idset.add(id);
          if (!(id in maxv) || maxv[id] < v)
            maxv[id] = v;
        }
      if (!idset.size) {
        alert('keine Datei ausgewählt');
        return false
      }
      let p=''
      for (const id of idset)
        p += '&PD_ZUO_D=' + id + '_' + maxv[id];
      let url = '${s['scriptname']}?SESS=${s['sessionid']}&PD_ZUO_P=' + p_pub_id + '&PD_ZUO_ABSCHNITT=' + p_form.PD_ZUO_ABSCHNITT.value + p;
      let xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText == 'OK')
          alert(this.responseText);
        else
          alert('Fehler beim Zuordnen: HTTP-Status ' + this.status + '/' + this.statusText);
        document.getElementById('PD_ZUO_BTN').disabled = false;
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Zuordnen: ' + e.message);
        document.getElementById('PD_ZUO_BTN').disabled = false;
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Zuordnen');
        document.getElementById('PD_ZUO_BTN').disabled = false;
      }
      document.getElementById('PD_ZUO_BTN').disabled = true;
      xhr.open('GET', url, true); // asynchron
      xhr.send();
    }
    % elif 'd_det_id' in s:
    <%
    js_str = ''
    for h in s['hoehlen']:
      optname = str(h['id'])
      if h['name']:
        optname += ', ' + h['name']
      if h['katnr']:
        optname = h['katnr'] + ' (' + optname + ')'
      js_str += "{'id':" + str(h['id']) + ",'t':'" + esc_js_str(optname) + "'},"
    %>\
    const dhoehlen = [${js_str.rstrip(',')}]
    var dnewhoe_idx = 0
    function add_dathoe() {
      dnewhoe_idx++;
      const parent_span = document.getElementById('D_DET_NEWKAT');
      const btn = document.getElementById('D_DET_NEWKAT_BTN');
      const span = parent_span.appendChild(document.createElement('span'));
      parent_span.appendChild(btn);
      let sel = document.createElement('select');
      sel.name = 'D_DET_NEWH'+dnewhoe_idx;
      sel.style.marginRight = '5px';
      let opt = document.createElement('option');
      opt.value = '';
      opt.textContent = 'Höhle auswählen...';
      sel.appendChild(opt);
      for (const h of dhoehlen) {
        opt = document.createElement('option');
        opt.value = h['id'];
        opt.textContent = h['t'];
        sel.appendChild(opt);
      }
      span.appendChild(sel);
      span.appendChild(document.createTextNode('als'));
      sel = document.createElement('select');
      sel.name = 'D_DET_NEWK'+dnewhoe_idx;
      sel.style.marginLeft = '5px';
      sel.style.marginRight = '2em';
      opt = document.createElement('option');
      opt.value = '';
      opt.textContent = 'nicht zugeordnet';
      opt.selected = true;
      sel.appendChild(opt);
      opt = document.createElement('option');
      opt.value = 'N';
      opt.textContent = 'Foto/Video/Audio';
      sel.appendChild(opt);
      opt = document.createElement('option');
      opt.value = 'P';
      opt.textContent = 'Plan';
      sel.appendChild(opt);
      opt = document.createElement('option');
      opt.value = 'B';
      opt.textContent = 'Bericht';
      sel.appendChild(opt);
      span.appendChild(sel);
    }
    function chk_newkat(p_form) {
      for (let i=1; i<=dnewhoe_idx; i++) {
        const s=p_form['D_DET_NEWH'+i];
        dop = false;
        % if len(s['d_det_hoehlen']):
        if ([${','.join("'"+str(h)+"'" for h in s['d_det_hoehlen'])}].includes(s.value))
          dop = true;
        % endif
        for (let j=1; j<i; j++)
          if (p_form['D_DET_NEWH'+j].value == s.value)
            dop = true;
        if (dop) {
          alert('Höhle mit ID='+s.value+' ist mehrfach angegeben, darf nur 1x zugeordnet werden');
          s.focus()
          return false;
        }
      }
      return true;
    }
    % elif 'hoehle' in s and s['hoehle']['schreibrecht']:
    var gen_maxind = ${len(s['hoehle']['gen'])};
    const genfrag = `
      ## Achtung: Backslashes müssen hier verdoppelt werden
      <td>
        <span style="margin-right:1em" title="Entstehung eines oder mehrerer Höhlenteile. Es können sich mehrere Angaben auf den selben Höhlenteil beziehen (und ihre Ganglängen in Summe die Gesamtlänge der Höhle übersteigen), z.B. bei einem in einen Keller umgewandelten Erdstall. Naturhöhle bitte nur dann auswählen, wenn mindestens ein zusammenhängender, natürlicher Höhlenteil katasterwürdige Größe (d.h. mind. 5 m) aufweist, denn kleinere natürliche Teile lassen sich in fast jeder Höhle finden.">
          <select name="GEN_GENESE#IDX">
            <option value="">bitte auswählen</option>
            % for g in s['hoehle']['genesen']:
            <option value="${g}">${'&nbsp;'*4*(len(g)-1)}${s['hoehle']['genesen'][g] |h}</option>
            % endfor
          </select>
          &#x2139;&#xFE0F;
        </span>
      </td><td>
        <span style="margin-right:1em" title="falls mehrere: 1 = dominant, 2 = weniger, usw., leer = am wenigsten">
          Dominanz
          <input type="number" name="GEN_PRIO#IDX" min="1" max="255" size="3" onchange="this.form.GEN_GENESE#IDX.required=this.value>''||this.form.GEN_PHASE#IDX.value>''||this.form.GEN_L#IDX.value>''">
          &#x2139;&#xFE0F;
        </span>
      </td><td>
        <span style="margin-right:1em" title="1 = älteste, 2 = zweitälteste usw.">
          Phase
          <input type="number" name="GEN_PHASE#IDX" min="1" max="255" size="3" onchange="this.form.GEN_GENESE#IDX.required=this.value>''||this.form.GEN_PRIO#IDX.value>''||this.form.GEN_L#IDX.value>''">
          &#x2139;&#xFE0F;
        </span>
      </td><td>
        <span title="Die Angabe der Ganglänge erübrigt sich, wenn die ganze Höhle auf die selbe Weise entstanden ist.">
          Ganglänge
          <input type="text" name="GEN_L#IDX" size="6" pattern="^(\\d{1,7}(\\.\\d?)?|\\d{0,7}\\.\\d)$" onchange="this.form.GEN_GENESE#IDX.required=this.value>''||this.form.GEN_PRIO#IDX.value>''||this.form.GEN_PHASE#IDX.value>''">
          &#x2139;&#xFE0F;
        </span>
      </td>
      <td>
        <input type="button" value="reset" style="margin-left:1em" onclick="this.form.GEN_GENESE#IDX.value='';this.form.GEN_PRIO#IDX.value='';this.form.GEN_PHASE#IDX.value='';this.form.GEN_L#IDX.value='';this.form.GEN_GENESE#IDX.required=false">
      </td>`;
    function add_genese() {
      const tbl = document.getElementById('GENTBL');
      const tr = document.createElement('tr');
      tr.innerHTML = genfrag.replaceAll('#IDX', ++gen_maxind);
      tbl.appendChild(tr);
      document.getElementsByName('GEN_GENESE'+gen_maxind)[0].focus();
    }
    function check_hoe() {
      const f=document.forms.HOEFRM;
      for (let i=1; i<gen_maxind; i++) {
        const el1 = f['GEN_GENESE'+i];
        if (!el1.disabled && el1.value)
          for (let j=i+1; j<=gen_maxind; j++) {
            const el2 = f['GEN_GENESE'+j];
            if (!el2.disabled && el1.value==el2.value) {
              alert('Wert ('+el2.selectedOptions[0].label.trim()+') darf nur 1x angegeben werden. Bei unterschiedlichen Höhlenteilen mit selber Entstehung die Ganglängen ggf. zusammenzählen.');
              el2.focus();
              return false;
            }
          }
      }
      return true;
    }
    % endif
    % if 'taxbaum' in s:
    function escapeHTML(p_str) {
      return p_str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }
    <%
      #tb_plus = "\u2795" # großes  Plus ohne Rahmen
      #tb_minus = "\u2796" # großes Minus ohne Rahmen
      tb_plus = "\u229E" # Plus mit Rahmen (&boxplus; oder &plusb;)
      tb_minus = "\u229F" # Minus mit Rahmen (&boxminus)
      #tb_sanduhr = "\u231B" # stehende Sanduhr
      tb_sanduhr = "\u23F3" # laufende Sanduhr
    %>\
    const uebernehmbare_ranks = {
    % for r in s['raenge']:
      % if r['auswaehlbar']:
      '${r['col']}': ${r['ebene']}${Unless(loop.last, ',')}
      % endif
    % endfor
    };
    function tb_uebern(p_id, p_ebene) {
      function tb_get_parent_id(p_id) {
        const pnode = document.getElementById('TB_' + p_id).parentNode;
        if (pnode.hasAttribute('id') && pnode.id.slice(0,3) == 'TB_' && pnode.id.slice(-2) == '_C')
          return pnode.id.slice(3,-2);
        return 0;
      }
      function tb_get_path(p_id) {
        if (!p_id)
          return '';
        let path = tb_get_path(tb_get_parent_id(p_id));
        const rank = document.getElementById('TB_' + p_id + '_RANK').innerText;
        if (rank in uebernehmbare_ranks) {
          if (path > '')
            path += '|';
          path += uebernehmbare_ranks[rank] + ':' + document.getElementById('TB_' + p_id + '_TAXON').innerText;
        }
        return path;
      }
      if ('BLIST_FRM' in opener.document.forms) {
        opener.document.forms.BLIST_FRM.BLIST_RANG.value = p_ebene <= ${s['gattungsebene']} ? ${s['artebene']} : p_ebene;
        opener.document.forms.BLIST_FRM.BLIST_TAXON.value = document.getElementById('TB_' + p_id + '_TAXON').innerText;
      } else {
        opener.document.forms.BESTFRM.BEST_RANG.value = p_ebene;
        opener.document.forms.BESTFRM.BEST_EPITHETON.value = '';
        opener.document.forms.BESTFRM.BEST_TAXON.value = document.getElementById('TB_' + p_id + '_TAXON').innerText;
        opener.document.forms.BESTFRM.BEST_PFAD.value = tb_get_path(p_id);
        opener.adj_ergebnis();
        opener.remove_epitheton(opener.document.forms.BESTFRM.BEST_TAXON);
        opener.pfad2displ();
      }
      opener.focus();
      window.close();
    }
    function set_tb_content(p_id, p_children) {
      var html = '';
      var indent_parent = document.getElementById('TB_'+p_id+'_INDENT').textContent;
      for (const [z_i, zeile] of p_children.entries()) {
        var indent = indent_parent;
        loeschen_ab = indent.indexOf('└');
        if (loeschen_ab == -1) {
          loeschen_ab = indent.indexOf('├');
          if (loeschen_ab > -1) {
            indent = indent.substring(0,loeschen_ab) + '│';
            loeschen_ab ++;
          }
        } else
          indent = indent.substring(0,loeschen_ab);
        if (loeschen_ab > -1) {
          indent += '&nbsp;'.repeat(indent_parent.length - loeschen_ab);
        }
        if (zeile.syn)
          html += '<div id="TB_' + zeile.id + '" class="tb tb_syn_div"><span class="tb_indent">' + indent + (p_children[p_children.length-1].syn ? '&nbsp;' : '│') + '</span> <span class="tb_syn">= ' + escapeHTML(zeile.taxon) + '</span>' + (zeile.lit ? ' <span class="tb_lit">' + escapeHTML(zeile.lit) + '</span>' : '') + '</div>\n';
        else {
          indent += (z_i == p_children.length - 1 ? '└' : '├') + '─'; // ─ ist &boxh;
          html += '<div id="TB_' + zeile.id + '" class="tb"><span onclick="flip_tb(' + zeile.id + ')"><span id="TB_' + zeile.id + '_INDENT" class="tb_indent">' + indent + '</span><span id="TB_SYM_' + zeile.id + '" class="tb_indent';
          if (zeile.anzsyn && ! zeile.anzc)
            html += ' tb_syn';
          <%
            uebernstr = ''
            if s['taxpick']:
              uebernstr = """ + (zeile.rank in uebernehmbare_ranks ? '<input type="button" class="tb_btn" value="übernehmen" onclick="tb_uebern(' + zeile.id + ',' + uebernehmbare_ranks[zeile.rank] + ')">' : '')"""
          %>\
          html += '">' + (zeile.anzc || zeile.anzsyn ? '${tb_plus}' : '─') + '</span> <span id="TB_' + zeile.id + '_TAXON">' + escapeHTML(zeile.taxon) + '</span>' + (zeile.lit ? ' <span class="tb_lit">'+escapeHTML(zeile.lit)+'</span>' : '') + (zeile.rank ? ' <span class="tb_rank">[<span id="TB_' + zeile.id + '_RANK">'+escapeHTML(zeile.rank)+'</span>]</span>'${uebernstr} : '') + '</span><div id="TB_' + zeile.id + '_C"></div></div>';
        }
      }
      document.getElementById('TB_'+p_id+'_C').innerHTML = html;
      document.getElementById('TB_SYM_'+p_id).innerText = '${tb_minus}';
    }
    function get_tb_content(p_id) {
      var tp_url = '${s['scriptname']}?TAXID=' + p_id;
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText != 'OK') {
          alert('Fehler beim Laden des Taxons: HTTP-Status ' + this.status + '/' + this.statusText);
          document.getElementById('TB_SYM_'+p_id).innerText = '${tb_plus}';
          return;
        }
        set_tb_content(p_id, JSON.parse(this.responseText));
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Laden des Taxons:' + e.message);
        document.getElementById('TB_SYM_'+p_id).innerText = '${tb_plus}';
      }
      xhr.timeout = 10000;
      xhr.ontimeout = function() {
        alert('Timeout beim Laden des Taxons');
        document.getElementById('TB_SYM_'+p_id).innerText = '${tb_plus}';
      }
      xhr.open('GET', tp_url, true); // asynchron
      xhr.send();
    }
    function taxsearch_ausbl(p_ausbl) {
      flip_classdisplay(!p_ausbl, 'tb');
    }
    function taxsearch() {
      const f = document.forms.TAXSEARCH_FRM;
      for (let i=0; i<5; i++)
        if (f.TAXSEARCH.value.charAt(i) == '*') {
          alert('Stern (Wildcard) erst ab der 6. Stelle erlaubt');
          return;
        }
      function taxsearch_enable(p_enable=true) {
        document.getElementById('TAXSEARCH_SUBM').disabled = !p_enable;
        document.getElementById('TAXSEARCH_RESET').disabled = !p_enable;
      }
      taxsearch_enable(false);
      var tp_url = '${s['scriptname']}?TAXSEARCH=' + encodeURIComponent(f.TAXSEARCH.value) + '&TAXSEARCH_RANG=' + encodeURIComponent(f.TAXSEARCH_RANG.value) + '&TAXSEARCH_SYN=' + (f.TAXSEARCH_SYN.checked ? '1' : '');
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText != 'OK') {
          alert('Fehler beim Laden der Ergebnisliste: HTTP-Status' + this.status + '/' + this.statusText);
          taxsearch_enable();
          return;
        }
        var json = JSON.parse(this.responseText);
        function set_parents_recursive(p_parents) {
          if (!p_parents)
            return;
          set_parents_recursive(p_parents.parent);
          const id = p_parents.id;
          const symspan = document.getElementById('TB_SYM_'+id);
          if (symspan.innerText != '${tb_minus}') { // wenn nicht schon geöffnet
            const childdiv = document.getElementById('TB_'+id+'_C');
            if (childdiv.innerHTML == '')
              set_tb_content(id, p_parents.children);
            else {
              symspan.innerText = '${tb_minus}';
              childdiv.style.display='block';
            }
          }
        }
        if (!json.length)
          alert(document.forms.TAXSEARCH_FRM.TAXSEARCH_SYN.checked ? 'nicht gefunden' : 'nicht gefunden, vielleicht in Synonymen?');
        else {
          for (const endnode of json) {
            set_parents_recursive(endnode.parent);
          }
          alert(json.length + ' Eintr' + (json.length == 1 ? 'ag' : 'äge') + ' gefunden');
          document.getElementById('TAXSEARCH_AUSBL').checked = true;
          tb_cleanup_classes();
          function display_recursive(p_node) {
            document.getElementById('TB_'+p_node.id).classList.add('tb_display_always');
            if (p_node.parent)
              display_recursive(p_node.parent);
          }
          for (const endnode of json) {
            display_recursive(endnode);
            // Node hervorheben
            document.getElementById('TB_'+endnode.id).classList.add('tb_selected');
          }
          taxsearch_ausbl(true);
        }
        taxsearch_enable();
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Laden der Ergebnisliste:' + e.message);
        taxsearch_enable();
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Laden der Ergebnisliste');
        taxsearch_enable();
      }
      xhr.open('GET', tp_url, true); // asynchron
      xhr.send();
    }
    function tb_cleanup_classes() {
      for (const el of document.getElementsByClassName('tb_display_always'))
        el.classList.remove('tb_display_always');
      for (const el of document.getElementsByClassName('tb_selected'))
        el.classList.remove('tb_selected');
    }
    function taxsearch_reset() {
      taxsearch_ausbl(false);
      tb_cleanup_classes();
      document.getElementById('TAXSEARCH_AUSBL').checked = false;
    }
    function flip_classdisplay(p_show, p_class) {
      for (const rule of document.getElementById('MAINSTYLE').sheet.cssRules)
        if (rule.selectorText == '.' + p_class)
          if (p_show)
            rule.style.removeProperty('display');
          else
            rule.style.setProperty('display','none');
          return;
    }
    function flip_lit(p_checked) {
      flip_classdisplay(p_checked, 'tb_lit');
    }
    function flip_rank(p_checked) {
      flip_classdisplay(p_checked, 'tb_rank');
    }
    function flip_syn(p_checked) {
      flip_classdisplay(p_checked, 'tb_syn_div');
    }
    function flip_tb(p_id) {
      var symspan = document.getElementById('TB_SYM_'+p_id);
      var childdiv = document.getElementById('TB_'+p_id+'_C');
      if (symspan.innerText == '${tb_plus}') {
        symspan.innerText = '${tb_sanduhr}';
        if (childdiv.innerHTML == '')
          get_tb_content(p_id);
        else {
          childdiv.style.display = 'block';
          symspan.innerText = '${tb_minus}';
        }
        // bewusstes Öffnen übersteuert Ausblenden-Checkbox
        document.getElementById('TAXSEARCH_AUSBL').checked = false;
        taxsearch_ausbl(false)
        document.getElementById('TB_'+p_id).scrollIntoView({behavior: 'smooth', block: 'nearest'});
      } else if (symspan.innerText == '${tb_minus}') {
        childdiv.style.display = 'none';
        symspan.innerText = '${tb_plus}'; // +
      }
    }
    % endif
    function anfrage(p_besuch_id, p_fund_i) {
      var url = '${s['scriptname']}?SESS=${s['sessionid']}&BESUCH_ID=' + p_besuch_id + '&FUND_I=' + p_fund_i + '&ANFRAGE_AN=' + document.forms.ANFRAGEFRM.ANFRAGE_AN.value + '&ANFRAGE_ANM=' + encodeURIComponent(document.forms.ANFRAGEFRM.ANFRAGE_ANM.value);
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var alerttext = 'Anfrage an ' + document.forms.ANFRAGEFRM.ANFRAGE_AN.options[document.forms.ANFRAGEFRM.ANFRAGE_AN.selectedIndex].text + ' gesendet';
        var resptext = this.responseText.trim();
        if (resptext == 'ok')
          alerttext += '.';
        else if (resptext.substring(0,9) == 'mailerr: ')
          alerttext += ', allerings ohne Mail wegen: ' + resptext.substring(9) + ')';
        else {
          alert('Anfrage schiefgegangen, falsche Parameter?');
          return false;
        }
        alert(alerttext);
        document.getElementById('ANFRAGEDIV').style.display = 'none';
        document.getElementById('BESTNEUBTNDIV').style.display='block';
        return true;
      }
      xhr.onerror = function() {
        alert('Anfrage schiefgegangen, Server nicht erreichbar?');
      }
      ## bei synchronen Requests ist debilerweise kein Timeout erlaubt
      ##xhr.timeout = 8000;
      ##xhr.ontimeout = function() {
      ##  alert('Timeout: keine Antwort vom Server nach 8 Sekunden');
      ##}
      xhr.open('GET', url, false); // synchron
      xhr.send();
    }
    function anleitung() {
      openlink('anleitung.html', true);
    }
    function lizenzen() {
      openlink('lizenzen.html', true);
    }
    function display_error(p_errm) {
      alert(p_errm);
    }
    function taxbaum_fuer_uebern(p_rang, p_taxon) {
      window.open('${s['scriptname']}?SESS=${s['sessionid']}&TAXBAUM=2&INITTAXSEARCH=' + encodeURIComponent(p_taxon) + '&INITTAXSEARCH_RANG=' + p_rang, 'TAXBAUM', 'height=800,width=1200,menubar=no,toolbar=no,location=no,personalbar=no,status=no,resizable=yes,scrollbars=yes,dependent=yes,minimizable=yes,titlebar=no');
    }
    function xlist_form2url(f) {
      var link = '${s['scriptname']}?SESS=${s['sessionid']}';
      for (const el of f.elements)
        if (el.name.substring(1,6) == 'LIST_' && !el.disabled && !((el.type=='checkbox' || el.type=='radio') && !el.checked))
          link += '&' + el.name + '=' + encodeURIComponent(el.value);
      return link
    }
    function openhoehle(p_hoehle_id=0, p_newwin=false) {
      openlink('${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID='+p_hoehle_id, p_newwin);
    }
    function openeingang(p_hoehle_id, p_eingang='', p_newwin=false) {
      openlink('${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID='+p_hoehle_id+'&REF='+p_eingang, p_newwin);
    }
    function openeingangort(p_hoehle_id, p_eingang, p_ort_id=0, p_newwin=false) {
      openlink('${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID='+p_hoehle_id+'&REF='+p_eingang+'&ORT_ID='+p_ort_id, p_newwin);
    }
    function openbesuch(p_id=0, p_newwin=false, p_duplicate=false) {
      openlink('${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID='+p_id + (p_duplicate ? '&DUP=1' : ''), p_newwin);
    }
    function openfund(p_besuchid, p_fund_i, p_newwin=false) {
      openlink('${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID='+p_besuchid+'&FUND_I='+p_fund_i, p_newwin);
    }
    function openbest(p_besuchid, p_fund_i, p_best_i, p_newwin=false) {
      openlink('${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID='+p_besuchid+'&FUND_I='+p_fund_i+'&BEST_I='+p_best_i, p_newwin);
    }
    % if 'cfg' in s:
    function adj_cfg_name_labels() {
      switch (document.forms.CFGFRM.NAME_STRUKTUR.value) {
        case 'VN': document.getElementById('NAME1_LABEL').textContent = 'Vorname(n)';
                   document.getElementById('NAME2_LABEL').textContent = 'Nachname';
                   return;
        case 'FG': document.getElementById('NAME1_LABEL').textContent = 'Familienname';
                   document.getElementById('NAME2_LABEL').textContent = '"Vorname"';
                   return;
        case 'PG': document.getElementById('NAME1_LABEL').textContent = 'Patronym';
                   document.getElementById('NAME2_LABEL').textContent = 'given name';
                   return;
        case 'GP': document.getElementById('NAME1_LABEL').textContent = 'given name';
                   document.getElementById('NAME2_LABEL').textContent = 'Patronym';
                   return;
        case 'R' : document.getElementById('NAME1_LABEL').textContent = 'Name';
                   document.getElementById('NAME2_LABEL').textContent = '';

      }
    }
    function check_mail_set() {
      if (!document.forms.CFGFRM.MAIL.value)
        alert('Mails bekommen Sie nur dann, wenn auch die Mailadresse versorgt ist.');
    }
    % endif
    % if 'eort' in s:
    function check_koo(p_f) {
      if ((p_f.ORT_KOOSYST.value || p_f.ORT_RW.value || p_f.ORT_HW.value) &&
          (!p_f.ORT_KOOSYST.value || !p_f.ORT_RW.value || !p_f.ORT_HW.value)
         ) {
        alert('Koo-System, RW und HW gemeinsam oder gar nicht erfassen');
        p_f.ORT_KOOSYST.focus();
        return false;
      }
      ## ein falscher Wert in der DB kann die Kartenanzeige für alle Höhlen kaputtmachen
      if (p_f.ORT_KOOSYST.value == 'G') {
        if (p_f.ORT_RW.value <= -180 || p_f.ORT_RW.value > 180) {
          alert ('RW (LON) nicht im erlaubten Bereich');
          p_f.ORT_RW.focus();
          return false;
        }
        if (p_f.ORT_HW.value < -90 || p_f.ORT_RW.value > 90) {
          alert ('HW (LAT) nicht im erlaubten Bereich');
          p_f.ORT_HW.focus();
          return false;
        }
      }
      return true;
    }
    % endif
    % if 'bliste' in s or 'besuch' in s or 'hlist_frm' in s or 'eort' in s:
    ## könnte man auch mit Ajax holen (hat Vor- und Nachteile);
    ## bei der Deklaration der riesen Arrays in JS müssen wir Mako von unnötigen Whitespaces abhalten
    const bl_bez = {
      % for bl in ('Burgenland', 'Kärnten', 'Niederösterreich', 'Oberösterreich', 'Salzburg', 'Steiermark', 'Tirol', 'Vorarlberg', 'Wien'):
      <%
        js_str = "'" + bl + "':{"
        for bezirk in s['bezirke']:
          if bezirk['bl'] == bl:
            js_str += "'" + str(bezirk['id']) + "':'" + bezirk['name'] + "',"
        js_str = js_str.rstrip(',') + '}'
      %>\
      ${js_str}${Unless(loop.last, ',')}
      % endfor
    }
    const bez_gde = {
      % for bezirk in s['bezirke']:
      <%
        js_str = "'" + str(bezirk['id']) + "':{"
        for gemeinde in s['gemeinden']:
          if gemeinde['bezirk_id'] == bezirk['id']:
            js_str += "'" + str(gemeinde['id']) + "':'" + gemeinde['name'] + "',"
        js_str = js_str.rstrip(',') + '}'
      %>\
      ${js_str}${Unless(loop.last, ',')}
      % endfor
    }
    function refresh_gemeinden(p_gemeinde_sel, p_bezirk) {
      for (const option of Array.from(p_gemeinde_sel.options))
        if (option.value && option.value!='L')
          option.remove()
      for (gde_id in bez_gde[p_bezirk])
        p_gemeinde_sel.add(new Option(bez_gde[p_bezirk][gde_id], gde_id));
    }
    function refresh_bezirke(p_bezirk_sel, p_bl) {
      for (const option of Array.from(p_bezirk_sel.options))
        if (option.value && option.value!='L')
          option.remove()
      for (bez_id in bl_bez[p_bl])
        p_bezirk_sel.add(new Option(bl_bez[p_bl][bez_id], bez_id));
    }
    function suchopts(p_form) {
      var f = p_form;
      function suchopts_json2form(p_json) {
        function setval(p_el, p_val) {
          if (p_el.tagName == 'INPUT' && (p_el.type == 'checkbox' || p_el.type == 'radio'))
            p_el.checked = p_el.value == p_val;
          else
            p_el.value = p_val;
        }
        for (let name in p_json)
          if (name in f)
            if (f[name].isArray)
              for (const el of f[name])
                setval(el, p_json[name])
            else
              setval(f[name], p_json[name])
      }
      function suchopts_form2json() {
        let json = {}
        for (let el of f.elements)
          if (el.name)
            if (el.tagName == 'INPUT')
              switch (el.type) {
                case 'checkbox':
                case 'radio':
                  if (el.checked) 
                    json[el.name] = el.value;
                  break;
                case 'button':
                case 'submit':
                case 'reset':
                  break;
                default:
                  json[el.name] = el.value;
              }
            else if (el.tagName == 'SELECT')
              json[el.name] = el.value;
        return json;
      }
      function suchopts_refresh_names(p_json) {
        const namesel = document.getElementById('SUCHOPT_NAME');
        const actionsel = document.getElementById('SUCHOPT_ACTION');
        const namesel_oldval = namesel.value;
        for (const option of Array.from(namesel.options))
          option.remove()
        for (const name of p_json)
          namesel.add(new Option(name, name));
        for (const option of actionsel.options)
          if (option.value!='N')
            option.style.display = p_json.length ? 'block' : 'none';
        if (p_json.length) {
          namesel.disabled = false;
        } else {
          actionsel.value = 'N';
          namesel.disabled = true;
        }
      }
      function suchopts_ajax_laden(p_name) {
        var url = '${s['scriptname']}?SESS=${s['sessionid']}&SUCHOPT_LADEN=' + encodeURIComponent(p_name) + '&FORM=' + f.name;
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
          var json = JSON.parse(this.responseText);
          suchopts_json2form(json.formdata);
        }
        xhr.onerror = function(e) {
          alert('Fehler beim Speichern des Suchprofils:' + e.message);
        }
        xhr.timeout = 5000;
        xhr.ontimeout = function() {
          alert('Timeout beim Speichern des Suchprofils');
        }
        xhr.open('GET', url, true); // asynchron
        xhr.send();
      }
      function suchopts_ajax_speichern(p_name, p_json) {
        var url = '${s['scriptname']}?SESS=${s['sessionid']}&SUCHOPT_SPEICHERN=' + encodeURIComponent(p_name) + '&FORM=' + f.name + '&JSON=' + encodeURIComponent(JSON.stringify(p_json));
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
          var json = JSON.parse(this.responseText);
          suchopts_refresh_names(json);
          document.getElementById('SUCHOPT_ACTION').value = 'S';
          document.getElementById('SUCHOPT_NAME').value = p_name;
        }
        xhr.onerror = function(e) {
          alert('Fehler beim Speichern des Suchprofils:' + e.message);
        }
        xhr.timeout = 5000;
        xhr.ontimeout = function() {
          alert('Timeout beim Speichern des Suchprofils');
        }
        xhr.open('GET', url, true); // asynchron
        xhr.send();
      }
      function suchopts_ajax_loeschen(p_name) {
        var url = '${s['scriptname']}?SESS=${s['sessionid']}&SUCHOPT_LOESCHEN=' + encodeURIComponent(p_name) + '&FORM=' + f.name;
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
          var json = JSON.parse(this.responseText);
          suchopts_refresh_names(json);
        }
        xhr.onerror = function(e) {
          alert('Fehler beim Speichern des Suchprofils:' + e.message);
        }
        xhr.timeout = 5000;
        xhr.ontimeout = function() {
          alert('Timeout beim Speichern des Suchprofils');
        }
        xhr.open('GET', url, true); // asynchron
        xhr.send();
      }
      let action = document.getElementById('SUCHOPT_ACTION').value;
      let namesel = document.getElementById('SUCHOPT_NAME');
      let name = namesel.value;
      switch (action) {
        case 'L': // laden
          suchopts_ajax_laden(name);
          return;
        case 'D': // delete
          suchopts_ajax_loeschen(name);
          return;
        case 'N': // neu
          let mit_default = false
          for (option of namesel.options)
            if (option.value == 'Default') {
              mit_default = true;
              break;
            }
          name = prompt('Name des neuen Suchprofils ("Default" wird automatisch geladen):', mit_default ? undefined : 'Default');
          if (!name)
            return;
          for (option of namesel.options)
            if (option.value == name) {
              if (!confirm('"' + option.value + '" existiert schon. Ersetzen?'))
                return
              break;
            }
          ## fallthrough
        default: // speichern
          suchopts_ajax_speichern(name, suchopts_form2json());
      }
    }
    % endif # 'bliste' in s or 'besuch' in s:
    % if 'hlist_frm' in s or 'bliste' in s:
    function adj_xlist_gemeinde() {
      var bezirk_sel = document.getElementById('XLIST_BEZIRK_SEL');
      if (bezirk_sel.style.display == 'none' || ! bezirk_sel.value || bezirk_sel.value == 'L') {
        document.getElementById('XLIST_GEMEINDE_LABEL').style.display = 'none';
        document.getElementById('XLIST_GEMEINDE_SEL').disabled = true;
        document.getElementById('XLIST_GEMEINDE_SEL').style.display = 'none';
      } else {
        refresh_gemeinden(document.getElementById('XLIST_GEMEINDE_SEL'), bezirk_sel.value);
        document.getElementById('XLIST_GEMEINDE_LABEL').style.display = 'inline-block';
        document.getElementById('XLIST_GEMEINDE_SEL').disabled = false;
        document.getElementById('XLIST_GEMEINDE_SEL').style.display = 'inline-block';
      }
    }
    function adj_xlist_bezirk() {
      var bl_sel = document.getElementById('XLIST_BL_SEL');
      if (bl_sel.style.display == 'none' || ! bl_sel.value || bl_sel.value == 'L') {
        document.getElementById('XLIST_BEZIRK_LABEL').style.display = 'none';
        document.getElementById('XLIST_BEZIRK_SEL').disabled = true;
        document.getElementById('XLIST_BEZIRK_SEL').style.display = 'none';
      } else {
        refresh_bezirke(document.getElementById('XLIST_BEZIRK_SEL'), bl_sel.value);
        document.getElementById('XLIST_BEZIRK_LABEL').style.display = 'inline-block';
        document.getElementById('XLIST_BEZIRK_SEL').disabled = false;
        document.getElementById('XLIST_BEZIRK_SEL').style.display = 'inline-block';
      }
      adj_xlist_gemeinde();
    }
    function adj_xlist_bl(p_land) {
      if (p_land == '' || p_land == 'L') {
        document.getElementById('XLIST_BL_LABEL').style.display = 'none';
        document.getElementById('XLIST_BL_TEXT').disabled = true;
        document.getElementById('XLIST_BL_TEXT').style.display = 'none';
        document.getElementById('XLIST_BL_SEL').disabled = true;
        document.getElementById('XLIST_BL_SEL').style.display = 'none';
      } else if (p_land == 'AT') {
        document.getElementById('XLIST_BL_LABEL').style.display = 'inline-block';
        document.getElementById('XLIST_BL_TEXT').disabled = true;
        document.getElementById('XLIST_BL_TEXT').style.display = 'none';
        document.getElementById('XLIST_BL_SEL').disabled = false;
        document.getElementById('XLIST_BL_SEL').style.display = 'inline-block';
      } else {
        document.getElementById('XLIST_BL_LABEL').style.display = 'inline-block';
        document.getElementById('XLIST_BL_TEXT').disabled = false;
        document.getElementById('XLIST_BL_TEXT').style.display = 'inline-block';
        document.getElementById('XLIST_BL_SEL').disabled = true;
        document.getElementById('XLIST_BL_SEL').style.display = 'none';
      }
      adj_xlist_bezirk();
    }
    % if 'bliste' in s:
    function check_bereiche(f) {
        if (f.BLIST_BEREICH_N.checked
            % for b in s['bereiche']:
            || f.BLIST_BEREICH_${b}.checked
            % endfor
           )
          return true;
        alert('bei den Bereichen mindestens eine Checkbox auswählen, sonst wäre die Ergebnisliste leer');
        f.BLIST_BEREICH_${list(s['bereiche'])[0]}.focus()
        return false;
    }
    % endif # 'bliste' in s
    % endif # 'hlist_frm' in s or 'bliste' in s
    <%
       ortdict = None
       if 'besuch' in s:
         ortdict = s['besuch']
         ortfrm = 'BESUCHFRM'
       elif 'eort' in s:
         ortdict = s['eort']
         ortfrm = 'ORTFRM'
    %>\
    % if ortdict:
    function ort_get_verwaltung() {
      var f = document.forms.${ortfrm};
      var epsg = 4326;
      switch (f.ORT_KOOSYST.value) {
        case 'M28': epsg=31257; break;
        case 'M31': epsg=31258; break;
        case 'M34': epsg=31259;
      }
      var adminAT_url = '${s['scriptname']}?BEZ_LON=' + f.ORT_RW.value + '&BEZ_LAT=' + f.ORT_HW.value + '&EPSG=' + epsg;
      var xhrAT = new XMLHttpRequest();
      xhrAT.onload = function() {
        var json = JSON.parse(this.responseText);
        if (json.in_AT) {
          if (f.ORT_LAND.value == 'AT') {
            if (document.getElementById('ORT_BL_SEL').value == json.bl) {
              if (f.ORT_BEZIRK.value != json.bezirk_id) {
                f.ORT_BEZIRK.value = json.bezirk_id;
                refresh_gemeinden(f.ORT_GEMEINDE, json.bezirk_id);
              }
            } else {
              document.getElementById('ORT_BL_SEL').value = json.bl;
              refresh_bezirke(f.ORT_BEZIRK, json.bl);
              f.ORT_BEZIRK.value = json.bezirk_id;
              refresh_gemeinden(f.ORT_GEMEINDE, json.bezirk_id);
            }
          } else {
            f.ORT_LAND.value = 'AT';
            document.getElementById('ORT_BL_SEL').value = json.bl;
            refresh_bezirke(f.ORT_BEZIRK, json.bl);
            f.ORT_BEZIRK.value = json.bezirk_id;
            refresh_gemeinden(f.ORT_GEMEINDE, json.bezirk_id);
          }
          f.ORT_GEMEINDE.value = json.gemeinde_id;
          adj_bl_attrs();
          return;
        }
        // Koo nicht in AT => Land/Bundesland von geonames.org holen
        var kooINT_url = (window.location.protocol.startsWith('https') ? 'https://secure' : 'http://api') + '.geonames.org/countrySubdivisionJSON?lang=de&username=${s['geonames_username']}&lng=' + json.lon + '&lat=' + json.lat;
        var xhrINT = new XMLHttpRequest();
        xhrINT.onload = function() {
          var json = JSON.parse(this.responseText);
          f.ORT_LAND.value = json.countryCode;
          document.getElementById('ORT_BL_TEXT').value = json.adminName1;
          adj_bl_attrs();
        }
        xhrINT.onerror = function() {
          alert('api.geonames.org nicht erreichbar');
        }
        xhrINT.timeout = 5000;
        xhrINT.ontimeout = function() {
          alert('Timeout beim Holen des Landes von geonames.org');
        }
        xhrINT.open('GET', kooINT_url, true); // asynchron
        xhrINT.send();
      }
      xhrAT.onerror = function() {
        alert('volki.at nicht erreichbar');
      }
      xhrAT.timeout = 5000;
      xhrAT.ontimeout = function() {
        alert('Timeout beim Holen der Gemeinde von volki.at');
      }
      xhrAT.open('GET', adminAT_url, true); // asynchron
      xhrAT.send();
    }
    function ort_get_sh() {
      var f = document.forms.${ortfrm};
      var epsg = 4326;
      switch (f.ORT_KOOSYST.value) {
        case 'M28': epsg=31257; break;
        case 'M31': epsg=31258; break;
        case 'M34': epsg=31259;
      }
      var sh_url = 'https://voibos.rechenraum.com/voibos/voibos?name=hoehenservice&Koordinate=' + f.ORT_RW.value + ',' + f.ORT_HW.value + '&CRS=' + epsg;
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var json = JSON.parse(this.responseText);
        if (json.abfragestatus == 'erfolgreich')
          f.ORT_SH.value = json.hoeheDTM;
        else if (json.abfragestatus != 'Die Abfrage-Koordinaten befinden sich nicht in Österreich.')
          alert('Abfragestatus: ' + json.abfragestatus);
      }
      xhr.onerror = function() {
        alert('Voibos-Server nicht erreichbar oder Browser verweigert CORS (Cross-Origin Resource Sharing');
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Holen der Seehöhe von Voibos');
      }
      xhr.open('GET', sh_url, true); // asynchron
      xhr.send();
    }
    % if ortdict['schreibrecht']:
    function adj_gemeinde_attrs() {
      if (document.forms.${ortfrm}.ORT_LAND.value=='AT' && document.forms.${ortfrm}.ORT_BEZIRK.value) {
        document.getElementById('ORT_GEMEINDE_LABEL').style.display = 'inline-block';
        document.forms.${ortfrm}.ORT_GEMEINDE.disabled = document.forms.${ortfrm}.ORT_LAND.disabled;
        document.forms.${ortfrm}.ORT_GEMEINDE.style.display = 'inline-block';
      } else {
        document.getElementById('ORT_GEMEINDE_LABEL').style.display = 'none';
        document.forms.${ortfrm}.ORT_GEMEINDE.disabled = true;
        document.forms.${ortfrm}.ORT_GEMEINDE.style.display = 'none';
      }
    }
    function adj_bezirk_attrs() {
      if (document.forms.${ortfrm}.ORT_LAND.value=='AT' && document.getElementById('ORT_BL_SEL').value) {
        document.getElementById('ORT_BEZIRK_LABEL').style.display = 'inline-block';
        document.forms.${ortfrm}.ORT_BEZIRK.disabled = document.forms.${ortfrm}.ORT_LAND.disabled;
        document.forms.${ortfrm}.ORT_BEZIRK.style.display = 'inline-block';
      } else {
        document.getElementById('ORT_BEZIRK_LABEL').style.display = 'none';
        document.forms.${ortfrm}.ORT_BEZIRK.disabled = true;
        document.forms.${ortfrm}.ORT_BEZIRK.style.display = 'none';
      }
      adj_gemeinde_attrs()
    }
    function adj_bl_attrs() {
      if (document.forms.${ortfrm}.ORT_LAND.value=='AT') {
        document.getElementById('ORT_BL_TEXT').disabled = true;
        document.getElementById('ORT_BL_TEXT').style.display = 'none';
        document.getElementById('ORT_BL_SEL').disabled = document.forms.${ortfrm}.ORT_LAND.disabled;
        document.getElementById('ORT_BL_SEL').style.display = 'inline-block';
      } else {
        document.getElementById('ORT_BL_TEXT').disabled = !document.forms.${ortfrm}.ORT_LAND.value;
        document.getElementById('ORT_BL_TEXT').style.display = 'inline-block';
        document.getElementById('ORT_BL_SEL').disabled = true;
        document.getElementById('ORT_BL_SEL').style.display = 'none';
      }
      adj_bezirk_attrs();
    }
    ## wenn keine Höhle, aber ORT_VERW_AUTO gesetzt ist, dann sollten die Felder alle auf readonly stehen, damit sie zwar übertragen werden (im Ggs. zu disabled), aber keine Eingabe möglich ist
    ## select kann man aber nicht auf readonly setzen, d.h. für den Submit müsste man künsteln
    ## darum sind die folgenden Funktionen auskommentiert und wir wählen stattdessen den umgekehrten Ansatz, nämlich dass die Eingabe möglich ist, dann aber die Checkbox automatisch uncheckt
    ##function adj_ort_verw_readonly() {
    ##  var f = document.forms.${ortfrm};
    ##  f.ORT_LAND.readonly = document.getElementById('ORT_BL_TEXT').readonly = document.getElementById('ORT_VERW_AUTO').checked;
    ##  document.getElementById('ORT_BL_SEL').disabled = f.ORT_BEZIRK.disabled = f.ORT_GEMEINDE.disabled = document.getElementById('ORT_VERW_AUTO').checked; // hier liegt das Problem
    ##}
    ##function adj_ort_sh_readonly() {
    ##  var f = document.forms.${ortfrm};
    ##  f.ORT_SH.readonly = document.getElementById('ORT_SH_AUTO').checked;
    ##}
    function adj_ort_verw_ajax() {
      var f = document.forms.${ortfrm};
      if (f.ORT_KOOSYST.value && f.ORT_RW.value && f.ORT_HW.value)
        if (document.getElementById('ORT_VERW_AUTO').checked)
          ort_get_verwaltung();
      ##adj_ort_verw_readonly();
    }
    function adj_ort_sh_ajax() {
      var f = document.forms.${ortfrm};
      if (f.ORT_KOOSYST.value && f.ORT_RW.value && f.ORT_HW.value)
        if (document.getElementById('ORT_SH_AUTO').checked)
          ort_get_sh();
      ##adj_ort_sh_readonly();
    }
    function adj_ort_ajax() {
      adj_ort_verw_ajax();
      adj_ort_sh_ajax();
    }
    % endif
    function adj_ort_inputs(p_changed) {
      var f = document.forms.${ortfrm};
      if (f.ORT_R.value == 'H') {
        % if ortdict['schreibrecht']:
        f.ORT_HOEHLE_ID.disabled = false;
        f.ORT_VERW_AUTO.disabled = true;
        f.ORT_SH_AUTO.disabled = true;
        % endif
        f.ORT_HOEHLE_ID.required = true;
        f.ORT_KOOSYST.disabled = true;
        f.ORT_RW.disabled = true;
        f.ORT_RW.required = false;
        f.ORT_HW.disabled = true;
        f.ORT_HW.required = false;
        f.ORT_KOOABW.disabled = true;
        f.ORT_LAND.disabled=true;
        f.ORT_SH.disabled = true;
        f.ORT_SHTOL.disabled = true;
        f.ORT_BESCHREIBUNG.disabled = true;
        f.ORT_ZUGANG.disabled = true;
      } else {
        % if ortdict['schreibrecht']:
        f.ORT_KOOSYST.disabled = false;
        f.ORT_VERW_AUTO.disabled = false;
        f.ORT_SH_AUTO.disabled = false;
        % endif
        f.ORT_HOEHLE_ID.disabled = true;
        f.ORT_HOEHLE_ID.required = false;
        f.ORT_RW.disabled = false;
        f.ORT_RW.required = true;
        f.ORT_HW.disabled = false;
        f.ORT_HW.required = true;
        f.ORT_KOOABW.disabled = false;
        f.ORT_LAND.disabled = false;
        f.ORT_SH.disabled = false;
        f.ORT_SHTOL.disabled = false;
        f.ORT_BESCHREIBUNG.disabled = false;
        f.ORT_ZUGANG.disabled = false;
      }
      if (p_changed) {
        adj_bl_attrs();
        // wenn Verwaltungseinheiten bzw. Sh schon befüllt, dann manuell, sonst automatisch
        f.ORT_VERW_AUTO.checked = f.ORT_R.value == 'H' || !(f.ORT_RW.value && f.ORT_HW.value);
        f.ORT_SH_AUTO.checked = f.ORT_R.value == 'H' || f.ORT_SH.value == '';
        document.getElementById('B2H_BTN').disabled = f.ORT_HOEHLE_ID.disabled || ! f.ORT_HOEHLE_ID.value;
      }
      ##adj_ort_verw_readonly();
      ##adj_ort_sh_readonly();
    }
    % endif
    % if 'hdateien' in s:
    function del_zuo(p_h_id, p_d_id) {
      let delzuo_url = '${s['scriptname']}?SESS=${s['sessionid']}&DEL_HDZUO_H=' + p_h_id + '&DEL_HDZUO_D=' + p_d_id;
      let xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText != 'OK') {
          alert('Fehler beim Löschen der Zuordnung zu Datei '+p_d_id+': HTTP-Status ' + this.status + '/' + this.statusText);
          return;
        }
        if (this.responseText.trim() != 'ok') {
          alert(this.responseText);
          return;
        }
        document.getElementById('HD_TR_'+p_d_id).style.textDecoration = 'line-through';
        document.getElementById('HD_BTNS_'+p_d_id).remove();
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Löschen der Zuordnung zu Datei '+p_d_id+': ' + e.message);
      }
      xhr.timeout = 2000;
      xhr.ontimeout = function() {
        alert('Timeout beim Löschen der Dateizuordnung');
      }
      xhr.open('GET', delzuo_url, true); // asynchron
      xhr.send();
    }
    % endif
    % if 'best' in s:
    function pfadval2displval(p_val, p_short=false) {
      let str = '';
      for (var node of p_val.split('|')) {
        if (str)
          str += p_short ? '→' : ' → ';
        str += node.substring(node.indexOf(':') + 1);
      }
      return str;
    }
    function pfad2displ() {
      document.getElementById('BEST_PFAD_DISPL').innerText = pfadval2displval(document.forms.BESTFRM.BEST_PFAD.value);
    }
    function taxcheck() {
      const f = document.forms.BESTFRM;
      if (!f.BEST_RANG.value || !f.BEST_TAXON.value || f.BEST_EPITHETON.required && !f.BEST_EPITHETON.value)
        return; // Pflichtfeld noch nicht versorgt
      document.getElementById('BEST_SUBM').disabled = true;
      document.getElementById('PFAD_SEL').style.display = 'none';
      var taxon = f.BEST_TAXON.value;
      if (! f.BEST_EPITHETON.disabled && f.BEST_EPITHETON.value)
        taxon += ' ' + f.BEST_EPITHETON.value;
      var tc_url = '${s['scriptname']}?TAXCHECK=' + encodeURIComponent(taxon) + '&TAXSEARCH_RANG=' + f.BEST_RANG.value;
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        if (this.statusText != 'OK') {
          alert('Fehler beim Prüfen des Taxons: HTTP-Status ' + this.status + '/' + this.statusText);
          document.getElementById('BEST_SUBM').disabled = false;
          return;
        }
        var json = JSON.parse(this.responseText);
        if (!json.taxa.length) {
          f.BEST_PFAD.value = '';
          alert('Warnung: Taxon mit diesem Rang nicht in COL. Keine automatische Ermittlung der übergeordneten Taxa möglich.');
          pfad2displ();
        } else if (json.taxa.length == 1) {
          f.BEST_PFAD.value = json.taxa[0];
          pfad2displ();
          if (json.syn)
            alert('In COL gefundener Eintrag ist ein Synonym.');
        } else {  // -> mehrere gefunden
          pfadsel = document.getElementById('PFAD_SEL');
          for (const option of Array.from(pfadsel.options))
            if (option.value)
              option.remove();
            else
              option.selected = true;
          for (const t of json.taxa)
            pfadsel.add(new Option(pfadval2displval(t,true), t));
          pfadsel.style.display = 'inline-block';
          alert(json.taxa.length + (json.syn ? ' Einträge (nur Synonyme!)' : ' gültige Einträge') + ' in COL gefunden. Bitte einen auswählen.');
          pfadsel.focus();
        }
        document.getElementById('BEST_SUBM').disabled = false;
      }
      xhr.onerror = function(e) {
        alert('Fehler beim Prüfen des Taxon:' + e.message);
        document.getElementById('BEST_SUBM').disabled = false;
      }
      xhr.timeout = 5000;
      xhr.ontimeout = function() {
        alert('Timeout beim Prüfen des Taxons');
        document.getElementById('BEST_SUBM').disabled = false;
      }
      xhr.open('GET', tc_url, true); // asynchron
      xhr.send();
    }
    function adj_prb(p_sel) {
      ## E=erhalten von <>
      ## G=gelagert in <>
      ## N=neue Nummer <>
      ## Ü=übergeben an <> für <>
      ## A=gesendet an <> per <>
      ## R=Rückmeldung von <> per <>
      ## W=entsorgt
      ## V=verschwunden
      ## S=Sonstiges
      const display_par1 = ['E','G','N','Ü','A','R'].includes(p_sel.value);
      const display_par2 = ['Ü','A','R'].includes(p_sel.value);
      p_sel.form.PRB_PAR1.style.display = display_par1 ? 'inline-block' : 'none';
      p_sel.form.PRB_PAR1.disabled = !display_par1;
      p_sel.form.PRB_PAR2.style.display = display_par2 ? 'inline-block' : 'none';
      p_sel.form.PRB_PAR2.disabled = !display_par2;
      let text1 = text2 = ''
      switch (p_sel.value) {
        case 'E': text1 = 'von';                break;
        case 'G': text1 = 'in';                 break;
        case 'Ü': text1 = 'an';  text2 = 'für'; break;
        case 'A': text1 = 'an';  text2 = 'per'; break;
        case 'R': text1 = 'von'; text2 = 'per';
      }
      document.getElementById('PRB_' + p_sel.form.PRB_ID.value + '_TXT1').innerText = text1;
      document.getElementById('PRB_' + p_sel.form.PRB_ID.value + '_TXT2').innerText = text2;
    }
    function adj_prb_all() {
      for (const sel of document.getElementsByName("PRB_AKTION"))
        adj_prb(sel);
    }
    function adj_bestimmer(p_val) {
      document.forms.BESTFRM.BEST_BESTIMMER.disabled = p_val == 'S';
      document.forms.BESTFRM.BEST_BESTIMMER.required = p_val == 'A';
      if (p_val == 'S')
        document.forms.BESTFRM.BEST_BESTIMMER.value='';
    }
    function adj_ergebnis() {
      const f = document.forms.BESTFRM;
      if (f.BEST_RANG.value == '${s['artebene']}' || f.BEST_RANG.value == '${s['artebene'] + 1}') {
        f.BEST_EPITHETON.disabled = false;
        f.BEST_EPITHETON.required = ! f.BEST_TAXON.value.includes('virus');
      } else {
        f.BEST_EPITHETON.disabled = true;
        f.BEST_EPITHETON.required = false;
      }
      if (f.BEST_RANG.value == '${s['artebene']}') {
        f.BEST_SSP.disabled = false;
        f.BEST_VAR.disabled = false;
        f.BEST_CV.disabled = false;
        f.BEST_FORM.disabled = false;
      } else {
        f.BEST_SSP.disabled = true;
        f.BEST_VAR.disabled = true;
        f.BEST_CV.disabled = true;
        f.BEST_FORM.disabled = true;
      }
    }
    // Wenn jemand Gattung und Epitheton im selben Feld eingibt: letzteres automatisch rausnehmen
    function remove_epitheton(p_inp) {
      if ((p_inp.form.BEST_RANG.value != '${s['artebene']}' && p_inp.form.BEST_RANG.value != '${s['artebene'] + 1}') || p_inp.form.BEST_EPITHETON.value > '')
        return;
      var val = p_inp.value;
      if (val.includes('virus')) {
        p_inp.form.BEST_EPITHETON.required = false;
        return; // bei Viren keine binäre Nomenklatur
      }
      p_inp.form.BEST_EPITHETON.required = true; // für den unwahrscheinlichen Fall, dass auf Virus geändert und wieder zurück
      var pos = val.indexOf(' ');
      if (pos < 1)
        return;
      p_inp.value = val.substring(0, pos);
      val = val.substring(pos+1);
      if (p_inp.form.BEST_RANG.value == '${s['artebene']}' && p_inp.form.BEST_SSP.value == '') {
        pos = val.indexOf(' ')
        if (pos > 0) {
          p_inp.form.BEST_SSP.value = val.substring(pos+1);
          val = val.substring(0, pos);
        }
      }
      p_inp.form.BEST_EPITHETON.value = val;
    }
    function taxbaum_from_best(p_form) {
      var searchstr = p_form.BEST_TAXON.value;
      if ((p_form.BEST_RANG.value == '${s['artebene']}' || p_form.BEST_RANG.value == '${s['artebene'] + 1}') && p_form.BEST_EPITHETON.value > '')
        searchstr += ' ' + p_form.BEST_EPITHETON.value;
      taxbaum_fuer_uebern(p_form.BEST_RANG.value, searchstr);
    }
    function best_subm_check() {
      sel = document.getElementById('PFAD_SEL')
      if (sel.style.display == 'none' || sel.value)
        return true;
      alert('Bitte auswählen.');
      sel.focus();
      return false;
    }
    % endif # best in s
    function openkarte(p_wasanz, p_id=0) {
      let url = '${s['scriptname']}?SESS=${s['sessionid']}&KARTE='+p_wasanz;
      if (p_id)
        url += '&ID=' + p_id;
      location.href = url;
    }
    % if 'karte' in s:
    function adj_map_height() {
      document.getElementById('MAPDIV').style.height = (window.innerHeight - ${If(s['koopick'],'16','36')}) + 'px' // Menühöhe (26 px) und Browser-Ungenauigkeit abziehen
    }
    % if s['koopick']:
    let defs = [
      ['BMN M28', '+proj=tmerc +lat_0=0 +lon_0=10.33333333333333 +k=1 +x_0=150000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs'
      ], // EPSG:31257
      ['BMN M31', '+proj=tmerc +lat_0=0 +lon_0=13.33333333333333 +k=1 +x_0=450000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs'
      ], // EPSG:31258
      ['BMN M34', '+proj=tmerc +lat_0=0 +lon_0=16.33333333333333 +k=1 +x_0=750000 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs'
      ] // EPSG:31259
    ];
    proj4.defs(defs);
    function koo_uebern(p_lon, p_lat, p_confirm=false) {
      let s = 'lon: '+p_lon+' lat: '+p_lat+'\n';
      for (const i of defs) {
        koo = proj4(i[0], [p_lon, p_lat]);
        s += i[0] + ': ' + koo[0] + ' / ' + koo[1] + '\n';
      }
      if (!p_confirm || confirm(s + 'übernehmen?')) {
        var f = opener.document.forms.BESUCHFRM || opener.document.forms.ORTFRM;
        f.ORT_RW.value=p_lon;
        f.ORT_HW.value=p_lat;
        f.ORT_KOOSYST.value='G';
        opener.adj_ort_ajax();
        f.ORT_KOOABW.focus()
        window.close();
      }
    }
    function mapclick(e) {
      koo_uebern(e.latlng.lng, e.latlng.lat, true);
    }
    % endif # s['koopick']
    function initkarte() {
      adj_map_height();
      var osmcarto = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        { maxZoom: 19,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000,
          crossOrigin: true,
          % endif
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OSM</a> contributors'
        });
      var tracestrack = L.tileLayer('https://tile.tracestrack.com/topo__/{z}/{x}/{y}.png?key=15591863c2c9f57a4791bb2cc8d90a4b',
        { maxZoom: 19,
          maxNativeZoom: 19,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*7,
          crossOrigin: true,
          % endif
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OSM</a> contributors / <a href="https://www.tracestrack.com/">Tracestrack</a>'
        });
      var opentopo = L.tileLayer('https://tile.opentopomap.org/{z}/{x}/{y}.png',
        { maxZoom: 19,
          maxNativeZoom: 17,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*7,
          crossOrigin: true,
          % endif
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OSM</a> contributors / <a href="https://opentopomap.org/about#datenstand">OpenTopoMap</a>'
        });
      var geoimage = L.tileLayer.wms(
        //http://gis.lfrz.gv.at/wmsgw/?key=4d80de696cd562a63ce463a58a61488d&VERSION=1.1.1&SERVICE=WMS&LAYERS=Luftbild_MR,Luftbild_1m,Luftbild_8m,Satellitenbild_30m&SRS=EPSG:4326&STYLES=,,,&FORMAT=image/jpeg
        'http://gis.lfrz.gv.at/wmsgw/?key=4d80de696cd562a63ce463a58a61488d',
        { format: 'image/jpeg',
          //layers: 'Luftbild_MR,Luftbild_1m,Luftbild_8m,Satellitenbild_30m',
          layers: 'Luftbild',
          styles: ',,,',
          version: '1.1.1',
          crs: L.CRS.EPSG4326,
          uppercase: true,
          minZoom: 12,
          maxZoom: 19,
          maxNativeZoom: 18,
          attribution: 'geoimage.at'
        }
      );
      /*
      var geoimage_maxres = L.tileLayer.wms(
        'https://gis.lfrz.gv.at/wmsgw/dopmaxresfree/?',
        { format: 'image/png',
          layers: 'Luftbild',
          crs: L.CRS.EPSG4326,
          attribution: 'geoimage.at'
        }
      );*/
      var basemap_of = L.tileLayer(
        'http://maps.wien.gv.at/basemap/bmaporthofoto30cm/normal/google3857/{z}/{y}/{x}.jpeg',
        { maxZoom: 19,
          maxNativeZoom: 18,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; basemap.at'
        }
      );
      var basemap_gelaende = L.tileLayer(
        'http://maps.wien.gv.at/basemap/bmapgelaende/grau/google3857/{z}/{y}/{x}.jpeg',
        { maxZoom: 19,
          maxNativeZoom: 17,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; basemap.at'
        }
      );
      var basemap_oberfl = L.tileLayer(
        'http://maps.wien.gv.at/basemap/bmapoberflaeche/grau/google3857/{z}/{y}/{x}.jpeg',
        { maxZoom: 19,
          maxNativeZoom: 17,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; basemap.at'
        }
      );
      var basemap_verw = L.tileLayer(
        'http://maps.wien.gv.at/basemap/geolandbasemap/normal/google3857/{z}/{y}/{x}.png',
        { maxZoom: 19,
          maxNativeZoom: 18,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; basemap.at'
        }
      );
      var basemap_hl = L.tileLayer(
        'http://maps.wien.gv.at/basemap/bmapgrau/normal/google3857/{z}/{y}/{x}.png',
        { format: 'image/png',
          maxZoom: 19,
          maxNativeZoom: 18,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; basemap.at'
        }
      );
      var esri_arcgis = L.tileLayer(
        'https://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        { maxZoom: 19,
          maxNativeZoom: 18,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; ESRI'
        }
      );
      var esri_clarity = L.tileLayer(
        'https://clarity.maptiles.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        { maxZoom: 19,
          maxNativeZoom: 18,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*14,
          crossOrigin: true,
          % endif
          attribution: '&copy; ESRI'
        }
      );
      var wmt = L.tileLayer('https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png',
        { maxZoom: 19,
          maxNativeZoom: 18,
          % if cache:
          useCache: true,
          cacheMaxAge: 86400000*7,
          crossOrigin: true,
          % endif
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OSM</a> contributors / <a href="https://hiking.waymarkedtrails.org/#help-legal">Waymarkedtrails</a>'
        });
      ##var emptymap = L.tileLayer('', {maxzoom: 21}); // bringt nichts, Zoom geht nur bis 18
      var maps = {
        'basemap.at Verw.+HL': basemap_hl,
        'basemap.at Verwaltung': basemap_verw,
        'basemap.at Gelände': basemap_gelaende,
        'basemap.at Oberfläche': basemap_oberfl,
        'basemap.at Orthofoto': basemap_of,
        'geoimage.at (ab Z12)': geoimage,
        'ESRI Arcgis': esri_arcgis,
        'ESRI Clarity': esri_clarity,
        'OSM Carto': osmcarto,
        'Tracestrack': tracestrack,
        'OpenTopoMap': opentopo
        ##'leer': emptymap
      };
      <%
        if 'katgr_zentrum' in s:
          centermap = s['katgr_zentrum']
          zoom = 10
        elif 'show_marker' in s:
          typ = s['show_marker'][0]
          key = s['show_marker'][1:]
          if typ == 'B': # Besuch
            showobj = s['karte']['besuchorte'][key]
          else:
            showobj = s['karte']['hoehlenorte'][key]
          showobj['show'] = True
          centermap = '[' + str(showobj['hw_g']) + ',' + str(showobj['rw_g']) + ']'
          zoom = '16'
        else:
          centermap = '[48, 15]'
          zoom = '9'
      %>\
      var map = L.map('MAPDIV', {
         center: ${centermap},
         zoom: ${zoom},
         ## erster Array-Eintrag ist Systemdefault, daher 2x im Array
         layers: [${['basemap_hl','tracestrack','osmcarto','opentopo','basemap_of','basemap_gelaende','basemap_oberfl','basemap_verw','basemap_hl','esri_arcgis','esri_clarity','geoimage'][s['useropts'].get('MapDefault') or 0]}],
         zoomControl: false
      });
      % if 'mapbounds' in s:
        map.fitBounds(${s['mapbounds']});
      % endif
      var icon_fund = L.divIcon({
        html: `<%include file="/icons/flag_icon.svg"/>`,
        iconSize:     [20, 31], // size of the icon
        iconAnchor:   [2, 31], // point of the icon which will correspond to marker's location
        className: ''
      });
      var icon_hoehle = L.divIcon({
        html: `<%include file="/icons/cave_icon.svg"/>`,
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 16], // point of the icon which will correspond to marker's location
        //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        className: ''
      });
      var icon_bergw = L.divIcon({
        html: `<%include file="/icons/bergw_icon.svg"/>`,
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 16], // point of the icon which will correspond to marker's location
        className: ''
      });
      var icon_bunk = L.divIcon({
        html: `<%include file="/icons/bunk_icon.svg"/>`,
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 16], // point of the icon which will correspond to marker's location
        className: ''
      });
      var icon_erdst = L.divIcon({
        html: `<%include file="/icons/erdst_icon.svg"/>`,
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 16], // point of the icon which will correspond to marker's location
        className: ''
      });
      var icon_kuenst = L.divIcon({
        html: `<%include file="/icons/kuenst_icon.svg"/>`,
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 16], // point of the icon which will correspond to marker's location
        className: ''
      });
      var funde = L.layerGroup();
      <% showindex = None %>\
      % for o in s['karte']['besuchorte'].values():
        <%
        popupstr = ""
        import html
        for b in o['besuche']:
          if s['koopick']:
            popupstr += '<div>' + b['besuch_d'] + ' ' + html.escape(b['user']) +'</div>'
          else:
            popupstr += '<div><a href="' + s['scriptname'] + '?SESS=' + s['sessionid'] + '&BESUCHID=' + str(b['id']) + '"'
            if s['useropts'].get('NewWinKF'):
              popupstr += ' target="_blank"'
            popupstr += '>' + b['besuch_d'] + '</a> ' + html.escape(b['user']) +'</div>'
        if s['koopick']:
          popupstr += '<div>Koordinaten: ' + str(o['rw_g']) + ' / ' + str(o['hw_g']) + '</div>'
          popupstr += '<input type="button" value="übernehmen" onclick="koo_uebern(' + str(o['rw_g']) + ',' + str(o['hw_g']) + ')">'
        else:
          popupstr += '<div><a href="' + s['scriptname'] + '?SESS=' + s['sessionid'] + '&BLISTE=1&XLIST_W='+str(o['rw_g']) + '&XLIST_O='+str(o['rw_g']) + '&XLIST_N='+str(o['hw_g']) + '&XLIST_S='+str(o['hw_g']) + '"'
          if s['useropts'].get('NewWinKF'):
            popupstr += ' target="_blank"'
          popupstr += '>Liste mit Details</a></div>'
        if 'show' in o:
          showindex = loop.index
        %>\
      const marker${loop.index} = L.marker([${o['hw_g']}, ${o['rw_g']}], {icon: icon_fund})
      marker${loop.index}.bindPopup('${popupstr |esc_js_str}');
      marker${loop.index}.bindTooltip('${len(o['besuche'])} ${'Besuch' if len(o['besuche']) == 1 else 'Besuche'}', {className: 'sonst_tt'})
      funde.addLayer(marker${loop.index});
      % endfor
      let popuphoe;
      var hoehlen = L.layerGroup([
      % for o in s['karte']['hoehlenorte'].values():
        % if 'show' in o: 
        popuphoe =
        % endif
        <%
          import html
          tooltip = popup = ''
          for h in o['hoehlen']:
            if tooltip:
              tooltip += ', '
              popup += '<hr>'
            tooltip += esc_js_str(h['katnr'] or h['name'] or '?')
            popup += '<a href="' + s['scriptname'] + '?SESS=' + s['sessionid'] + '&HOEHLEID=' + str(h['id']) + '"'
            if s['useropts'].get('NewWinKH'):
              popup += ' target="_blank"'
            popup += '>' + esc_js_str(html.escape(h['katnr']))
            if h['name']:
              popup += If(h['katnr'],' ') + esc_js_str(html.escape(h['name']))
            popup += '</a><br>± ' + str(h['kooabw']) + '&#x2009;m'
            if h['sh']:
              popup += ', Sh: ' + str(h['sh']) + '&#x2009;m'
        %>\
        L.marker([${o['hw_g']}, ${o['rw_g']}], {icon: ${{'H': 'icon_hoehle', 'B': 'icon_bergw', 'L': 'icon_bunk', 'E': 'icon_erdst', 'K': 'icon_kuenst'}[o['genese'] or 'H']}})
          .bindTooltip('${tooltip}', {className: 'sonst_tt'})
          .bindPopup('${popup}')${Unless(loop.last, ',')}
      % endfor
      ]);
      % if s['wasanz'][0] == 'K':
      % for kuenstlich in (0,1):
      var grenze_arr = [];
      % for k in s['katgr'][kuenstlich]:
      var grenze_geometry = JSON.parse('${k['flaeche']}');
      var grenze_geojson = {
        type: 'Feature',
        properties: {
          label: '${If(len(s['katgr'])==1, 'Katastergruppe ')}${k['nr'] |esc_js_str}',
          color: '#${If(len(s['katgr'])==1, 'ff4444', ['ff3333','eeee00','3355ff','00ff22'][k['farbe']-1])}'
        },
        geometry: grenze_geometry
      };
      grenze_arr.push(grenze_geojson);
      % endfor
      var grenze_options = {
        coordsToLatLng: function (coords) {
          //return new L.LatLng(coords[0], coords[1], coords[2]);
           return coords;
        },
        onEachFeature: function(p_feature, p_layer) {
          p_layer.bindTooltip(p_feature.properties.label, {permanent: false, sticky: true, direction: 'top', className: 'katgr_tt'});
        },
        style: function(p_feature) {
          ## https://leafletjs.com/reference.html#path-option
          return {fillColor: p_feature.properties.color, fillOpacity: ${If(len(s['katgr'][kuenstlich])==1, 0.2, 0.12)}, weight: 1, color: "#000000"};
        }
      }
      var grenzen${kuenstlich} = L.geoJSON(grenze_arr, grenze_options);
      % endfor # for kuenstlich in (0,1)
      % endif
      var overlays = {
        % if s['wasanz'] == 'K':
        'Kat.gruppe': grenzen0,
        % elif s['wasanz'][0] == 'K':
        'Kat.gruppen ¬K': grenzen0,
        'Kat.gruppen K': grenzen1,
        % endif
        'Funde': funde,
        'Höhlen': hoehlen,
        'Wanderrouten': wmt
      };
      % if s['wasanz'] == 'H':
      hoehlen.addTo(map);
      % elif s['wasanz'] == 'F':
      funde.addTo(map);
      % elif s['wasanz'] in ('K','KN'):
      grenzen0.addTo(map);
      % elif s['wasanz'] == 'KK':
      grenzen1.addTo(map);
      % endif
      L.control.layers(maps, overlays).addTo(map);
      L.control.zoom({position: 'bottomright'}).addTo(map);
      ## den Button für die Lokalisierung nur bei https anzeigen, andernfalls keinen Platz dafür vergeuden weil die gängigen Browser die Lokalisierung sowieso verweigern
      % if s['karte']['showlocate']:
      L.control.locate({position: 'bottomright'}).addTo(map);
      % endif
      L.control.scale({metric: true, imperial: false}).addTo(map);
      % if s['koopick']:
      map.on('click', mapclick);
      % endif
      % if showindex is not None:
      marker${showindex}.openPopup();
      % elif 'show_marker' in s:
      if (popuphoe)
        popuphoe.openPopup();
      % endif
    }
    % endif # 'karte' in s
    function adj_body_width() {\
      <%
        if 'karte' in s:
          maxwidth = 600
        elif 'bliste' in s:
          maxwidth = 1600
        else:
          maxwidth = 1200
      %>
      if (window.screen.width + 200 < window.innerWidth) {
        document.getElementById('VIEWPORT').setAttribute('content','width=' + (window.screen.width < ${maxwidth} ? ${maxwidth} : window.screen.width));
      }
    }
  </script>
  <style id="MAINSTYLE">
    * {
      font-family: Cantarell, Arial, Helvetica, Verdana, Trebuchet, "Trebuchet MS", "DejaVu Sans", sans-serif;
    }
    .menuentry {
      border: 1px solid black;
      background-color:#bbffbb;
      text-align: center;
      vertical-align:middle;
    }
    .menuentry:hover {
      background-color:#ffffbb;
    }
    .graytable {
      border: 1px solid black;
      border-collapse:collapse;
      background-color:#eeeeee;
    }
    .horizborder {
      border-top: 1px solid #444444;
      border-bottom: 1px solid #444444;
    }
    .liste {
      border-collapse: collapse;
    }
    .liste th {
      border: 1px solid black;
      background-color:#aaccff
    }
    .liste td {
      border: 1px solid black;
      background-color:#cce4ff
    }
    .liste tr[onclick]:hover td {
      background-color:#ffffbb
    }
    .liste_selected td {
      background: repeating-linear-gradient(
        315deg,
        #ffffbb,
        #ffffbb 5px,
        #cce4ff 5px,
        #cce4ff 10px
      );
    }
    .liste_selected:hover td {
      background: #ffffbb
    }
    .sonst_tt {
      padding: 0;
      font-size: 14px;
      height: 18px;
      line-height: normal;
      vertical-align: middle;
    }
    % if 'karte' in s and s['wasanz'] == 'K':
    .katgr_tt {
      padding: 0;
      font-size: 16px;
      height: 20px;
      line-height: normal;
      vertical-align: middle;
      background-color: rgba(255,255,255,0.5);
      color: ${If(len(s['katgr'])==1, 'blue', 'red')}
      ##background-color: none;
      ##border-color: none;
      ##background: none;
      ##border: none;
      ##box-shadow:none;
    }
    % elif 'taxbaum' in s:
    .tb_indent {
      font-family: "DejaVu Sans Mono", monospace;
      font-size: 1.2em
    }
    .tb_taxon {
    }
    .tb_rank {
      color:#0000ff
    }
    .tb_lit {
      color:#00bb00
    }
    .tb_syn {
      color:#dd6600
    }
    .tb_syn_div {
    }
    .tb {
    }
    .tb_display_always {
      display: block
    }
    .tb_selected {
      background-color:#ffff77
    }
    .tb_btn {
      margin-bottom:-1px;
      font-size:70%;
      height:1.7em;
      margin-left:1em
    }
    % endif
  </style>
</head>
<body onload="
  % if 'error' in s:
    display_error('${s['error']}');
  % endif
  % if 'cfg' in s:
    adj_cfg_name_labels();
    % if 'cfgmsg' in s:
    alert('${s['cfgmsg'] |esc_js_str}');
    % endif
  % endif
  % if 'karte' in s:
    initkarte()
  % endif
  % if 'inittaxsearch' in s and s['inittaxsearch']:
    taxsearch();
  % endif
  % if 'dliste' in s and not s['hdzuo_h'] and not s['fdzuo_b'] and not s['pdzuo_p']:
    for (const o of document.forms.DSEARCH_FRM.DSEARCH_HOEHLE_ID.options)
      if (o.value)
        document.forms.HD_ZUO_FRM.HD_ZUO_H.appendChild(o.cloneNode(true))
    document.forms.HD_ZUO_FRM.HD_ZUO_H.value = ''
  % endif
  % if 'd_upl' in s and s['firstid']:
    % if s['firstid'] == s['lastid']:
    alert('Datei angelegt mit ID=${s['firstid']}');
    % else:
    alert('Dateien angelegt mit IDs ${s['firstid']} bis ${s['lastid']}');
    % endif
  % endif
  % if 'eort' in s:
    document.forms.ORTFRM.ORT_KOOSYST.focus();
  % elif 'eingang' in s:
    document.forms.EINGANGFRM.NAME.focus();
  % endif
  % if 'besuch' in s:
    adj_ort_inputs();
  % endif
  % if 'best' in s:
    adj_ergebnis();
    pfad2displ();
  % endif
  % if 'fund' in s:
    % if 'proben' in s:
    adj_prb_all();
    % endif
    ## zu Bestimmung nur dann scrollen, wenn diese sicher explizit ausgewählt wurde
    % if 'best' in s and s['best']['form_oeffnen'] and not (s['best']['i'] and s['besuch']['schreibrecht'] and s['best']['schreibrecht']):
    scroll_to_id('BESTFRM_DIV', false);
    % else:
    scroll_to_id('FUNDFRM', false);
    % endif
  % elif 'eingang_soeben_angelegt' in s and s['eingang_soeben_angelegt']:
    scroll_to_id('ORTFRM');
  % endif
    screen.orientation.onchange = adj_body_width();
    adj_body_width();
  "
  % if 'karte' in s:
    onresize="adj_map_height()"
  % endif
>
  % if not s['koopick'] and not s['taxpick']:
  <div style="display:inline-block; cursor:pointer">
    <%
       menuheight = 26
       offset_x = 8
       offset_y = 10
       colnum = 0
       widthsum = offset_x
    %>\
    % for m in s['menu']:
      <% colnum += 1 %>\
      <div style="position:absolute; left:${widthsum}px; top:0">
        <table style="display:inline-block; width:${m['breite']}px; height:${menuheight}px; border: 1px solid black; background-color:#44ff44" onclick="event.stopPropagation(); flipmenu(${colnum})">
          <tr>
            <th style="width:${m['breite']}px; text-align:center; vertical-align:middle">
              ${m['name']}
            </th>
          </tr>
        </table>
      </div>
      <div id="MENU${colnum}" style="position: absolute; left:${widthsum}px; top:${menuheight}px; z-index:1000; display:none">
        <table style="border-collapse:collapse">
        % for r in m['submenu']:
         <tr>
           <td class="menuentry" style="width:${m['breite']-3}px" onclick="closemenus();${r[1]}">${r[0]}</td>
         </tr>
        % endfor
        </table>
      </div>
      <% widthsum = widthsum + m['breite'] %>\
    % endfor
  </div>
  % endif # if not s['koopick']
  % if s['anz_anfragen']:
  <div style="position:absolute; left:${widthsum + 10}px; top:3px; color:red">${s['anz_anfragen']} offene Bestimmungsanfrage${If(s['anz_anfragen'] > 1, 'n')}</div>
  % endif
  <div id="LOGDIV" style="z-index:901; background-color:white; display:none; position:absolute; margin-top:50px;border:3px solid #0000ff">
    <h3 style="margin:3px">Ein-/Ausloggen</h3>
    <form name="LOGFRM" method="post" action="${s['scriptname']}">
      % if s['user'] :
        <div style="margin-left:2px;margin-top:1em">Eingeloggt als ${s['user'] |h} (${s['realname'] |h}).</div>
        <div style="text-align:center;margin-top:5px">
          <input type="button" value="Logout" onclick="location.href='${s['scriptname']}'">
        </div>
      <hr>
      % endif
      <table>
        <tr>
          <td>Loginname</td>
          <td><input type="text" name="USER" maxlength="10" required></td>
        </tr><tr>
          <td>Passwort</td>
          <td><input type="text" name="PASS" maxlength="20" required> <span onclick="const c=document.getElementById('PASSVIS');c.checked=!c.checked;c.onchange()" style="margin-left:1em">&#x1F441;</span><input type="checkbox" id="PASSVIS" checked onchange="this.form.PASS.type=this.checked?'text':'password'"></td>
        </tr>
      </table>
      <div style="text-align:center;margin-top:5px">
        <input type="submit" value="Einloggen" style="text-align:center">
      </div>
      <hr>
      <div style="text-align:center">
        <input type="button" value="Abbruch" onclick="document.getElementById('LOGDIV').style.display='none'">
      </div>
    </form>
  </div>
  % if 'debug' in s:
  <p>Debug: ${s['debug'] |h}
  % endif
  % if 'karte' in s: # IF_00003
  <div id="MAPDIV" style="width:100%${If(s['koopick'], ';cursor:crosshair')}"></div>
  % elif 'cfg' in s: # IF_00003
  <h3 style="margin-top:10px">Einstellungen</h3>
  <p>Eingeloggt als ${s['user'] |h} (${s['realname'] |h}).</p>
  <hr>
  <form name="PASSWDFRM" method="post" action="${s['scriptname']}" onsubmit="if (this.NPASS2.value != this.NPASS1.value) {alert('Eingaben verschieden');return false} return true">
    <input type="hidden" name="SESS" value="${s['sessionid']}">
    <input type="hidden" name="CFG" value="1">
    <p>Passwort ändern?</p>
    <table><tr><td>neues Passwort</td><td>neues Passwort nochmal</td><td/></tr>
           <tr><td><input type="text" name="NPASS1" required></td><td><input type="text" name="NPASS2" required></td><td><input type="submit" value="ändern"></td></tr>
    </table>
  </form>
  <hr>
  <form name="CFGFRM" method="post" action="${s['scriptname']}">
    <input type="hidden" name="SESS" value="${s['sessionid']}">
    <input type="hidden" name="CFG" value="1">
    <div>Realname: <span style="margin-left:2em">Struktur</span>
      <select name="NAME_STRUKTUR" onchange="adj_cfg_name_labels()">
        <option value="VN"${If(s['cfg_name_struktur'] =='' or s['cfg_name_struktur'] =='VN', ' selected')}>Vorname Nachname</option>
        <option value="FG"${If(s['cfg_name_struktur'] =='FG', ' selected')}>Familienname GivenName (Ungarn, China...)</Option>
        <option value="PG"${If(s['cfg_name_struktur'] =='PG', ' selected')}>Patronym GivenName (Teile Indiens)</Option>
        <option value="GP"${If(s['cfg_name_struktur'] =='GP', ' selected')}>GivenName Patronym (Island)</option>
        <option value="R"${If(s['cfg_name_struktur'] =='R', ' selected')}>Role Account</option>
      </select>
    </div>
    <div style="margin-top:5px">
      <span id="NAME1_LABEL"></span>
      <input type="text" name="NAME1" maxlength="20" required value="${s['cfg_name1'] |h}">
      <span id="NAME2_LABEL" style="margin-left:1em"></span>
      <input type="text" name="NAME2" maxlength="20" required value="${s['cfg_name2'] |h}">
    </div>
    <p>
      Mail <input type="email" name="MAIL" maxlength="30" value="${s['cfg_mail'] |h}">
      <input type="checkbox" id="MAILANZ" name="MAILANZ" value="1"${If(s['useropts'].get('MailAnz'), ' checked')} style="margin-left:1em"> <label for="MAILANZ">anderen Usern anzeigen</label>
    </p>
    Personenbeschreibung, weitere Kontaktdaten etc. (max. 10000 Zeichen)
    <br>
    <textarea name="USERBESCHREIBUNG" maxlength="10000" cols="80" rows="10">${s['cfg_userbeschreibung'] |h}</textarea>
    <table style="margin-top:5px">
      <tr>
        <td>Benachrichtigungsmails bei erhaltenen Bestimmungsanfragen</td>
        <td>
          <select name="MAILBESTANFR" onchange="if (this.value) check_mail_set()">
            <option value="0"${Unless(s['useropts'].get('MailBestAnfr'), ' selected')}>nie</option>
            <option value="1"${If(s['useropts'].get('MailBestAnfr') == 1, ' selected')}>bei erster seit letztem Mal online</option>
            <option value="2"${If(s['useropts'].get('MailBestAnfr') == 2, ' selected')}>bei jeder</option>
          </select>
        </td>
      </tr><tr>
        <td>Benachrichtigungsmails bei Erledigung gesendeter Bestimmungsanfragen</td>
        <td>
          <select name="MAILBESTERL" onchange="if (this.value) check_mail_set()">
            <option value="0"${Unless(s['useropts'].get('MailBestErl'), ' selected')}>nie</option>
            <option value="1"${If(s['useropts'].get('MailBestErl') == 1, ' selected')}>bei erster seit letztem Mail online</option>
            <option value="2"${If(s['useropts'].get('MailBestErl') == 2, ' selected')}>bei jeder</option>
          </select>
        </td>
      </tr><tr title="Caching kann bei schlechter Internetverbindung oder langsamem Kartenserver (TMS) etwas bringen, sonst wird das Laden der Karte damit eher langsamer durch den Overhead.">
        <td>Default-Hintergrundkarte</td>
        <td>
          <select name="MAPDEFAULT">
            <option value=""${Unless(s['useropts'].get('MapDefault'), ' selected')}>Systemdefault</option>
            % for i in range(11):
            <option value="${i+1}"${If(s['useropts'].get('MapDefault')==i+1, ' selected')}>${['Tracestrack','OSM Carto','OpenTopoMap','basemap.at Orthofoto','basemap.at Gelände','basemap.at Oberfläche','basemap.at Verwaltung','basemap.at Verw.+HL','ESRI Arcgis','ESRI Clarity','geoimage.at'][i] |h}</option>
            % endfor
          </select>
        </td>
      </tr><tr>
        <td>Hintergrundkarte cachen</td>
        <td>
          <select name="MAPCACHE">
            <option value="0"${Unless(s['useropts'].get('MapCache'), ' selected')}>nein</option>
            <option value="1"${If(s['useropts'].get('MapCache'), ' selected')}>ja</option>
          </select>
          &#x2139;&#xFE0F;
        </td>
      </tr><tr>
        <td>Default-Autor für eigene Dateien</td>
        <td>
          <input type="text" name="FILEDFLTAUTOR" maxlength="100" value="${s['useropts'].get('FileDfltAutor') or '' |h}">
        </td>
      </tr><tr>
        <td>Default-Lizenz für eigene Dateien</td>
        <td>
          <input type="text" name="FILEDFLTLIZENZ" maxlength="200" value="${s['useropts'].get('FileDfltLizenz') or '' |h}" list="FILEDFLT_LL">
          <datalist id="FILEDFLT_LL">
            <option value="CC BY 4.0"></option>
            <option value="CC BY-SA 4.0"></option>
            <option value="CC BY-NC 4.0"></option>
            <option value="CC BY-NC-SA 4.0"></option>
            <option value="CC BY-ND 4.0"></option>
            <option value="CC BY-NC-ND 4.0"></option>
            <option value="Public Domain"></option>
          </datalist>
        </td>
      </tr><tr>
        <td>Default-Urheber für eigene Höhlen-Ortsangaben</td>
        <td>
          <input type="text" name="ORTDFLTAUTOR" maxlength="100" value="${s['useropts'].get('FileDfltAutor') or '' |h}">
        </td>
      </tr><tr>
        <td>Default-Lizenz für eigene Höhlen-Ortsangaben</td>
        <td>
          <input type="text" name="ORTDFLTLIZENZ" maxlength="100" value="${s['useropts'].get('OrtDfltLizenz') or '' |h}"  list="ORTDFLT_LL">
          <datalist id="ORTDFLT_LL">
            <option value="VDBL 1.0"></option>
            <option value="CC BY 4.0"></option>
            <option value="CC BY-SA 4.0"></option>
            <option value="CC BY-NC 4.0"></option>
            <option value="CC BY-NC-SA 4.0"></option>
            <option value="CC BY-ND 4.0"></option>
            <option value="CC BY-NC-ND 4.0"></option>
            <option value="DbCL 1.0"></option>
            <option value="Public Domain"></option>
          </datalist>
        </td>
      </tr><tr>
        <td>neues Fenster bei:</td>
        <td>
          <span style="margin-right:1em"><input type="checkbox" id="NEWWIN_LH" name="NEWWIN_LH" value="1"${If(s['useropts'].get('NewWinLH'), ' checked')}> <label for="NEWWIN_LH">Liste → Höhle</label></span>
          <span style="margin-right:1em"><input type="checkbox" id="NEWWIN_KH" name="NEWWIN_KH" value="1"${If(s['useropts'].get('NewWinKH'), ' checked')}> <label for="NEWWIN_KH">Karte ↔ Höhle</label></span>
          <span style="margin-right:1em"><input type="checkbox" id="NEWWIN_LF" name="NEWWIN_LF" value="1"${If(s['useropts'].get('NewWinLF'), ' checked')}> <label for="NEWWIN_LF">Liste → Fund</label></span>
          <span style="margin-right:1em"><input type="checkbox" id="NEWWIN_KF" name="NEWWIN_KF" value="1"${If(s['useropts'].get('NewWinKF'), ' checked')}> <label for="NEWWIN_KF">Karte ↔ Fund</label></span>
          <span><input type="checkbox" id="NEWWIN_HF" name="NEWWIN_HF" value="1"${If(s['useropts'].get('NewWinHF'), ' checked')}> <label for="NEWWIN_HF">Höhle ↔ Fund</label></span>
        </td>
      </tr>
    </table>
    <div style="text-align:center; margin-top:10px">
      <input type="submit" value="speichern">
    </div>
  </form>
  % elif 'userliste' in s: # IF_00003
  <div style="margin-top:10px">
    <form action="${s['scriptname']}">
      <input type="hidden" name="USERLIST" value="1">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      filtern nach Name
      <input type="text" name="USERSEARCH_NAME" value="${s['usersearch_name'] |h}" maxlength="20">
      <span style="margin-left:1em">Sortierung</span>
      <select name="USERSEARCH_SORT">
        <option value="I"${Unless(s['usersearch_sort'] == 'N', ' selected')}>ID</option>
        <option value="N"${If(s['usersearch_sort'] == 'N', ' selected')}>Name</option>
      </select>
      <input type="submit" value="Suche" style="margin-left:1em">
    </form>
  </div>
  <div style="margin-top:10px">
    <b>Benutzerliste:</b>
    % if s['userliste']:
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>ID</th>
        % if s['admin']:
        <th>Loginname</th>
        % endif
        <th>Name</th>
        <th>Mail</th>
        <th>zuletzt online</th>
        % if s['admin']:
        <th>letztes Login</th><th>Logout</th>
        % endif
        <th>Gruppen</th>
      </tr>
      % for u in s['userliste']:
      <tr onclick="openuser(${u['id']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px">${u['id']}</td>
        % if s['admin']:
        <td>${u['username']}</td>
        % endif
        <td>
          % if u['name_struktur'] == 'R':
            <b>${u['name1'] |h}</b>
          % elif u['name_struktur'] in ('FG','GP'):
            <b>${u['name1'] |h}</b> ${u['name2'] |h}
          % else:
            ${u['name1'] |h} <b>${u['name2'] |h}</h>
          % endif
        </td>
        <td>${u['mail'] |h}</td>
        <td>${u['active_d']}</td>
        % if s['admin']:
        <td>${u['login_d']}</td>
        <td>${u['logout_d']}</td>
        % endif
        <td>${u['gruppen'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    <br>nicht gefunden
    % endif
  </div>
  % elif 'grpliste' in s: # IF_00003
  <div style="margin-top:10px">
    <form action="${s['scriptname']}">
      <input type="hidden" name="GRPLIST" value="1">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      filtern nach Name
      <input type="text" name="GRPSEARCH_NAME" value="${s['grpsearch_name'] |h}" maxlength="20">
      <span style="margin-left:1em">Sortierung</span>
      <select name="GRPSEARCH_SORT">
        <option value="I"${Unless(s['grpsearch_sort'] == 'N', ' selected')}>ID</option>
        <option value="N"${If(s['grpsearch_sort'] == 'N', ' selected')}>Name</option>
      </select>
      <input type="submit" value="Suche" style="margin-left:1em">
    </form>
  </div>
  <div style="margin-top:10px">
    <b>Gruppen:</b>
    % if s['grpliste']:
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>ID</th><th>Name</th><th>Beschreibung</th><th>in Gruppen</th><th>Untergruppen</th>
      </tr>
      % for g in s['grpliste']:
      <tr onclick="opengrp(${g['id']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px"">${g['id']}</td>
        <td>${g['name'] |h}</td>
        <td>${g['beschreibung'] |h}</td>
        <td>${g['pgruppen'] |h}</td>
        <td>${g['cgruppen'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    <br>nicht gefunden
    % endif
  </div>
  % elif 'katgrliste' in s: # IF_00003
  <div style="margin-top:10px">
    <input type="button" value="alle ¬K in Karte" onclick="openkarte('KN')"> oder
    <input type="button" value="alle K in Karte" onclick="openkarte('KK')"> oder:
  </div>
  <div style="margin-top:2px">
    <form action="${s['scriptname']}">
      <input type="hidden" name="KATGRLIST" value="1">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      filtern nach:<span style="margin-left:1em">Nummer</span>
      <input type="text" name="KATGRSEARCH_NR" value="${s['katgrsearch_nr'] |h}" maxlength="5">
      <span style="margin-left:1em">Name</span>
      <input type="text" name="KATGRSEARCH_NAME" value="${s['katgrsearch_name'] |h}" maxlength="80">
      <span style="margin-left:1em">Sortierung</span>
      <select name="KATGRSEARCH_SORT">
        <option value="I"${Unless(s['katgrsearch_sort'] == 'N', ' selected')}>Nummer</option>
        <option value="N"${If(s['katgrsearch_sort'] == 'N', ' selected')}>Name</option>
      </select>
      <input type="submit" value="Suche" style="margin-left:1em">
    </form>
  </div>
  <div style="margin-top:10px">
    <b>Katastergruppen:</b>
    % if s['katgrliste']:
    (Zeile anklicken → Karte)
    <table class="liste">
      <tr>
        <th>Nr</th><th>Name</th><th>katasterführende Vereine</th>
      </tr>
      % for g in s['katgrliste']:
      <tr onclick="openkarte('K','${g['nr']}')" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px"">${g['nr']}</td>
        <td>${g['name'] |h}</td>
        <td>${g['vereine'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    <br>nicht gefunden
    % endif
  </div>
  % elif 'vrnliste' in s: # IF_00003
  <div style="margin-top:10px">
    <form action="${s['scriptname']}">
      <input type="hidden" name="VRNLIST" value="1">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      filtern nach Name
      <input type="text" name="VRNSEARCH_NAME" value="${s['vrnsearch_name'] |h}" maxlength="20">
      <span style="margin-left:1em">Sortierung</span>
      <select name="VRNSEARCH_SORT">
        <option value="I"${Unless(s['vrnsearch_sort'] == 'N', ' selected')}>ID</option>
        <option value="N"${If(s['vrnsearch_sort'] == 'N', ' selected')}>Name</option>
      </select>
      <input type="submit" value="Suche" style="margin-left:1em">
    </form>
  </div>
  <div style="margin-top:10px">
    <b>Vereine:</b>
    % if len(s['vrnliste']):
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>ID</th><th>Name</th><th>Rechtsform</th><th>Beschreibung</th><th>Homepage</th><th>in Verein/Verband</th>
      </tr>
      % for v in s['vrnliste']:
      <tr onclick="openverein(${v['id']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px"">${v['id']}</td>
        <td>${v['name'] |h}</td>
        <td>${v['rechtsform'] |h}</td>
        <td>${v['beschreibung'] |h}</td>
        <td>
          % if v['homepage']:
          <a href="${v['homepage'] |h}" onclick="event.stopPropagation()">${v['homepage'] |h}</a>
          % endif
        </td>
        <td>${v['pvereine'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    <br>noch keine erfasst
    % endif
    % if s['admin']:
    <div style="margin-top:10px"><input type="button" value="neu" onclick="openverein(0)"></div>
    % endif
  </div>
  % elif 'taxbaum' in s: # IF_00003
  <div style="margin-top:10px;margin-bottom:7px">
    <b>Baum nach <a href="https://www.catalogueoflife.org/">Catalogue of Life</a> 2023:</b>
  </div>
  <div style="display:flex;flex-wrap:wrap;align-items:center;margin-bottom:5px">
    <span>
      <input type="checkbox" checked id="FLIPLIT" onchange="flip_lit(this.checked)"> <label for="FLIPLIT">Autor/Jahr</label>
      <input type="checkbox" checked id="FLIPRANK" onchange="flip_rank(this.checked)" style="margin-left:1.3em"> <label for="FLIPRANK">Rang</label>
      <input type="checkbox" checked id="FLIPSYN" onchange="flip_syn(this.checked)" style="margin-left:1.3em"> <label for="FLIPSYN">Synonyme</label>
    </span>
    <span style="margin-left:1.5em;margin-right:5px"><b>Suche:</b></span>
    <span>
      <form name="TAXSEARCH_FRM" class="graytable" style="padding:2px;margin-right:5px" onsubmit="taxsearch();return false">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:1em" title="* (Stern) steht als Wildcard für beliebig viele Zeichen außer Leerzeichen. Die ersten 5 Zeichen dürfen kein Stern sein, sonst würde die Abfrage zu lang dauern. Beispiel: &quot;Scoli* *x&quot; findet Scoliopteryx libatrix.">Taxon <input type="text" name="TAXSEARCH" value="${s['inittaxsearch']}" required> &#x2139;&#xFE0F;</span>
          <span style="margin-right:1em"><input type="checkbox" id="TAXSEARCH_SYN" name="TAXSEARCH_SYN"> <label for="TAXSEARCH_SYN">auch Synonyme</label></span>
          <span style="margin-right:1em">Rang
            <select name="TAXSEARCH_RANG">
              <option value=""${Unless(s['inittaxsearch_rang'], ' selected')}>egal</option>
              % for r in s['raenge']:
              % if r['auswaehlbar']:
              <option value="${r['ebene']}"${If(s['inittaxsearch_rang'] == r['ebene'], ' selected')}>${r['col'] |h} (${r['name_de'] |h})</option>
              % endif
              % endfor
            </select>
          </span>
          <input type="submit" id="TAXSEARCH_SUBM" value="laden" style="margin-right:1em">
          <input type="reset" id="TAXSEARCH_RESET" value="reset" onclick="taxsearch_reset();return true">
        </span>
      </form>
    </span>
    <span><input type="checkbox" id="TAXSEARCH_AUSBL"${If(s['inittaxsearch'], ' checked')} onchange="taxsearch_ausbl(this.checked)"> <label for="TAXSEARCH_AUSBL">Rest ausblenden</label></span>
  </div>
  % for t in s['taxbaum']:
  <div id="TB_${t['id']}" class="tb">
    <span onclick="flip_tb(${t['id']})"><span id="TB_${t['id']}_INDENT" class="tb_indent"></span><span id="TB_SYM_${t['id']}" class="tb_indent">${tb_plus}</span> <span class="tb_taxon">${t['taxon'] |h}</span><span class="tb_rank"> [<span id="TB_${t['id']}_RANK">${t['rank']}</span>]</span></span>
    <div id="TB_${t['id']}_C"></div>
  </div>
  % endfor
  % endif # IF_00003
  % if 'ranglist' in s:
  <div style="margin-top:10px">
    <b>Ränge:</b>
    <table class="liste">
      <tr>
        <th>Ebene</th><th>lat. Name</th><th>lat. Abk.1</th><th>lat. Abk.2</th><th>dt. Name</th><th>engl. Name</th><th>in COL</th><th>Suffix Tiere</th><th>Suffix Pflanzen</th><th>Suffix Pilze</th><th>Suffix Algen</th><th>Suffix Bakterien</th><th>Suffix Viren</th><th>auswählbar</th>
      </tr>
      % for r in s['raenge']:
      <tr>
        % if r['synonym']:
        <td style="text-align:right">
          (=${r['ebene']})
        % else:
        <td style="text-align:right;padding-right:4px">
          ${r['ebene']}
        % endif
        </td>
        <td>${r['name_la'] |h}</td>
        <td>${r['name_la_abk1'] |h}</td>
        <td>${r['name_la_abk2'] |h}</td>
        <td>${r['name_de'] |h}</td>
        <td>${r['name_en'] |h}</td>
        <td>${r['col'] |h}</td>
        <td>${r['suffix_tiere'] |h}</td>
        <td>${r['suffix_pflanzen'] |h}</td>
        <td>${r['suffix_pilze'] |h}</td>
        <td>${r['suffix_algen'] |h}</td>
        <td>${r['suffix_bakterien'] |h}</td>
        <td>${r['suffix_viren'] |h}</td>
        <td>${'ja' if r['auswaehlbar'] else 'nein'}</td>
      </tr>
      % endfor
    </table>
  </div>
  <div style="margin-top:5px">Alle Fehler oder Änderungswünsche bitte dem Datenbankadministrator mitteilen.</div>
  % endif # ranglist
  % if 'alist' in s: # IF_00002
  <div style="margin-top:10px">
    <b>offene erhaltene Bestimmungsanfragen:</b>
    % if s['alist']['ein']:
    (sortiert nach Anfragedatum; Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>Anfragedatum</th><th>User</th><th>Besuchdatum</th><th>Besuch-ID</th><th>Fund-Index</th><th>Anmerkung</th>
      </tr>
      % for a in s['alist']['ein']:
      <tr onclick="openfund(${a['besuch_id']},${a['fund_i']})" style="cursor:pointer">
        <td>${a['gesendet_d']}</td>
        <td>${a['realname'] |h}</td>
        <td>${a['besuch_d']}</td>
        <td style="text-align:right;padding-right:2px">${a['besuch_id']}</td>
        <td style="text-align:right;padding-right:2px">${a['fund_i']}</td>
        <td>${a['anmerkung'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    <br>keine
    % endif
  </div>
  <div style="margin-top:10px">
    <b>gesendete Bestimmungsanfragen:</b>
    % if s['alist']['aus']:
    (absteigend sortiert nach Bearbeitungsdatum)
    <table class="liste">
      <tr>
        <th>Anfragedatum</th><th>an User</th><th>Besuchdatum</th><th>Besuch-ID</th><th>Fund-Index</th><th>Anmerkung</th><th>bearbeitet am</th><th>Status</th>
      </tr>
      % for a in s['alist']['aus']:
      <tr>
        <td>${a['gesendet_d']}</td>
        <td>${a['realname'] |h}</td>
        <td>${a['besuch_d']}</td>
        <td style="text-align:right;padding-right:2px">${a['besuch_id']}</td>
        <td style="text-align:right;padding-right:2px"><a href="javascript:openfund(${a['besuch_id']},${a['fund_i']})">${a['fund_i']}</a></td>
        <td>${a['anmerkung'] |h}</td>
        <td>${a['erledigt_d']}</td>
        <td>
          % if a['status'] == 'N':
          offen
          % elif a['status'] == 'O':
          obsolet
          % elif a['status'] == 'E':
          bestimmt als: ${a['bestimmt_als'] |h}
          % else: # kann sonst nur 'A' sein
          abgelehnt
            % if a['ablehngrund']:
            mit der Begründung: ${a['ablehngrund'] |h}
            % endif
          % endif
        </td>
      </tr>
      % endfor
    </table>
    % else:
    <br>keine
    % endif
  </div>
  % elif 'dsearch_id' in s: # IF_00002
  <div style="margin-top:5px">
    % if s['hdzuo_h']:
    Dateien suchen für Zuordnung zur Höhle ${s['hdzuo_h_text'] |h}:
    % elif s['fdzuo_b']:
    Dateien suchen für Zuordnung zum Fund ${s['fdzuo_b']}/${s['fdzuo_i']}:
    % elif s['pdzuo_p']:
    Dateien suchen für Zuordnung zur Publikation ${s['pdzuo_text'] |h}:
    % else:
    Dateien suchen:
    % endif
  </div>
  <form name="DSEARCH_FRM">
    <table class="graytable">
      <tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">
            ID
            <input type="number" name="DSEARCH_ID" size="8" value="${s['dsearch_id']}">
          </span>
          <span style="margin-right:2em">
            Name
            <input type="text" name="DSEARCH_FILENAME" size="20" value="${s['dsearch_filename'] |h}">
            <select name="DSEARCH_RE">
              <option value="S" ${If(s['dsearch_re'] in('','S'), ' selected')}>Substring</option>
              <option value="X" ${If(s['dsearch_re']=='X', ' selected')}>exakt</option>
              <option value="A" ${If(s['dsearch_re']=='A', ' selected')}>Anfang</option>
              <option value="E" ${If(s['dsearch_re']=='E', ' selected')}>Ende</option>
              <option value="R" ${If(s['dsearch_re']=='R', ' selected')}>PCRE</option>
              <option value="G" ${If(s['dsearch_re']=='G', ' selected')}>Globbing</option>
            </select>
          </span>
          MIME-Type
          <select name="DSEARCH_MIME">
            <option value=""${Unless(s['dsearch_mime'], ' selected')}>egal</option>
            ##<option value="L"${{If(s['dsearch_mime']=='L', ' selected')}>leer</option>
            % for t in s['dsearch_mimetypes']:
            <option value="${t |h}"${If(s['dsearch_mime']==t, ' selected')}>${t |h}</option>
            % endfor
          </select>
        </span>
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">
            verantw. User:
            % if s['user']:
            <label for="DSEARCH_OWNER_EIGENE" style="margin-left:1em">eigene</label><input type="checkbox" id="DSEARCH_OWNER_EIGENE" name="DSEARCH_OWNER_EIGENE" value="1"${If(s['dsearch_owner_eigene'], ' checked')} onchange="this.form.DSEARCH_OWNER.disabled=this.checked">
            % endif
            <span style="margin-left:1em">Name</span> <input type="text" name="DSEARCH_OWNER" value="${s['dsearch_owner'] |h}"${If(s['dsearch_owner_eigene'], ' disabled')}>
          </span>
          <span>
            hochgeladen von User:
            % if s['user']:
            <label for="DSEARCH_UPLOADER_EIGENE" style="margin-left:1em">eigene</label><input type="checkbox" id="DSEARCH_UPLOADER_EIGENE" name="DSEARCH_UPLOADER_EIGENE" value="1"${If(s['dsearch_uploader_eigene'], ' checked')} onchange="this.form.DSEARCH_UPLOADER.disabled=this.checked">
            % endif
            <span style="margin-left:1em">Name</span> <input type="text" name="DSEARCH_UPLOADER" value="${s['dsearch_uploader'] |h}"${If(s['dsearch_uploader_eigene'], ' disabled')}>
          </span>
        </span>
      </td></tr><tr><td class="horizborder">
        Anlagedatum von <input type="date" name="DSEARCH_ANL_VON" value="${s['dsearch_anl_von'] |h}"> bis <input type="date" name="DSEARCH_ANL_BIS" value="${s['dsearch_anl_bis'] |h}" style="margin-right:1em">
        Uploaddatum von <input type="date" name="DSEARCH_UPL_VON" value="${s['dsearch_upl_von'] |h}"> bis <input type="date" name="DSEARCH_UPL_BIS" value="${s['dsearch_upl_bis'] |h}">
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          Höhlen:
          <select name="DSEARCH_HOEHLEN" onchange="this.form.DSEARCH_HOEHLE_ID.disabled = this.form.DSEARCH_KATEGORIE.disabled = this.value!='H'">
            <option value=""${Unless(s['dsearch_hoehlen'], ' selected')}>egal</option>
            <option value="H"${If(s['dsearch_hoehlen'] == 'H', ' selected')}>ausschließlich</option>
            <option value="K"${If(s['dsearch_hoehlen'] == 'K', ' selected')}>keine</option>
          </select>
          <span style="margin-left:1em">Katnr(ID,Name)</span>
          <select name="DSEARCH_HOEHLE_ID"${Unless(s['dsearch_hoehlen'] == 'H', ' disabled')}>
            <option value=""${Unless(s['dsearch_hoehle_id'], ' selected')}>egal</option>
            % for h in s['hoehlen']:
            <option value="${h['id']}"${If(s['dsearch_hoehle_id'] == h['id'], ' selected')}>${If(h['katnr'], h['katnr'] + ' (') |h}${h['id']}${If(h['name'], ', ' + h['name']) |h}${If(h['katnr'], ')')}</option>
            % endfor
          </select>
          <span style="margin-left:1em">Kategorie</span>
          <select name="DSEARCH_KATEGORIE"${Unless(s['dsearch_hoehlen'] == 'H', ' disabled')} style="margin-right:1em">
            <option value=""${Unless(s['dsearch_kategorie'], ' selected')}>egal</option>
            <option value="M"${If(s['dsearch_kategorie'] == 'M', ' selected')}>Foto/Video/Audio</option>
            <option value="P"${If(s['dsearch_kategorie'] == 'P', ' selected')}>Plan</option>
            <option value="B"${If(s['dsearch_kategorie'] == 'B', ' selected')}>Bericht/Beschreibung</option>
          </select>
          <span style="margin-left:3em;margin-right:1em">Besuch-ID <input type="number" name="DSEARCH_BESUCH_ID" size="6" value="${s['dsearch_besuch_id']}"></span>
          <span>Fund-Index <input type="number" name="DSEARCH_FUND_I" size="3" value="${s['dsearch_fund_i']}"></span>
        </span>
      % if not s['fdzuo_b']:
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">
            <label for="DSEARCH_ALLEVERS">auch alte Dateiversionen</label>
            <input type="checkbox" value="1" id="DSEARCH_ALLEVERS" name="DSEARCH_ALLEVERS"${If(s['dsearch_allevers'], ' checked')}>
          </span>
        </span>
      % endif
      </td></tr>
    </table>
    <div style="margin-top:8px">
      <input type="hidden" name="DLIST" value="2">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="DEL" id="DLIST_DEL" value="">
      Sortierung:
      <select name="SORT1" onchange="if (this.value==this.form.SORT2.value) this.form.SORT2.value=(this.value=='I' ? 'V':'I')">
        <option value="I"${If(s['sort1'] in ('','I'), ' selected')}>ID</option>
        <option value="N"${If(s['sort1'] == 'N', ' selected')}>Dateiname</option>
        <option value="G"${If(s['sort1'] == 'G', ' selected')}>Größe</option>
        <option value="A"${If(s['sort1'] == 'A', ' selected')}>Anlagedatum</option>
        <option value="U"${If(s['sort1'] == 'U', ' selected')}>Uploaddatum</option>
      </select>
      <select name="SORT1_DIR">
        <option value="ASC"${Unless(s['sort1_dir'] == 'DESC', ' selected')}>aufsteigend</option>
        <option value="DESC"${If(s['sort1_dir'] == 'DESC', ' selected')}>absteigend</option>
      </select>
      dann
      <select name="SORT2">
        <option value="I"${If(s['sort2'] == 'I', ' selected')}>ID</option>
        <option value="V"${If(s['sort2'] == 'V' or s['sort2'] == '' and s['sort1'] in ('','I'), ' selected')}>Version</option>
        <option value="N"${If(s['sort2'] == 'N', ' selected')}>Dateiname</option>
        <option value="G"${If(s['sort2'] == 'G', ' selected')}>Größe</option>
        <option value="A"${If(s['sort2'] == 'A', ' selected')}>Anlagedatum</option>
        <option value="U"${If(s['sort2'] == 'U', ' selected')}>Uploaddatum</option>
      </select>
      <select name="SORT2_DIR">
        <option value="ASC"${Unless(s['sort2_dir'] == 'DESC', ' selected')}>aufsteigend</option>
        <option value="DESC"${If(s['sort2_dir'] == 'DESC', ' selected')}>absteigend</option>
      </select>
      <input type="submit" value="Suche" style="margin-left:2em;border:2px solid black">
      % if s['fdzuo_b']:
      <input type="button" value="zurück zum Fund" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID=${s['fdzuo_b']}&FUND_I=${s['fdzuo_i']}'" style="margin-left:1em">
      % elif s['hdzuo_h']:
      <input type="button" value="zurück zur Höhle" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID=${s['hdzuo_h']}'" style="margin-left:1em">
      % elif s['pdzuo_p']:
      <input type="button" value="zurück zur Publikation" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&PUB_ID=${s['pdzuo_p']}'" style="margin-left:1em">
      % endif
    </div>
    % if s['hdzuo_h']:
    <input type="hidden" name="HDZUO_H" value="${s['hdzuo_h']}">
    % elif s['fdzuo_b']:
    <input type="hidden" name="FDZUO_B" value="${s['fdzuo_b']}">
    <input type="hidden" name="FDZUO_I" value="${s['fdzuo_i']}">
    % elif s['pdzuo_p']:
    <input type="hidden" name="PDZUO_P" value="${s['pdzuo_p']}">
    % endif
  </form>
  % if 'dliste' in s:
  <div style="margin-top:8px">
  % if len(s['dliste']):
    <b>Suchergebnis:</b> (Zeile anklicken für Details)
    <table class="liste">
      <tr>
        <th>id</th><th>Version</th><th>Dateiname</th><th>Mime-Typ</th><th>Größe</th><th>hochgeladen am</th><th>verantw. User</th><th>hochgeladen von</th><th>Recht</th><th>Aktionen</th><th>Auswahl</th>
      </tr>
      % for d in s['dliste']:
      <tr onclick="ddetails(${d['id']},${d['v']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px">${d['id']}</td>
        <td style="text-align:right;padding-right:2px">${d['v']}</td>
        <td>${d['filename'] |h}</td>
        <td>${d['mime'] |h}</td>
        <td style="text-align:right;padding-right:2px">${d['size']}</td>
        <td>${d['upload_d']}</td>
        <td>${d['owner'] |h}</td>
        <td>${d['uploader'] |h}</td>
        <td>${['keines','lesen','schreiben'][d['recht']]}</td>
        % if d['recht']:
        <td onclick="event.stopPropagation()" style="cursor:default">
          <input type="button" value="anzeigen" style="margin-left:3px;margin-right:5px" onclick="window.open('${s['scriptname']}?SESS=${s['sessionid']}&OPENFILE=${d['id']}&V=${d['v']}','_blank')">
          <a href="media/${d['id']}_${d['code']}_${d['v']}" target="_blank" type="${d['mime'] |h}" download="${d['filename'] |h}" style="margin-right:3px"><input type="button" value="download"></a>
        </td>
        % else:
        <td/>
        % endif
        % if d['recht']:
        <td style="text-align:center" onclick="this.firstChild.checked = !this.firstChild.checked;event.stopPropagation()"><input type="checkbox" name="DCHK${d['id']}_${d['v']}" value="1" onclick="event.stopPropagation()"><input type="hidden" id="DRECHT${d['id']}_${d['v']}" value="${d['recht']}"></td>
        % else:
        <td style="text-align:center"><input type="checkbox" disabled onclick="event.stopPropagation()"></td>
        % endif
      </tr>
      % endfor
    </table>
    <div style="margin-top:5px">
      auswählen:
      <input type="button" value="alle" onclick="for (const c of document.getElementsByTagName('input')) if (c.name.substring(0,4)=='DCHK') c.checked=true">
      <input type="button" value="keine" onclick="for (const c of document.getElementsByTagName('input')) if (c.name.substring(0,4)=='DCHK') c.checked=false" style="margin-right:2em">
      ausgewählte:
      <a id="ZIPBTN" target="_blank" type="application/zip" download="biodb_download.zip"><input type="button" value="download ZIP" onclick="let v='';for (const c of document.getElementsByTagName('input')) if (c.name.substring(0,4)=='DCHK' && c.checked) v+=(v?',':'')+c.name.substring(4); document.getElementById('ZIPBTN').href=v?'${s['scriptname']}?SESS=${s['sessionid']}&ZIP='+v:&quot;javascript:alert('nichts ausgewählt')&quot;" style="margin-left:1em"></a>
      % if s['user']:
      <span title="von ausgewählten Dateien alle(!) Versionen löschen" style="margin-left:1em"><input type="button" value="löschen" onclick="let v='',anz=0;for (const c of document.getElementsByTagName('input')) if (c.name.substring(0,4)=='DCHK' && c.checked) {if (document.getElementById('DRECHT'+c.name.substring(4)).value!='2') {alert('kein Schreibrecht auf ID='+c.name.substring(4,c.name.indexOf('_')));return false} anz++;v+=(v?',':'')+c.name.substring(4)} if(anz){if(confirm(anz+' Datei'+(anz>1?'en':'')+' löschen?')){document.getElementById('DLIST_DEL').value=v;document.forms.DSEARCH_FRM.submit()}}else alert('nichts ausgewählt')"> &#x2139;&#xFE0F;</span>
      % if s['fdzuo_b']:
      <form name="FD_ZUO_FRM" onsubmit="do_fd_zuo(this,${s['fdzuo_b']},${s['fdzuo_i']});return false" style="margin-left:1em;display:inline-block">
        zu Fund ${s['fdzuo_b']}/${s['fdzuo_i']}
        <input type="checkbox" id="FD_ZUO_H" name="FD_ZUO_H" value="1" checked> <label for="FD_ZUO_H">und Höhle</label>
        <input id="FD_ZUO_BTN" type="submit" value="zuordnen">
      </form>
      % elif s['pdzuo_p']:
      <form name="PD_ZUO_FRM" onsubmit="do_pd_zuo(this,${s['pdzuo_p']});return false" style="margin-left:1em;display:inline-block">
        zu Publikation ${s['pdzuo_text'] |h}
        als
        <select name="PD_ZUO_ABSCHNITT" required>
          <option value="" selected>bitte auswählen</option>
          <option value="H">Hauptdokument</option>
          <option value="A">Anhang</option>
        </select>
        <input id="PD_ZUO_BTN" type="submit" value="zuordnen">
      </form>
      % else:
      <form name="HD_ZUO_FRM" onsubmit="do_hd_zuo(this);return false" style="margin-left:1em;display:inline-block">
        zu Höhle
        <select name="HD_ZUO_H"${If(s['hdzuo_h'], ' disabled')}>
          % if s['hdzuo_h']:
          <option value="${s['hdzuo_h']}" selected>${s['hdzuo_h_text'] |h}</option>
          % else:
          <option value="" selected>bitte auswählen</option>
          ## Rest wird in body onload aus anderer Selectbox kopiert, damit nicht die doppelte Menge vom Server übertragen wird
          % endif
        </select>
        als
        <select name="HD_ZUO_K">
          <option value="M" selected>Foto/Video/Audio</option>
          <option value="P">Plan</option>
          <option value="B">Bericht/Beschreibung</option>
        </select>
        <input id="HD_ZUO_BTN" type="submit" value="zuordnen">
        % if not s['hdzuo_h']: # sonst Button gleich unterm Suchformular
        <input type="button" value="zur Höhle" onclick="if (!this.form.HD_ZUO_H.value) alert('noch keine Höhle ausgewählt'); else location.href='${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID=' + this.form.HD_ZUO_H.value">
        % endif
      </form>
      % endif
      % endif # if s['user']:
    </div>
  % else: # if len(s['dliste'])
  keine gefunden
  % endif # if len(s['dliste'])
  </div>
  % endif # if 'dliste' in s
  % elif 'd_det_id' in s: # IF_00002
  <div style="margin-top:5px"><b>Datei-Details:</b></div> 
  <form name="D_DET_FRM" method="post" enctype="multipart/form-data" action="${s['scriptname']}" onsubmit="return chk_newkat(this)">
    <table><tr><td>
      <table class="graytable">
        <tr>
          <td class="horizborder" colspan="2">
            <span style="margin-right:2em">Datei-ID: <b>${s['d_det_id']}</b></span>
            <span style="margin-right:1em">verantw. User</span>
            % if s['admin'] or s['user_is_owner']:
            <select name="D_DET_OWNER" required style="margin-right:2em">
              % for u in s['d_det_users']:
              <option value="${u['id']}"${If(u['id']==s['d_det_owner_id'], ' selected')}>${u['name'] |h}</option>
              % endfor
            </select>
            % else:
            <a href="javascript:openuser(${s['d_det_owner_id']})">${[u['name'] for u in s['d_det_users'] if u['id']==s['d_det_owner_id']][0] |h}</a> <span title="Link öffnet in neuem Fenster" style="margin-right:2em">&#x2139;&#xFE0F</span>
            % endif
            angelegt am ${s['d_det_angelegt_d']}
            % if s['d_det_geaendert_von'] and s['d_det_geaendert_d']:
            <span style="margin-left:1em">
            geändert von ${s['d_det_geaendert_von'] |h} am ${s['d_det_geaendert_d']}
            </span>
            % endif
          </td>
        </tr>
        <tr>
          <td class="horizborder" colspan="2">
            Version
            <input type="button" value="älteste" ${If(s['d_det_v']==1, 'disabled', 'onclick="ddetails('+str(s['d_det_id'])+',1)"')}>
            <input type="button" value="<<" ${If(s['d_det_v']==1, 'disabled', 'onclick="ddetails('+str(s['d_det_id'])+','+str(s['d_det_v']-1)+')"')}>
            <b>${s['d_det_v']}</b> von ${s['d_det_maxv']}
            <input type="button" value=">>" ${If(s['d_det_v']==s['d_det_maxv'], 'disabled', 'onclick="ddetails('+str(s['d_det_id'])+','+str(s['d_det_v']+1)+')"')}>
            <input type="button" value="neueste" ${If(s['d_det_v']==s['d_det_maxv'], 'disabled', 'onclick="ddetails('+str(s['d_det_id'])+','+str(s['d_det_maxv'])+')"')} style="margin-right:2em">
            hochgeladen von ${s['d_det_uploader'] |h} am ${s['d_det_upload_d']}
            <div style="margin-top:2px">
              Änderungskommentar
              <input type="text" name="D_DET_AENDKOMM" maxlength="100" size="50" value="${s['d_det_aendkomm'] |h}"${Unless(s['d_det_recht']==2, ' readonly')} style="margin-right:2em">
              zuletzt bearbeitet von ${s['d_det_lastuser'] |h}
            </div>
            <div style="margin-top:2px">
              Dateiname <input type="text" name="D_DET_FILENAME" maxlength="100" size="40" value="${s['d_det_filename'] |h}"${Unless(s['d_det_recht']==2, ' readonly')} style="margin-right:2em">
              MIME-Type <input type="text" name="D_DET_MIME" maxlength="30" size="10" value="${s['d_det_mime'] |h}"${Unless(s['d_det_recht']==2, ' readonly')} style="margin-right:2em">
              Dateigröße:
              % if not s['d_det_size_str']:
              ?
              % elif s['d_det_size'] < 1024:
              ${s['d_det_size']} Bytes
              % else:
              ${s['d_det_size_str']} (${s['d_det_size']} Bytes)
              % endif
            </div>
            % if s['d_det_recht'] == 2:
            <div style="margin-top:2px">
              <fieldset>
                <legend>andere Datei hochladen</legend>
                <table>
                  <tr><td>
                    <input type="file" name="D_DET_NEUDATEI" capture="environment" onchange="document.getElementById('D_DET_RESET').disabled = this.form.D_DET_FILENAME_AUTO.disabled = this.form.D_DET_MIME_AUTO.disabled = this.value=='';for (i of document.getElementsByName('D_DET_VERSIONIEREN')) i.disabled=this.value==''">
                    <input type="button" id="D_DET_RESET" value="Auswahl rückgängig" onclick="this.form.D_DET_NEUDATEI.value='';this.disabled=this.form.D_DET_FILENAME_AUTO.disabled=this.form.D_DET_MIME_AUTO.disabled=true;for (i of document.getElementsByName('D_DET_VERSIONIEREN')) i.disabled=true" disabled style="margin-right:2em">
                    Dateiname
                    <select name="D_DET_FILENAME_AUTO" style="margin-right:1em" diabled>
                      <option value="1" selected>von neuer Datei übernehmen</option>
                      <option value="">wie oben angegeben</option>
                    </select>
                   MIME-Type
                    <select name="D_DET_MIME_AUTO" disabled>
                      <option value="1" selected>automatisch</option>
                      <option value="">wie oben angegeben</option
                    </select>
                  </td></tr>
                  <tr><td>
                  <fieldset>
                  <legend>bei JPG</legend>
                  ${verkl_block()}
                  </fieldset>
                  </td></tr>
                  % if s['d_det_ersetzrecht']:
                  <tr><td>
                    <span title="Wer eine Dateiversion hochgeladen hat, kann sie überschreiben. Beim Versionieren wird hingegen eine neue Version angelegt, die bestehende bleibt erhalten.">
                    <input type="radio" id="D_DET_R_VERS" name="D_DET_VERSIONIEREN" value="1" checked disabled> <label for="D_DET_R_VERS">versionieren</label>
                    <input type="radio" id="D_DET_R_UEBERSCH" name="D_DET_VERSIONIEREN" value="" disabled style="margin-left:1em"> <label for="D_DET_R_UEBERSCH">überschreiben</label> &#x2139;&#xFE0F;</span>
                  </td></tr>
                  % endif
                </table>
              </fieldset>
            </div>
            % endif
          </td>
        </tr>
        <tr>
          <td class="horizborder">
            Beschreibung
          </td>
          <td class="horizborder">
            <textarea name="D_DET_BESCHR" cols="80" maxlength="1000" rows="3"${Unless(s['d_det_recht']==2, ' readonly')}>${s['d_det_beschreibung'] |h}</textarea>
          </td>
        </tr>
        ## folgende 2 Tabellenzeilen sind von der Ortsmaske hierher kopiert. Sollten vielleicht besser an einer einzigen Stelle als Block definiert werden.
        <tr>
          <td class="horizborder"><span style="margin-right:1em">Urheberrecht:</span></td>
          <td class="horizborder">
            <span style="display:flex;flex-wrap:wrap">
              <table style="border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span title="Autor (wenn bekannt) oder Quelle" style="margin-right:5px">Autor &#x2139;&#xFE0F;</span></td>
                  <td><input type="text" name="D_DET_QUELLE" size="50" maxlength="100" value="${s['d_det_quelle'] |h}"${Unless(s['d_det_recht']==2, ' readonly')}></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Lizenz</span></td>
                  <td>
                    <input type="text" name="D_DET_LIZENZ" value="${s['d_det_lizenz'] |h}" list="D_DET_LL" maxlength="200" size="50"${Unless(s['d_det_recht']==2, ' readonly')}>
                    <datalist id="D_DET_LL">
                      <option value="CC BY 4.0"></option>
                      <option value="CC BY-SA 4.0"></option>
                      <option value="CC BY-NC 4.0"></option>
                      <option value="CC BY-NC-SA 4.0"></option>
                      <option value="CC BY-ND 4.0"></option>
                      <option value="CC BY-NC-ND 4.0"></option>
                      <option value="Public Domain"></option>
                    </datalist>
                  </td>
                </tr>
              </table>
            </span>
          </td>
        </tr>
        <tr>
          <td class="horizborder"><span style="margin-right:1em">Zugriffsrechte:</span></td>
          <td class="horizborder">
            <span style="display:flex;flex-wrap:wrap">
              <table style="border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Geheimhaltungsgrund</span></td>
                  <td><input type="text" name="D_DET_GEHEIM" maxlength="100" size="30" value="${s['d_det_geheim'] |h}"${Unless(s['d_det_recht']==2, ' readonly')}></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
                <tr>
                  <td title="Lesezugriff einschränken auf die angegebenen Benutzergruppen. Mehrfachauswahl möglich, Untergruppen sind aber schon automatisch inkludiert. Ebenso haben Gruppen mit Schreibrecht automatisch auch Lesezugriff."><span style="margin-right:5px">Leserecht für Gruppen &#x2139;&#xFE0F;</span></td>
                  <td><select name="D_DET_GRUPPEN_R" multiple size="3"${Unless(s['user_is_owner'] or s['admin'], ' disabled')}>
                    % for g in s['gruppen']:
                    <option value="${g['id']}"${If(g['r'],' selected')}>${g['name'] |h}</option>
                    % endfor
                  </select></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse">
                <tr>
                  <td title="Schreib- und Leserecht an die angegebenen Benutzergruppen. Mehrfachauswahl möglich, Untergruppen sind aber schon automatisch inkludiert. Ebenso haben immer auch der für die Datei verantwortliche Benutzer und Administratoren Schreibrecht. Schreibrecht impliziert Leserecht."><span style="margin-right:5px">Schreibrecht für Gruppen &#x2139;&#xFE0F;</span></td>
                  <td><select name="D_DET_GRUPPEN_W" multiple size="3"${Unless(s['user_is_owner'] or s['admin'], ' disabled')}>
                    % for g in s['gruppen']:
                    <option value="${g['id']}"${If(g['w'],' selected')}>${g['name'] |h}</option>
                    % endfor
                  </select></td>
                </tr>
              </table>
            </span>
          </td>
        </tr>
        <tr>
          <td class="horizborder"><span style="margin-right:1em">zugeordnete Höhlen:</span></td>
          <td class="horizborder"><div style="display:flex;flex-wrap:wrap">
            % for h_id in s['d_det_hoehlen']:
            <span style="display:flex;flex-wrap:wrap">
              <select name="D_DET_HOEHLE_ID${h_id}" style="margin-right:5px">
                % for h in s['hoehlen']:
                % if h['id'] == h_id:
                 <option value="${h['id']}">${If(h['katnr'], h['katnr'] + ' (') |h}${h['id']}${If(h['name'], ', ' + h['name']) |h}${If(h['katnr'], ')')}</option>
                % endif
                % endfor
              </select>
              als
              <select name="D_DET_KATEGORIE${h_id}" style="margin-left:5px;margin-right:2em">
                <option value="M"${If(s['d_det_hoehlen'][h_id]=='M',' selected', Unless(s['d_det_recht']==2, ' disabled'))}>Foto/Video/Audio</option>
                <option value="P"${If(s['d_det_hoehlen'][h_id]=='P',' selected', Unless(s['d_det_recht']==2, ' disabled'))}>Plan</option>
                <option value="B"${If(s['d_det_hoehlen'][h_id]=='B',' selected', Unless(s['d_det_recht']==2, ' disabled'))}>Bericht/Beschreibung</option>
                % if s['d_det_recht']==2:
                <option value="">Zuordnung löschen</option>
                % endif
              </select>
            </span>
            % endfor
            % if s['d_det_recht'] == 2:
            <span id="D_DET_NEWKAT" style="display:flex;flex-wrap:wrap">
              <input type="button" id="D_DET_NEWKAT_BTN" value="+" onclick="add_dathoe()">
            </span>
            % endif
          </div></td>
        </tr>
      </table>
    </td></tr>
    % if s['d_det_recht']:
    <tr><td>
      <div style="text-align:center; margin-top:5px">
        % if s['d_det_recht'] == 2:
        <input type="hidden" name="D_DET_ID" value="${s['d_det_id']}">
        <input type="hidden" name="D_DET_V" value="${s['d_det_v']}">
        <input type="hidden" name="SESS" value="${s['sessionid']}">
        <input type="hidden" name="IDEM" value="${s['idem']}">
        <span style="margin-right:2em">
          <input type="submit" value="speichern" style="border:2px solid black">
          % if s['user_is_owner'] or s['admin']:
          <input type="button" value="${If(s['d_det_maxv']>1, 'alle Versionen löschen','löschen')}" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_DET_ID=${s['d_det_id']}&IDEM=${s['idem']}&D_DET_DEL=2'" style="margin-left:2em">
          % endif
          % if s['d_det_ersetzrecht'] and s['d_det_v'] > 1 and s['d_det_v'] == s['d_det_maxv']:
          <input type="button" value="diese (neueste) Version löschen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_DET_ID=${s['d_det_id']}&D_DET_V=${s['d_det_v']}&IDEM=${s['idem']}&D_DET_DEL=1'" style="margin-left:1em">
          % endif
        </span>
        % endif
        <input type="button" value="anzeigen" style="margin-right:1em" onclick="window.open('${s['scriptname']}?SESS=${s['sessionid']}&OPENFILE=${s['d_det_id']}&V=${s['d_det_v']}','_blank')">
        <a href="media/${s['d_det_id']}_${s['d_det_code']}_${s['d_det_v']}" target="_blank" type="${s['d_det_mime'] |h}" download="${s['d_det_filename'] |h}"><input type="button" value="download"></a>
      </div>
    </td></tr>
    % endif # if s['d_det_recht']
    </table>
  </form>
  % elif 'd_upl' in s: # IF_00002
  <div style="margin-top:5px"><b>Datei(en) hochladen:</b></div>
  <form name="D_UPL" method="post" enctype="multipart/form-data" action="${s['scriptname']}">
    <table><tr><td>
      <table class="graytable">
        <tr>
          <td class="horizborder" colspan="2">
            <input type="file" name="DATEI" multiple required capture="environment" onchange="document.getElementById('DATEI_RESET').disabled=this.value==''">
            <input type="button" id="DATEI_RESET" value="Auswahl rückgängig" onclick="this.form.DATEI.value='';this.disabled=true" disabled>
          </td>
        </tr>
        <tr>
          <td class="horizborder">Dateiname:</td>
          <td class="horizborder">
            <span title="wenn leer, dann werden die Dateinamen übernommen">ändern auf &#x2139;&#xFE0F; <input type="text" name="DFILENAME" maxlength="100" size="20" onchange="this.form.DNUM.disabled=!this.value"></span>
            <span title="Wird vor der Dateieindung eingefügt, z.B. spinne_.jpg ⟶ spinne_1.jpg, spinne_2.jpg usw."><input type="checkbox" id="DNUM" name="DNUM" value="1" disabled><label for="DNUM">fortlaufende Nummer einfügen</label> &#x2139;&#xFE0F;</span>
          </td>
        </tr>
        <tr>
          <td>
            MIME-Type
          </td>
          <td>
            <span style="margin-right:1em"><input type="radio" id="DMIME_A" name="DMIME" value="A" onchange="this.form.DMIME_MNL.disabled=this.form.DMIME.value!='M'" checked> <label for="DMIME_A">auto</label></span>
            <span style="margin-right:1em"><input type="radio" id="DMIME_K" name="DMIME" value="K" onchange="this.form.DMIME_MNL.disabled=this.form.DMIME.value!='M'"> <label for="DMIME_K">keiner</label></span>
            <input type="radio" id="DMIME_M" name="DMIME" value="M" onchange="this.form.DMIME_MNL.disabled=this.form.DMIME.value!='M'"> <label for="DMIME_M">manuell</label>: <input type="text" name="DMIME_MNL" maxlength="30" size="10" disabled>
          </td>
        </tr>
        <tr>
          <td class="horizborder">
            bei JPG:
          </td>
          <td class="horizborder">
            ## geht im Moment nicht weil Tools wie jhead nicht installiert
            ##<span style="margin-right:1em">
            ##  <input type="checkbox"> zurechtdrehen
            ##</span>
            ## Mako-"block" geht hier nicht, weil If() zum Parsing-Zeitpunkt noch nicht bekannt ist
            <%def name="verkl_block()">
            <span>
              <span style="margin-right:1em">
                autom. verkleinern
                <select name="VERKL_REDU" onchange="this.form.VERKL_ABS.disabled=this.form.VERKL_ABS_EINH.disabled=(['N','R'].includes(this.value));this.form.VERKL_REL.disabled=(['N','A'].includes(this.value));this.form.VERKL_MIN.disabled=this.form.VERKL_MIN_EINH.disabled=this.form.VERKL_QUAL.disabled=this.form.VERKL_SKAL.disabled=this.value=='N';this.form.VERKL_SKAL_PX.disabled=this.form.VERKL_SKAL_MINPX.disabled=this.value=='N' || !this.form.VERKL_SKAL.checked">
                  <option value="N"${If(s['verkl_redu']=='N', ' selected')}>nein</option>
                  <option value="A"${If(s['verkl_redu']=='A', ' selected')}>wenn Größenreduktion abs.</option>
                  <option value="R"${If(s['verkl_redu']=='R', ' selected')}>wenn Größenreduktion rel.</option>
                  <option value="B"${If(s['verkl_redu']=='B', ' selected')}>wenn Größenreduktion abs. oder rel.</option>
                </select>
              </span>
              <span style="margin-right:1em">
                abs.
                <input name="VERKL_ABS" type="number" value="${s['verkl_abs']}" min="1" max="1023" size="5"${If(s['verkl_redu'] in('N','R'),' disabled')}>
                <select name="VERKL_ABS_EINH"${If(s['verkl_redu'] in('N','R'),' disabled')}>
                  <option value="K"${If(s['verkl_abs_einh']=='K', ' selected')}>K</option>
                  <option value="M"${If(s['verkl_abs_einh']=='M', ' selected')}>M</option>
                </select>
              </span>
              <span style="margin-right:1em">
                rel. [%]
                <input name="VERKL_REL" type="number" value="${s['verkl_rel']}" min="20" max="99" size="3"${If(s['verkl_redu'] in('N','A'),' disabled')}>
              </span>
              <span>
                falls Ausgangsdatei ≥
                <input name="VERKL_MIN" type="number" value="${s['verkl_min']}" min="1" max="1023" size="5"${If(s['verkl_redu']=='N',' disabled')}>
                <select name="VERKL_MIN_EINH"${If(s['verkl_redu']=='N',' disabled')}>
                  <option value="K"${If(s['verkl_min_einh']=='K', ' selected')}>K</option>
                  <option value="M"${If(s['verkl_min_einh']=='M', ' selected')}>M</option>
                </select>
              </span>
              <br>
              <span style="margin-right:1em">
                Qualität [%]
                <input name="VERKL_QUAL" type="number" value="${s['verkl_qual']}" min="10" max="99" size="3"${If(s['verkl_redu']=='N',' disabled')}>
              </span>
              <span style="margin-right:2em"${If(s['verkl_redu']=='N',' disabled')}>
                <input id="VERKL_SKAL" name="VERKL_SKAL" type="checkbox" value="1"${If(s['verkl_skal'], ' checked')} onchange="this.form.VERKL_SKAL_PX.disabled=this.form.VERKL_SKAL_MINPX.disabled=!this.checked"${If(s['verkl_redu']=='N',' disabled')}>
                <label for="VERKL_SKAL">skalieren</label> auf max. px lange Seite
                <input name="VERKL_SKAL_PX" type="number" value="${s['verkl_skal_px']}" min="800" max="4000" size="5"${If(s['verkl_redu']=='N' or not s['verkl_skal'],' disabled')}>
                wenn >
                <input name="VERKL_SKAL_MINPX" type="number" value="${s['verkl_skal_minpx'] or 3200}" min="1000" max="99999" size="5"${If(s['verkl_redu']=='N' or not s['verkl_skal'],' disabled')}>
              </span>
              <input type="button" value="als Default speichern" onclick="verkl_save_default(this.form)">
            </span>
            </%def>
            ${verkl_block()}
          </td>
        </tr>
        <tr>
          <td class="horizborder">
            Beschreibung
          </td>
          <td class="horizborder">
            <textarea name="DBESCHR" cols="80" maxlength="1000" rows="3"></textarea>
          </td>
        </tr>
        ## folgende 2 Tabellenzeilen sind von der Ortsmaske hierher kopiert. Sollten vielleicht besser an einer einzigen Stelle als Block definiert werden.
        <tr>
          <td class="horizborder"><span style="margin-right:1em">Urheberrecht:</span></td>
          <td class="horizborder">
            <span style="display:flex;flex-wrap:wrap">
              <table style="border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span title="Autor (wenn bekannt) oder Quelle" style="margin-right:5px">Autor &#x2139;&#xFE0F;</span></td>
                  <td><input type="text" name="DQUELLE" size="50" maxlength="100" value="${s['useropts'].get('FileDfltAutor') or '' |h}"></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Lizenz</span></td>
                  <td>
                    <input type="text" name="DLIZENZ" list="D_LL" maxlength="200" size="50" value="${s['useropts'].get('FileDfltLizenz') or '' |h}">
                    <datalist id="D_LL">
                      <option value="CC BY 4.0"></option>
                      <option value="CC BY-SA 4.0"></option>
                      <option value="CC BY-NC 4.0"></option>
                      <option value="CC BY-NC-SA 4.0"></option>
                      <option value="CC BY-ND 4.0"></option>
                      <option value="CC BY-NC-ND 4.0"></option>
                      <option value="Public Domain"></option>
                    </datalist>
                  </td>
                </tr>
              </table>
            </span>
          </td>
        </tr>
        % if s['ret_pubid']:
        <tr>
          <td class="horizborder">
            <span style="margin-right:1em">zuordnen zur Publikation:</span>
          </td>
          <td class="horizborder">
            <input type="text" readonly value="${s['pub_displtext'] |h}">
            als
            <select name="DABSCHNITT" required>
              <option value="" selected>bitte auswählen</option>
              <option value="H">Hauptdokument</option>
              <option value="A">Anhang</option>
            </select>
          </td>
        </td>
        % elif not s['ret_fundi']:
        <tr>
          <td class="horizborder"><span style="margin-right:1em">Zugriffsrechte:</span></td>
          <td class="horizborder">
            <span style="display:flex;flex-wrap:wrap">
              <table style="border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Geheimhaltungsgrund</span></td>
                  <td><input type="text" name="DGEHEIM" maxlength="100" size="30"></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
                <tr>
                  <td title="Lesezugriff einschränken auf die angegebenen Benutzergruppen. Mehrfachauswahl möglich, Untergruppen sind aber schon automatisch inkludiert. Ebenso haben Gruppen mit Schreibrecht automatisch auch Lesezugriff."><span style="margin-right:5px">Leserecht für Gruppen &#x2139;&#xFE0F;</span></td>
                  <td><select name="DGRUPPEN_R" multiple size="3">
                    % for g in s['gruppen']:
                    <option value="${g['id']}">${g['name'] |h}</option>
                    % endfor
                  </select></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse">
                <tr>
                  <td title="Schreib- und Leserecht an die angegebenen Benutzergruppen. Mehrfachauswahl möglich, Untergruppen sind aber schon automatisch inkludiert. Ebenso haben immer auch der für die Datei verantwortliche Benutzer (also Sie) und Administratoren Schreibrecht. Schreibrecht impliziert Leserecht."><span style="margin-right:5px">Schreibrecht für Gruppen &#x2139;&#xFE0F;</span></td>
                  <td><select name="DGRUPPEN_W" multiple size="3">
                    % for g in s['gruppen']:
                    <option value="${g['id']}">${g['name'] |h}</option>
                    % endfor
                  </select></td>
                </tr>
              </table>
            </span>
          </td>
        </tr>
        <tr>
          <td class="horizborder">
            <span style="margin-right:1em">zuordnen zu Höhle(n):</span>
          </td>
          <td class="horizborder">
            <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
              <tr>
                <td title="Mehrfachauswahl möglich">Katnr(ID,Name) &#x2139;&#xFE0F;</td>
                <td title="Mehrfachauswahl möglich">
                  <select name="DHOEHLE_ID" multiple style="margin-right:1em">
                    % for h in s['hoehlen']:
                    <option value="${h['id']}"${If(h['id']==s['ret_hoehleid'],' selected')}>${If(h['katnr'], h['katnr'] + ' (') |h}${h['id']}${If(h['name'], ', ' + h['name']) |h}${If(h['katnr'], ')')}</option>
                    % endfor
                  </select>
                </td>
                <td>
                  als
                </td>
                <td>
                  <select name="DKATEGORIE">
                    <option value="M">Foto/Video/Audio</option>
                    <option value="P">Plan</option>
                    <option value="B">Bericht/Beschreibung</option>
                  </select>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        % elif s['fund_h']:
        <tr>
          <td colspan="2">
          <input type="checkbox" id="DFUNDH" name="DFUNDH" value="1" checked> <label for="DFUNDH">als Foto/Video/Audio der Höhle zuordnen</label>: ${s['fund_h_text'] |h}
          </td>
        </tr>
        % endif
      </table>
    </td></tr><tr><td>
      <div style="text-align:center; margin-top:5px">
        % if s['ret_hoehleid']:
        <input type="button" value="abbrechen + zurück zur Höhle" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID=${s['ret_hoehleid']}'">
        <input type="submit" value="hochladen + zurück zur Höhle" style="border:2px solid black;margin-left:1em;margin-right:1em">
        <input type="button" value="hochladen + weitere" onclick="if(this.form.reportValidity()) {this.form.HOEHLEID.name='RET_HOEHLEID';this.form.submit()}">
        % elif s['ret_fundi']:
        <input type="button" value="abbrechen + zurück zum Fund" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID=${s['ret_besuchid']}&FUND_I=${s['ret_fundi']}'">
        <input type="submit" value="hochladen + zurück zum Fund" style="border:2px solid black;margin-left:1em;margin-right:1em">
        <input type="button" value="hochladen + weitere" onclick="if(this.form.reportValidity()) {this.form.BESUCHID.name='RET_BESUCHID';this.form.FUND_I.name='RET_FUNDI';this.form.submit()}">
        % elif s['ret_pubid']:
        <input type="button" value="abbrechen + zurück zur Publikation" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&PUB_ID=${s['ret_pubid']}'">
        <input type="submit" value="hochladen + zurück zur Publikation" style="border:2px solid black;margin-left:1em;margin-right:1em">
        <input type="button" value="hochladen + weitere" onclick="if(this.form.reportValidity()) {this.form.PUB_ID.name='RET_PUBID';this.form.submit()}">
        % else:
        <input type="submit" value="hochladen">
        % endif
      </div>
    </td></tr></table>
    <input type="hidden" name="SESS" value="${s['sessionid']}">
    <input type="hidden" name="D_UPL" value="2">
    <input type="hidden" name="IDEM" value="${s['idem']}">
    % if s['ret_hoehleid']:
    <input type="hidden" name="HOEHLEID" value="${s['ret_hoehleid']}">
    % elif s['ret_besuchid']:
    <input type="hidden" name="BESUCHID" value="${s['ret_besuchid']}">
    <input type="hidden" name="FUND_I" value="${s['ret_fundi']}">
    % elif s['ret_pubid']:
    <input type="hidden" name="PUB_ID" value="${s['ret_pubid']}">
    % endif
  </form>
  % elif 'publiste' in s: # IF_00002
  <form name="HLIST_FRM" action="${s['scriptname']}">
    <div style="margin-top:5px">einschränken auf:</div>
    <table class="graytable">
      <tr>
        <td class="horizborder" style="display:flex;flex-wrap:wrap">
          <span style="display:flex;flex-wrap:wrap">
            <span style="margin-right:2em">ID <input type="number" name="PSEARCH_ID" size="5" value="${s['psearch_id']}"></span>
            <span style="margin-right:2em">User <input type="text" name="PSEARCH_USER" value="${s['psearch_user'] |h}" maxlength="20"></span>
            <span style="margin-right:2em">
              Status
              <select name="PSEARCH_STATUS">
                <option value=""${Unless(s['psearch_status'], ' selected')}>egal</option>
                <option value="1"${If(s['psearch_status']=='1', ' selected')}>Erstpublikation</option>
                <option value="P"${If(s['psearch_status']=='P', ' selected')}>publiziert</option>
                <option value="E"${If(s['psearch_status']=='E', ' selected')}>Entwurf</option>
                <option value="F"${If(s['psearch_status']=='F', ' selected')}>Fremdpublikation</option>
              </select>
            </span>
            <span>
              Autor(en)
              <input type="text" name="PSEARCH_AUTOR" value="${s['psearch_autor'] |h}">
            </span>
          </span>
        </td>
      </tr><tr>
        <td class="horizborder">
          <span style="display:flex;flex-wrap:wrap">
            <span style="margin-right:2em">
              Titel
              <input type="text" name="PSEARCH_TITEL" value="${s['psearch_titel'] |h}" size="30">
            </span>
            <span style="margin-right:2em">
              Zeitschrift
              <input type="text" name="PSEARCH_ZEITSCHRIFT" value="${s['psearch_zeitschrift'] |h}" size="30">
            </span>
            <span style="margin-right:2em">
              Herausgeber/Verlag
              <input type="text" name="PSEARCH_HERAUSGEBER" value="${s['psearch_herausgeber'] |h}">
            </span>
            <span>
              Jahr von
              <input type="text" name="PSEARCH_JAHRVON" value="${s['psearch_jahrvon']}" size="4" maxlength="4" pattern="^[12]\d{3}$">
               bis
              <input type="text" name="PSEARCH_JAHRBIS" value="${s['psearch_jahrbis']}" size="4" maxlength="4" pattern="^[12]\d{3}$">
            </span>
          </span>
        </td>
      </tr>
    </table>
    <div style="margin-top:8px">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="PLIST" value="2">
      <input type="submit" value="Suche">
    </div>
  </form>
  <div style="margin-top:10px">
    <b>Publikationen:</b>
    % if len(s['publiste']):
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>ID</th><th>Status</th><th>Autor</th><th>Titel</th><th>Untertitel</th><th>Zeitschrift</th><th>Ausgabe</th><th>Seiten</th><th>Jahr</th><th>Aufl.</th><th>Herausgeber</th><th>Publ.datum</th><th>Link</th><th>Anz. Dateien</th>
      </tr>
      % for p in s['publiste']:
      <tr onclick="openpub(${p['id']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px"">${p['id']}</td>
        <td>${p['status']}</td>
        <td>${p['autor'] |h}</td>
        <td>${p['titel'] |h}</td>
        <td>${p['untertitel'] |h}</td>
        <td>${p['zeitschrift'] |h}</td>
        <td>${p['ausgabe'] |h}</td>
        <td>${p['seiten'] |h}</td>
        <td>${p['jahr'] |h}</td>
        <td>${p['auflage'] |h}</td>
        <td>${p['herausgeber'] |h}</td>
        <td>${p['publik_d'] |h}</td>
        <td>
          % if p['link']:
          <a href="${p['link'] |h}" onclick="event.stopPropagation()">${p['link'] |h}</a>
          % endif
        </td>
        <td>${p['anzdateien'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    <br>noch keine erfasst
    % endif
  % elif 'pub_id' in s: # IF_00002
  <form name="PUBFRM" action="${s['scriptname']}">
    <div style="margin-top:5px"><b>Publikation:</b></div>
    <table class="graytable">
      % if s['pub_id']:
      <tr>
        <td colspan="2" class="horizborder">
          <span style="margin-right:2em">
            ID <input name="PUB_ID" size="4" value="${s['pub_id']}" readonly>
          </span>
          <span style="margin-right:2em">
            angelegt von <a href="javascript:openuser(${s['pub_userid']})">${s['pub_user_realname'] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F</span>
            am ${s['pub_angelegt_d'] |h}
          </span>
        </td>
      </tr>
      % endif
      <tr>
        <td class="horizborder">
          Status
        </td>
        <td class="horizborder">
          <select name="PUB_STATUS"${If(s['pub_schreibrecht'], ' required')}>
            % if s['pub_schreibrecht'] and not s['pub_status']:
            <option value="" selected>bitte auswählen</option>
            % endif
            <option value="1"${If(s['pub_status']=='1', ' selected', Unless(s['pub_schreibrecht'], ' disabled'))}>Erstpublikation</option>
            <option value="P"${If(s['pub_status']=='P', ' selected', Unless(s['pub_schreibrecht'], ' disabled'))}>publiziert</option>
            <option value="E"${If(s['pub_status']=='E', ' selected', Unless(s['pub_schreibrecht'], ' disabled'))}>Entwurf</option>
            <option value="F"${If(s['pub_status']=='F', ' selected', Unless(s['pub_schreibrecht'], ' disabled'))}>Fremdpublikation</option>
          </select>
        </td>
      </tr><tr>
        <td class="horizborder">
          Datum der Veröffentlichung
        </td>
        <td class="horizborder">
          <input name="PUB_PUBLIK_D" type="date" value="${s['pub_publik_d']}"${Unless(s['pub_schreibrecht'], ' readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Autor
        </td>
        <td class="horizborder">
          <input name="PUB_AUTOR" type="text" value="${s['pub_autor'] |h}" maxlength="100" size="50" ${If(s['pub_schreibrecht'], 'required', 'readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Titel
        </td>
        <td class="horizborder">
          <input name="PUB_TITEL" type="text" value="${s['pub_titel'] |h}" maxlength="100" size="50" ${If(s['pub_schreibrecht'], 'required', 'readonly')} style="margin-right:2em">
          Untertitel
          <input name="PUB_UNTERTITEL" type="text" value="${s['pub_untertitel'] |h}" maxlength="120" size="50"${Unless(s['pub_schreibrecht'], ' readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Zeitschrift
        </td>
        <td class="horizborder">
          <input name="PUB_ZEITSCHRIFT" type="text" value="${s['pub_zeitschrift'] |h}" maxlength="120" size="100"${Unless(s['pub_schreibrecht'], ' readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Jahr
        </td>
        <td class="horizborder">
          <input name="PUB_JAHR" type="text" value="${s['pub_jahr'] |h}" size="4" maxlength="4" pattern="^[12]\d{3}$"${Unless(s['pub_schreibrecht'], ' readonly')} style="margin-right:2em">
          Ausgabe
          <input name="PUB_AUSGABE" type="text" value="${s['pub_ausgabe'] |h}" size="5" maxlength="10" size="10"${Unless(s['pub_schreibrecht'], ' readonly')} style="margin-right:2em">
          <span title="für Artikel, die Teil eines Sammelwerks oder einer Ausgabe einer Zeitschrift sind">
            Seiten &#x2139;&#xFE0F;
          </span>
          <input name="PUB_SEITEN" type="text" value="${s['pub_seiten'] |h}" maxlength="20" size="20"${Unless(s['pub_schreibrecht'], ' readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Auflage
        </td>
        <td class="horizborder">
          <input name="PUB_AUFLAGE" type="text" value="${s['pub_auflage'] |h}" size="50" maxlength="50"${Unless(s['pub_schreibrecht'], ' readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Herausgeber/Verlag
        </td>
        <td class="horizborder">
          <input name="PUB_HERAUSGEBER" type="text" value="${s['pub_herausgeber'] |h}" maxlength="100" size="50"${Unless(s['pub_schreibrecht'], ' readonly')} style="margin-right:1em">
          Kommissionsverlag
          <input name="PUB_KOMMVERLAG" type="text" value="${s['pub_kommverlag'] |h}" maxlength="70" size="30"${Unless(s['pub_schreibrecht'], ' readonly')}>
        </td>
      </tr><tr>
        <td class="horizborder">
          Link
        </td>
        <td class="horizborder">
          % if s['pub_schreibrecht']:
          <input name="PUB_LINK" type="url" value="${s['pub_link'] |h}" maxlength="120" size="70">
          <input type="button" value="öffnen" onclick="if(this.form.PUB_LINK.value)location.href=this.form.PUB_LINK.value">
          % else:
          <a href="${s['pub_link'] |h}">${s['pub_link'] |h}</a>
          % endif
        </td>
      </tr><tr>
        <td class="horizborder">
          Anmerkung
        </td>
        <td class="horizborder">
          <textarea name="PUB_ANMERKUNG" cols="80" rows="3" maxlength="500"${Unless(s['pub_schreibrecht'], ' readonly')}>${s['pub_anmerkung'] |h}</textarea></td>
        </td>
      </tr>
      % if s['pub_schreibrecht']:
      <tr>
        <td class="horizborder">
          expl. zugeordnete Höhlen
        </td>
        <td class="horizborder" title="Mehrfachauswahl möglich. Über zugeordnete Dateien werden Bezüge zu Höhlen automatisch erkannt. Für den Fall, dass die Dokumente fehlen (Fremdpublikation) lassen sich Bezüge zu Höhlen hier explizit angeben.">
          <table style="padding:0;border-collapse:collapse"><tr>
            <td>
              <select name="PHOEHLE_ID" multiple size="20" style="margin-right:1em">
                % for h in s['hoehlen']:
                <option value="${h['id']}"${If(h['id'] in s['pub_exphoehlen'],' selected')}>${If(h['katnr'], h['katnr'] + ' (') |h}${h['id']}${If(h['name'], ', ' + h['name']) |h}${If(h['katnr'], ')')}</option>
                % endfor
              </select>
            </td><td>
              &#x2139;&#xFE0F;
            </td>
          </tr></table>
        </td>
      </tr>
      % endif
    </table>
    <div style="margin-top:10px">
    % if s['pub_schreibrecht']:
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="PUB_ID" value="${s['pub_id']}">
      % if s['pub_id']:
      <input type="button" value="löschen" onclick="if(confirm('Publikation wirklich löschen?i${If(len(s['pub_dateien']), ' (Dateien werden nicht automatisch mitgelöscht.)')}')) location.href='${s['scriptname']}?SESS=${s['sessionid']}&DELPUB=${s['pub_id']}&IDEM=${s['idem']}&PLIST=1'" style="margin-right:2em">
      % endif
      <input type="hidden" name="IDEM" value="${s['idem']}">
      <input type="submit" value="${'ändern' if s['pub_id'] else 'anlegen'}" style="margin-right:2em">
    % endif
    </div>
  </form>
  % if s['pub_id']:
  % if s['pub_schreibrecht']:
  <form action="${s['scriptname']}">
  <input type="hidden" name="SESS" value="${s['sessionid']}">
  <input type="hidden" name="PUB_ID" value="${s['pub_id']}">
  <input type="hidden" name="PD_LIST_AEND" value="1">
  <input type="hidden" name="IDEM" value="${s['idem']}">
  % endif
  <div style="margin-top:20px">
    <b>Dateien:</b>
    % if len(s['pub_dateien']):
    <table class="liste" style="margin:2px">
      <tr>
        <th>Index</th>
        <th>Abschnitt</th>
        <th>Name</th>
        <th>Datei-ID</th>
        <th>Version</th>
        <th>Dateiname</th>
        <th>Größe</th>
        % if s['pub_schreibrecht']:
        <th>löschen</th>
        % endif
        <th>Aktionen</th>
      </tr>
      % for d in s['pub_dateien']:
      <tr id="PD_TR_${d['datei_id']}">
        % if s['pub_schreibrecht']:
        <td><input type="number" name="PD_LIST_I_${d['datei_id']}" value="${d['i']}" min="0" max="999"></td>
        <td>
          <select name="PD_LIST_ABSCHNITT_${d['datei_id']}">
            <option value="H"${If(d['abschnitt']=='H', ' selected')}>Hauptdokument</option>
            <option value="A"${If(d['abschnitt']=='A', ' selected')}>Anhang</option>
          </select>
        </td>
        <td><input type="text" name="PD_LIST_NAME_${d['datei_id']}" value="${d['name'] |h}" maxlength="14"></td>
        % else:
        <td style="text-align:right;padding-right:2px">${d['i']}</td>
        <td>${'Hauptdokument' if d['abschnitt']=='H' else 'Anhang' if d['abschnitt']=='A' else '?'}</td>
        <td>${d['name']}</td>
        % endif
        <td style="text-align:right;padding-right:2px">${d['datei_id']}</td>
        <td style="text-align:right;padding-right:2px">${d['datei_v']}</td>
        <td>${d['filename'] |h}</td>
        <td style="text-align:right;padding-right:2px">${d['size']}</td>
        % if s['pub_schreibrecht']:
        <td style="text-align:center"><input type="checkbox" name="PD_LIST_DEL" value="${d['datei_id']}"></td>
        % endif
        <td>
          <div id="PD_BTNS_${d['datei_id']}">
            <input type="button" value="Details" onclick="ddetails(${d['datei_id']},${d['datei_v']})">
            % if d['recht']:
            <input type="button" value="anzeigen" onclick="window.open('${s['scriptname']}?SESS=${s['sessionid']}&OPENFILE=${d['datei_id']}&V=${d['datei_v']}','_blank')">
            <a href="media/${d['datei_id']}_${d['code']}_${d['datei_v']}" target="_blank" type="${d['mime'] |h}" download="${d['filename'] |h}"><input type="button" value="download"></a>
            % endif
          </div>
        </td>
      </tr>
      % endfor
    </table>
    % else:
    keine
    % endif
    % if s['pub_schreibrecht']:
    <div style="margin-top:10px">
      <input type="submit" value="Änd. an Zuordnungen speichern" style="margin-right:1em">
      <input type="button" value="${If(len(s['pub_dateien']), 'weitere ')}zuordnen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&DLIST=1&PDZUO_P=${s['pub_id']}'" style="margin-right:1em">
      <input type="button" value="neue hochladen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_UPL=1&RET_PUBID=${s['pub_id']}'">
    </div>
    % endif
  </div>
  % if s['pub_schreibrecht']:
  </form>
  % endif
  <div style="margin-top:10px">
    <b>zugeordnete Höhlen:</b>
    % if len(s['pub_hoehlen']):
    (Zeile anklicken zum Öffnen)
    <table class="liste" style="margin:2px">
      <tr>
        <th>ID</th>
        <th>Kat.gr.</th>
        <th>Nr.</th>
        <th>Name</th>
      </tr>
      % for h in s['pub_hoehlen']:
      <tr onclick="openhoehle(${h['id']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px">${h['id']}</td>
        <td>${h['katgr'] |h}</td>
        <td style="text-align:right;padding-right:5px">${h['nr']}</td>
        <td>${h['name'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    keine
    % endif
  </div>
  % endif # s['pub_id']
  % endif # IF_00002
  <%def name="ortsuche()">
      <tr><td class="horizborder">
        Land
        <select name="XLIST_LAND" onchange="adj_xlist_bl(this.value)">
          <option value=""${If(s['xlist_land'] == '', ' selected')}>egal</option>
          <option value="L"${If(s['xlist_land'] == 'L', ' selected')}>leer</option>
          % for land in s['laender']:
          <option value="${land['code']}"${If(s['xlist_land'] == land['code'], ' selected')}>${land['name'] |h}</option>
          % endfor
        </select>
        <span id="XLIST_BL_LABEL" style="margin-left:1em${If(s['xlist_land'] in ('', 'L'), ';display:none')}">Bundesland</span>
        <input type="text" id="XLIST_BL_TEXT" name="XLIST_BL"
          % if s['xlist_land'] in ('','L','AT'):
          disabled style="display:none"
          % else:
          value="${s['xlist_bl'] |h}"
          % endif
        >
        <select name="XLIST_BL" id="XLIST_BL_SEL" onchange="adj_xlist_bezirk()"
          % if s['xlist_land'] != 'AT':
          disabled style="display:none"
          % endif
        >
          <option value=""${Unless(s['xlist_bl'], ' selected')}>egal</option>
          <option value="L"${If(s['xlist_land'] == 'AT' and s['xlist_bl'] == 'L', ' selected')}>leer</option>
          % for bl in ('Burgenland', 'Kärnten', 'Niederösterreich', 'Oberösterreich', 'Salzburg', 'Steiermark', 'Tirol', 'Vorarlberg', 'Wien'):
          <option value="${bl}"${If(s['xlist_land']=='AT' and s['xlist_bl']==bl, ' selected')}>${bl}</option>
          % endfor
        </select>
        <span id="XLIST_BEZIRK_LABEL" style="margin-left:1em${If(s['xlist_land'] != 'AT' or s['xlist_bl'] in ('', 'L'), ';display:none')}">Bezirk/Statuarstadt</span>
        <select name="XLIST_BEZIRK" id="XLIST_BEZIRK_SEL" onchange="adj_xlist_gemeinde()"
          % if s['xlist_land'] != 'AT' or s['xlist_bl'] in ('', 'L'):
          disabled style="display:none"
          % endif
        >
          <option value=""${If(s['xlist_bezirk'] == '', ' selected')}>egal</option>
          <option value="L"${If(s['xlist_bezirk'] == 'L', ' selected')}>leer</option>
          % for bezirk in s['bezirke']:
            % if bezirk['bl'] == s['xlist_bl']:
          <option value="${bezirk['id']}"${If(s['xlist_bezirk']==str(bezirk['id']), ' selected')}>${bezirk['name']}</option>
            % endif
          % endfor
        </select>
        <span id="XLIST_GEMEINDE_LABEL" style="margin-left:1em${If(s['xlist_bezirk'] in ('','L'), ';display:none')}">Gemeinde/Stadtbezirk</span>
        <select name="XLIST_GEMEINDE" id="XLIST_GEMEINDE_SEL"
          % if s['xlist_bezirk'] in ('','L'):
          disabled style="display:none"
          % endif
        >
          <option value=""${If(s['xlist_gemeinde'] == '', ' selected')}>egal</option>
          <option value="L"${If(s['xlist_gemeinde'] == 'L', ' selected')}>leer</option>
          % for gemeinde in s['gemeinden']:
            % if gemeinde['bezirk_id'] == s['xlist_bezirk']:
          <option value="${gemeinde['id']}"${If(s['xlist_gemeinde']==str(gemeinde['id']), ' selected')}>${gemeinde['name']}</option>
            % endif
          % endfor
        </select>
      </td></tr><tr><td class="horizborder">
        Bounding box (Dezimalgrad WGS84):
        <span style="margin-left:2em">W</span>
        <input type="text" name="XLIST_W" id="BBOX_L" size="12" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${s['xlist_w'] |h}">
        <span style="margin-left:2em">O</span>
        <input type="text" name="XLIST_O" id="BBOX_R" size="12" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${s['xlist_o'] |h}">
        <span style="margin-left:2em">N</span>
        <input type="text" name="XLIST_N" id="BBOX_T" size="12" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${s['xlist_n'] |h}">
        <span style="margin-left:2em">S</span>
        <input type="text" name="XLIST_S" id="BBOX_B" size="12" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${s['xlist_s'] |h}">
        <input type="button" value="aus Karte" onclick="window.open('rect.html','BBOX_SEL','height=800,width=700,menubar=no,toolbar=no,location=no,personalbar=no,status=no,resizable=yes,scrollbars=no,dependent=yes,minimizable=yes,titlebar=no')">
        <input type="button" value="&#x1F5D1;" onclick="document.getElementById('BBOX_L').value='';document.getElementById('BBOX_R').value='';document.getElementById('BBOX_T').value='';document.getElementById('BBOX_B').value=''">
      </td></tr><tr><td class="horizborder">
        Seehöhe
        <select name="XLIST_SHREL" onchange="this.form.XLIST_SH.disabled = !this.value">
          <option value=""${Unless(s['xlist_shrel'], ' selected')}>egal</option>
          <option value="M"${If(s['xlist_shrel'] == 'M', ' selected')}>mindestens</option>
          <option value="G"${If(s['xlist_shrel'] == 'G', ' selected')}>gleich</option>
          <option value="H"${If(s['xlist_shrel'] == 'H', ' selected')}>höchstens</option>
        </select>
        <input type="text" name="XLIST_SH" size="4" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${s['xlist_sh']}"${Unless(s['xlist_shrel'], ' disabled')}>
      </td></tr>
  </%def>
  <%def name="suchopts_formpart()">
        % if s['user']:
        <span style="margin-left:2em" title="Suchkriterien unter einem Suchprofil speichern bzw. daraus laden, damit häufig gebrauchte nicht jedes Mal neu eingegeben werden müssen.">
          Suche
          <select id="SUCHOPT_ACTION" onchange="document.getElementById('SUCHOPT_NAME').disabled=this.value=='N'">
            <option value="L" ${'selected' if len(s['suchopts']) else 'style="display:none"'}>laden</option>
            <option value="S"${Unless(len(s['suchopts']), ' style="display:none"')}>speichern</option>
            <option value="N"${Unless(len(s['suchopts']), ' selected')}>neu</option>
            <option value="D"${Unless(len(s['suchopts']), ' style="display:none"')}>löschen</option>
          </select>
          <select id="SUCHOPT_NAME"${Unless(len(s['suchopts']), ' disabled')}>
            % for so in s['suchopts']:
            <option value="${so |h}">${so |h}</option>
            % endfor
          </select>
          <input type="button" value="ok" onclick="suchopts(this.form)">
          &#x2139;&#xFE0F;
        </span>
        % endif
  </%def>
  <%def name="ortfelder_koo(p_schreibrecht, p_pflicht, p_koosystem, p_rw, p_hw, p_kooabw)">
            <span style="margin-right:1em">
              % if p_schreibrecht:
              <span title="Wegen der Kontinentaldrift (Europa bewegt sich jährlich 2-3 cm nach NO) eignen sich für Österreich BMN-Koordinaten besser als geografische oder UTM/WGS84." style="margin-right:5px">
              % endif
              Koo-System
              % if p_schreibrecht:
              &#x2139;&#xFE0F;
              </span>
              % endif
              <select name="ORT_KOOSYST"${If(p_schreibrecht and p_pflicht, ' required')} ${If(p_schreibrecht, 'onchange="adj_ort_ajax()"')}>
                % if not p_koosystem:
                <option value="" selected>${If(p_schreibrecht,'bitte auswählen')}</option>
                % endif 
                <option value="G"${If(p_koosystem=='G', ' selected', Unless(p_schreibrecht, ' disabled'))}>Dezimalgrad WGS84</option>
                <option value="M28"${If(p_koosystem=='M28', ' selected', Unless(p_schreibrecht, ' disabled'))}>BMN M28</option>
                <option value="M31"${If(p_koosystem=='M31', ' selected', Unless(p_schreibrecht, ' disabled'))}>BMN M31</option>
                <option value="M34"${If(p_koosystem=='M34', ' selected', Unless(p_schreibrecht, ' disabled'))}>BMN M34</option>
              </select>
            </span>
            <span style="margin-right:1em">
              RW <input type="text" name="ORT_RW" size="12" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${p_rw}"${If(p_schreibrecht and p_pflicht, ' required')} ${'onchange="this.value=this.value.trim();adj_ort_ajax()"' if p_schreibrecht else 'readonly'}>
            </span>
            <span style="margin-right:1em">
              HW <input type="text" name="ORT_HW" size="12" pattern="^(\d*(\.\d*)?|\d*\.\d+)$" value="${p_hw}"${If(p_schreibrecht and p_pflicht, ' required')} ${'onchange="this.value=this.value.trim();adj_ort_ajax()"' if p_schreibrecht else 'readonly'}>
            </span>
            % if p_schreibrecht:
            <input type="button" value="aus Karte" onclick="window.open('${s['scriptname']}?KARTE=P', 'KARTE', 'height=800,width=1200,menubar=no,toolbar=no,location=no,personalbar=no,status=no,resizable=yes,scrollbars=no,dependent=yes,minimizable=yes,titlebar=no')" style="margin-right:2em">
            % endif
            max. Fehler in m
            <input type="number" name="ORT_KOOABW" size="4" min="0" max="100000" value="${p_kooabw}"${Unless(p_schreibrecht, ' readonly')}>
  </%def>
  <%def name="ortfelder_verw(p_schreibrecht, p_neu, p_uebernahme_von_hoehle_moeglich, p_uebernommen_von_hoehle, p_land, p_bl, p_bezirk_id, p_gemeinde_id)">
            % if p_schreibrecht:
            <span style="margin-right:1em" title="automatisch aus Koordinaten (für Österreich über volki.at, für andere Länder über geonames.org)${If(p_uebernahme_von_hoehle_moeglich, ' oder Höhle')}">
              <input type="checkbox" id="ORT_VERW_AUTO"${If(p_neu or p_uebernommen_von_hoehle, ' checked')}${If(p_uebernommen_von_hoehle, ' disabled')} onchange="adj_ort_verw_ajax()">
              <label for="ORT_VERW_AUTO">autom.</label> &#x2139;&#xFE0F;
            </span>
            % endif
            <span style="margin-right:1em">
              Land
              <select name="ORT_LAND" style="max-width:15em"${If(p_schreibrecht, 'onchange="document.getElementById(\'ORT_VERW_AUTO\').checked=false; adj_bl_attrs()"')}>
                <option value=""${Unless(p_land, ' selected', Unless(p_schreibrecht, ' disabled'))}>nicht angegeben</option>
                % for land in s['laender']:
                <option value="${land['code']}"${If(p_land == land['code'], ' selected', Unless(p_schreibrecht, ' disabled'))}>${land['name'] |h}</option>
               % endfor
              </select>
            </span>
            <span style="margin-right:1em">
              Bundesland
              ## für AT eine Select-Box, sonst Freitextfeld
              <input type="text" id="ORT_BL_TEXT" name="ORT_BL"
                % if p_schreibrecht:
                onchange="document.getElementById('ORT_VERW_AUTO').checked=false"
                % else:
                readonly
                % endif
                % if p_land == 'AT':
                disabled style="display:none"
                % elif p_land:
                value="${p_bl |h}"
                % else:
                disabled
                % endif
              >
              <select name="ORT_BL" id="ORT_BL_SEL"
                % if p_land != 'AT':
                disabled style="display:none"
                % elif p_uebernommen_von_hoehle:
                disabled
                % endif
                % if p_schreibrecht:
                onchange="document.getElementById('ORT_VERW_AUTO').checked=false; refresh_bezirke(this.form.ORT_BEZIRK,this.value); adj_bezirk_attrs()"
                % endif
              >
                <option value=""${If(p_land=='AT' and not p_bl, ' selected', Unless(p_schreibrecht, ' disabled'))}>nicht angegeben</option>
                % for bl in ('Burgenland', 'Kärnten', 'Niederösterreich', 'Oberösterreich', 'Salzburg', 'Steiermark', 'Tirol', 'Vorarlberg', 'Wien'):
                <option value="${bl}"${If(p_land=='AT' and p_bl==bl, ' selected', Unless(p_schreibrecht, ' disabled'))}>${bl}</option>
                % endfor
              </select>
            </span>
            <span style="margin-right:1em">
              <span id="ORT_BEZIRK_LABEL"${If(p_land != 'AT' or not p_bl, ' style="display:none"')}">Bezirk/Statuarstadt</span>
              <select name="ORT_BEZIRK"
                % if p_land != 'AT' or not p_bl:
                disabled style="display:none"
                % elif p_uebernommen_von_hoehle:
                disabled
                % endif
                % if p_schreibrecht:
                onchange="document.getElementById('ORT_VERW_AUTO').checked=false; refresh_gemeinden(this.form.ORT_GEMEINDE,this.value); adj_gemeinde_attrs()"
                % endif
              >
                <option value=""${Unless(p_bezirk_id, ' selected', Unless(p_schreibrecht, ' disabled'))}>nicht angegeben</option>
                % for bezirk in s['bezirke']:
                  % if bezirk['bl'] == p_bl:
                  <option value="${bezirk['id']}"${If(p_bezirk_id==bezirk['id'], ' selected', Unless(p_schreibrecht, ' disabled'))}>${bezirk['name']}</option>
                  % endif
                % endfor
              </select>
            </span>
            <span id="ORT_GEMEINDE_LABEL"${Unless(p_bezirk_id, 'style="display:none"')}>Gemeinde/Stadtbezirk</span>
            <select name="ORT_GEMEINDE"
              % if not p_bezirk_id:
              disabled style="display:none"
              % elif p_uebernommen_von_hoehle:
              disabled
              % endif
              % if p_schreibrecht:
              onchange="document.getElementById('ORT_VERW_AUTO').checked=false"
              % endif
            >
              <option value=""${Unless(p_gemeinde_id, ' selected', Unless(p_schreibrecht, ' disabled'))}>nicht ausgewählt</option>
              % for gemeinde in s['gemeinden']:
                % if gemeinde['bezirk_id'] == p_bezirk_id:
              <option value="${gemeinde['id']}"${If(p_gemeinde_id==gemeinde['id'], ' selected', Unless(p_schreibrecht, ' disabled'))}>${gemeinde['name']}</option>
                % endif
              % endfor
            </select>
  </%def>
  <%def name="ortfelder_sh(p_schreibrecht, p_neu, p_uebernahme_von_hoehle_moeglich, p_uebernommen_von_hoehle, p_sh, p_shtol)">
            % if p_schreibrecht:
            <span style="margin-right:1em" title="automatisch aus Koordinaten (über Voibos, nur für AT)${If(p_uebernahme_von_hoehle_moeglich, ' oder Höhle')}">
              <input type="checkbox" id="ORT_SH_AUTO"${If(p_neu or p_uebernommen_von_hoehle, ' checked')}${If(p_uebernommen_von_hoehle, ' disabled')} onchange="adj_ort_sh_ajax()">
              <label for="ORT_SH_AUTO">autom.</label> &#x2139;&#xFE0F;
            </span>
            % endif
            <input type="text" name="ORT_SH" size="4" pattern="^(\d{1,4}(\.\d?)?|\d{0,4}\.\d)$" value="${p_sh}"${If(not p_schreibrecht or p_uebernommen_von_hoehle, ' readonly')}${If(p_schreibrecht, ' onchange="document.getElementById(\'ORT_SH_AUTO\').checked=false"')} style="margin-right:1em">
            max. Fehler in m
            <input type="text" name="ORT_SHTOL" size="3" pattern="^(\d{1,3}(\.\d?)?|\d{0,3}\.\d)$" value="${p_shtol}"${Unless(p_schreibrecht, ' readonly')}>
            m
  </%def>
  % if 'hlist_frm' in s: # IF_00001
  <form name="HLIST_FRM">
    <div style="margin-top:5px">einschränken auf:</div>
    <table class="graytable">
      <tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">
            Name
            <input type="text" name="HLIST_NAME" size="20" value="${s['hlist_name'] |h}">
            <select name="HLIST_RE">
              <option value="S" ${If(s['hlist_re'] in('','S'), ' selected')}>Substring</option>
              <option value="X" ${If(s['hlist_re']=='X', ' selected')}>exakt</option>
              <option value="A" ${If(s['hlist_re']=='A', ' selected')}>Anfang</option>
              <option value="E" ${If(s['hlist_re']=='E', ' selected')}>Ende</option>
              <option value="R" ${If(s['hlist_re']=='R', ' selected')}>PCRE</option>
              <option value="G" ${If(s['hlist_re']=='G', ' selected')}>Globbing</option>
            </select>
          </span>
          <span style="margin-right:1em">
            Kat.gruppe
            <select name="HLIST_KATGR">
              <option value=""${Unless(s['hlist_katgr'], ' selected')}>egal</option>
              % for g in s['hlist_katgruppen']:
              <option value="${g |h}"${If(s['hlist_katgr'] == g, ' selected')}>${g |h}</option>
              % endfor
            </select>
          </span>
          <span style="margin-right:1em">
            <label for="HLIST_MITOHNEK">mit+ohne K</label> <input type="checkbox" id="HLIST_MITOHNEK" name="HLIST_MITOHNEK" value="1"${If(s['hlist_mitohnek'], ' checked')}>
          </span>
          <span style="margin-right:1em">
            Nummer
            <select name="HLIST_MITNR" onchange="this.form.HLIST_NR.disabled=this.value!='M'">
              <option value=""${Unless(s['hlist_mitnr'], ' selected')}>egal</option>
              <option value="M"${If(s['hlist_mitnr'] == 'M', ' selected')}>mit</option>
              <option value="O"${If(s['hlist_mitnr'] == 'O', ' selected')}>ohne</option>
            </select>
          </span>
          <span>
            <input type="text" name="HLIST_NR" value="${s['hlist_nr']}" size="4"${Unless(s['hlist_mitnr'], ' disabled')}>
          </span>
        </span>
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">Höhle-ID <input type="number" name="HLIST_ID" size="6" value="${s['hlist_id'] |h}"></span>
          <span style="margin-right:2em">Ort-ID <input type="number" name="HLIST_ORT_ID" size="6" value="${s['hlist_ort_id'] |h}"></span>
          <span>
            Höhle angelegt von:
            % if s['user']:
            <label for="HLIST_EIGENE" style="margin-left:1em">eigene</label><input type="checkbox" id="HLIST_EIGENE" name="HLIST_EIGENE" value="1"${If(s['hlist_eigene'], ' checked')} onchange="this.form.HLIST_USER.disabled=this.checked">
            % endif
            <span style="margin-left:1em">Name</span> <input type="text" name="HLIST_USER" value="${s['hlist_user'] |h}"${If(s['hlist_eigene'], ' disabled')}>
          </span>
        </span>
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">
            besonders geschützt
            <select name="HLIST_BESG">
              <option value=""${Unless(s['hlist_besg'], ' selected')}>egal</option>
              <option value="0"${If(s['hlist_besg']=='0', ' selected')}>nein</option>
              <option value="1"${If(s['hlist_besg']=='1', ' selected')}>ja</option>
            </select>
          </span>
          <span style="margin-right:2em">Naturdenkmal
            <select name="HLIST_NATD">
              <option value=""${Unless(s['hlist_natd'], ' selected')}>egal</option>
              <option value="0"${If(s['hlist_natd']=='0', ' selected')}>nein</option>
              <option value="3"${If(s['hlist_natd']=='3', ' selected')}>ja oder Gebiet</option>
              <option value="1"${If(s['hlist_natd']=='1', ' selected')}>ja</option>
              <option value="2"${If(s['hlist_natd']=='2', ' selected')}>Gebiet</option>
            </select>
          </span>
          <span style="margin-right:2em">Kulturdenkmal
            <select name="HLIST_KULD">
              <option value=""${Unless(s['hlist_kuld'], ' selected')}>egal</option>
              <option value="0"${If(s['hlist_kuld']=='0', ' selected')}>nein</option>
              <option value="1"${If(s['hlist_kuld']=='1', ' selected')}>ja</option>
            </select>
          </span>
          <span>Schauhöhle
            <select name="HLIST_SCHAU">
              <option value=""${Unless(s['hlist_schau'], ' selected')}>egal</option>
              <option value="0"${If(s['hlist_schau']=='0', ' selected')}>nein</option>
              <option value="1"${If(s['hlist_schau']=='1', ' selected')}>ja</option>
            </select>
          </span>
        </span>
      </td></tr><tr><td>
        Entstehung
        <select name="HLIST_GENESE">
          <option value=""${Unless(s['hlist_genese'], ' selected')}>egal</option>
          % for g in s['genesen']:
          <option value="${g}"${If(s['hlist_genese']==g, ' selected')}>${'&nbsp;'*4*(len(g)-1)}${s['genesen'][g] |h}</option>
          % endfor
        </select>
      </td></tr>
      ${ortsuche()}
    </table>
    <input type="hidden" name="HLISTE" value="1">
    <input type="hidden" name="SESS" value="${s['sessionid']}">
    <div style="display:flex;flex-wrap:wrap">
      <span style="margin-top:8px;margin-right:2em">
        1 Zeile pro
        <select name="HLIST_ZEILE">
          <option value="H"${If(s['zeile'] in ('H',''), ' selected')}>Höhle</option>
          <option value="E"${If(s['zeile'] == 'E', ' selected')}>Eingang</option>
          <option value="O"${If(s['zeile'] == 'O', ' selected')}>Ortsangabe</option>
        </select>
        ## maximale Anzahl Zeilen (oder Zeile n-m oder Seite n mit m Zeilen/Seite)
        <span title="max. Anzahl Zeilen, 0 = unbegrenzt">max. &#x2139;&#xFE0F; <input type="number" name="HLIST_MAXZEILEN" step="100" min="0" size="5" value="${If(s['hlist_maxzeilen'] is None or s['hlist_maxzeilen'] == '', '0', s['hlist_maxzeilen'])}"></span>
        <span style="margin-left:1em">Sortierung:</span>
        <select name="SORT1" onchange="this.form.SORT2.disabled = this.form.SORT2_DIR.disabled = this.value == 'I'">
          <option value="K"${If(s['sort1'] in ('','K'), ' selected')}>Katnr.</option>
          <option value="N"${If(s['sort1'] == 'N', ' selected')}>Name</option>
          <option value="I"${If(s['sort1'] == 'I', ' selected')}>ID</option>
          ##<option value="A"${If(s['sort1'] == 'A', ' selected')}>Anlagedatum</option>
          <option value="S"${If(s['sort1'] == 'S', ' selected')}>Seehöhe</option>
          <option value="L"${If(s['sort1'] == 'L', ' selected')}>Länge</option>
          <option value="H"${If(s['sort1'] == 'H', ' selected')}>Hu</option>
        </select>
        <select name="SORT1_DIR">
          <option value="ASC"${Unless(s['sort1_dir'] == 'DESC', ' selected')}>aufsteigend</option>
          <option value="DESC"${If(s['sort1_dir'] == 'DESC', ' selected')}>absteigend</option>
        </select>
        dann
        <select name="SORT2"${If(s['sort1'] == 'I', ' disabled')}>
          <option value="K"${If(s['sort2'] == 'K' or s['sort2'] == '' and s['sort1'] not in ('','K'), ' selected')}>Katnr.</option>
          <option value="N"${If(s['sort2'] == 'N' or s['sort2'] == '' and s['sort1'] in ('','K'),' selected')}>Name</option>
          <option value="I"${If(s['sort2'] == 'I', ' selected')}>ID</option>
          ##<option value="A"${If(s['sort2'] == 'A', ' selected')}>Anlagedatum</option>
          <option value="S"${If(s['sort2'] == 'S', ' selected')}>Seehöhe</option>
          <option value="L"${If(s['sort2'] == 'L', ' selected')}>Länge</option>
          <option value="H"${If(s['sort2'] == 'H', ' selected')}>Hu</option>
        </select>
        <select name="SORT2_DIR"${If(s['sort1'] == 'I', ' disabled')}>
          <option value="ASC"${Unless(s['sort2_dir'] == 'DESC', ' selected')}>aufsteigend</option>
          <option value="DESC"${If(s['sort2_dir'] == 'DESC', ' selected')}>absteigend</option>
        </select>
      </span>
      <span style="margin-top:8px">
        <input type="hidden" name="KARTE">
        <input type="submit" value="Suche (→ Liste)" style="border:2px solid black">
        <input type="button" value="Suche (→ Karte)" style="margin-left:1em" onclick="if(this.form.reportValidity()) {this.form.KARTE.value='H'; this.form.submit(); this.form.KARTE.value='';return false}">
        <span style="margin-left:1em" title="Spreadsheet erzeugen und herunterladen. Kann in Excel / Libreoffice / Google Drive usw. eingelesen werden.">
          <a id="XLSX" href="" target="_blank" type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" download="hoehlen.xlsx"><input type="button" value="XLSX" onclick="document.getElementById('XLSX').href=xlist_form2url(this.form)+'&HXLSX=1'"></a>
          &#x2139;&#xFE0F;
        </span>
        <span style="margin-left:1em" title="GPX-Datei (für GPS-Geräte von Garmin) mit den Koordinaten der ausgewählten Höhlen erzeugen und herunterladen.">
          <a id="GPX" href="" target="_blank" type="application/gpx+xml" download="hoehlen.gpx"><input type="button" value="GPX" onclick="document.getElementById('GPX').href=xlist_form2url(this.form)+'&HGPX=1'"></a>
          &#x2139;&#xFE0F;
        </span>
        ${suchopts_formpart()}
      </span>
    </div>
  </form>
  % if 'hliste' in s:
  <div style="margin-top:10px">
    % if len(s['hliste']):
    <b>Suchergebnis:</b> (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>Höhle-ID</th>
        <th>Kat.gr.</th>
        <th>Nr.</th>
        <th>Name</th>
        <th>andere Namen</th>
        <th>Länge</th>
        <th>Hu -</th>
        <th>Hu +</th>
        <th>He</th>
        <th>Ve</th>
        <th>bes. geschützt</th>
        <th>Naturdenkmal</th>
        <th>Kulturdenkmal</th>
        <th>Schauhöhle</th>
        <th>Erkundung</th>
        <th>Vermessung</th>
        <th>Wasser</th>
        <th>Eis</th>
        <th>Zustand</th>
        % if s['zeile'] != 'H':
        <th>Eingang</th>
        <th>Eingangsname</th>
        % endif
        <th>Koo-System</th>
        <th>RW</th>
        <th>HW</th>
        <th>Toleranz</th>
        <th>Seehöhe</th>
        <th>Land</th>
        <th>Bundesland</th>
        <th>Bezirk</th>
        <th>Gemeinde</th>
      </tr>
      % for h in s['hliste']:
      <tr
        % if s['zeile'] == 'H':
        onclick="openhoehle(${h['id']}${If(s['useropts'].get('NewWinLH'), ', true')})"
        % elif s['zeile'] == 'E':
        onclick="openeingang(${h['id']}, '${h['eingang']}'${If(s['useropts'].get('NewWinLH'), ', true')})"
        % else: # 'O'
        onclick="openeingangort(${h['id']}, '${h['eingang']}', ${h['o_id']}${If(s['useropts'].get('NewWinLH'), ', true')})"
        % endif
        style="cursor:pointer${If(h['ersetzt'], ';text-decoration:line-through')}"
      >
      <td style="text-align:right;padding-right:5px">${h['id']}</td>
      <td>${h['katgr'] |h}</td>
      <td style="text-align:right;padding-right:5px">${h['nr']}</td>
      <td>${h['name'] |h}</td>
      <td>${h['andere_namen'] |h}</td>
      <td style="text-align:right;padding-right:5px">${h['l']}</td>
      <td style="text-align:right;padding-right:5px">${h['hu_minus']}</td>
      <td style="text-align:right;padding-right:5px">${h['hu_plus']}</td>
      <td style="text-align:right;padding-right:5px">${h['he']}</td>
      <td style="text-align:right;padding-right:5px">${h['ve']}</td>
      <td>${h['besonders_geschuetzt'] |h}</td>
      <td>${h['naturdenkmal'] |h}</td>
      <td>${h['kulturdenkmal'] |h}</td>
      <td>${h['schauhoehle'] |h}</td>
      <td>${h['erkundungsstand'] |h}</td>
      <td>${h['vermessungsstand'] |h}</td>
      <td>${h['wasser'] |h}</td>
      <td>${h['eis'] |h}</td>
      <td>${h['zustand'] |h}</td>
      % if s['zeile'] != 'H':
      <td>${h['eingang'] |h}</td>
      <td>${h['eingang_name'] |h}</td>
      % endif
      <td>${h['koosystem']}</td>
      <td style="text-align:right">${h['rw']}</td>
      <td style="text-align:right">${h['hw']}</td>
      <td style="text-align:right;padding-right:2px">${h['kooabw']}</td>
      <td style="text-align:right;padding-right:2px">${h['sh']}</td>
      <td>${h['land']}</td>
      <td>${h['bl']}</td>
      <td>${h['bezirk']}</td>
      <td>${h['gemeinde']}</td>
      </tr>
      % endfor
    </table>
    % else:
    nichts gefunden
    % endif
  % endif # 'hliste' in s
  % elif 'hoehle' in s: # IF_00001
    <% hoe = s['hoehle'] %>\
    <% ro_attr = '' if hoe['schreibrecht'] else ' readonly' %>\
  <form name="HOEFRM" method="post" action="${s['scriptname']}" onsubmit="return check_hoe()">
    % if hoe['ersetzgrund']:
    <div style="margin-top:5px">
      <div style="font-size:150%; color:red; margin-bottom:5px">&#9888; Die Höhle wurde ersetzt mit der Begründung:<br>${hoe['ersetzgrund'] |h}</div>
      % for e in hoe['ersetzungen']:
      <div>
        % if e['eingang_alt']:
        Eingang ${e['eingang_alt'] |h}
        % endif
        ⟶
        <a href="${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID=${e['id_neu']}${If(e['eingang_neu'], '&REF='+e['eingang_neu']) |h}">
          ${e['katgr_neu'] |h}/${e['nr_neu']} (ID ${e['id_neu']})
          % if e['eingang_neu']:
          Eingang ${e['eingang_neu'] |h}
          % endif
        </a>
      </div>
      % endfor
    </div>
    % endif
    <div style="margin-top:5px"><b>Höhlenstammdaten:</b></div>
    <table class="graytable">
      <tr>
        <td class="horizborder">Katnr.</td>
        <td class="horizborder">
          % if hoe['schreibrecht']:
          <select name="KATGR_NUM" onchange="this.form.NR.disabled=!this.value">
            <option value=""${Unless(hoe['katgr_num'], ' selected')}>${If(hoe['id'], '-', 'bitte auswählen')}</option>
            % for grnr in hoe['katgruppen']:
            <option value="${grnr |h}"${If(grnr==hoe['katgr_num'], ' selected')}>${grnr |h} (${hoe['katgruppen'][grnr] |h})</option>
            % endfor
          </select>
          % elif hoe['katgr_num'] in hoe['katgruppen']:
          <input type="text" value="${hoe['katgr_num'] |h} (${hoe['katgruppen'][hoe['katgr_num']] |h})" readonly style="width:${len(str(hoe['katgr_num']))+len(hoe['katgruppen'][hoe['katgr_num']])+1}ch">
          % else:
          <input type="text" value="-" readonly style="width:1ch">
          % endif
          /
          <input name="NR" ${If(hoe['schreibrecht'], 'type="number" min="1"', 'type="text" size="'+str(len(str(hoe['nr'])))+'"')} value="${hoe['nr']}"${Unless(hoe['katgr_num'], ' disabled')}${ro_attr} size="5">
          % if hoe['nr']:
          <span style="margin-left:1em">
            <button type="button" ${If(hoe['previd'], 'onclick="openhoehle(' + str(hoe['previd']) + ')" title="zu ' + str(hoe['katgr_num']) + '/' + str(hoe['prevnr']) + ' wechseln"', 'disabled title="keine vorangehende Höhle in dieser Katastergruppe"')}>&#9664;</button>
            <button type="button" ${If(hoe['nextid'], 'onclick="openhoehle(' + str(hoe['nextid']) + ')" title="zu ' + str(hoe['katgr_num']) + '/' + str(hoe['nextnr']) + ' wechseln"', 'disabled title="keine nächste Höhle in dieser Katastergruppe"')}>&#9654;</button>
          </span>
          % endif
          % if hoe['id'] and hoe['schreibrecht']:
          <span style="margin-left:2em"><input type="checkbox" id="ERSETZT_CBX"${If(hoe['ersetzgrund'], ' checked')} onchange="document.getElementById('ERSETZ').style.display=this.checked?'table-row':'none'; this.form.ERSETZGRUND.disabled=!this.checked; this.form.ERSETZGRUND.required=this.checked"> <label for="ERSETZT_CBX">ersetzt</label></span>
          % endif
        </td>
      </tr>
      % if hoe['id'] and hoe['schreibrecht']:
      <tr id="ERSETZ"${Unless(hoe['ersetzgrund'], 'style="display:none"')}>
        <td class="horizborder">Ersetzung:</td>
        <td class="horizborder">
          Grund
          <input type="text" name="ERSETZGRUND" value="${hoe['ersetzgrund'] |h}" maxlength="200" size="30" ${If(hoe['ersetzgrund'], 'required', 'disabled')} style="margin-right:2em">
          ganze Höhle ersetzt durch
          <%
            ers = {'id_neu': 0, 'katgr_neu': '', 'nr_neu': '', 'eingang_neu': ''}
            for e in hoe['ersetzungen']:
              if not e['eingang_alt']:
                ers = e
                break
          %>\
          <select name="NEUHOE_KATGR0" style="margin-left:1em">
            <option value="">nicht angegeben</option>
            % for grnr in hoe['katgruppen']:
            <option value="${grnr |h}"${If(grnr==ers['katgr_neu'] or not ers['nr_neu'] and grnr==hoe['katgr_num'], ' selected')}>${grnr |h}</option>
            % endfor
          </select>
          /
          <input name="NEUHOE_NR0" type="number" min="1" size="5" value="${ers['nr_neu']}" style="margin-right:8px">
          Eingang
          <input type="text" name="NEUHOE_REF0" size="2" value="${ers['eingang_neu'] |h}">
          <div style="margin-top: 5px">Eingänge (wenn nicht gelistet, dann zuerst bitte anlegen):</div>
          % if 'eingaenge' in s and len(s['eingaenge']):
          % for ein in s['eingaenge']:
          <%
            i = loop.index + 1
            ers = {'id_neu': 0, 'katgr_neu': '', 'nr_neu': '', 'eingang_neu': ''}
            for e in hoe['ersetzungen']:
              if e['eingang_alt'] == ein['ref']:
                ers = e
                break
          %>\
          <div>
            Eingang <input type="text" name="ALTHOE_REF${i}" readonly value="${ein['ref'] |h}">
            <span style="margin-left:1em;margin-right:1em">⟶</span>
            <select name="NEUHOE_KATGR${i}">
              <option value="">bitte auswählen</option>
              % for grnr in hoe['katgruppen']:
              <option value="${grnr |h}"${If(grnr==ers['katgr_neu'] or not ers['nr_neu'] and grnr==hoe['katgr_num'], ' selected')}>${grnr |h}</option>
              % endfor
            </select>
            /
            <input name="NEUHOE_NR${i}" type="number" min="1" size="5" value="${ers['nr_neu']}" style="margin-right:8px">
            Eingang
            <input type="text" name="NEUHOE_REF${i}" size="2" value="${ers['eingang_neu'] |h}">
          </div>
          % endfor
          % endif
        </td>
      </tr>
      % endif
      % if 'ersetzt' in hoe:
      <tr>
        <td class="horizborder">ersetzt:</td>
        <td class="horizborder">
          % for e in hoe['ersetzt']:
            <a href="${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID=${e['id']}">
              ${e['katgr'] |h}/${e['nr']} (ID ${e['id']})${Unless(loop.last, ',')}
            </a>
          % endfor
        </td>
      </tr>
      % endif
      <tr>
        <td class="horizborder">Name</td>
        <td class="horizborder">
          <input type="text" name="HNAME" maxlength="75" size="50" value="${hoe['name']|h}" ${'required' if hoe['schreibrecht'] else ' readonly'}>
          <span style="margin-left:1em">
            weitere Namen
            <input type="text" name="ANDERE_NAMEN" maxlength="200" size="50" value="${hoe['andere_namen']|h}"${ro_attr}>
          </span>
        </td>
      </tr>
      % if hoe['id']:
      <tr><td>Höhle-ID</td>
        <td>
          <input name="HOEHLEID" size="5" value="${hoe['id']}" readonly style="margin-right:2em">
          angelegt von <a href="javascript:openuser(${hoe['userid']})">${hoe['user_realname'] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F</span>
          % if hoe['created']:
          am ${hoe['created'] |h}
          % endif
          % if hoe['lastuserid']:
          <span style="margin-left:1em">zuletzt geändert von </span>
          <a href="javascript:openuser(${hoe['lastuserid']})">${hoe['lastuser_realname'] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F;</span>
          % if hoe['changed']:
          am ${hoe['changed'] |h}
          % endif
          % endif
        </td>
      </tr>
      % endif # if hoe['id']
      <tr>
        <td class="horizborder">Dimensionen:</td>
        <td class="horizborder">
          <span style="margin-right:1em">Ganglänge <input type="text" name="L" size="6" pattern="^(\d{1,7}(\.\d?)?|\d{0,7}\.\d)$" value="${hoe['l']}"${ro_attr}></span>
          <span style="margin-right:1em"><span title="Boden-Höhenunterschied von Eingang a abwärts">Hu &#xFF0D; &#x2139;&#xFE0F;</span> <input type="text" name="HU_MINUS" size="5" pattern="^(\d{1,4}(\.\d?)?|\d{0,4}\.\d)$" value="${hoe['hu_minus']}"${ro_attr}></span>
          <span style="margin-right:1em"><span title="Boden-Höhenunterschied von Eingang a aufwärts">Hu &#xFF0B; &#x2139;&#xFE0F;</span> <input type="text" name="HU_PLUS" size="5" pattern="^(\d{1,4}(\.\d?)?|\d{0,4}\.\d)$" value="${hoe['hu_plus']}"${ro_attr}></span>
          <span style="margin-right:1em"><span title="Horizontalerstreckung">He &#x2139;&#xFE0F;</span> <input type="text" name="HE" size="6" pattern="^(\d{1,7}(\.\d?)?|\d{0,7}\.\d)$" value="${hoe['he']}"${ro_attr}></span>
          <span title="Vertikalerstreckung">Ve &#x2139;&#xFE0F;</span> <input type="text" name="VE" size="5" pattern="^(\d{1,4}(\.\d?)?|\d{0,4}\.\d)$" value="${hoe['ve']}"${ro_attr}>
        </td>
      </tr>
      <tr>
        <td class="horizborder">Forschungsstand:</td>
        <td>
          Erkundung
          <select name="ERKUNDUNGSSTAND" style="margin-right:2em">
            <option value=""${Unless(hoe['erkundungsstand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for erkundungsstand in hoe['erkundungsstaende']:
            <option value="${erkundungsstand |h}"${If(erkundungsstand==hoe['erkundungsstand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${hoe['erkundungsstaende'][erkundungsstand] |h}</option>
            % endfor
          </select>
          Vermessung
          <select name="VERMESSUNGSSTAND" style="margin-right:2em">
            <option value=""${Unless(hoe['vermessungsstand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for vermessungsstand in hoe['vermessungsstaende']:
            <option value="${vermessungsstand |h}"${If(vermessungsstand==hoe['vermessungsstand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${hoe['vermessungsstaende'][vermessungsstand] |h}</option>
            % endfor
          </select>
          Zustand
          <select name="ZUSTAND">
            <option value=""${Unless(hoe['zustand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for zustand in hoe['zustaende']:
            <option value="${zustand |h}"${If(zustand==hoe['zustand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${hoe['zustaende'][zustand] |h}</option>
            % endfor
          </select>
        </td>
      </tr>
      <tr>
        <td class="horizborder">Schutz:</td>
        <td class="horizborder">
          Naturdenkmal
          <select name="NATURDENKMAL" style="margin-right:1em">
            <option value=""${Unless(hoe['naturdenkmal'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nein</option>
            <option value="1"${If(hoe['naturdenkmal']==1, ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>Höhle</option>
            <option value="2"${If(hoe['naturdenkmal']==2, ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>Gebiet</option>
          </select>
          <span style="margin-right:1em"><input type="checkbox" id="KULTURDENKMAL" name="KULTURDENKMAL" value="1"${If(hoe['kulturdenkmal'], ' checked')}${Unless(hoe['schreibrecht'], ' disabled')}> <label for="KULTURDENKMAL">Kulturdenkmal</label></span>
          <span style="margin-right:1em"><input type="checkbox" id="BESONDERS_GESCHUETZT" name="BESONDERS_GESCHUETZT" value="1"${If(hoe['besonders_geschuetzt'], ' checked')}${Unless(hoe['schreibrecht'], ' disabled')}> <label for="BESONDERS_GESCHUETZT">besonders geschützt</label></span>
          <span style="margin-right:1em"><input type="checkbox" id="SCHAUHOEHLE" name="SCHAUHOEHLE" value="1"${If(hoe['schauhoehle'], ' checked')}${Unless(hoe['schreibrecht'], ' disabled')}> <label for="SCHAUHOEHLE">Schauhöhle</label></span>
          Website <input type="url" name="WEBSITE" maxlength="100" value="${hoe['website']}"${ro_attr}>
        </td>
      </tr>
      <tr>
        <td class="horizborder">Wasser</td>
        <td class="horizborder">
          <select name="WASSER" style="margin-right:2em">
            <option value=""${Unless(hoe['wasser'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for wasser in hoe['wasseroptions']:
            <option value="${wasser |h}"${If(wasser==hoe['wasser'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${hoe['wasseroptions'][wasser] |h}</option>
            % endfor
          </select>
          Eis
          <select name="EIS">
            <option value=""${If(hoe['eis'] == '', ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            <option value="0"${If(hoe['eis'] == '0', ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nein</option>
            <option value="1"${If(hoe['eis'] == '1', ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>ja</option>
          </select>
        </td>
      </tr>
      <tr>
        <td class="horizborder">Entstehung</td>
        <td class="horizborder">
          <table id="GENTBL" style="border-collapse:collapse">
            % for g in hoe['gen']:
            <tr id="GENTBL_ROW${loop.index+1}">
              <td>
                <span style="margin-right:1em">
                  % for i in range(len(g['genese'])):
                  % if i:
                  ⟶
                  % endif
                  ${hoe['genesen'][g['genese'][0:i+1]] |h}
                  % endfor
                  % if hoe['schreibrecht']:
                  <input type="hidden" name="GEN_GENESE${loop.index+1}" value="${g['genese']}">
                  % endif
                </span>
              </td><td>
                <span style="margin-right:1em" title="falls mehrere: 1 = dominant, 2 = weniger, usw., leer = am wenigsten">
                  Dominanz
                  <input $(If(hoe['schreibrecht'], 'type="number" min="1" max="255"', 'type="text"')} size="3" name="GEN_PRIO${loop.index+1}" value="${g['prio']}"${Unless(hoe['schreibrecht'], ' readonly')}>
                  &#x2139;&#xFE0F;
                </span>
              </td><td>
                <span style="margin-right:1em" title="1 = älteste, 2 = zweitälteste usw.">
                  Phase
                  <input $(If(hoe['schreibrecht'], 'type="number" min="1" max="255"', 'type="text"')} size="3" name="GEN_PHASE${loop.index+1}" value="${g['phase']}"${Unless(hoe['schreibrecht'], ' readonly')}>
                  &#x2139;&#xFE0F;
                </span>
              </td><td>
                <span title="Die Angabe der Ganglänge erübrigt sich, wenn die ganze Höhle auf die selbe Weise entstanden ist.">
                  Ganglänge
                  <input type="text" name="GEN_L${loop.index+1}" size="6" pattern="^(\d{1,7}(\.\d?)?|\d{0,7}\.\d)$" value="${g['l']}"${Unless(hoe['schreibrecht'], ' readonly')}>
                  &#x2139;&#xFE0F;
                </span>
              </td>
              % if hoe['schreibrecht']:
              <td>
                <input type="button" value="streichen" style="margin-left:1em" onclick="if(this.value=='streichen') {document.getElementById('GENTBL_ROW${loop.index+1}').style.textDecoration='line-through';this.form.GEN_GENESE${loop.index+1}.disabled=this.form.GEN_PRIO${loop.index+1}.disabled=this.form.GEN_L${loop.index+1}.disabled=true;this.value='zurückholen'} else {document.getElementById('GENTBL_ROW${loop.index+1}').style.textDecoration='';this.form.GEN_GENESE${loop.index+1}.disabled=this.form.GEN_PRIO${loop.index+1}.disabled=this.form.GEN_L${loop.index+1}.disabled=false;this.value='streichen'}">
              </td>
              % endif
            </tr>
            % endfor
          </table>
          % if hoe['schreibrecht']:
          <div>
            <input type="button" value="+" onclick="add_genese()">
          </div>
          % endif
        </td>
      </tr>
      % if not hoe['id']:
      ## umständlich zu programmieren, aber es erleichtert den Erfassern die Eingabe, wenn sie nicht 2 Formulare nacheinander absenden müssen um Höhle+Anmerkung anzulegen
      <tr>
        <td class="horizborder">Anmerkung</td>
        <td><textarea name="HANM" cols="80" maxlength="10000" rows="5"></textarea></td>
      </tr>
      % endif
    </table>
    <div style="margin-top:10px">
    % if hoe['schreibrecht']:
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      % if hoe['id']:
      <input type="button" value="löschen" onclick="if(confirm('Höhle wirklich löschen?')) location.href='${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID=${hoe['id']}&DELHOEHLE=1&IDEM=${s['idem']}'"${If('abhaengige_datensaetze_vorhanden' in hoe, ' disabled title="Löschen unzulässig, solang von anderen Datensätzen referenziert"')} style="margin-right:2em">
      % endif
      <input type="hidden" name="IDEM" value="${s['idem']}">
      <input type="submit" value="${'ändern' if hoe['id'] else 'anlegen'}" style="margin-right:2em">
    % endif
    % if 'eingaenge' in s and len(s['eingaenge']): # fixme: besser nur wenn Koo vorhanden sind und Leserecht darauf besteht
      <input type="button" value="zu Karte" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&KARTE=H&OPEN_HOE=${hoe['id']}'${If(s['useropts'].get('NewWinKH'), ', true')})" style="margin-right:2em">
      <input type="button" value="${hoe['anzfunde']} Fund${If(hoe['anzfunde']!=1,'e')}" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&BLISTE=1&BLIST_HOEHLEN=H&BLIST_HOEHLE_ID=${hoe['id']}'${If(s['useropts'].get('NewWinHF'), ', true')})"${Unless(hoe['anzfunde'],' disabled')} style="margin-right:2em">
      % if s['user']:
      <input type="button" value="Fund anlegen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID=0&ORT_HOEHLE_ID=${hoe['id']}'">
      % endif
    % endif
    </div>
  </form>
  % if hoe['id']:
  <div style="margin-top:20px">
    <b>Anmerkungen:</b>
    % if len(s['anmerkungen']):
    <table class="liste">
      <tr>
        <th>User</th>
        <th>zuletzt geändert</th>
        <th>Text</th>
        <th>Aktionen</th>
      </tr>
      % for a in s['anmerkungen']:
      <tr>
        <td>
          % if a['user_aenderbar']:
          <select name="ANMUSER" form="ANM_${a['userid']}" required>
            % for u in hoe['users']:
            <%
              into_select = True
              if u['id'] != a['userid']:
                for a1 in s['anmerkungen']:
                  if a1['userid'] == u['id'] and a1['text']:
                    into_select = False
                    break
            %>\
            % if into_select:
            <option value="${u['id']}"${If(u['id']==a['userid'], ' selected')}>${u['name'] |h}</option>
            % endif
            % endfor
          </select>
          % else:
          <a href="javascript:openuser(${a['userid']})">${[u['name'] for u in hoe['users'] if u['id']==a['userid']][0] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F;</span>
          % endif
        </td>
        <td>${a['geaendert_d']}</td>
        <td><textarea cols="80" maxlength="10000" rows="${max (2 if a['schreibrecht'] else 1, min (5, max(a['text'].count(chr(10))-1, (len(a['text'])-1)//80+1)))}" ${If(a['schreibrecht'], 'name="ANMTEXT" form="ANM_' + str(a['userid']) + '" required', 'readonly')}>${a['text'] |h}</textarea></td>
        <td>
          % if a['schreibrecht']:
          <form id="ANM_${a['userid']}" method="post" action="${s['scriptname']}">
          <input type="hidden" name="SESS" value="${s['sessionid']}">
          <input type="hidden" name="HOEHLEID" value="${hoe['id']}">
          <input type="hidden" name="ANMOLDUSER" value="${a['userid']}">
          <input type="hidden" name="DELANM" value="">
          <input type="submit" value="${If(a['text'], 'ändern', 'anlegen')}">
          % if a['text']:
          <input type="button" value="löschen" onclick="this.form.ANMTEXT.disabled=true;this.form.DELANM.value='1';this.form.submit()">
          % endif
          </form>
          % endif
          % if s['user'] and s['userid'] != a['userid']:
          <span title="User in neuem Fenster öffnen"><input type="button" value="User" onclick="openuser(${a['userid']})"> &#x2139;&#xFE0F;</span>
          % endif
        </td>
      </tr>
      % endfor
    </table>
    % else: # if len(s['anmerkungen'])
    <br>noch keine erfasst
    % endif # if len(s['anmerkungen'])
  </div>
  <div style="margin-top:20px">
    <b>Eingänge:</b>
    % if len(s['eingaenge']):
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr><th>Kürzel</th><th>Name</th><th>Wasser</th><th>Morphologie</th><th>Zustand</th><th>Koo-System</th><th>RW</th><th>HW</th><th>Toleranz</th><th>Seehöhe</th><th>Land</th><th>Bundesland</th><th>Bezirk</th><th>Gemeinde</th></tr>
      % for e in s['eingaenge']:
      <tr onclick="openeingang(${hoe['id']},'${e['ref']}')"${If('eingang' in s and s['eingang']['ref']==e['ref'], ' class="liste_selected"')} style="cursor:pointer">
        <td>${e['ref'] |h}</td>
        <td>${e['name'] |h}</td>
        <td>${e['wasser'] |h}</td>
        <td>${e['morph']}</td>
        <td>${e['zustand'] |h}</td>
        <td>${e['koosystem']}</td>
        <td style="text-align:right">${e['rw']}</td>
        <td style="text-align:right">${e['hw']}</td>
        <td style="text-align:right;padding-right:2px">${e['kooabw']}</td>
        <td style="text-align:right;padding-right:2px">${e['sh']}</td>
        <td>${e['land']}</td>
        <td>${e['bl']}</td>
        <td>${e['bezirk']}</td>
        <td>${e['gemeinde']}</td>
      </tr>
      % endfor
    </table>
    % else: # if len(s['eingaenge'])
    <br>noch keine erfasst
    % endif # if len(s['eingaenge'])
    % if hoe['schreibrecht'] and ('eingang' not in s or s['eingang']['ref']):
    <form method="post" action="${s['scriptname']}" style="margin-top:10px">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="HOEHLEID" value="${hoe['id']}">
      <input type="hidden" name="REF" value="">
      <input type="submit" value="${If(len(s['eingaenge']), 'weiteren ')}Eingang anlegen">
    </form>
    % endif # schreibrecht...
  </div>
  % if 'eingang' in s:
  <% eingang = s['eingang'] %>\
  <form id="EINGANGFRM" name="EINGANGFRM" method="post" action="${s['scriptname']}" method="post" style="margin-top:20px">
    <b>Eingang:</b>
    <table class="graytable">
      <tr>
        <td>Kürzel</td>
        <td class="horizborder"><input type="text" name="REF" size="2" maxlength="2" value="${eingang['newref'] |h}" ${If(hoe['schreibrecht'], 'required', 'readonly')}></td>
        <td class="horizborder">Name</td>
        <td colspan="3" class="horizborder"><input type="text" name="NAME" maxlength="100" size="30" value="${eingang['name'] |h}"${ro_attr}></td>
      </tr>
      <tr>
        <td class="horizborder">Wasser</td>
        <td class="horizborder">
          <select name="WASSER" style="margin-right:2em">
            <option value=""${Unless(eingang['wasser'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for wasser in eingang['ewasseropts']:
            <option value="${wasser |h}"${If(wasser==eingang['wasser'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${eingang['ewasseropts'][wasser] |h}</option>
            % endfor
          </select>
        </td>
        <td class="horizborder">Morphologie</td>
        <td class="horizborder">
          <select name="MORPH" style="margin-right:2em">
            <option value=""${Unless(eingang['morph'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for morph in eingang['emorphs']:
            <option value="${morph |h}"${If(morph==eingang['morph'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${eingang['emorphs'][morph] |h}</option>
            % endfor
          </select>
        </td>
        <td class="horizborder">Zustand</td>
        <td class="horizborder">
          <select name="ZUSTAND">
            <option value=""${Unless(eingang['zustand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for zustand in eingang['ezustaende']:
            <option value="${zustand |h}"${If(zustand==eingang['zustand'], ' selected', Unless(hoe['schreibrecht'], ' disabled'))}>${eingang['ezustaende'][zustand] |h}</option>
            % endfor
          </select>
        </td>
      </tr>
    </table>
    % if hoe['schreibrecht'] or 'eorte' in s and len(s['eorte']):
    <div style="margin-top:10px">
      % if hoe['schreibrecht']:
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="HOEHLEID" value="${hoe['id']}">
      <input type="hidden" name="ORIG_REF" value="${eingang['ref'] |h}">
      <input type="hidden" name="IDEM" value="${s['idem']}">
      % if eingang['ref']:
      <input type="hidden" name="DELEING" value="">
      <input type="button" value="löschen" onclick="if(confirm('Eingang ${eingang['ref'] |esc_js_str} löschen?')) {this.form.DELEING.value='1';this.form.submit()}" style="margin-right:2em">
      % endif
      <input type="submit" value="${'ändern' if eingang['ref'] else 'anlegen'}" style="margin-right:2em">
      % endif # schreibrecht
      % if 'eorte' in s and len(s['eorte']):
      <input type="button" value="zu Karte" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&HLISTE=1&HLIST_ZEILE=E&KARTE=H&OPEN_HOE=${hoe['id']}${eingang['ref'] |esc_js_str}'${If(s['useropts'].get('NewWinKH'), ', true')})" style="margin-right:2em">
      % endif
    </div>
    % endif
  </form>

  % if eingang['ref']:
  <div style="margin-top:20px">
    <b>Ortsangabe${Unless(s['eingang_soeben_angelegt'], 'n')} zu Eingang ${eingang['ref'] |h}:</b>
    % if 'eorte' in s and len(s['eorte']):
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        <th>ID</th>
        <th>User</th>
        <th>Koo-System</th>
        <th>RW</th>
        <th>HW</th>
        <th>Koo ±</th>
        <th>Seehöhe</th>
        <th>Sh ±</th>
        <th>Land</th>
        <th>Bundesland</th>
        <th>Bezirk</th>
        <th>Gemeinde</th>
        <th>angelegt am</th>
        <th>geändert am</th>
      </tr>
      % for o in s['eorte']:
      <tr onclick="openeingangort(${hoe['id']},'${eingang['ref']}',${o['id']})"${If('eort' in s and s['eort']['id']==o['id'], ' class="liste_selected"')} style="cursor:pointer">
        <td>${o['id']}</td>
        <td>${o['user'] |h}</td>
        <td>${o['koosystem']}</td>
        <td style="text-align:right">${o['rw']}</td>
        <td style="text-align:right">${o['hw']}</td>
        <td style="text-align:right;padding-right:2px">${o['kooabw']}</td>
        <td style="text-align:right;padding-right:2px">${o['sh']}</td>
        <td style="text-align:right;padding-right:2px">${o['sh_tol']}</td>
        <td>${o['land']}</td>
        <td>${o['bl']}</td>
        <td>${o['bezirk']}</td>
        <td>${o['gemeinde']}</td>
        <td>${o['angelegt_d']}</td>
        <td>${o['geaendert_d']}</td>
      </tr>
      % endfor
    </table>
    % elif not s['eingang_soeben_angelegt']: # if 'eorte' in s and len(s['eorte'])
    <br><span style="margin-right:1em">noch keine erfasst oder nicht leseberechtigt</span>
    % endif # if 'eorte' in s and len(s['eorte'])

    % if 'eort' in s:
    <% eort = s['eort'] %>\
    <% ro_attr = '' if eort['schreibrecht'] else ' readonly' %>\
    <form id="ORTFRM" name="ORTFRM" method="post" action="${s['scriptname']}"${Unless(s['eingang_soeben_angelegt'], ' style="margin-top:20px"')} onsubmit="return check_koo(this)">
      % if not s['eingang_soeben_angelegt']: # sonst Überschrift schon ausgegeben, s.o.
      <b>Ortsangabe:</b>
      % endif
      <table class="graytable">
        <tr><td colspan="2">
          % if eort['id']:
          <span style="margin-right:1em">Ort-ID</span>
          <input name="ORT_ID" size="6" value="${eort['id']}" readonly style="margin-right:2em">
          % endif
          <span style="margin-right:1em">User</span>
          % if eort['kataster']:
          <select name="ORT_USER" required>
            % for u in hoe['users']:
            <option value="${u['id']}"${If(u['id']==eort['user'], ' selected')}>${u['name'] |h}</option>
            % endfor
          </select>
          % else:
          <a href="javascript:openuser(${eort['user']})">${[u['name'] for u in hoe['users'] if u['id']==eort['user']][0] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F;</span>
          % endif
          % if eort['kataster'] and eort['user'] != s['userid']:
          <span title="ändert User, Urheber und Lizenz; für importierte Daten, die von einem selber stammen">
          <input type="button" value="auf mich ändern" onclick="this.form.ORT_USER.value='${s['userid']}';this.form.ORT_QUELLE.value='${s['useropts'].get('OrtDfltAutor') |esc_js_str}'; this.form.ORT_LIZENZ.value='${s['useropts'].get('OrtDfltLizenz') |esc_js_str}'">
          &#x2139;&#xFE0F;
          </span>
          % endif
        </td></tr>
        <tr>
          <td class="horizborder">Koordinaten:</td>
          <td class="horizborder">${ortfelder_koo(eort['schreibrecht'], False, eort['koosystem'], eort['rw'], eort['hw'], eort['kooabw'])}</td>
        </tr>
        <tr>
          <td class="horizborder">Verwaltungseinheit:</td>
          <td class="horizborder">${ortfelder_verw(eort['schreibrecht'], not eort['id'], False, False, eort['land'], eort['bl'], eort['bezirk_id'], eort['gemeinde_id'])}</td>
        </tr>
        <tr>
          <td class="horizborder">Seehöhe:</td>
          <td class="horizborder">${ortfelder_sh(eort['schreibrecht'], not eort['id'], False, False, eort['sh'], eort['sh_tol'])}</td>
        </tr>
        <tr>
          <td class="horizborder">Ortsbeschreibung</td>
          <td class="horizborder"><textarea name="ORT_BESCHREIBUNG" maxlength="10000" cols="80" rows="2"${ro_attr}>${eort['beschreibung'] |h}</textarea></td>
        </tr>
        <tr>
          <td class="horizborder">Zugang</td>
          <td class="horizborder"><textarea name="ORT_ZUGANG" maxlength="2000" cols="80" rows="2"${ro_attr}>${eort['zugang'] |h}</textarea></td>
        </tr>
        <tr>
          <td class="horizborder">Urheberrecht:</td>
          <td class="horizborder">
            <span style="display:flex;flex-wrap:wrap">
              <table style="border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Datenquelle/Urheber</span></td>
                  <td><input type="text" name="ORT_QUELLE" value="${eort['quelle']}" size="50" maxlength="100"${ro_attr}></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Lizenz</span></td>
                  <td>
                    <input name="ORT_LIZENZ" list="ORT_LL" value="${eort['lizenz'] |h}" maxlength="200" size="70"${ro_attr}>
                    <datalist id="ORT_LL">
                      <option value="VDBL 1.0"></option>
                      <option value="CC BY 4.0"></option>
                      <option value="CC BY-SA 4.0"></option>
                      <option value="CC BY-NC 4.0"></option>
                      <option value="CC BY-NC-SA 4.0"></option>
                      <option value="CC BY-ND 4.0"></option>
                      <option value="CC BY-NC-ND 4.0"></option>
                      <option value="DbCL 1.0"></option>
                      <option value="Weitergabe nur an Mitglieder des Landesvereins für Höhlenkunde in Wien und NÖ">
                      <option value="Public Domain"></option>
                    </datalist>
                  </td>
                </tr>
              </table>
            </span>
          </td>
        </tr>
        <tr>
          <td class="horizborder">Zugriffsrechte:</td>
          <td class="horizborder">
            <span style="display:flex;flex-wrap:wrap">
              <table style="border-collapse:collapse;margin-right:2em">
                <tr>
                  <td><span style="margin-right:5px">Geheimhaltungsgrund</span></td>
                  <td><input type="text" name="ORT_GEHEIM" maxlength="100" size="30" value="${eort['geheim'] |h}"${ro_attr}></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse;margin-right:2em">
                <tr>
                  <td title="Lesezugriff einschränken auf die angegebenen Benutzergruppen. Mehrfachauswahl möglich, Untergruppen sind aber schon automatisch inkludiert. Ebenso haben Gruppen mit Schreibrecht automatisch auch Lesezugriff."><span style="margin-right:5px">Leserecht für Gruppen &#x2139;&#xFE0F;</span></td>
                  <td><select name="ORT_GRUPPEN_R" multiple size="3">
                    % for g in eort['gruppen']:
                    <option value="${g['id']}"${If(g['r'], ' selected', Unless(eort['schreibrecht'], ' disabled'))}>${g['name'] |h}</option>
                    % endfor
                  </select></td>
                </tr>
              </table>
              <table style="display:inline-block;border-collapse:collapse">
                <tr>
                  <td title="Schreib- und Leserecht an die angegebenen Benutzergruppen. Mehrfachauswahl möglich, Untergruppen sind aber schon automatisch inkludiert. Ebenso haben immer auch der Benutzer, dem die Ortsangabe zugewiesen ist (üblicherweise der Erfasser) und die Gruppe &quot;Kataster&quot; Schreibrecht. Schreibrecht impliziert Leserecht."><span style="margin-right:5px">Schreibrecht für Gruppen &#x2139;&#xFE0F;</span></td>
                  <td><select name="ORT_GRUPPEN_W" multiple size="3">
                    % for g in eort['gruppen']:
                    <option value="${g['id']}"${If(g['w'], ' selected', Unless(eort['schreibrecht'], ' disabled'))}>${g['name'] |h}</option>
                    % endfor
                  </select></td>
                </tr>
              </table>
              % if eort['schreibrecht']:
              <table style="border-collapse:collapse;margin-left:2em">
                <tr><td>
                  <input type="button" value="reset" onclick="this.form.ORT_GEHEIM.value='';this.form.ORT_GRUPPEN_R.selectedIndex=-1;this.form.ORT_GRUPPEN_W.selectedIndex=-1">
                </td></tr>
              </table>
              % endif
            </span>
          </td>
        </tr>
      </table>
      % if eort['schreibrecht']:
      <div style="margin-top:10px">
        <input type="hidden" name="SESS" value="${s['sessionid']}">
        <input type="hidden" name="HOEHLEID" value="${hoe['id']}">
        <input type="hidden" name="REF" value="${eingang['ref'] |h}">
        <input type="hidden" name="ORT_ID" value="${eort['id']}">
        <input type="hidden" name="IDEM" value="${s['idem']}">
        % if eort['id']:
        <input type="hidden" name="DELORT" value="">
        <input type="button" value="löschen" onclick="this.form.DELORT.value='1';this.form.submit()" style="margin-right:2em">
        % endif # eort['id']
        <input type="submit" value="${'ändern' if eort['id'] else 'anlegen'}">
        % if eort['kataster'] and eort['id']:
        <input type="button" value="schließen und neue Ortsangabe anlegen" onclick="openeingangort(${hoe['id']},'${eingang['ref']}',0)" style="margin-left:2em">
        % endif
      </div>
      % elif s['user'] and not eort['eigene_ortsangabe_vorhanden']:
        <input type="button" value="schließen und eigene Ortsangabe anlegen" onclick="openeingangort(${hoe['id']},'${eingang['ref']}',0)">
      % endif # schreibrecht
    </form>
    % elif s['user']:
    <input type="button" value="neu" onclick="openeingangort(${hoe['id']},'${eingang['ref']}',0)" style="margin-top:10px">
    % endif
  </div>
  % endif # eingang['ref']:
  % endif # 'eingang' in s
  <div style="margin-top:20px">
    <b>Dateien:</b>
    <table style="border-collapse: collapse">
      <tr style="background-color:#ffbb88">
        <th style="text-align:center;border: 1px solid black">
          Pläne
          % if len(s['hdateien']['P']):
          <input type="button" value="Galerie" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&GALERIE=1&HOEHLEID=${hoe['id']}&KATEGORIE=P', true)" style="margin-left:1em">
          % endif
        </th>
        <th style="text-align:center;border: 1px solid black">
          Fotos/Video/Audio
          % if len(s['hdateien']['M']):
          <input type="button" value="Galerie" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&GALERIE=1&HOEHLEID=${hoe['id']}&KATEGORIE=M', true)" style="margin-left:1em">
          % endif
        </th>
        <th style="text-align:center;border: 1px solid black">Berichte</th>
      </tr>
      <tr style="background-color:#ffeecc">
        % for k in ('P','M','B'):
        <td style="border: 1px solid black;vertical-align:top">
        % if len(s['hdateien'][k]):
        <table class="liste" style="margin:2px">
          <tr>
            <th>ID</th>
            <th>Dateiname</th>
            <th>Größe</th>
            <th>Beschreibung</th>
            <th>Aktionen</th>
          </tr>
          % for d in s['hdateien'][k]:
          <tr id="HD_TR_${d['id']}">
            <td style="text-align:right;padding-right:2px">${d['id']}</td>
            <td>${d['filename'] |h}</td>
            <td style="text-align:right;padding-right:2px">${d['size']}</td>
            <td>${d['beschreibung'] |h}</td>
            <td>
              <div id="HD_BTNS_${d['id']}">
                <input type="button" value="Details" onclick="ddetails(${d['id']},${d['v']})">
                % if d['recht']:
                <input type="button" value="anzeigen" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&OPENFILE=${d['id']}&V=${d['v']}', true)">
                <a href="media/${d['id']}_${d['code']}_${d['v']}" target="_blank" type="${d['mime'] |h}" download="${d['filename'] |h}"><input type="button" value="download"></a>
                % endif
                % if hoe['schreibrecht'] or d['recht']==2:
                <input type="button" value="Zuo. löschen" onclick="if (confirm('Zuordnung von ${d['filename'] |esc_js_str} zu dieser Höhle löschen? (Die Datei bleibt dabei erhalten. Zum Löschen der Datei stattdessen über den Button &quot;Details&quot; gehen.)')) del_zuo(${hoe['id']},${d['id']})">
                % endif
              </div>
            </td>
          </tr>
          % endfor
        </table>
        % else:
        keine
        % endif
        </td>
        % endfor
      </tr>
    </table>
    <div style="margin-top:10px">
    <input type="button" value="Dateiliste" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&DLIST=2&DSEARCH_HOEHLEN=H&DSEARCH_HOEHLE_ID=${hoe['id']}'">
    % if s['user']:
    <input type="button" value="${If(len(s['hdateien']['M']) or len(s['hdateien']['P']) or len(s['hdateien']['B']), 'weitere ')}zuordnen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&DLIST=1&HDZUO_H=${hoe['id']}'" style="margin-left:1em;margin-right:1em">
    <input type="button" value="neue hochladen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_UPL=1&RET_HOEHLEID=${hoe['id']}'">
    % endif
    </div>
  </div>
  <div style="margin-top:10px">
    <b>Publikationen:</b>
    % if len(s['hpub']):
    (Zeile anklicken zum Öffnen)
    <table class="liste" style="margin:2px">
      <tr>
        <th>ID</th>
        <th>Autor</th>
        <th>Titel</th>
        <th>Jahr</th>
        <th>Status</th>
      </tr>
      % for p in s['hpub']:
      <tr onclick="openpub(${p['id']})" style="cursor:pointer">
        <td style="text-align:right;padding-right:2px">${p['id']}</td>
        <td>${p['autor'] |h}</td>
        <td>${p['titel'] |h}</td>
        <td>${p['jahr'] |h}</td>
        <td>${p['status'] |h}</td>
      </tr>
      % endfor
    </table>
    % else:
    keine
    % endif
  </div>
  % endif # hoe['id']
  % elif 'bliste' in s: # IF_00001
  <form name="BLIST_FRM" onsubmit="return check_bereiche(this)">
    <div style="margin-top:10px">einschränken auf:</div>
    <table class="graytable">
      <tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:2em">
            <select name="BLIST_RANG">
              % for r in s['raenge']:
              % if r['auswaehlbar'] and r['ebene'] != s['artebene']+1 and r['name_la'] != 'genus':
              <option value="${r['ebene']}"${If(r['ebene'] == s['blist_rang'] or not s['blist_rang'] and r['ebene'] == s['artebene'], ' selected')}>${'Gattung/Art' if r['ebene'] == s['artebene'] else r['name_de']}</option>
              % endif
              % endfor
            </select>
            <input type="text" name="BLIST_TAXON" value="${s['blist_taxon'] |h}">
            <input type="button" value="aus COL" onclick="taxbaum_fuer_uebern('', this.form.BLIST_TAXON.value)">
            <input type="button" value="&#x1F5D1;" onclick="this.form.BLIST_TAXON.value=''">
          </span>
          <span style="margin-right:2em">
            bestimmt
            <select name="BLIST_BESTMINMAX">
              <option value="MAX"${Unless(s['blist_bestminmax'] == 'MIN', ' selected')}>maximal</option>
              <option value="MIN"${If(s['blist_bestminmax'] == 'MIN', ' selected')}>mindestens</option>
            </select>
            bis auf
            <select name="BLIST_BESTRANG">
              <option value=""${Unless(s['blist_bestrang'], ' selected')}>egal</option>
              % for r in s['raenge']:
              % if r['auswaehlbar']:
            <option value="${r['ebene']}"${If(s['blist_bestrang']==r['ebene'], ' selected')}>${r['name_de'] |h}</option>
              % endif
              % endfor
            </select>
          </span>
          <span>
            Bestimmer:
            <input type="text" name="BLIST_BESTIMMER" value="${s['blist_bestimmer'] |h}">
          </span>
        </span>
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:1em">Ort-ID <input type="number" name="BLIST_ORT_ID" size="6" value="${s['blist_ort_id'] |h}"></span>
          <span style="margin-right:2em">Besuch-ID <input type="number" name="BLIST_ID" size="6" value="${s['blist_id'] |h}"></span>
          <span>
            Erfasser:
            % if s['user']:
            <label for="BLIST_EIGENE" style="margin-left:1em">eigene</label><input type="checkbox" id="BLIST_EIGENE" name="BLIST_EIGENE" value="1"${If(s['blist_eigene'], ' checked')} onchange="this.form.BLIST_USER.disabled=this.checked">
            % endif
            <span style="margin-left:1em">Name</span> <input type="text" name="BLIST_USER" value="${s['blist_user'] |h}"${If(s['blist_eigene'], ' disabled')}>
          </span>
        </span>
      </td></tr><tr><td class="horizborder">
        Besuchdatum von <input type="date" name="BLIST_BDAT_VON" value="${s['blist_bdat_von'] |h}"> bis <input type="date" name="BLIST_BDAT_BIS" value="${s['blist_bdat_bis'] |h}">
        <input type="button" value="&#x1F5D1;" onclick="this.form.BLIST_BDAT_VON.value='';this.form.BLIST_BDAT_BIS.value=''">
      </td></tr>
      ${ortsuche()}
      <tr><td class="horizborder">
        Höhlen:
        <select name="BLIST_HOEHLEN" onchange="this.form.BLIST_HOEHLE_ID.disabled = this.value!='H'">
          <option value=""${Unless(s['blist_hoehlen'], ' selected')}>egal</option>
          <option value="H"${If(s['blist_hoehlen'] == 'H', ' selected')}>ausschließlich</option>
          <option value="K"${If(s['blist_hoehlen'] == 'K', ' selected')}>keine</option>
        </select>
        <span style="margin-left:1em">Katnr(ID,Name)</span>
        <select name="BLIST_HOEHLE_ID"${Unless(s['blist_hoehlen'] == 'H', ' disabled')}>
          <option value=""${Unless(s['blist_hoehle_id'], ' selected')}>egal</option>
          % for h in s['hoehlen']:
          <option value="${h['id']}"${If(s['blist_hoehle_id'] == h['id'], ' selected')}>${If(h['katnr'], h['katnr'] + ' (') |h}${h['id']}${If(h['name'], ', ' + h['name']) |h}${If(h['katnr'], ')')}</option>
          % endfor
        </select>
      </td></tr><tr><td class="horizborder">
        <span style="display:flex;flex-wrap:wrap">
          <span style="margin-right:1em" title="photisch: mit Tageslicht&#13;mesophotisch: Übergangszone (Dämmerlicht)&#13;aphotisch: ewige Dunkelheit&#13;Lampenflora: zeitweilig oder ständig künstlich beleuchtet&#13;Eingangsschachtgrund: wo abgestürzte Tiere hinfallen und verenden">Bereiche &#x2139;&#xFE0F;</span>
          % for b in s['bereiche']:
          <span style="margin-right:1em">
            <input type="checkbox" id="BLIST_BEREICH_${b}" name="BLIST_BEREICH_${b}" value="1"${If(s['blist_bereich_'+b], ' checked')}> <label for="BLIST_BEREICH_${b}">${s['bereiche'][b] |h}</label>
          </span>
          % endfor
          <span>
            <input type="checkbox" id="BLIST_BEREICH_N" name="BLIST_BEREICH_N" value="1"${If(s['blist_bereich_N'], ' checked')}> <label for="BLIST_BEREICH_N">nicht angegeben</label>
          </span>
        </span>
      </td></tr><tr><td class="horizborder">
      Probe: Nr. <input type="text" name="BLIST_PRB_NR" value="${s['blist_prb_nr'] |h}">
      <span style="margin-left:1em">Erfasser:</span>
      % if s['user']:
      <label for="BLIST_PRB_EIGENE" style="margin-left:1em">eigene</label><input type="checkbox" id="BLIST_PRB_EIGENE" name="BLIST_PRB_EIGENE" value="1"${If(s['blist_prb_eigene'], ' checked')} onchange="this.form.BLIST_PRB_USER.disabled=this.checked">
      % endif
      <span style="margin-left:1em">Name</span> <input type="text" name="BLIST_PRB_USER" value="${s['blist_prb_user'] |h}"${If(s['blist_prb_eigene'], ' disabled')}>
      </td></tr>
    </table>
    <input type="hidden" name="BLISTE" value="1">
    <input type="hidden" name="SESS" value="${s['sessionid']}">
    <div style="display:flex;flex-wrap:wrap">
      <span style="margin-top:8px;margin-right:2em">
        1 Zeile pro
        <select name="BLIST_ZEILE">
          <option value="O"${If(s['zeile'] == 'O', ' selected')}>Ort</option>
          <option value="B"${If(s['zeile'] in ('B',''), ' selected')}>Besuch</option>
          <option value="F"${If(s['zeile'] == 'F', ' selected')}>Fund</option>
          <option value="Bg"${If(s['zeile'] == 'Bg', ' selected')}>Bestimmung</option>
        </select>
        ## hier kann die maximale Anzahl Zeilen (oder Zeile n-m oder Seite n mit m Zeilen/Seite) herkommen; wird im Moment aber noch nicht benötigt
        ## <span title="max. Anzahl Zeilen, 0 = unbegrenzt">max. &#x2139;&#xFE0F; <input type="number" name="BLIST_MAXZEILEN" step="100" min="0" value="${s['blist_maxzeilen'] or 0}"></span>
        <span style="margin-left:1em">Sortierung:</span>
        <select name="SORT1" onchange="this.form.SORT2.disabled = this.form.SORT2_DIR.disabled = this.value == 'ID'">
          <option value="ID"${If(s['sort1'] in ('','ID'), ' selected')}>ID</option>
          <option value="U" ${If(s['sort1'] == 'U',' selected')}>Benutzer</option>
          <option value="BD"${If(s['sort1'] == 'BD', ' selected')}>Besuch-Datum</option>
          <option value="AD"${If(s['sort1'] == 'AD', ' selected')}>Anlagedatum</option>
          <option value="SH"${If(s['sort1'] == 'SH', ' selected')}>Seehöhe</option>
        </select>
        <select name="SORT1_DIR">
          <option value="ASC"${Unless(s['sort1_dir'] == 'DESC', ' selected')}>aufsteigend</option>
          <option value="DESC"${If(s['sort1_dir'] == 'DESC', ' selected')}>absteigend</option>
        </select>
        dann
        <select name="SORT2"${If(s['sort1'] in ('','ID'), ' disabled')}>
          <option value="ID"${If(s['sort2'] in ('','ID'), ' selected')}>ID</option>
          <option value="U" ${If(s['sort2'] == 'U', ' selected')}>Benutzer</option>
          <option value="BD"${If(s['sort2'] == 'BD', ' selected')}>Besuch-Datum</option>
          <option value="AD"${If(s['sort2'] == 'AD', ' selected')}>Anlagedatum</option>
          <option value="SH"${If(s['sort2'] == 'SH', ' selected')}>Seehöhe</option>
        </select>
        <select name="SORT2_DIR"${If(s['sort1'] in ('','ID'), ' disabled')}>
          <option value="ASC"${Unless(s['sort2_dir'] == 'DESC', ' selected')}>aufsteigend</option>
          <option value="DESC"${If(s['sort2_dir'] == 'DESC', ' selected')}>absteigend</option>
        </select>
      </span>
      <span style="margin-top:8px">
        <input type="hidden" name="KARTE">
        <input type="submit" value="Suche (→ Liste)" style="border:2px solid black">
        <input type="button" value="Suche (→ Karte)" style="margin-left:1em" onclick="if(this.form.reportValidity()) {this.form.KARTE.value='F'; this.form.submit(); this.form.KARTE.value=''}">
        <span style="margin-left:1em" title="Spreadsheet erzeugen und herunterladen. Kann in Excel / Libreoffice / Google Drive usw. eingelesen werden.">
          <a id="XLSX" href="" target="_blank" type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" download="funde.xlsx"><input type="button" value="XLSX" onclick="document.getElementById('XLSX').href=xlist_form2url(this.form)+'&XLSX=1'"></a>
          &#x2139;&#xFE0F;
        </span>
        <span style="margin-left:1em" title="GPX-Datei (für GPS-Geräte von Garmin) mit den Koordinaten der ausgewählten Orte erzeugen und herunterladen.">
          <a id="GPX" href="" target="_blank" type="application/gpx+xml" download="funde.gpx"><input type="button" value="GPX" onclick="document.getElementById('GPX').href=xlist_form2url(this.form)+'&GPX=1'"></a>
          &#x2139;&#xFE0F;
        </span>
        ${suchopts_formpart()}
      </span>
    </div>
  </form>
  <div style="margin-top:10px">
    % if len(s['bliste']):
    <b>Suchergebnis:</b> (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr>
        % if s['zeile'] == 'O':
        <th>Ort-ID</th>
        % else:
        <th>Besuch-ID</th>
        <th>angelegt von</th>
        <th>Besuch-Datum</th>
        % if s['zeile'] != 'Bg':
        <th>angelegt am</th>
        <th>geändert am</th>
        % endif
        % endif
        <th>Höhle</th>
        <th>Koo-System</th>
        <th>RW</th>
        <th>HW</th>
        <th>Toleranz</th>
        <th>Seehöhe</th>
        <th>Land</th>
        <th>Bundesland</th>
        <th>Bezirk</th>
        <th>Gemeinde</th>
        <th>Ortsbeschreibung</th>
        % if s['zeile'] == 'O':
        <th>Besuche</th>
        % else:
        <th>Kommentar</th>
        % if s['zeile'] == 'B':
        <th>Funde</th>
        % elif s['zeile'] == 'F':
        <th>Fundbeschreibung</th>
        <th>Bereich</th>
        % endif
        % endif
        % if s['zeile'] in ('F','Bg'):
        <th>Anzahl Indiv.</th>
        <th>Art</th>
        % else:
        <th>Arten</th>
        % endif
        % if s['zeile'] == 'Bg':
        <th>Bestimmer</th>
        <th>Best.komm.</th>
        % else:
        <th>Art unbestimmt</th>
        % endif
      </tr>
      % for b in s['bliste']:
      <tr
        % if s['zeile'] == 'O':
        onclick="var f=document.forms.BLIST_FRM;f.reset();f.BLIST_ORT_ID.value='${b['ort_id']}';f.BLIST_ZEILE.value='B';f.submit()"
        % else:
        % if s['zeile'] == 'B':
        onclick="openbesuch(${b['besuch_id']}${If(s['useropts'].get('NewWinLF'), ', true')})"
        % elif s['zeile'] == 'F':
        onclick="openfund(${b['besuch_id']}, ${b['fund_i']}${If(s['useropts'].get('NewWinLF'), ', true')})"
        % else: # 'Bg'
        onclick="openbest(${b['besuch_id']}, ${b['fund_i']}, ${b['best_i']}${If(s['useropts'].get('NewWinLF'), ', true')})"
        % endif
        style="cursor:pointer"
        % endif
      >
        % if s['zeile'] == 'O':
        <td style="text-align:right;padding-right:2px">${b['ort_id']}</td>
        % else:
        <td style="text-align:right;padding-right:2px">${b['besuch_id']}</td>
        <td>${b['user']}</td>
        <td>${b['besuch_d']}</td>
        % if s['zeile'] != 'Bg':
        <td>${b['angelegt_d']}</td>
        <td>${b['geaendert_d']}</td>
        % endif
        % endif
        <td>${b['hoehle'] |h}</td>
        <td>${b['koosystem']}</td>
        <td style="text-align:right">${b['rw']}</td>
        <td style="text-align:right">${b['hw']}</td>
        <td style="text-align:right;padding-right:2px">${b['kooabw']}</td>
        <td style="text-align:right;padding-right:2px">${b['sh']}</td>
        <td>${b['land']}</td>
        <td>${b['bl']}</td>
        <td>${b['bezirk']}</td>
        <td>${b['gemeinde']}</td>
        <td>${b['ortsbeschreibung'] |h}</td>
        % if s['zeile'] == 'O':
        <td style="text-align:right;padding-right:5px">${b['anz_besuche'] |h}</td>
        % else:
        <td>${b['kommentar'] |h}</td>
        % if s['zeile'] == 'B':
        <td style="text-align:right;padding-right:5px">${b['anz_funde']}</td>
        % elif s['zeile'] == 'F':
        <td>${b['fundbeschreibung'] |h}</td>
        <td>${b['bereich'] |h}</td>
        % endif
        % endif
        % if s['zeile'] in ('F','Bg'):
        <td style="text-align:right;padding-right:2px">${b['individuen'] |h}</td>
        % endif
        <td>${b['arten'] |h}</td>
        % if s['zeile'] == 'Bg':
        <td>${b['bestimmer'] |h}</td>
        <td>${b['bestkomm'] |h}</td>
        % else:
        <td style="text-align:right;padding-right:5px">${b['anz_unbestimmt']}</td>
        % endif
      </tr>
      % endfor
    </table>
    % else:
    nichts gefunden
    % endif
  </div>
  % elif 'besuch' in s: # IF_00001
    <% besuch = s['besuch'] %>\
    <% ro_attr = '' if besuch['schreibrecht'] else ' readonly' %>\
  <form name="BESUCHFRM" method="post" action="${s['scriptname']}">
    <div style="margin-top:5px"><b>Ort und Datum:</b></div>
    <table class="graytable">
      % if besuch['id']:
      <tr><td>Besuch-ID</td><td><input name="BESUCHID" size="6" value="${besuch['id']}" readonly></td></tr>
      <tr>
        <td>angelegt von</td>
        <td>
          <a href="javascript:openuser(${besuch['userid']})">${besuch['user_realname'] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F;</span>
          <span style="margin-left:1em">am</span> <input name="ANGELEGT_DT" type="datetime_local" value="${besuch['angelegt_dt'] |h}" readonly>
          <span style="margin-left:1em">geändert am</span> <input name="GEAENDERT_DT" type="datetime_local" value="${besuch['geaendert_dt'] |h}" readonly>
        </td>
      </tr>
      % endif
      <tr><td class="horizborder">Besuch-Datum</td><td class="horizborder"><input name="BESUCH_D" type="date" value="${besuch['besuch_d']}" ${"required onchange=\"now=new Date(); nowstr = now.getFullYear().toString() + '-' + (now.getMonth()+1).toString().padStart(2,'0') + '-' + now.getDate().toString().padStart(2,'0'); if (this.value > nowstr) alert('Warnung: Datum liegt in der Zukunft.')\"" if besuch['schreibrecht'] else 'readonly'}><span style="margin-left:2em">Uhrzeit</span><input name="BESUCH_T" type="time" value="${besuch['besuch_t']}"${ro_attr} style="margin-left:2em"></td></tr>
      <tr>
        <td class="horizborder">Ort</td>
        <td class="horizborder">
          <input type="radio" id="ORT_R_HOEHLE" name="ORT_R" value="H" ${'required' if besuch['schreibrecht'] else 'disabled'}${If(besuch['hoehle_id'], ' checked')} onchange="adj_ort_inputs(true)">
          <label for="ORT_R_HOEHLE">Höhle</label>
          <span style="margin-left:1em">
            Katnr(ID,Name)
            <select name="ORT_HOEHLE_ID">
              % if not besuch['hoehle_id']:
              <option value="" selected>${If(besuch['schreibrecht'], 'bitte auswählen')}</option>
              % endif
              % for h in s['hoehlen']:
              <option value="${h['id']}"${If(besuch['hoehle_id']==h['id'], ' selected', Unless(besuch['schreibrecht'], ' disabled'))}>${If(h['katnr'], h['katnr'] + ' (') |h}${h['id']}${If(h['name'], ', ' + h['name']) |h}${If(h['katnr'], ')')}</option>
              % endfor
            </select>
          </span>
          <input type="radio" id="ORT_R_KOO" name="ORT_R" value="K" ${'required' if besuch['schreibrecht'] else 'disabled'}${Unless(besuch['hoehle_id'], ' checked')} onchange="adj_ort_inputs(true)" style="margin-left:3em">
          <label for="ORT_R_KOO">anderer Fundort</label
        <hr>
          <div>
            <span style="margin-right: 2em">Koordinaten:</span>
            ${ortfelder_koo(besuch['schreibrecht'], True, besuch['koosystem'], besuch['rw'], besuch['hw'], besuch['kooabw'])}
          </div>
          <div style="margin-top:2px">
            <span style="margin-right: 2em">Verwaltungseinheit:</span>
            ${ortfelder_verw(besuch['schreibrecht'], not besuch['id'], True, bool(besuch['hoehle_id']), besuch['land'], besuch['bl'], besuch['bezirk_id'], besuch['gemeinde_id'])}
          </div>
          <div style="margin-top:2px">
            <span style="margin-right: 2em">Seehöhe:</span>
            ${ortfelder_sh(besuch['schreibrecht'], not besuch['id'], True, bool(besuch['hoehle_id']), besuch['sh'], besuch['sh_tol'])}
          </div>
          <table style="margin-top:2px;border-collapse:collapse">
            <tr>
              <td style="vertical-align:middle">Ortsbeschreibung</td>
              <td><textarea name="ORT_BESCHREIBUNG" maxlength="10000" cols="80" rows="3"${If(not besuch['schreibrecht'] or besuch['hoehle_id'], ' readonly')}>${besuch['ortsbeschreibung'] |h}</textarea></td>
            </tr><tr>
              <td style="vertical-align:middle">Zugang</td>
              <td><textarea name="ORT_ZUGANG" maxlength="2000" cols="80" rows="3"${If(not besuch['schreibrecht'] or besuch['hoehle_id'], ' readonly')}>${besuch['zugang'] |h}</textarea></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr><td class="horizborder" title="Anlass, Teilnehmer, Wetterbedingungen...">Kommentar &#x2139;&#xFE0F;</td><td class="horizborder"><textarea name="BESUCHKOMMENTAR" maxlength="10000" cols="80" rows="5"${ro_attr}>${besuch['kommentar'] |h}</textarea></td></tr>
    </table>
    <div style="margin-top:10px">
    % if besuch['schreibrecht']:
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      % if besuch['id']:
      <input type="button" value="löschen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID=${besuch['id']}&DELBESUCH=1&IDEM=${s['idem']}'"${If('abhaengige_datensaetze_vorhanden' in besuch, ' disabled title="Löschen unzulässig, solang Dateien, Proben oder Bestimmungen dranhängen"')} style="margin-right:2em">
      % endif
    % endif
    % if besuch['id']:
      <input type="button" value="zu Karte" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&KARTE=F&OPEN_ORT=${besuch['id']}'${If(s['useropts'].get('NewWinKF'), ', true')})" style="margin-right:2em">
    % endif
      <input type="button" id="B2H_BTN" value="zu Höhle" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&HOEHLEID='+this.form.ORT_HOEHLE_ID.value${If(s['useropts'].get('NewWinHF'), ', true')})"${Unless(besuch['hoehle_id'], ' disabled')} style="margin-right:2em">
    % if besuch['schreibrecht']:
      <input type="hidden" name="IDEM" value="${s['idem']}">
      <input type="submit" value="${'ändern' if besuch['id'] else 'anlegen'}" style="margin-right:2em">
    % endif
    % if besuch['id'] and s['user']:
      <span title="Ort übernehmen, anderes Datum"><input type="button" value="nochmals besucht" onclick="openbesuch(${besuch['id']},false,true)"> &#x2139;&#xFE0F;</span>
    % endif
    </div>
  </form>
  % if besuch['id']:
  <div style="margin-top:20px">
    <b>Liste der Funde (Individuen oder Populationen) bei diesem Besuch:</b>
    % if 'funde' in s and len(s['funde']):
    (Zeile anklicken zum Öffnen)
    <table class="liste">
      <tr><th>Index</th><th>Beschreibung</th><th>Anzahl Indiv.</th><th>Bereich</th><th>Anzahl Dateien</th><th>Anzahl Bestimmungen</th><th>bestimmt auf</th><th>Ergebnis</th></tr>
      % for f in s['funde']:
      <tr onclick="openfund(${besuch['id']},${f['i']})"${If('fund' in s and s['fund']['i']==f['i'], ' class="liste_selected"')} style="cursor:pointer">
        <td style="text-align:right;padding-right:5px">${f['i']}</td><td>${f['beschreibung'] |h}</td><td style="text-align:right;padding-right:5px">${f['individuen']}</td><td>${f['bereich']}</td><td style="text-align:right;padding-right:5px">${f['anzdateien']}</td><td style="text-align:right;padding-right:5px">${f['anzbest']}</td><td>${f['rang']}</td><td>${f['taxon'] |h}</td>
      </tr>
      % endfor
    </table>
    % else: # s['funde']
    <br>noch keine erfasst
    % endif # s['funde']
    % if besuch['schreibrecht'] and ('fund' not in s or s['fund']['i']):
    <form method="post" action="${s['scriptname']}" style="margin-top:10px">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="BESUCHID" value="${besuch['id']}">
      <input type="hidden" name="FUND_I" value="0">
      <input type="submit" value="${If(s['funde'], 'weiteren ')}Fund anlegen">
    </form>
    % endif # schreibrecht
  </div>
  % endif # besuch['id']
  % if 'fund' in s:
  <% fund = s['fund'] %>\
  <% ro_attr = '' if fund['schreibrecht'] else ' readonly' %>\
  <form id="FUNDFRM" name="FUNDFRM" method="post" action="${s['scriptname']}" style="margin-top:20px">
    <b>Fund:</b>
    <table class="graytable">
      <tr><td colspan="2">
        Besuch-ID <input name="FUND_BESUCHID" size="6" value="${fund['besuchid']}" readonly>
        % if fund['i']:
        <span style="margin-left:2em">Fund-Index</span> <input name="FUND_I" size="2" value="${fund['i']}" readonly>
        % endif
      </td></tr>
      <tr>
        <td class="horizborder" title="Anzahl gesehene bzw. gezählte Individuen">Anzahl &#x2139;&#xFE0F;</td>
        <td class="horizborder">
          <input type="number" name="FUND_INDIVIDUEN" value="${fund['individuen']}" min="1" max="10000000" style="margin-right:1em"${If(fund['schreibrecht'], 'onchange="this.form.FUND_INDIV_GENAU.disabled=!this.value"', ' readonly')}>
          <select name="FUND_INDIV_GENAU"${Unless(fund['individuen']>'', ' disabled')}>
            <option value="G"${If(fund['indiv_genau'] in ('G',''), ' selected', Unless(fund['schreibrecht'], ' disabled'))}>genau</option>
            <option value="U"${If(fund['indiv_genau']=='U', ' selected', Unless(fund['schreibrecht'], ' disabled'))}>ungefähr</option>
            <option value="M"${If(fund['indiv_genau']=='M', ' selected', Unless(fund['schreibrecht'], ' disabled'))}>mindestens</option>
          </select>
        </td>
      </tr>
      <tr>
        <td class="horizborder">Bereich</td>
        <td class="horizborder">
          <select name="FUND_BEREICH">
            <option value=""${Unless(fund['bereich'], ' selected', Unless(fund['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for b in s['bereiche']:
            <option value="${b}"${If(fund['bereich']==b, ' selected', Unless(fund['schreibrecht'], ' disabled'))}>${s['bereiche'][b] |h}</option>
            % endfor
          </select>
        </td>
      </tr>
      <tr><td align="right" class="horizborder">Beschreibung (Merkmale, welche Stelle...)</td><td class="horizborder"><textarea name="FUND_BESCHREIBUNG" maxlength="10000" cols="80" rows="5"${ro_attr}>${fund['beschreibung'] |h}</textarea></td></tr>
      ## mit dem Anlegen des Fundes gleichzeitiges Hochladen von Dateien vorerst auskommentiert, weil weitere Maskenfelder (Beschreibung, Lizenz...) nötig wären
      ##% if not fund['i']:
      ##<tr>
      ##  <td class="horizborder">Dateien</td>
      ##  <td class="horizborder" colspan="3">
      ##    <input type="file" name="FUND_DATEI" multiple capture="environment" onchange="document.getElementById('FUND_DATEI_RESET').disabled=this.value==''">
      ##    <input type="button" id="FUND_DATEI_RESET" value="Auswahl rückgängig" onclick="this.form.FUND_DATEI.value='';this.disabled=true" disabled>
      ##  </td>
      ##</tr>
      ##% endif
    </table>
    % if fund['schreibrecht']:
    <div style="margin-top:10px">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="BESUCHID" value="${besuch['id']}">
      <input type="hidden" name="IDEM" value="${s['idem']}">
      % if fund['i']:
      <input type="hidden" name="DELFUND" value="">
      <input type="button" value="löschen"${If(len(s['dateien']) or len(s['proben']) > 1 or len(s['bestimmungen']), ' disabled title="Löschen unzulässig, solang Dateien, Proben oder Bestimmungen dranhängen"')} onclick="this.form.DELFUND.value='1';this.form.submit()" style="margin-right:2em">
      % else:
      <input type="hidden" name="FUND_I" value="0">
      % endif
      <input type="submit" value="${'ändern' if fund['i'] else 'anlegen'}">
      ##folgende Buttons verkürzen den Arbeitsablauf, erfordern aber eine etwas andere Logik in fund.py, und zwar müsste zuerst der Fund gespeichert und erst dann die Masken für die Dateiverwaltung vorbereitet werden
      ##% if not fund['i']:
      ##<input type="button" value="anlegen + Dateien hochladen">
      ##<input type="button" value="anlegen + bestehende Dateien zuordnen">
      ##% endif
    </div>
    % endif # schreibrecht
  </form>
  % if fund['i']:
  <div style="margin-top:15px"><b>Dateien:</b><br>
  % if len(s['dateien']):
    <table class="liste">
      <tr><th>Datei-ID</th><th>Dateiname</th><th>MIME-Typ</th><th>Größe</th><th>Upload-Datum</th><th>Aktionen</th></tr>
      % for f in s['dateien']:
      <tr>
        <td style="text-align:right;padding-right:2px">${f['id']}</td><td>${f['filename'] |h}</td><td>${f['mime'] |h}</td>
        <td style="text-align:right">${f['filesize']}</td>
        <td>${f['upload_d']}</td>
        <td>
          <input type="button" value="anzeigen" style="margin-right:1em" onclick="openlink('${s['scriptname']}?SESS=${s['sessionid']}&OPENFILE=${f['id']}', true)">
          <a href="media/${f['id']}" target="_blank" type="${f['mime'] |h}" download="${f['filename'] |h}" style="margin-right:1em"><input type="button" value="download"></a>
          <input type="button" value="Details" onclick="ddetails(${f['id']},${f['v']})">
          % if f['loeschrecht']:
          <input type="button" value="löschen" style="margin-left:1em" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID=${fund['besuchid']}&FUND_I=${fund['i']}&DELFILE=${f['id']}&IDEM=${s['idem']}'">
          % endif
          % if fund['schreibrecht']:
          <input type="button" value="Zuordnung löschen" style="margin-left:1em" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&BESUCHID=${fund['besuchid']}&FUND_I=${fund['i']}&DELFILEZUO=${f['id']}&IDEM=${s['idem']}'">
          % endif
        </td>
      </tr>
      % endfor
    </table>
  % else:
    noch keine
  % endif # dateien
  </div>
  % if len(s['dateien']) or fund['schreibrecht']:
  <div style="margin-top:10px">
    % if len(s['dateien']):
    <input type="button" value="Fotogalerie" onclick="openlink('${s['scriptname']}?GALERIE=1&BESUCHID=${fund['besuchid']}&FUND_I=${fund['i']}', true)" style="margin-right:2em">
    % endif
    % if fund['schreibrecht']:
    <input type="button" value="${If(len(s['dateien']), 'weitere ')}zuordnen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&DLIST=1&FDZUO_B=${besuch['id']}&FDZUO_I=${fund['i']}'" style="margin-right:1em">
    <input type="button" value="neue hochladen" onclick="location.href='${s['scriptname']}?SESS=${s['sessionid']}&D_UPL=1&RET_BESUCHID=${besuch['id']}&RET_FUNDI=${fund['i']}'">
    % endif
  </div>
  % endif # len(s['dateien']) or fund['schreibrecht')
  <div style="margin-top:15px"><b>Proben:</b>
    % if not s['proben'] or not s['proben'][0]['id']: # wenn kein Eintrag oder nur für Neuanlage
    <br>keine erfasst
    % endif
    % for p in s['proben']:
    % if not p['id']:
    <div id="PRB_NEUBTN" style="margin-top:10px">
      <input type="button" value="neu" onclick="document.forms.PRB_NEUFRM.PRB_DATUM.value=new Date().toJSON().slice(0,10);document.getElementById('PRB_NEUBTN').style.display='none';document.forms.PRB_NEUFRM.style.display='block';document.forms.PRB_NEUFRM.PRB_NR.focus()">
    </div>
    % endif
    <form${Unless(p['id'], ' name="PRB_NEUFRM" style="display:none"')}>
      am <input type="date" name="PRB_DATUM" value="${p['datum']}" ${'required' if p['schreibrecht'] else 'readonly'} style="margin-right:5px">
      Nr. <input type="text" name="PRB_NR" value="${p['nr']}"${Unless(p['schreibrecht'], ' readonly')}>
      <select name="PRB_AKTION" ${'onchange="adj_prb(this)"' if p['schreibrecht'] else ' disabled'}>
        % for a in s['prb_aktionen']:
        <option value="${a}"${If(a == p['aktion'], ' selected')}>${s['prb_aktionen'][a] |h}</option>
        % endfor
      </select>
      <span id="PRB_${p['id']}_TXT1"></span>
      <input type="text" name="PRB_PAR1" maxlength="50" value="${p['par1']}"${Unless(p['schreibrecht'], ' readonly')} style="display:none">
      <span id="PRB_${p['id']}_TXT2"></span>
      <input type="text" name="PRB_PAR2" maxlength="50" value="${p['par2']}"${Unless(p['schreibrecht'], ' readonly')} style="display:none">
      <span style="margin-left:1em">Anmerkung</span>
      <input type="text" name="PRB_ANMERKUNG" maxlength="1000" value="${p['anmerkung']}"${Unless(p['schreibrecht'], ' readonly')}>
      <input type="hidden"name="PRB_ID" value="${p['id']}">
      <span style="margin-left:1em">
      % if p['schreibrecht']:
        <input type="hidden" name="SESS" value="${s['sessionid']}">
        <input type="hidden" name="BESUCHID" value="${besuch['id']}">
        <input type="hidden" name="FUND_I" value="${fund['i']}">
        % if p['id']:
        <input type="hidden" name="PRB_ACTION" value="AEND">
        <input type="submit" value="ändern">
        <input type="button" value="löschen" onclick="this.form.PRB_ACTION.value='DEL';this.form.submit()">
        % else:
        <input type="hidden" name="PRB_ACTION" value="NEU">
        <input type="submit" value="anlegen">
        % endif
      % else:
        erfasst von <a href="javascript:openuser(${p['user']})">${p['user_realname'] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F</span>
      % endif
      </span>
    </form>
    % endfor
  </div>
  <div style="margin-top:15px"><b>Bestimmungen:</b>
    % if len(s['bestimmungen']):
    (Zeile anklicken zum Öffnen)
    % endif
    <br>
    % if len(s['bestimmungen']):
      <table class="liste">
        <tr><th>Index</th><th>angelegt von</th><th>Bestimmer</th><th>Rang</th><th>Taxon</th><th>Geschlecht</th><th>Methode</th><th>Detaillevel</th><th>Kommentar</th><th>angelegt am</th><th>geändert am</th></tr>
        % for b in s['bestimmungen']:
        <tr onclick="openbest(${besuch['id']},${fund['i']},${b['i']})"${If(s['best']['i'] == b['i'], ' class="liste_selected"')} style="cursor:pointer">
          <td style="text-align:right;padding-right:5px">${b['i']}</td>
          <td>${b['user_realname'] |h}</td>
          <td>${b['bestimmer'] |h}</td>
          <td>${b['rang'] |h}</td>
          <td>
            ${b['taxon'] |h}
            % if b['epitheton']:
            ${b['epitheton'] |h}
            % endif
            % if b['ssp']:
            ssp. ${b['ssp'] |h}
            % endif
            % if b['var']:
            var. ${b['var'] |h}
            % endif
            % if b['cv']:
            cv. ${b['cv'] |h}
            % endif
            % if b['form']:
            f. ${b['form'] |h}
            % endif
          </td>
          <td>
            % if b['geschlecht'] == 'W':
            ♀
            % elif b['geschlecht'] == 'M':
            ♂
            % elif b['geschlecht'] == 'B':
            ♀+♂
            % elif b['geschlecht'] == 'w':
            u.a. ♀
            % elif b['geschlecht'] == 'm':
            u.a. ♂
            % elif b['geschlecht'] == 'Z':
            ⚥
            % endif
          </td>
          <td>${b['methode'] |h}</td>
          <td>${b['detail'] |h}</td>
          <td>${b['kommentar'] |h}</td>
          <td>${b['angelegt_d']}</td>
          <td>${b['geaendert_d']}</td>
        </tr>
        % endfor
      </table>
    % else:
    noch keine
    % endif # bestimmungen
  </div>
  <% best = s['best'] %>\
  <div id="BESTFRM_DIV"
    % if not best['form_oeffnen']:
    style="display:none"
    % endif
  >
    % if 'offene_anfrage' in s['fund'] and best['schreibrecht']:
    <div style="margin-top:20px">
      <span style="color:red">Bestimmungsanfrage</span> von <a href="javascript:openuser(${s['besuch']['userid']})">${s['besuch']['user_realname'] |h}</a>
    </div>
    <table class="graytable">
      <tr><td>Anfragedatum</td><td style="padding-left:1em">${s['fund']['offene_anfrage']['anfrage_d']}</td></tr>
      <tr><td>Kommentar</td><td style="padding-left:1em"><textarea readonly>${s['fund']['offene_anfrage']['kommentar'] |h}</textarea></td></tr>
    </table>
    <form method="POST">
      <input type="hidden" name="SESS" value="${s['sessionid']}">
      <input type="hidden" name="BESUCHID" value="${besuch['id']}">
      <input type="hidden" name="FUND_I" value="${fund['i']}">
      <input type="hidden" name="ABLEHNUNG" value="1">
      ## automatisch zu den noch offenen Anfragen gehen - erfordert aber Anpassung in fund.py, damit Besuch+Fund nicht geöffnet werden
      ##% if s['anz_anfragen'] > 1:
      ##<input type="hidden" name="ALISTE" value="1">
      ##% endif
      mit der Begründung <input type="text" name="ABLEHNGRUND" maxlength="1000" size="50"> <input type="submit" value="ablehnen">
    </form>
    oder Bestimmung erfassen:
    % endif
    <% ro_attr = '' if best['schreibrecht'] else ' readonly' %>\
    <form name="BESTFRM" method="post" action="${s['scriptname']}" onsubmit="return best_subm_check()">
      <table class="graytable"${Unless('offene_anfrage' in s['fund'], ' style="margin-top:20px"')}>
        <tr><td colspan="2">
          Besuch-ID <input name="BEST_BESUCHID" size="4" value="${fund['besuchid']}" readonly>
          <span style="margin-left:2em">Fund-Index</span> <input name="BEST_FUND_I" size="2" value="${fund['i']}" readonly>
        % if best['i']:
          <span style="margin-left:2em">Bestimmung-Index</span> <input name="BEST_I" size="2" value="${best['i']}" readonly>
        </td></tr>
        <tr><td colspan="2">
          angelegt von <a href="javascript:openuser(${best['userid']})">${best['user_realname'] |h}</a> <span title="Link öffnet in neuem Fenster">&#x2139;&#xFE0F;</span>
          <span style="margin-left:1em">am</span> <input name="BEST_ANGELEGT_DT" type="datetime_local" value="${best['angelegt_dt'] |h}" readonly>
          <span style="margin-left:1em">geändert am</span> <input name="BEST_GEAENDERT_DT" type="datetime_local" value="${best['geaendert_dt'] |h}" readonly>
        % endif
        </td></tr>
        <tr><td class="horizborder">Bestimmer</td><td class="horizborder">
          <select ${If(best['schreibrecht'], 'onchange=adj_bestimmer(this.value)')}>
            <option value="S"${Unless(best['bestimmer'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>selbst</option>
            <option value="A"${If(best['bestimmer'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>jemand ohne Account</option>
          </select>
          <input type="text" name="BEST_BESTIMMER" value="${best['bestimmer'] |h}"${ro_attr}${Unless(best['bestimmer'], ' disabled')}${If(best['schreibrecht'] and best['bestimmer'], ' required')}>
        </td></tr>
        <tr><td class="horizborder">Methode</td><td class="horizborder">
          <select name="BEST_METHODE">
            <option value=""${Unless(best['methode'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>nicht angegeben</option>
            % for m in s['methoden']:
            <option value="${m['key']}"${If(best['methode']==m['key'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>${m['text'] |h}</option>
            % endfor
          </select>
          <span style="margin-left:2em">
            Detaillevel
            <select name="BEST_DETAIL">
              <option value=""${Unless(best['detail'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>nicht angegeben</option>
              % for d in s['details']:
              <option value="${d['key']}"${If(best['detail']==d['key'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>${d['text'] |h}</option>
              % endfor
            </select>
          </span>
        </td></tr>
        <tr><td class="horizborder">Ergebnis</td><td class="horizborder">
          Rang
          <select name="BEST_RANG"${If(best['schreibrecht'], ' required')} onchange="adj_ergebnis();taxcheck()">
            % if not best['rang']:
            <option value="" selected>bitte auswählen</option>
            % endif
            % for r in s['raenge']:
            % if r['auswaehlbar']:
            <option value="${r['ebene']}"${If(best['rang']==r['ebene'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>${r['name_de'] |h}</option>
            % endif
            % endfor
          </select>
          <input type="text" name="BEST_TAXON" maxlength="30" value="${best['taxon']}" ${'required onchange="remove_epitheton(this);taxcheck()"' if best['schreibrecht'] else 'readonly'}>
          <input type="text" name="BEST_EPITHETON" maxlength="30" value="${best['epitheton']}"${'onchange="taxcheck()"' if best['schreibrecht'] else 'readonly'}>
          ssp.
          <input type="text" name="BEST_SSP" value="${best['ssp']}"${ro_attr}>
          var.
          <input type="text" name="BEST_VAR" value="${best['var']}"${ro_attr}>
          cv.
          <input type="text" name="BEST_CV" value="${best['cv']}"${ro_attr}>
          f.
          <input type="text" name="BEST_FORM" value="${best['form']}"${ro_attr}>
          <div style="margin-top:2px">
            <input type="button" value="in COL ansehen${If(best['schreibrecht'], '/auswählen')}" onclick="taxbaum_from_best(this.form)">
            <input type="hidden" name="BEST_PFAD" value="${best['pfad']}">
            <select id="PFAD_SEL" style="margin-left:1em;display:none" onchange="this.form.BEST_PFAD.value=this.value;pfad2displ()">
              <option value="">bitte auswählen</option>
            </select>
            <span id="BEST_PFAD_DISPL" style="margin-left:1em"></span>
          </div>
          <div style="margin-top:2px">
            Geschlecht
            <select name="BEST_GESCHLECHT">
              <option value=""${Unless(best['geschlecht'], ' selected', Unless(best['schreibrecht'], ' disabled'))}>nicht angegeben</option>
               <option value="W"${If(best['geschlecht']=='W', ' selected', Unless(best['schreibrecht'], ' disabled'))}>weiblich</option>
              <option value="M"${If(best['geschlecht']=='M', ' selected', Unless(best['schreibrecht'], ' disabled'))}>männlich</option>
              <option value="B"${If(best['geschlecht']=='B', ' selected', Unless(best['schreibrecht'], ' disabled'))}>♀+♂ (bei &gt;1 Ind.)</option>
              <option value="w"${If(best['geschlecht']=='w', ' selected', Unless(best['schreibrecht'], ' disabled'))}>♀ dabei (bei &gt;1 Ind.)</option>
              <option value="m"${If(best['geschlecht']=='m', ' selected', Unless(best['schreibrecht'], ' disabled'))}>♂ dabei (bei &gt;1 Ind.)</option>
              <option value="K"${If(best['geschlecht']=='K', ' selected', Unless(best['schreibrecht'], ' disabled'))}>noch nicht entwickelt</option>
              <option value="Z"${If(best['geschlecht']=='Z', ' selected', Unless(best['schreibrecht'], ' disabled'))}>Zwitter</option>
              <option value="N"${If(best['geschlecht']=='N', ' selected', Unless(best['schreibrecht'], ' disabled'))}>nicht erkennbar</option>
            </select>
          </div>
        </td></tr>
        <tr><td class="horizborder">Kommentar</td><td class="horizborder"><textarea name="BEST_KOMMENTAR" maxlength="1000" cols="80" rows="5"${ro_attr}>${best['kommentar'] |h}</textarea></td></tr>
      </table>
      <div style="margin-top:10px">
      % if best['schreibrecht']:
        <input type="hidden" name="SESS" value="${s['sessionid']}">
        <input type="hidden" name="BESUCHID" value="${besuch['id']}">
        <input type="hidden" name="FUND_I" value="${fund['i']}">
        % if best['i']:
        <input type="hidden" name="DELBEST" value="">
        <input type="button" value="löschen" onclick="this.form.DELBEST.value='1';this.form.submit()">
        % else:
        <input type="hidden" name="BEST_I" value="0">
        <input type="button" value="abbrechen" onclick="document.getElementById('BESTFRM_DIV').style.display='none';document.getElementById('BESTNEUBTNDIV').style.display='block'">
        % endif
        <input type="hidden" name="IDEM" value="${s['idem']}">
        <input type="submit" id="BEST_SUBM"value="${'ändern' if best['i'] else 'anlegen'}" style="margin-left:2em">
        % if best['i'] and fund['schreibrecht']:
        <input type="button" value="jemand anderen um Bestimmung bitten..." style="margin-left:2em" onclick="document.getElementById('ANFRAGEDIV').style.display='block'; scroll_to_id('ANFRAGEDIV')">
        % endif
      % elif s['user'] and not best['eigene_bestimmung_vorhanden']:
        <input type="button" value="schließen und eigene Bestimmung anlegen" onclick="openbest(${besuch['id']},${fund['i']},0)">
        <%block name="anfrage_bearbeiten_info">
        % if 'offene_anfrage' in s['fund']:
        ⟵ hier klicken um die <span style="color:red">offene Bestimmungsanfrage</span> zu erledigen oder abzulehnen
        % endif
        </%block>
      % endif # best['schreibrecht']
      </div>
    </form>
  </div>
  % if not best['i'] and s['user'] or fund['schreibrecht']:
  <div id="BESTNEUBTNDIV" style="margin-top:10px;display:${'none' if best['form_oeffnen'] else 'block'}">
    % if not best['i']:
    <input type="button" value="neu" style="margin-right:2em" onclick="document.getElementById('BESTNEUBTNDIV').style.display='none'; document.getElementById('BESTFRM_DIV').style.display='block'; scroll_to_id('BESTFRM_DIV')">
    ${anfrage_bearbeiten_info()}
    % endif
    % if fund['schreibrecht']:
    <input type="button" value="jemand anderen um Bestimmung bitten..." onclick="document.getElementById('BESTNEUBTNDIV').style.display='none'; document.getElementById('ANFRAGEDIV').style.display='block'; scroll_to_id('ANFRAGEDIV')">
    % endif
  </div>
  % endif # if not best['i'] and s['user'] or fund['schreibrecht']
  % if fund['schreibrecht']:
  <div id="ANFRAGEDIV" style="display:none">
    ## könnte man auch mit Ajax befüllen; außerdem sollte es für den Empfänger eine Suchfunktion geben
    <form name="ANFRAGEFRM">
      <table class="graytable" style="margin-top:20px">
        <tr>
          <td>Bestimmungsanfrage an:</td>
          <td>
            <select name="ANFRAGE_AN" onchange="document.getElementById('ANFRBTN').disabled=!this.value">
              <option value="">bitte auswählen</option>
              % for u in fund['users']:
              <option value="${u['id']}">${u['name'] |h}</option>
              % endfor
            </select>
          </td>
        </tr>
        <tr>
          <td>Anmerkung</td>
          <td><textarea name="ANFRAGE_ANM" maxlength="1000" cols="80" rows="5"></textarea>
        </tr>
      </table>
      <input type="button" value="abbrechen" onclick="document.getElementById('ANFRAGEDIV').style.display='none'; ${Unless(best['i'], "document.getElementById('BESTNEUBTNDIV').style.display='block';")}this.form.reset()">
      <input id="ANFRBTN" type="button" value="Anfrage senden" onclick="anfrage(${fund['besuchid']}, ${fund['i']})" disabled style="margin-left:2em">
    </form>
  </div>
  % endif # fund['schreibrecht']
  % endif # fund['i']
  % endif # 'fund' in s
  % endif # IF_00001
</body>
