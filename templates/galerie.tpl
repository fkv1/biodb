<head>
  <title>Galerie</title>
  <style>
    img {
      max-width: 100%;
      height: auto;
    }
  </style>
</head>
<body>
% if len(g):
  % for bild in g:
  <div${'' if loop.first else ' style="margin-top:10px"'}>
    <img src="${bild['url'] |h}" alt="${bild['filename'] |h}" title="${bild['filename'] |h}">
    % if bild['beschreibung']:
    <br>${bild['beschreibung'] |h}
    % endif
  </div>
  % endfor
% else:
  <div>keine Bilder</div>
% endif
</body>
