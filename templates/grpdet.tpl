<head>
  <title>
  % if u['selgrp_id']:
    Benutzergruppe
  % else:
    Benutzergruppe anlegen
  % endif
  </title>
  <style>
    .graytable {
      border: 1px solid black;
      border-collapse:collapse;
      background-color:#eeeeee;
    }
    .graytable td {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      padding: 3px;
    }
  </style>
</head>
<body
  % if 'statusmsg' in u:
  onload="alert('${u['statusmsg']}');
    % if 'closewindow' in u:
    window.close()
    % endif
  "
  % endif
>
  % if u['admin']:
  <form action="${u['scriptname']}">
    % if not u['selgrp_id']:
    <b>Gruppe anlegen:</b><br>
    % endif
  % endif
  <table><tr><td>
    <table class="graytable">
      % if u['selgrp_id']:
      <tr>
        <td>ID</td>
        <td><input type="number" name="GRPMOD_ID" readonly value="${u['selgrp_id']}"></td>
      </tr>
      % endif
      <tr>
        <td>Name</td>
        <td>
          <input type="text" name="GRPMOD_NAME" value="${u['selgrp_name'] |h}"
            % if u['admin']:
            required maxlength="20" onchange="this.value=this.value.trim()"
            % else:
            readonly
            % endif
          >
        </td>
      </tr>
      <tr>
        <td><span title="Mehrfachauswahl möglich">in Gruppen &#x2139;&#xFE0F</span></td>
        <td>
          <select name="GRPMOD_GRP" multiple>
            % for g in u['gruppen']:
            % if g['id'] != u['selgrp_id']: # Gruppe kann nicht Untergruppe von sich selbst sein
            <option value="${g['id']}"${' selected' if g['id'] in u['selgrp_grp'] else '' if u['admin'] else ' disabled'}>${g['name'] |h}</option>
            % endif
            % endfor
          </select>
        </td>
      </tr>
      <tr>
        <td>Beschreibung</td>
        <td><textarea name="GRPMOD_BESCHREIBUNG" cols="80" rows="4" ${'maxlength="100"' if u['admin'] else 'readonly'}>${u['selgrp_beschreibung'] |h}</textarea></td>
      </tr>
    </table>
  </td></tr><tr><td>
    <div style="margin-top:5px;text-align:center">
      <input type="button" value="${'schließen' if u['selgrp_id'] else 'abbrechen'}" onclick="window.close()">
      % if u['admin']:
      <input type="hidden" name="SESS" value="${u['sessionid'] |h}">
      <input type="submit" value="${'ändern' if u['selgrp_id'] else 'anlegen'}" style="margin-left:2em">
      % if u['selgrp_id']:
      <input type="hidden" name="GRPMOD_DEL" value="">
      <input type="button" value="Gruppe löschen" onclick="this.form.GRPMOD_DEL.value='1'; this,form.submit()" style="margin-left:2em">
      % endif
      % endif
    </div>
  </td></tr></table>
  % if u['admin']:
  </form>
  % endif
</body>
