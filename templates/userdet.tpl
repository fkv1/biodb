<head>
  <title>
  % if u['seluser_id']:
    Benutzer-Details
  % else:
    Benutzer anlegen
  % endif
  </title>
  <style>
    .graytable {
      border: 1px solid black;
      border-collapse:collapse;
      background-color:#eeeeee;
    }
    .graytable td {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      padding: 3px;
    }
  </style>
  <script>
    function adj_to_struktur(p_struktur) {
      document.forms.USERDET_FRM.USERMOD_NAME2.required = p_struktur != 'R';
      document.forms.USERDET_FRM.USERMOD_NAME2.disabled = p_struktur == 'R';
      document.forms.USERDET_FRM.USERMOD_USERNAME.required = p_struktur != 'R';
      document.forms.USERDET_FRM.USERMOD_PASSWD.required = p_struktur != 'R';
    }
  </script>
</head>
<body
  % if 'statusmsg' in u:
  onload="alert('${u['statusmsg']}');
    % if 'closewindow' in u:
    window.close()
    % endif
  "
  % endif
>
  % if u['admin']:
  <form name="USERDET_FRM" action="${u['scriptname']}" onsubmit="if(this.USERMOD_PASSWD.value=='' and this.USERMOD_NAME_STRUKTUR.value!='R') return confirm('ohne Passwort kann der User sich nicht einloggen. Trotzdem so speichern?')">
    % if not u['seluser_id']:
    <b>User anlegen:</b><br>
    % endif
  % endif
  <table><tr><td>
    <table class="graytable">
      % if u['seluser_id']:
      <tr>
        <td>ID</td>
        <td><input type="number" name="USERMOD_ID" readonly value="${u['seluser_id']}"></td>
      </tr>
      % endif
      % if u['admin'] or u['seluser_username'] == u['user']: # andere User geht der Loginname nichts an
      <tr>
        <td>
          % if u['admin']:
          <span title="muss eindeutig sein!">Loginname &#x2139;&#xFE0F</span>
          % else:
          Loginname
          % endif
        </td>
        <td>
          <input type="text" name="USERMOD_USERNAME" value="${u['seluser_username'] |h}"
            % if u['admin']:
            ${'' if u['seluser_name_struktur']=='R' else 'required '}maxlength="10" pattern="^[-_ a-z0-9]+$" onchange="this.value=this.value.trim()"
            % else:
            readonly
            % endif
          >
        </td>
      </tr>
      % endif
      % if u['admin']: # der User braucht sein eigenes Passwort nicht sehen, das wär nur ein Sicherheitsrisiko (z.B. wenn grad wer über die Schulter schaut)
      <tr>
        <td>Passwort</td>
        <td><input type="text" name="USERMOD_PASSWD" maxlength="20" value="${u['seluser_passwd'] |h}"></td>
      </tr>
      % endif
      <tr>
        <td><span title="Mehrfachauswahl möglich">Gruppen &#x2139;&#xFE0F</span></td>
        <td>
          <select name="USERMOD_GRP" multiple>
            % for g in u['gruppen']:
            <option value="${g['id']}"${' selected' if g['id'] in u['seluser_grp'] else '' if u['admin'] else ' disabled'}>${g['name'] |h}</option>
            % endfor
          </select>
        </td>
      </tr>
      <tr>
        <td>Realname:</td>
        <td>
          <select name="USERMOD_NAME_STRUKTUR"${' onchange="adj_to_struktur(this.value)"' if u['admin'] else ''}>
            <option value="VN"${' selected' if u['seluser_name_struktur'] =='' or u['seluser_name_struktur'] =='VN' else '' if u['admin'] else ' disabled'}>Vorname Nachname</option>
            <option value="FG"${' selected' if u['seluser_name_struktur'] =='FG' else '' if u['admin'] else ' disabled'}>Familienname GivenName (Ungarn, China...)</Option>
            <option value="PG"${' selected' if u['seluser_name_struktur'] =='PG' else '' if u['admin'] else ' disabled'}>Patronym GivenName (Teile Indiens)</Option>
            <option value="GP"${' selected' if u['seluser_name_struktur'] =='GP' else '' if u['admin'] else ' disabled'}>GivenName Patronym (Island)</option>
            <option value="R"${' selected' if u['seluser_name_struktur'] =='R' else '' if u['admin'] else ' disabled'}>Role Account</option>
          </select>
          <div style="margin-top:3px">
            <input type="text" name="USERMOD_NAME1" required value="${u['seluser_name1'] |h}" ${'maxlength="20"' if u['admin'] else 'readonly'}>
            <input type="text" name="USERMOD_NAME2" ${'disabled' if u['seluser_name_struktur']=='R' else 'required'} value="${u['seluser_name2'] |h}" ${'maxlength="20"' if u['admin'] else 'readonly'} style="margin-left:1em">
          </div>
        </td>
      </tr>
      <tr>
        <td>Mailadresse</td>
        <td>
          % if u['seluser_mail'] or u['admin']:
          <input type="email" name="USERMOD_MAIL" value="${u['seluser_mail'] |h}" ${'maxlength="30"' if u['admin'] else 'readonly'}>
          % if u['admin']:
          <span style="margin-left:1em" title="allen eingeloggten Usern anzeigen"><input type="checkbox" id="USERMOD_MAILANZ" name="USERMOD_MAILANZ" value="1"${' checked' if u['seluser_mailanz'] else ''}> <label for="USERMOD_MAILANZ">anzeigen</label> &#x2139;&#xFE0F</span>
          % endif
          % if u['seluser_maillink']:
          <span style="margin-left:1em"><a href="mailto:${u['seluser_maillink'] |h}">anmailen</a></span>
          % endif
          % else:
          verborgen oder nicht erfasst
          % endif
        </td>
      </tr>
      <tr>
        <td>Beschreibung</td>
        <td><textarea name="USERMOD_BESCHREIBUNG" cols="80" rows="10" ${'maxlength="10000"' if u['admin'] else 'readonly'}>${u['seluser_beschreibung'] |h}</textarea></td>
      </tr>
    </table>
  </td></tr><tr><td>
    <div style="margin-top:5px;text-align:center">
      <input type="button" value="${'schließen' if u['seluser_id'] else 'abbrechen'}" onclick="window.close()">
      % if u['admin']:
      <input type="hidden" name="SESS" value="${u['sessionid'] |h}">
      <input type="submit" value="${'ändern' if u['seluser_id'] else 'anlegen'}" style="margin-left:2em">
      % if u['seluser_id']:
      <input type="hidden" name="USERMOD_DEL" value="">
      <input type="button" value="User löschen" onclick="this.form.USERMOD_DEL.value='1'; this,form.submit()" style="margin-left:2em">
      % endif
      % endif
    </div>
  </td></tr></table>
  % if u['admin']:
  </form>
  % endif
</body>
