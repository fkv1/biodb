<head>
  <title>
  % if u['selverein_id']:
    Verein
  % else:
    Verein anlegen
  % endif
  </title>
  <style>
    .graytable {
      border: 1px solid black;
      border-collapse:collapse;
      background-color:#eeeeee;
    }
    .graytable td {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      padding: 3px;
    }
  </style>
</head>
<body
  % if 'statusmsg' in u:
  onload="alert('${u['statusmsg']}');
    % if 'closewindow' in u:
    window.close()
    % endif
  "
  % endif
>
  % if u['admin']:
  <form action="${u['scriptname']}">
    % if not u['selverein_id']:
    <b>Verein anlegen:</b><br>
    % endif
  % endif
  <table><tr><td>
    <table class="graytable">
      % if u['selverein_id']:
      <tr>
        <td>ID</td>
        <td><input type="number" name="VEREINMOD_ID" readonly value="${u['selverein_id']}"></td>
      </tr>
      % endif
      <tr>
        <td>Name</td>
        <td>
          <input type="text" name="VEREINMOD_NAME" size="50" value="${u['selverein_name'] |h}"
            % if u['admin']:
            required maxlength="80" onchange="this.value=this.value.trim()"
            % else:
            readonly
            % endif
          >
        </td>
      </tr>
      <tr>
        <td><span title="Mehrfachauswahl möglich">Mitgliedsverein/Sektion von &#x2139;&#xFE0F</span></td>
        <td>
          <select name="VEREINMOD_VRN" multiple>
            % for v in u['vereine']:
            % if v['id'] != u['selverein_id']:
            <option value="${v['id']}"${' selected' if v['id'] in u['selverein_vrn'] else '' if u['admin'] else ' disabled'}>${v['name'] |h}</option>
            % endif
            % endfor
          </select>
        </td>
      </tr>
      <tr>
        <td>Rechtsform</td>
        <td>
          <select name="VEREINMOD_RECHTSFORM">
            <option value=""${' selected' if not u['selverein_rechtsform'] else '' if u['admin'] else ' disabled'}>nicht angegeben</option>
            <option value="V"${' selected' if u['selverein_rechtsform'] == 'V' else '' if u['admin'] else ' disabled'}>Verein</option>
            <option value="S"${' selected' if u['selverein_rechtsform'] == 'S' else '' if u['admin'] else ' disabled'}>Sektion</option>
            <option value="B"${' selected' if u['selverein_rechtsform'] == 'B' else '' if u['admin'] else ' disabled'}>Behörde</option>
          </select>
        </td>
      </tr>
      <tr>
        <td><span title="Vereinszweck, Vereinssitz, Ansprechpartner...">Beschreibung &#x2139;&#xFE0F</span></td>
        <td><textarea name="VEREINMOD_BESCHREIBUNG" cols="80" rows="4" ${'maxlength="1000"' if u['admin'] else 'readonly'}>${u['selverein_beschreibung'] |h}</textarea></td>
      </tr>
      <tr>
        <td>Homepage</td>
        <td><input type="url" name="VEREINMOD_HP" size="50" value="${u['selverein_homepage'] |h}" {'maxlength="100"' if u['admin'] else 'readonly'}><input type="button" value="öffnen" onclick="if (this.form.VEREINMOD_HP.value) window.open(this.form.VEREINMOD_HP.value,'_blank')"></td>
    </table>
  </td></tr><tr><td>
    <div style="margin-top:5px;text-align:center">
      <input type="button" value="${'schließen' if u['selverein_id'] else 'abbrechen'}" onclick="window.close()">
      % if u['admin']:
      <input type="hidden" name="SESS" value="${u['sessionid'] |h}">
      <input type="submit" value="${'ändern' if u['selverein_id'] else 'anlegen'}" style="margin-left:2em">
      % if u['selverein_id']:
      <input type="hidden" name="VEREINMOD_DEL" value="">
      <input type="button" value="Verein löschen" onclick="this.form.VEREINMOD_DEL.value='1'; this,form.submit()" style="margin-left:2em">
      % endif
      % endif
    </div>
  </td></tr></table>
  % if u['admin']:
  </form>
  % endif
</body>
